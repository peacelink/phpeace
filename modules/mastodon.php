<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/cache.php");

class Mastodon {

    public $server;
    
    public $mastodon_admin;
    private $mastodon_admin_token;
    public $mastodon_local;
    private $mastodon_local_token;
    
    private $next_link;
    
    function __construct()
    {
        $conf = new Configuration();
        $this->server = $conf->Get("mastodon_server");
        $this->mastodon_admin = $conf->Get("mastodon_admin");
        $this->mastodon_admin_token = $conf->Get("mastodon_admin_token");
        $this->mastodon_local = $conf->Get("mastodon_local");
        $this->mastodon_local_token = $conf->Get("mastodon_local_token");
    }
    
    public function Account($id) {
        $cache = new Cache();
        $response = $cache->Get('json',"{$this->server}/api/v1/accounts/{$id}/");
        return json_decode($response, true);
    }
    
    public function Cron() {
        return $this->Welcome();
    }
    
    public function FollowAdmin() {
        // get admin current followers
        $followers = $this->Followers($this->mastodon_admin);
        $list = [];
        $users = [];
        foreach($followers as $follower) {
            $list[] = $follower['username'];
        }
        $accounts = $this->Request($this->mastodon_admin_token, 'admin/accounts?local=1', array(), true);
        foreach($accounts as $a) {
            if(!in_array($a['username'], $list)) {
                $users[] = $a['username'];
            }
        }
        return $users;
    }
    
    private function Followers($id) {
        return $this->Request($this->mastodon_admin_token, "accounts/$id/followers",array(),true);
    }
    
    private function HandleHeaderLine($curl, $header_line) {
        $header = explode(':', $header_line, 2);
        if (count($header) >= 2 && $header[0]=='Link') {
            $matches = array();
            preg_match('/<(.*?(?:(?:\?|\&)page=(\d+).*)?)>.*rel="next"/', $header[1], $matches, PREG_UNMATCHED_AS_NULL);
            $this->next_link = $matches[1];
        }
        return strlen($header_line);
    }
    
    private function Request($token, $endpoint, $data=array(), $recursive=false) {
        $this->next_link = null;
        $url = "{$this->server}/api/v1/{$endpoint}";
        $headers = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if(count($data)>0) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } else {
            $headers[] = 'Content-Type: application/json';
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($recursive) {
            curl_setopt($ch, CURLOPT_HEADERFUNCTION, "self::HandleHeaderLine");
        }
        if($token!='') {
            $headers[] = 'Authorization: Bearer ' . $token;
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        $response = curl_exec($ch);
        curl_close($ch);
        $response_array = json_decode($response, true);
        if($recursive && !is_null($this->next_link)) {
            $url_parts = parse_url($this->next_link);
            if (isset($url_parts['query'])) {
                $vars = array();
                parse_str($url_parts['query'],$vars);
                if(isset($vars['max_id']) && is_numeric($vars['max_id'])) {
                    $endpoint = $this->QueryStringAddVariable($endpoint, 'max_id', $vars['max_id']);
                    $response2 = $this->Request($token, $endpoint, $data, true);
                    $response_array = array_merge($response_array, $response2);
                }
            }
        }
        return $response_array;
    }
    
    private function QueryStringAddVariable($url,$name,$value) {
        $url_parts = parse_url($url);
        $params = array();
        if (isset($url_parts['query'])) {
            parse_str($url_parts['query'], $params);
        }
        $params[$name] = $value;
        $url_parts['query'] = http_build_query($params);
        return $url_parts['path'] . '?' . $url_parts['query'];
    }
    
    public function Toots() {
        $cache = new Cache;
        $url = "{$this->server}/api/v1/accounts/{$this->mastodon_pck}/statuses";
        return $cache->Get('json',$url);
    }
    
    private function Welcome() {
        $messages = array();
        
        include_once(SERVER_ROOT."/../classes/ini.php");
        $ini = new Ini();
        $last_follower_id = $ini->GetModule("mastodon","last_follower_id","0");
        $welcome = $ini->GetModule("mastodon","welcome","");
        
        $followers = $this->Followers($this->mastodon_admin);
        $newest_user_id = $followers[0]['id'];

        if ($welcome!="" & ($last_follower_id < $newest_user_id) && ($last_follower_id>0) && ($newest_user_id != "")) {
            
            $ini->SetModule("mastodon","last_follower_id",$newest_user_id);
            
            foreach( array_reverse($followers) as $user_json ) {
                $user_id = $user_json['id'];
                $user_username = $user_json['username'];
                
                // pick only newest users
                if (($last_follower_id < $user_id) && ($user_id != "")) {
                    
                    $status_data = array(
                        "status" => sprintf($welcome,"@" . $user_username),
                        "language" => "it",
                        "visibility" => "direct"
                    );
                    $this->Request($this->mastodon_admin_token, 'statuses', $status_data);
                    
                    $messages[] = "Welcome new user ({$user_id}) @{$user_username}";
                }
            }
        }
        return $messages;
    }
}

?>
