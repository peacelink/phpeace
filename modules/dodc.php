<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
define('DODC_DATABASE',"pck_dodc");
define('DODC_SEPARATOR',"!");

class DodContractors
{
	public $id_topic = 2;
	
	public function Categories( &$rows )
	{
		$sqlstr = " SELECT * FROM categories ";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr);
	}

	public function CategoriesTotals( &$rows )
	{
		$sqlstr = " SELECT ct.id_category,ct.description,SUM(c.iamount) AS total,COUNT(c.id_contract) AS counter
			FROM categories ct
			INNER JOIN contracts c ON ct.id_category=c.id_category
			GROUP BY c.id_category
			ORDER BY total DESC ";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function CategoryContractors(&$rows,$id_category)
	{
		$sqlstr = "SELECT co.id_contractor,co.name,SUM(c.iamount) AS total 
			FROM contracts c 
			INNER JOIN contractors co ON c.id_parent=co.id_contractor 
			WHERE c.id_category=$id_category
			GROUP BY c.id_parent ORDER BY total desc";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function CategoryGet($id_category)
	{
		$row = array();
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->query_single($row,"SELECT id_category,code,description FROM categories WHERE id_category='$id_category' ");
		return $row;
	}
	
	private function CategoryGetByCode($code)
	{
		$row = array();
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->query_single($row,"SELECT id_category,code,description FROM categories WHERE code='$code' ");
		return $row;
	}
	
	public function ContractGet($id_contract)
	{
		$row = array();
		$db =& Db::globaldb();
		$this->SelectDb();
		$sqlstr = "SELECT c.*,UNIX_TIMESTAMP(start_date) AS start_date_ts,UNIX_TIMESTAMP(end_date) AS end_date_ts,
			ca.description AS category_description,pc.description AS product_description
			FROM contracts c 
			INNER JOIN categories ca ON c.id_category=ca.id_category
			INNER JOIN product_codes pc ON c.product_code=pc.code
			WHERE c.id_contract='$id_contract' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function ContractorGet($id_contractor)
	{
		$row = array();
		$db =& Db::globaldb();
		$this->SelectDb();
		$sqlstr = "SELECT cr.* FROM contractors cr WHERE cr.id_contractor='$id_contractor' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function ContractorTotalGet($id_contractor)
	{
		$row = array();
		$db =& Db::globaldb();
		$this->SelectDb();
		$sqlstr = "SELECT SUM(amount) AS total FROM contracts WHERE id_contractor='$id_contractor' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function ContractInsert($number,$fiscal_year,$start_date,$end_date,$id_contractor,$id_parent,$id_office,$id_category,$product_code,$city,$county,$country,$country_code,$amount,$multiyear,$notes,$iamount)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->begin();
		$db->lock( "contracts" );
		$id_contract = $db->nextId( "contracts", "id_contract" );
		$sqlstr = "INSERT INTO contracts (id_contract,number,fiscal_year,start_date,end_date,id_contractor,id_parent,id_office,id_category,product_code,city,county,country,country_code,amount,multiyear,notes,iamount) 
			VALUES ($id_contract,'$number','$fiscal_year','$start_date','$end_date','$id_contractor','$id_parent','$id_office','$id_category','$product_code','$city','$county','$country','$country_code',$amount,$multiyear,'$notes',$iamount)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_contract;
	}
	
	public function ContractorGetByDuns($duns)
	{
		$row = array();
		if($duns != '000000000')
		{
			$db =& Db::globaldb();
			$this->SelectDb();
			$db->query_single($row,"SELECT id_contractor,duns,name FROM contractors WHERE duns='$duns' ");
		}
		return $row;
	}
	
	public function ContractorInsert($duns,$name,$address,$city,$postcode,$country_code)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->begin();
		$db->lock( "contractors" );
		$id_contractor = $db->nextId( "contractors", "id_contractor" );
		$sqlstr = "INSERT INTO contractors (id_contractor,duns,name,address,city,postcode,country_code) 
			VALUES ($id_contractor,'$duns','$name','$address','$city','$postcode','$country_code')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_contractor;
	}

	private function ContractorUpdate($id_contractor,$contractor_name,$contractor_address,$contractor_city,$contractor_postcode,$contractor_country_code)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->begin();
		$db->lock( "contractors" );
		$sqlstr = "UPDATE contractors SET name='$contractor_name',address='$contractor_address',
			city='$contractor_city',postcode='$contractor_postcode',country_code='$contractor_country_code' 
			WHERE id_contractor='$id_contractor' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	private function ContractorUpdateName($id_contractor,$contractor_name)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->begin();
		$db->lock( "contractors" );
		$sqlstr = "UPDATE contractors SET name='$contractor_name' WHERE id_contractor='$id_contractor' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function Contractors( &$rows, $country_code="",$id_office=0 )
	{
		$sqlstr = "SELECT id_contractor,name,total FROM contractors_totals WHERE 1 ";
		if($country_code!="")
			$sqlstr .= " AND country_code='$country_code' ";
		if($id_office>0)
			$sqlstr .= " AND id_office='$id_office' ";
		$sqlstr .= " ORDER BY total DESC";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function Contracts( &$rows, $id_contractor, $parent=false )
	{
		if($parent)
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description,ct.name AS contractor_name
				FROM contracts c 
				INNER JOIN categories ca ON c.id_category=ca.id_category 
				INNER JOIN contractors ct ON c.id_contractor=ct.id_contractor 
				WHERE c.id_parent=$id_contractor 
				ORDER BY c.start_date DESC  ";
		}
		else 
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description
				FROM contracts c 
				INNER JOIN categories ca ON c.id_category=ca.id_category 
				WHERE c.id_contractor=$id_contractor 
				ORDER BY c.start_date DESC  ";
		}
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ContractsPeriod( &$rows, $id_contractor, $parent, $year, $month )
	{
		if($parent)
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description,ct.name AS contractor_name
				FROM contracts c
				INNER JOIN categories ca ON c.id_category=ca.id_category
				INNER JOIN contractors ct ON c.id_contractor=ct.id_contractor 
				WHERE c.id_parent=$id_contractor AND YEAR(start_date)=$year AND MONTH(start_date)=$month
				ORDER BY c.start_date DESC  ";
		}
		else 
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description 
				FROM contracts c
				INNER JOIN categories ca ON c.id_category=ca.id_category
				WHERE c.id_contractor=$id_contractor AND YEAR(start_date)=$year AND MONTH(start_date)=$month
				ORDER BY c.start_date DESC  ";		
		}
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ContractsPeriods( &$rows, $id_contractor, $parent )
	{
		$sqlstr = "SELECT UNIX_TIMESTAMP(start_date) as start_date_ts,COUNT(id_contract) AS contracts,DATE_FORMAT(start_date,\"%Y%m\") AS sort,
			DATE_FORMAT(start_date,\"%Y\") AS year,DATE_FORMAT(start_date,\"%m\") AS month,SUM(amount) AS amount
			FROM contracts 
			WHERE " . ($parent? "id_parent":"id_contractor") . "=$id_contractor 
			GROUP BY fiscal_year ORDER BY sort DESC";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ContractsOffice( &$rows, $id_contractor, $id_office, $parent )
	{
		if($parent)
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description,ct.name AS contractor_name 
				FROM contracts c
				INNER JOIN categories ca ON c.id_category=ca.id_category
				INNER JOIN contractors ct ON c.id_contractor=ct.id_contractor 
				WHERE c.id_office=$id_office " . ($id_contractor>0? " AND c.id_contractor=$id_contractor":"")
				. " ORDER BY c.start_date DESC ";
		}
		else 
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description 
				FROM contracts c
				INNER JOIN categories ca ON c.id_category=ca.id_category
				WHERE c.id_office=$id_office " . ($id_contractor>0? " AND c.id_contractor=$id_contractor":"")
				. " ORDER BY c.start_date DESC ";
		}
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ContractsOffices( &$rows, $id_contractor, $parent )
	{
		$sqlstr = "SELECT o.name,COUNT(id_contract) AS contracts,COUNT(c.id_office) AS counter,SUM(amount) AS amount,o.id_office
			FROM contracts c
			INNER JOIN offices o ON c.id_office=o.id_office
			WHERE " . ($parent? "id_parent":"id_contractor") . "=$id_contractor 
			GROUP BY c.id_office ORDER BY counter DESC";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ContractsCategory( &$rows, $id_contractor, $id_category, $parent )
	{
		if($parent)
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description,ct.name AS contractor_name 
				FROM contracts c
				INNER JOIN categories ca ON c.id_category=ca.id_category
				INNER JOIN contractors ct ON c.id_contractor=ct.id_contractor 
				WHERE c.id_parent=$id_contractor AND c.id_category=$id_category
				ORDER BY c.start_date DESC  ";
		}
		else 
		{
			$sqlstr = " SELECT c.*,UNIX_TIMESTAMP(c.start_date) AS start_date_ts,ca.description 
				FROM contracts c
				INNER JOIN categories ca ON c.id_category=ca.id_category
				WHERE c.id_contractor=$id_contractor AND c.id_category=$id_category
				ORDER BY c.start_date DESC  ";
		}
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ContractsCategories( &$rows, $id_contractor,$parent )
	{
		$sqlstr = "SELECT ca.description,COUNT(id_contract) AS contracts,COUNT(c.id_category) AS counter,
			SUM(amount) AS amount,ca.id_category
			FROM contracts c
			INNER JOIN categories ca ON c.id_category=ca.id_category
			WHERE " . ($parent? "id_parent":"id_contractor") . "=$id_contractor 
			GROUP BY c.id_category ORDER BY counter DESC";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function DatabaseInit($views_only=false)
	{
		if(!$views_only)
			$this->DatabaseSchema();
		$this->DatabaseViews();
	}
	
	public function DatabaseReset()
	{
		$this->SelectDb();
		$db =& Db::globaldb();
		$db->query( "TRUNCATE contractors" );
		$db->query( "TRUNCATE contracts" );
		$db->query( "TRUNCATE offices" );
		$db->query( "TRUNCATE programs" );
	}
	
	private function DatabaseSchema()
	{
		$db =& Db::globaldb();
		if($this->SelectDb())
		{
			$db->query("DROP TABLE IF EXISTS `contractors`");
			$db->query("CREATE TABLE `contractors` (
				`id_contractor` mediumint(9) NOT NULL default '0',
				`duns` CHAR(9) NOT NULL default '',
				`cage` CHAR(5) NOT NULL default '',
				`name` VARCHAR(150) NOT NULL default '',
				`address` VARCHAR(150) NOT NULL default '',
				`email1` VARCHAR(50) NOT NULL default '',
				`email2` VARCHAR(50) NOT NULL default '',
				`city` VARCHAR(15) NOT NULL default '',
				`postcode` VARCHAR(5) NOT NULL default '',
				`id_geo` TINYINT(4) NOT NULL default '0',
				`country_code` CHAR(2) NOT NULL default '',
  				PRIMARY KEY (`id_contractor` ),
  				KEY  `country_code` (`country_code` )
  			)");
			$db->query("DROP TABLE IF EXISTS `contracts`");
			$db->query("CREATE TABLE `contracts` (
				`id_contract` mediumint(9) NOT NULL default '0',
				`number` CHAR(15) NOT NULL default '',
				`fiscal_year` SMALLINT(4) NULL default '0',
				`start_date` date NULL default NULL,
				`end_date` date NULL default NULL,
				`id_contractor` mediumint(9) NOT NULL default '0',
				`id_parent` mediumint(9) NOT NULL default '0',
				`id_office` mediumint(9) NOT NULL default '0',
				`id_category` smallint(3) NOT NULL default '0',
				`product_code` CHAR(4) NOT NULL default '',
				`notes` VARCHAR(300) NOT NULL default '',
				`city` VARCHAR(20) NOT NULL default '',
				`county` VARCHAR(22) NOT NULL default '',
				`country` CHAR(10) NOT NULL default '',
				`country_code` CHAR(2) NOT NULL default '',
				`amount` BIGINT(13) NOT NULL default '0',
				`iamount` BIGINT(13) NOT NULL default '0',
				`multiyear` TINYINT(1) NOT NULL default '0',
  				PRIMARY KEY  (`id_contract` ),
  				KEY  `id_contractor` (`id_contractor` ),
  				KEY  `id_parent` (`id_parent` ),
  				KEY  `country_code` (`country_code` )
  			)");
			$db->query("DROP TABLE IF EXISTS `offices`");
			$db->query("CREATE TABLE `offices` (
				`id_office` mediumint(9) NOT NULL default '0',
				`fips` CHAR(4) NOT NULL default '',
				`code` CHAR(6) NOT NULL default '',
				`name` VARCHAR(32) NOT NULL default '',
  				PRIMARY KEY  (`id_office` )
  			)");
			$db->query("DROP TABLE IF EXISTS `categories`");
			$db->query("CREATE TABLE `categories` (
				`id_category` smallint(3) NOT NULL default '0',
				`code` CHAR(3) NOT NULL default '',
				`description` VARCHAR(100) NOT NULL default '',
  				PRIMARY KEY  (`id_category` )
  			)");
			$db->query("DROP TABLE IF EXISTS `product_codes`");
			$db->query("CREATE TABLE `product_codes` (
				`code` VARCHAR(4) NOT NULL default '',
				`description` VARCHAR(255) NOT NULL default '',
  				PRIMARY KEY  (`code` )
  			)");
			$db->query("DROP TABLE IF EXISTS `fips`");
			$db->query("CREATE TABLE `fips` (
				`code` VARCHAR(4) NOT NULL default '',
				`agency` VARCHAR(10) NOT NULL default '',
				`description` VARCHAR(255) NOT NULL default '',
  				PRIMARY KEY  (`code` )
  			)");
			$db->Restore("import/dodc/categories.sql");
			$db->Restore("import/dodc/product_codes.sql");
			$db->Restore("import/dodc/fips.sql");
		}
	}
	
	private function DatabaseViews()
	{
		$db =& Db::globaldb();
		if($this->SelectDb())
		{
			$db->query("DROP VIEW IF EXISTS `contractors_totals`");
			$db->query("CREATE VIEW `contractors_totals` AS 
				SELECT cr.id_contractor,cr.name,SUM(c.iamount) AS total,cr.country_code
				FROM contractors cr
				INNER JOIN contracts c ON cr.id_contractor=c.id_parent
				GROUP BY cr.id_contractor 
				ORDER BY total DESC
				");
		}		
	}
	
	public function DunsLookup($duns)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$data = array();
		$data['ctl00$ContentPlaceHolder1$txtDUNS'] = $duns;
		$data['ctl00$ContentPlaceHolder1$btnSearch'] = "Search";		
		$result = $fm->PostData("https://www.bpn.gov","/CCRSearch/Search.aspx",$data);
		return $result;
	}
	
	public function ImportDuns()
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$files = $fm->DirFiles("import/dodc/duns");
		if(count($files)>0)
		{
			$contractors = array();
			foreach($files as $file)
			{
				$basename = basename($file);
				$lines = $fm->TextFileLines("import/dodc/duns/{$basename}");
				$counter = 0;
				foreach($lines as $line)
				{
					if($counter>0)
					{
						$vals = array();
						$vals = explode(",",$line);
						if(count($vals)>1)
						{
							array_walk($vals, array($this,'TrimValue'));
							array_walk($vals, array($this,'TrimQuotes'));
							$contractor = array();
							$contractor['duns'] = $vals[0];
							$contractor['cage'] = $vals[2];
							$contractor['name'] = $vals[3];
							$contractor['address'] = $vals[4];
							$contractor['city'] = $vals[6];
							$contractor['province'] = $vals[7];
							$contractors[] = $contractor;
						}
					}
					$counter++;
				}
			}
			$db =& Db::globaldb();
			$this->SelectDb();
			$db->begin();
			$db->lock( "contractors" );
			foreach($contractors as $contractor)
			{
				$id_contractor = $db->nextId( "contractors", "id_contractor" );
				$name = $db->SqlQuote($contractor['name']);
				$address = $db->SqlQuote($contractor['address']);
				$city = $db->SqlQuote($contractor['city']);
				$id_geo = (int)$contractor['province'];
				$sqlstr = "INSERT INTO contractors (id_contractor,duns,cage,name,address,city,postcode,id_geo,country_code) 
					VALUES ($id_contractor,'{$contractor['duns']}','{$contractor['cage']}','$name','$address','$city','','$id_geo','IT')";
				$res[] = $db->query( $sqlstr );
			}
			Db::finish( $res, $db);		
		}
		return count($contractors);
	}

	public function ImportFixes()
	{
		$filename = "import/dodc/fixes.sql";
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		if($fm->Exists($filename))
		{
			$db =& Db::globaldb();
			$this->SelectDb();
			$db->Restore($filename);
		}
	}
	
	public function ImportYearCsv($file,$year)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		if($fm->Exists("import/dodc/{$file}"))
		{
			$db =& Db::globaldb();
			$valid = $this->ImportValidate($year);
			$lines = $fm->TextFileLines("import/dodc/{$file}");
			$counter = 1;
			foreach($lines as $line)
			{
				$vals = array();
				$vals = explode(DODC_SEPARATOR,$line);
				$num_vals = count($vals);
				if($num_vals==$valid['fields'])
				{
					array_walk($vals, array($this,'TrimValue'));
					if($vals[$valid['origin']]=="A" && ($vals[$valid['contractor_country_code']]=="IT" || $vals[$valid['country_code']]=="IT"))
					{
						$office_code = $vals[$valid['office_code']];
						$fips = $vals[$valid['fips']];
						$office = $this->OfficeGetByCode($office_code);
						$id_office = (int)$office['id_office'];
						if(!$id_office>0)
						{
							$office_name = $db->SqlQuote($vals[$valid['office_name']]);
							$id_office = $this->OfficeInsert($office_code,$office_name,$fips);
						}
						elseif($fips!=$office['fips'])
						{
							if(substr($office['fips'],2,2)=="00")
								$this->OfficeFipsUpdate($id_office,$fips);
						}
						$contractor_code = sprintf("%09s",$vals[$valid['contractor_code']]);
						$contractor_name = $db->SqlQuote($vals[$valid['contractor_name']]);
						$contractor_address = $db->SqlQuote($vals[$valid['contractor_address']]);
						$contractor_city = $db->SqlQuote($vals[$valid['contractor_city']]);
						$contractor_country_code = $this->UsaState($vals[$valid['contractor_country_code']])? "US" : $db->SqlQuote($vals[$valid['contractor_country_code']]);
						$contractor_postcode = $db->SqlQuote($vals[$valid['contractor_postcode']]);
						$contractor = $this->ContractorGetByDuns($contractor_code);
						$id_contractor = (int)$contractor['id_contractor'];
						if(!$id_contractor>0)
						{
							$id_contractor = $this->ContractorInsert($contractor_code,$contractor_name,$contractor_address,$contractor_city,$contractor_postcode,$contractor_country_code);
						}
						elseif($contractor['name']=="")
						{
							$this->ContractorUpdate($id_contractor,$contractor_name,$contractor_address,$contractor_city,$contractor_postcode,$contractor_country_code);
						}
						$parent_code = $vals[$valid['parent_code']];
						$id_parent = $id_contractor;
						if($parent_code!=$contractor_code)
						{
							$parent = $this->ContractorGetByDuns($parent_code);
							$id_parent = (int)$parent['id_contractor'];
							if(!$id_parent>0)
							{
								$id_parent = $this->ContractorInsert($parent_code,'','','','','');
							}
						}
						$number = $vals[$valid['number']];
						if(strlen($vals[$valid['fiscal_year']])==6)
							$fiscal_year = substr($vals[$valid['fiscal_year']],0,4);
						$start_date = "";
						if(strlen($vals[$valid['start_date']])==8)
							$start_date = substr($vals[$valid['start_date']],0,4) . "-" . substr($vals[$valid['start_date']],4,2) . "-" . substr($vals[$valid['start_date']],6,2);
						$end_date = "";
						if(strlen($vals[$valid['end_date']])==8)
							$end_date = substr($vals[$valid['end_date']],0,4) . "-" . substr($vals[$valid['end_date']],4,2) . "-" . substr($vals[$valid['end_date']],6,2);
						$product_code = $db->SqlQuote($vals[$valid['product']]);
						$category_code = substr($product_code,0, is_numeric(substr($product_code,0,1))? 2:1);
						$category = $this->CategoryGetByCode($category_code);
						$id_category = (int)$category['id_category'];
						$city = ($vals[$valid['city']]!="*")? $db->SqlQuote($vals[$valid['city']]) : "";
						$county = ($vals[$valid['county']]!="*")? $db->SqlQuote($vals[$valid['county']]) : "";
						$country = $db->SqlQuote($vals[$valid['country']]);
						$country_code = $db->SqlQuote($vals[$valid['country_code']]);
						$country_code = $this->UsaState($vals[$valid['country_code']])? "US" : $db->SqlQuote($vals[$valid['country_code']]);
						$amount = (int)$vals[$valid['amount']];
						$iamount = $this->Inflation($year, $amount);
						$multiyear = $vals[$valid['multiyear']]=="Y"? "1":"0";
						$multiyear_cost = (int)$vals[$valid['multiyear_cost']];
						$notes = "";
						if($amount==0 && $multiyear=="1" && $multiyear_cost>0)
							$amount = $multiyear_cost;
						$this->ContractInsert($number,$fiscal_year,$start_date,$end_date,$id_contractor,$id_parent,$id_office,$id_category,$product_code,$city,$county,$country,$country_code,$amount,$multiyear,$notes,$iamount);
						$counter++;
					}
				}
			}
			$return = "$counter contracts imported for year $year\n";
		}
		else 
			$return = "File $file not found";
		return $return;
	}
	
	public function ImportYearContractors($year)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$db =& Db::globaldb();
		$filename = "import/dodc/italy_{$year}/contractors.csv";
		$db =& Db::globaldb();
		$lines = $fm->TextFileLines($filename);
		$counter = 0;
		foreach($lines as $line)
		{
			$vals = array();
			$vals = explode("|",$line);
			if(count($vals)==2 && $vals[1]!="")
			{
				$contractor_code = sprintf("%09s",$vals[1]);
				$contractor_name = $db->SqlQuote($vals[0]);
				$contractor = $this->ContractorGetByDuns($contractor_code);
				$id_contractor = (int)$contractor['id_contractor'];
				if(!$id_contractor>0)
				{
					$id_contractor = $this->ContractorInsert($contractor_code,$contractor_name,"","","","");
					$counter ++;
				}
				elseif($contractor['name']=="")
				{
					$this->ContractorUpdateName($id_contractor,$contractor_name);
				}
			}
		}
		return "$counter contractors imported for year $year\n";
	}
	
	public function ImportYearXml($year)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$db =& Db::globaldb();
		$categories = array();
		$this->Categories($categories);
		$counter = 0;
		foreach($categories as $category)
		{
			$xml_file = "import/dodc/italy_{$year}/cat_{$category['code']}.xml";
			$year_dump = new DOMDocument();
			$year_dump->loadXML($fm->TextFileRead($xml_file));
			$records = $year_dump->getElementsByTagName("record");
			if($records->length > 0)
			{
				for($i=0; $i<$records->length; $i++)
				{
					$record = $records->item($i);
					$office_code = $record->getElementsByTagName("contractingOfficeID")->item(0)->textContent;
					$fips = substr($record->getElementsByTagName("mod_agency")->item(0)->textContent,0,4);
					$office = $this->OfficeGetByCode($office_code);
					$id_office = (int)$office['id_office'];
					if(!$id_office>0)
					{
						$office_name = $record->getElementsByTagName("contractingOfficeAgencyID")->item(0)->textContent;
						$id_office = $this->OfficeInsert($office_code,$db->SqlQuote($office_name),$fips);
					}
					elseif($fips!=$office['fips'])
					{
						if(substr($office['fips'],2,2)=="00")
							$this->OfficeFipsUpdate($id_office,$fips);
					}
					$contractor_code = $record->getElementsByTagName("DUNSNumber")->item(0)->textContent;
					if(strlen($contractor_code)==13)
						$contractor_code = substr($contractor_code,0,9);
					$contractor = $this->ContractorGetByDuns($contractor_code);
					$id_contractor = (int)$contractor['id_contractor'];
					$contractor_state = substr($record->getElementsByTagName("state")->item(0)->textContent,0,2);
					$contractor_country_code = ($contractor_state!="" && $this->UsaState($contractor_state))? "US":"";
					$contractor_cd = $record->getElementsByTagName("vendor_cd")->item(0)->textContent;
					$contractor_name = $db->SqlQuote($record->getElementsByTagName("vendorName")->item(0)->textContent);
					$contractor_postcode = $record->getElementsByTagName("ZIPCode")->item(0)->textContent;
					if(strpos($contractor_cd,"ZZ99")>0 && is_numeric($contractor_postcode))
						$contractor_country_code = "IT";
					$contractor_address = $db->SqlQuote($record->getElementsByTagName("streetAddress")->item(0)->textContent);
					$contractor_city = $db->SqlQuote($record->getElementsByTagName("city")->item(0)->textContent);
					if(!$id_contractor>0)
						$id_contractor = $this->ContractorInsert($contractor_code,$contractor_name,$contractor_address,$contractor_city,$contractor_postcode,$contractor_country_code);
					elseif($contractor['name']=="")
					{
						$this->ContractorUpdate($id_contractor,"UNKNOWN",$contractor_address,$contractor_city,$contractor_postcode,$contractor_country_code);
					}
					$parent_contractor_code = $record->getElementsByTagName("parentDUNSNumber")->item(0)->textContent;
					$parent_code = ($parent_contractor_code!="" && $parent_contractor_code!="000000000" && $parent_contractor_code!="0000000000000" && $parent_contractor_code!=$contractor_code)? $parent_contractor_code : $contractor_code;
					if(strlen($parent_code)==13)
						$parent_code = substr($parent_code,0,9);
					$parent = $this->ContractorGetByDuns($parent_code);
					$id_parent = (int)$parent['id_contractor'];
					if(!$id_parent>0)
						$id_parent = $this->ContractorInsert($parent_code,"UNKNOWN","","","","");
					$product_code = substr($record->getElementsByTagName("productOrServiceCode")->item(0)->textContent,0,4);
					$category_code = substr($product_code,0, is_numeric(substr($product_code,0,1))? 2:1);
					$category = $this->CategoryGetByCode($category_code);
					$id_category = (int)$category['id_category'];
					$number = $record->getElementsByTagName("PIID")->item(0)->textContent;
					$fiscal_year = $record->getElementsByTagName("fiscal_year")->item(0)->textContent;
					$start_date = $record->getElementsByTagName("signedDate")->item(0)->textContent;
					$end_date = $record->getElementsByTagName("currentCompletionDate")->item(0)->textContent;
					$city = "";
					$county = "";
					$country = "Italy";
					$country_code = "IT";
					$amount = $record->getElementsByTagName("obligatedAmount")->item(0)->textContent;
					$iamount = $this->Inflation($year, $amount);
					$multiyear = $record->getElementsByTagName("multiYearContract")->item(0)->textContent == "No"? "0":"1";
					$notes = $db->SqlQuote($record->getElementsByTagName("descriptionOfContractRequirement")->item(0)->textContent);
					// contract
					$this->ContractInsert($number,$fiscal_year,$start_date,$end_date,$id_contractor,$id_parent,$id_office,$id_category,$product_code,$city,$county,$country,$country_code,$amount,$multiyear,$notes,$iamount);
					$counter++;
				}
			}
		}
		return "$counter contracts imported for year $year\n";
	}
	
	private function Inflation($year,$amount)
	{
		$inflation = 1;
		switch($year)
		{
			case "1997";
				$inflation = 1.4107;
			break;
			case "1998";
				$inflation = 1.3891;
			break;
			case "1999";
				$inflation = 1.3591;
			break;
			case "2000";
				$inflation = 1.3149;
			break;
			case "2001";
				$inflation = 1.2792;
			break;
			case "2002";
				$inflation = 1.2586;
			break;
			case "2003";
				$inflation = 1.2305;
			break;
			case "2004";
				$inflation = 1.1986;
			break;
			case "2005";
				$inflation = 1.1593;
			break;
			case "2006";
				$inflation = 1.1231;
			break;
			case "2007";
				$inflation = 1.0920;
			break;
			case "2008";
				$inflation = 1.0516;
			break;
			case "2009";
				$inflation = 1.0554;
			break;
			case "2010";
				$inflation = 1.0384;
			break;
		}
		return $amount * $inflation;
	}
	
	private function ImportValidate($year)
	{
		$valid = array();
		switch($year)
		{
			case "1997";
			case "1998";
			case "1999";
			case "2000";
				$valid['fields'] = 79;
			break;
			case "2001";
			case "2002";
				$valid['fields'] = 87;
			break;
			case "2003";
				$valid['fields'] = 88;
			break;
			case "2004";
			case "2005";
			case "2006";
				$valid['fields'] = 89;
			break;
		}
		switch($year)
		{
			case "1997";
			case "1998";
			case "1999";
			case "2000";
				$valid['origin'] = 6;
				$valid['fips'] = 1;
				$valid['office_code'] = 3;
				$valid['office_name'] = 4;
				$valid['contractor_code'] = 11;
				$valid['parent_code'] = 13;
				$valid['contractor_name'] = 16;
				$valid['contractor_address'] = 17;
				$valid['contractor_city'] = 18;
				$valid['contractor_country_code'] = 19;
				$valid['contractor_postcode'] = 20;
				$valid['program_code'] = 34;
				$valid['program_description'] = 35;
				$valid['number'] = 5;
				$valid['fiscal_year'] = 0;
				$valid['start_date'] = 9;
				$valid['end_date'] = 10;
				$valid['product'] = 32;
				$valid['description'] = 33;
				$valid['city'] = 24;
				$valid['county'] = 25;
				$valid['country'] = 26;
				$valid['country_code'] = 23;
				$valid['amount'] = 28;
				$valid['multiyear'] = 30;
				$valid['multiyear_cost'] = 31;
				$valid['weapon_code'] = 36;
				$valid['weapon_name'] = 37;
				$valid['actions'] = 27;
			break;
			case "2001";
			case "2002";
			case "2003";
				$valid['origin'] = 6;
				$valid['fips'] = 2;
				$valid['office_code'] = 3;
				$valid['office_name'] = 4;
				$valid['contractor_code'] = 13;
				$valid['parent_code'] = 15;
				$valid['contractor_name'] = 17;
				$valid['contractor_address'] = 18;
				$valid['contractor_city'] = 19;
				$valid['contractor_country_code'] = 20;
				$valid['contractor_postcode'] = 21;
				$valid['program_code'] = 34;
				$valid['program_description'] = 35;
				$valid['number'] = 5;
				$valid['fiscal_year'] = 0;
				$valid['start_date'] = 11;
				$valid['end_date'] = 12;
				$valid['product'] = 32;
				$valid['description'] = 33;
				$valid['city'] = 25;
				$valid['county'] = 26;
				$valid['country'] = 27;
				$valid['country_code'] = 24;
				$valid['amount'] = 28;
				$valid['multiyear'] = 30;
				$valid['multiyear_cost'] = 31;
				$valid['weapon_code'] = 36;
				$valid['weapon_name'] = 37;
				$valid['actions'] = 86;
			break;
			case "2004";
			case "2005";
			case "2006";
				$valid['origin'] = 6;
				$valid['fips'] = 2;
				$valid['office_code'] = 3;
				$valid['office_name'] = 4;
				$valid['contractor_code'] = 14;
				$valid['parent_code'] = 16;
				$valid['contractor_name'] = 18;
				$valid['contractor_address'] = 19;
				$valid['contractor_city'] = 20;
				$valid['contractor_country_code'] = 21;
				$valid['contractor_postcode'] = 22;
				$valid['program_code'] = 35;
				$valid['program_description'] = 36;
				$valid['number'] = 5;
				$valid['fiscal_year'] = 0;
				$valid['start_date'] = 12;
				$valid['end_date'] = 13;
				$valid['product'] = 33;
				$valid['description'] = 34;
				$valid['city'] = 26;
				$valid['county'] = 27;
				$valid['country'] = 28;
				$valid['country_code'] = 25;
				$valid['amount'] = 29;
				$valid['multiyear'] = 31;
				$valid['multiyear_cost'] = 32;
				$valid['weapon_code'] = 37;
				$valid['weapon_name'] = 38;
				$valid['actions'] = 86;
			break;
		}
		return $valid;
	}
	
	public function OfficeContractors(&$rows,$id_office)
	{
		$sqlstr = "SELECT co.id_contractor,co.name,SUM(c.iamount) AS total 
			FROM contracts c 
			INNER JOIN contractors co ON c.id_contractor=co.id_contractor 
			WHERE c.id_office=$id_office 
			GROUP BY c.id_contractor ORDER BY total desc";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	private function OfficeDelete($id_office)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->begin();
		$db->lock( "offices" );
		$res[] = $db->query("DELETE FROM offices WHERE id_office='$id_office' ");
		Db::finish( $res, $db);
	}
	
	private function OfficeFipsUpdate($id_office,$fips)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->begin();
		$db->lock( "offices" );
		$sqlstr = "UPDATE offices SET fips='$fips' WHERE id_office='$id_office' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function OfficeGet($id_office)
	{
		$sqlstr = "SELECT o.id_office,o.code,o.name,o.fips,f.description,f.agency,SUM(c.iamount) AS total,COUNT(c.id_contract) AS counter
			FROM offices o
			INNER JOIN fips f ON o.fips=f.code
			INNER JOIN contracts c ON o.id_office=c.id_office
			WHERE o.id_office='$id_office' 
			GROUP BY o.id_office";
		$row = array();
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function OfficeGetByCode($code)
	{
		$row = array();
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->query_single($row,"SELECT id_office,code,name,fips FROM offices WHERE code='$code' ");
		return $row;
	}
	
	private function OfficeInsert($code,$name,$fips)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$db->begin();
		$db->lock( "offices" );
		$id_office = $db->nextId( "offices", "id_office" );
		$sqlstr = "INSERT INTO offices (id_office,code,name,fips) 
			VALUES ($id_office,'$code','$name','$fips')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_office;
	}

	public function Offices( &$rows )
	{
		$sqlstr = " SELECT o.id_office,o.name,f.agency,SUM(c.iamount) AS total,COUNT(c.id_contract) AS counter
			FROM offices o
			INNER JOIN fips f ON o.fips=f.code
			INNER JOIN contracts c ON o.id_office=c.id_office
			GROUP BY o.id_office
			ORDER BY total DESC ";
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function OfficeMerge($id_from,$id_to)
	{
		if($id_from>0 && $id_to>0)
		{
			$db =& Db::globaldb();
			$this->SelectDb();
			$db->begin();
			$db->lock( "contracts" );
			$sqlstr = "UPDATE contracts SET id_office='$id_to'  WHERE id_office='$id_from' ";
			$res[] = $db->query($sqlstr);
			Db::finish( $res, $db);
			$this->OfficeDelete($id_from);
		}
	}

	public function OfficeSearch(&$rows, $params, $paged=true)
	{
		$db =& Db::globaldb();
		$this->SelectDb();
		$rows = array();
		$sqlstr = "SELECT o.id_office,o.name,o.code
			 FROM offices o ";
		$sqlstr .= " WHERE 1=1 ";
		if (strlen($params['name']) > 0)
			$sqlstr .= " AND (o.name LIKE '%{$params['name']}%') ";
		if ($params['id_from']>0)
			$sqlstr .= " AND o.id_office<>{$params['id_from']} ";
		$sqlstr .= " ORDER BY o.name";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	public function SelectDb()
	{
		$db =& Db::globaldb();
		return $db->select_db(DODC_DATABASE);
	}

	public function SubContractors(&$rows,$id_contractor)
	{
		$sqlstr = "SELECT cr.id_contractor,cr.name,SUM(c.iamount) AS total FROM contracts c
			INNER JOIN contractors cr ON c.id_contractor=cr.id_contractor 
			WHERE c.id_parent=$id_contractor AND c.id_contractor<>c.id_parent
			GROUP BY cr.id_contractor ORDER BY total DESC";
		$db =& Db::globaldb();
		$this->SelectDb();
		return $db->QueryExe($rows, $sqlstr );
	}

	private function TrimValue(&$value)
	{
		$value = trim($value);
	}
	
	private function TrimQuotes(&$value)
	{
		$value = trim($value,"\"");
	}
	
	public function UsaState($country_code)
	{
		$usa_states = array("AK","AL","AR","AS","AZ","CA","CO","CT","DC","DE","FL","FM","GA","HI","IA","ID","IL","IN","KS","KY","LA","MA","MD","ME","MI","MN","MO","MP","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UM","UT","VA","WA","WI","WV");
		return is_numeric($country_code) || in_array($country_code,$usa_states);
	}
}
?>
