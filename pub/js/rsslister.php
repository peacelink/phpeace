<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$url = $get['url'];
$ttl = (int)$get['ttl'];
$items = (int)$get['items'];

include_once(SERVER_ROOT."/../classes/layout.php");
$l = new Layout;

header("Content-type: text/javascript; charset=" . $l->conf->Get("encoding"));
if($url!="")
{
	if(!$items>0)
		$items = 15;
	include_once(SERVER_ROOT."/../classes/rss.php");
	$r = new Rss();
	$xml = $r->Get($url,$ttl);
	$xmldata = $l->th->TextReplaceForJavascript($xml,true);
?>

var xmldata = '<?=$xmldata;?>';

var items_limit = <?=$items;?>;

function rss_slister(xmlstring, divId, divClass,items_limit){
this.items_limit=items_limit
this.listerid=divId 
this.pointer=0
	
if (window.ActiveXObject) {
	this.doc=new ActiveXObject("Microsoft.XMLDOM");
	this.doc.async="false";
	this.doc.loadXML(xmlstring);
} else {
	var parser=new DOMParser();
	this.doc=parser.parseFromString(xmlstring,"text/xml");
}
this.initialize()
}

rss_slister.prototype.initialize=function(){ 
if(this.doc.getElementsByTagName("item").length==0){ 
document.getElementById(this.listerid).innerHTML="<!-- no news is good news -->"
return
}
var instanceOfLister=this
var listerDiv=document.getElementById(this.listerid)
var listercontent='<ul class="rss-list">'
this.feeditems=this.doc.getElementsByTagName("item")
for (var i=0; i<Math.min(this.feeditems.length,this.items_limit); i++){
listercontent+='<li><a href="'+this.feeditems[i].getElementsByTagName("link")[0].firstChild.nodeValue+'">'+this.feeditems[i].getElementsByTagName("title")[0].firstChild.nodeValue+'</a>'
var desc = this.feeditems[i].getElementsByTagName("description")[0].firstChild
if(desc!=null)
	listercontent+="<div class=\"ltdesc\">"+desc.nodeValue+"</div>"
listercontent+="</li>"
}
listercontent+="</ul>"

listerDiv.innerHTML=listercontent
}

new rss_slister(xmldata,'lister-id','lister',items_limit)

<?php } ?>

