(function($) {
	var globalUsername, xhrStatus, xhrRequest;

	var phwidgets = {
	
		selectors :  function () {
			return {
				html : $("html"),
				body : $("body"),
				header : $("#header"),
				content : $("#widgets-content"),
				message : $("#ajaxmessage"),
				error : $("#ajaxerror"),
				tabNav : $("#tab-navigation"),
				tabAnchor : $("#tab-navigation li"),
				tabEdit : $("span.btn-edit-tab"),
				tabWrapper : $("#tab-wrapper"),
				tabWrapperLoading : $("#tab-wrapper-loading"),
				tabAddForm : $("#tab-add-form"),
				tabDialogForm : $("#tab-dialog-form"),
				widgetWrappers : $("ul.widget-wrapper"),
				widgets : $("li.widget"),
				widgetHeaders : $("div.widget-header"),
				widgetContents : $("div.widget-content"),
				widgetFooter : $("div.widget-footer"),
				addTab : $("#add-tab"),
				widgetClose : $("span.widget-close"),
				widgetToggle : $("span.widget-toggle"),
				widgetShare : $("span.widget-share"),
				widgetReport : $("span.widget-report"),
				addTabDialog : $("#add-tab-dialog"),
				addTabForm : $("#add-tab-form"),
				tabClose : $("span.tab-close"),
				reportAbuseDialog : $("#report-abuse-dialog")
			};
		},
		
		options : {            
			serverPath : phpeace.pub_web.replace(/^https?\:\/\//i, ""),
			servicePath : '/widgets/service.php?',
			openinviterPath : '/widgets/openinviter.php?',
			shareWidgetPath : '/widgets/service.php?',
			roundedCorners : '4px',
			shareWidgetEmailSplitter : '\n',
			tabInitialCount: 100,
			protocol: (document.location.protocol == "https:") ? "https://" : "http://"
		},
		
		pages : {
			ie6 : '.ie6',
			homepage : '.homepage',
			widgetLibrary : '.homepage-widget_library',
			widgetCreate : '.homepage-widget_create',
			widgetEdit : '.homepage-widget_edit',
			widgetCategory : '.homepage-widget_category',
			myWidgets : '.homepage-mywidgets',
			widgetSearchTitle : '.homepage-widget_search',
			widgetSearchContent : '.homepage-widget_content_search'
		},
		
		getData : {
			
			init : function () {
				phwidgets.getData.tab('action=get&type=tabs', phwidgets.outputData.tab); 
			},
			
			tab : function (params, callback) {
				phwidgets.getData.process('tab', params, callback, phwidgets.getData.tabCallBack);
			},
			
			tabCallBack : function () {
				phwidgets.getData.widget('action=get&type=widgets', phwidgets.outputData.widget);        
			},
			
			widget : function (params, callback, tabId) {
				if (!tabId){ tabId = 'tab-1'; }
				phwidgets.getData.process('widget', params, callback, null, tabId);
			},
			
			process : function(type, params, callback, callback2, tabId) {
				var s = phwidgets.selectors(),
					o = phwidgets.options;
				jQuery.ajaxSetup({
					url : o.protocol + o.serverPath + o.servicePath + params,
					dataType : 'json',
					timeout : 60000,
					global: false,
					cache : false,
					success : function (data) {
							callback(type,data);
							
							if (callback2 !== null) {
								callback2();
							}
					}
				});
				
				if (type == 'widget' && tabId !== undefined) {
					jQuery.ajax({
						data : 'tabId='+ tabId
					});
				}
				else {
					jQuery.ajax();
				}
			},
			
			processOpenInviter : function(emailAddress, password, widgetId, callback) {
				var splitter = emailAddress.split('@');
				var userName = splitter[0];
				var emailProvider = splitter[1];
				emailProvider = emailProvider.split('.')[0];
				
				var s = phwidgets.selectors();
				var o = phwidgets.options;
				var params = 'email=' + emailAddress + '&pwd=' + password + '&provider=' + emailProvider ;
				
				$("#share-widget-dialog form").remove().empty();
				var loading = '<p id="contacts_loading">' + widgets_labels.contacts_loading + '</p>';
				$("#share-widget-dialog").append(loading);

				jQuery.ajax({
					url : o.protocol + o.serverPath + o.openinviterPath + 'action=retrievecontacts',
					type : 'POST',
					data : params,
					error : function(data) {
						$("#share-widget-dialog p").remove().empty();
						$("#share-widget-dialog").append('<p>' + data.responseText + '</p>');
					},
					success : function (data) {
						$("#share-widget-dialog p").remove().empty();
						callback(widgetId, data);
					}
				});
			},
			
			preview : function (data, previewContainer) {
				var o = phwidgets.options;
				$.ajax({
					url :  o.protocol + o.serverPath + o.servicePath + 'action=previewwidget', 
					type : 'POST',
					dataType : 'json',
					data : data,
					error : function (x) {    
						alert(x);
					},
					success : function (data)  { 
						var widget = phwidgets.outputData.preview(data);
						if (widget !== null) {
							previewContainer.qtip('api').updateContent(widget); 
						}
					}
				});
			}
		},
		
		saveShareWidgetData : function(jsonData){
			var o = phwidgets.options;
			$.ajax({
				url : o.protocol + o.serverPath + o.shareWidgetPath + '&action=sharewidgets',
				type : 'POST',
				data : 'data=' + jsonData,
				beforeSend: function () {
					var container = $("#share-widget-dialog div.widget-share");
					var waitingMsg = '<div class="widget-share"><div class="loading"><h4>' + widgets_labels.sending + '</h4><p>' + widgets_labels.sending_wait + '</p></div>';
					container.replaceWith(waitingMsg);
				},
				success : function (data) {
					var container = $("#share-widget-dialog div.widget-share");
					var successMsg = '<div class="widget-share-done"><h4>' + widgets_labels.sent + '</h4><p>' + widgets_labels.sent_shared + '</p></div>';
					var failMsg = '<div class="widget-share-done"><h4>' + widgets_labels.sent_failed + '</h4><p>' + data.message + '</p></div>';
					
					if (data.status == "FAIL"){
						container.replaceWith(failMsg);
					} else {
						container.replaceWith(successMsg);
					}
				},
				error : function (xhr, ajaxOptions, thrownError){
					var container = $("#share-widget-dialog div.widget-share");
					var errorMsg = '<div class="widget-share-done"><h4>' + widgets_labels.problem + '</h4><p>' + widgets_labels.sent_error + '</p></div>';
					container.replaceWith(errorMsg);
				}
			});
		},
		
		saveData : function (action, params, callback, callbackParams) {
			if (action != 'reportabuse' && phwidgets.helper.checkUser() === false) 
				return;
			var o = phwidgets.options;
			$.ajax({
				url : o.protocol + o.serverPath + o.servicePath + '&action='+action,
				type : 'POST',
				dataType : 'json',
				data : params,
				error : function (err){
					if (callback !== undefined && callback !== null)
						callback(callbackParams, false, err);
				},
				success : function (data) {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages,
						tabs = s.tabWrapper,
						status = (data.status == "SUCCESS") ? true : false;
						
					if (callback !== undefined && callback !== null) {
						callback(callbackParams, status, data.message);
					}
				}
			});

		},
		
		outputData :{
		
			init : function () {
			
			},
			
			preview : function(widget) {

				var widgetType = widget.widgetType.toLowerCase(),
					layoutType = widget.layoutType,
					html = '';
				
				html += '<ul>';
				
				if ( layoutType === undefined || widgetType === 'advance' ) {
					html += '<li class="clearfix preview widget ' + widgetType + '" ' + 'id="' + widget.id + '">';
				} else {
					html += '<li class="clearfix preview widget ' + widgetType + ' ' + layoutType + '" ' + 'id="' + widget.id + '">';
				}
				
				html += '<div class="widget-header clearfix"><h3 title="' + widget.title + '">' + widget.short_title + '</h3></div><div class="widget-content clearfix">';
				
				if (widgetType == "manual") {
					if (widget.image != ""){
						html += '<img width="280px" src="' + widget.image + '">';
					}
					html += '<p class="content">' + widget.content + '</p>';
					if (widget.link != ""){
						html += '<p>' + '<a href="' + widget.link + '" target="_blank">' + widget.link  + '</a></p>';
					}
				}
				
				if (widgetType == "advance") {
					html += '<ul>';
					for ( var j=0; j < widget.contents.length; j++) {
						var content = widget.contents,
							current = content[j];
						html += '<li class="clearfix">';
						if ( current.image !== '' ) {
							if (current.content !== '' && current.link_url !== '') {
								html += '<img src="' + current.image +  '" alt="' + current.content + '" title="' + current.content + '" width="60 height="40" />';
							} else {
								html += '<img src="' + current.image +  '" alt="' + current.content + '" title="' + current.content + '" width="270" />';
							}
						}
						if ( current.content !== '' ) {
							html += '<p>' + current.content + '</p>';
						}
						if ( current.link_url !== '' ) {
							html += '<p><a href="' + current.link_url + '"target="_blank">' + current.link_url + '</a></p>';
						}
						html += '</li>';
					}
					html += '</ul>';
				}
				
				if (widgetType == "custom") {
					html += '<div class="widget-content clearfix custom">' + widget.content + '</div>';
				}
				
				if (widgetType == "feature") {
					html += '<div class="widget-content clearfix custom">' + widget.content + '</div>';
				}

				// rss widgets
				if (widgetType == "rss") {
					html += '<ul>';
					
					if (layoutType == "list_item") {
						for ( var j=0; j < widget.contents.length; j++) {
							var content = widget.contents;
							html += '<li>';
							if (content[j].image != "") {
								html += '<img src="' + content[j].image + '" title="' + content[j].title  + '" alt="' + content[j].title +  '" />';
							}
							html += '<p><a href="' + content[j].link + '" target="_blank">'  + content[j].title + '</a></p><p>' + content[j].description + '</p>';
							html += '</li>';
						}
					}
					
					if (layoutType == "list_with_image") {
						for ( var j=0; j < widget.contents.length; j++) {
							var content = widget.contents;
							html += '<li>';
							if (content[j].image != "") {
								html += '<img src="' + content[j].image + '" title="' + content[j].title  + '" alt="' + content[j].title +  '" />';
							}
							html += '<p><a href="' + content[j].link + '" title="' + content[j].title+ '" target="_blank">' + content[j].title + '</a></p><p>' + content[j].description + '</p>';
							html += '</li>';
						}
					}
					
					if (layoutType == "list_images") {
						for ( var j=0; j < widget.contents.length; j++) {
							var content = widget.contents;
							html += '<li>';
							html += '<p><a href="' + content[j].link + '" target="_blank"><img src="' + content[j].image + '" alt="' + content[j].alt + '" title="' + content[j].alt + '" /></a></p>';
							html += '</li>';
						}
					}
					
					if (layoutType == "list_with_thumbnail") {
						for ( var j=0; j < widget.contents.length; j++) {
							var content = widget.contents;
							html += '<li class="clearfix">';
							if ( content[j].image == '' ) {
								content[j].image = '/images/24.png';
							}
							html += '<a href="' + content[j].link + '"target="_blank"><img src="' + content[j].image +  '" alt="' + content[j].alt + '" title="' + content[j].alt + '" width="60 height="40" /></a><p><a href="' + content[j].link + '"target="_blank">' + content[j].title + '</a><p>' + content[j].description + '</p>';
							html += '</li>';
						}
					}
					
					html += '</ul>';
				}
			
				html += '</div>';
				html += '<div class="widget-footer clearfix copyright-notice"><p>' + widgets_labels.copyright + '</p></div>';
				html += '</li><li class="preview-message">' + widgets_labels.preview_close + '</li></ul>';
					
				return html;

			},
			
			tab : function (type, data) {
				if(data && data.restricted) {
					$('#widgets-content').hide();
					$('#home-content').show();
				}
				else {
					var s = phwidgets.selectors(),
					o = phwidgets.options;
					globalUsername = data.username;
					var tabLink = null;
					var tabContainer = null;
					for ( var i = 0; i < data.tabs.length; i++ ) {
						var tab = data.tabs;
						if (tab[i].id == 'add-tab') {
							tabLink = '<li class="' + tab[i].id + '"><a id="btn-'+ tab[i].id +'" href="#' + tab[i].id + "\"'" + '><span>' + widgets_labels.tab_add + '</span></a></li>';
							tabContainer = '<div id="' + tab[i].id + '" class="clearfix"><ul class="widget-wrapper column-1" style=""></ul><ul class="widget-wrapper column-2" style=""></ul><ul class="widget-wrapper column-3" style=""></ul></div>';
						}
						else if (tab[i].id == 'add-widget') {
							tabLink = '<li class=' + tab[i].id + '><a id="btn-'+ tab[i].id +'" href="#' + tab[i].id + "\"'" + '><span class="ui-icon ui-icon-circle-plus" style="float: left; margin-right: 10px; "></span>' + widgets_labels.widget_add_edit + '</a></li>';
							tabContainer = '<div id="' + tab[i].id + '" class="clearfix"><ul class="widget-wrapper column-1" style=""></ul><ul class="widget-wrapper column-2" style=""></ul><ul class="widget-wrapper column-3" style=""></ul></div>';
						}
						else {
							tabLink = '<li class=' + tab[i].id + '><a href="#' + tab[i].id + '"><span class="btn-edit-tab">' + tab[i].name + '</span></a>';
							
							if (phwidgets.helper.checkUser())
								tabLink += '<span class="tab-close ui-icon ui-icon-close">' + widgets_labels.tab_remove + '</span>';
							
							tabLink += '</li>';
							
							tabContainer = '<div id="' + tab[i].id + '" class="clearfix"><ul class="widget-wrapper column-1 clearfix" style=""></ul><ul class="widget-wrapper column-2 clearfix" style=""></ul><ul class="widget-wrapper column-3 clearfix" style=""></ul></div>';
						}
						$(tabLink).appendTo(s.tabNav);
						$(tabContainer).appendTo(s.tabWrapper);
					}
				}
			},
			
			widget : function (type, data) {
				var e = $('ul.widget-wrapper li'),
					emptyTab = '<div id="empty-tab" class="empty-tab"><h4>' + widgets_labels.widgets_tab_no + '</h4><p>' + widgets_labels.widgets_tab_add + '</p></div>';
				if (data !== undefined) {
					if (e.length) {
						for ( i = 1; i <= 3; i++) {
							$('#' + data.id + ' .' + 'column-' + i).empty();
						}
					}
					
					$("#empty-tab").empty().remove();
					if (data.widgets == '') {
						$('#tab-' + data.id).prepend(emptyTab);
					} else {
						for (var i = 0; i < data.widgets.length; i++){
							
							var widget = data.widgets;
							var widgetType = widget[i].widgetType.toLowerCase(),
								layoutType = widget[i].layoutType,
								html = '';
							
							if ( layoutType === undefined || widgetType === 'advance' ) {
								html += '<li class="clearfix widget ' + widgetType + '" ' + 'id="' + widget[i].id + '">';
							} else {
								html += '<li class="clearfix widget ' + widgetType + ' ' + layoutType + '" ' + 'id="' + widget[i].id + '">';
							}
							
							html += '<div class="widget-header clearfix"><h3 title="' + widget[i].title + '">' + widget[i].short_title + '</h3><span class="widget-close ui-icon ui-icon-close">' + widgets_labels.widget_remove + '</span><span class="widget-toggle ui-icon ui-icon-circle-triangle-n">' + widgets_labels.widget_collapse + '</span></div><div class="widget-content clearfix">';
							
							if (widgetType == "manual") {
								if (widget[i].image != ""){
									html += '<img width="280px" src="' + widget[i].image + '">';
								}
								html += '<p class="content">' + widget[i].content + '</p>';
								if (widget[i].link != ""){
									html += '<p>' + '<a href="' + widget[i].link + '" target="_blank">' + widget[i].link  + '</a></p>';
								}
							}
							
							if (widgetType == "advance") {
								html += '<ul>';
								for ( var j=0; j < widget[i].contents.length; j++) {
									var content = widget[i].contents,
										current = content[j];
									html += '<li class="clearfix">';
									if ( current.image !== '' ) {
										if (current.content !== '' && current.link_url !== '') {
											html += '<img src="' + current.image +  '" alt="' + current.content + '" title="' + current.content + '" width="60 height="40" />';
										} else {
											html += '<img src="' + current.image +  '" alt="' + current.content + '" title="' + current.content + '" width="270" />';
										}
									}
									if ( current.content !== '' ) {
										html += '<p>' + current.content + '</p>';
									}
									if ( current.link_url !== '' ) {
										html += '<p><a href="' + current.link_url + '"target="_blank">' + current.link_url + '</a></p>';
									}
									html += '</li>';
								}
								html += '</ul>';
							}
							
							if (widgetType == "feature") {
								html += '<div class="widget-content clearfix custom">' + widget[i].content + '</div>';
							}

							if (widgetType == "custom") {
								html += '<div class="widget-content clearfix custom">' + widget[i].content + '</div>';
							}
							
							// rss widgets
							if (widgetType == "rss") {
								html += '<ul>';
								
								if (layoutType == "list_item") {
									for ( var j=0; j < widget[i].contents.length; j++) {
										var content = widget[i].contents;
										html += '<li>';
										if (content[j].image != "") {
											html += '<img src="' + content[j].image + '" title="' + content[j].title  + '" alt="' + content[j].title +  '" />';
										}
										html += '<p><a href="' + content[j].link + '" target="_blank">'  + content[j].title + '</a></p><p>' + content[j].description + '</p>';
										html += '</li>';
									}
								}
								
								if (layoutType == "list_with_image") {
									for ( var j=0; j < widget[i].contents.length; j++) {
										var content = widget[i].contents;
										html += '<li>';
										if (content[j].image != "") {
											html += '<img src="' + content[j].image + '" title="' + content[j].title  + '" alt="' + content[j].title +  '" />';
										}
										html += '<p><a href="' + content[j].link + '" title="' + content[j].title+ '" target="_blank">' + content[j].title + '</a></p><p>' + content[j].description + '</p>';
										html += '</li>';
									}
								}
								
								if (layoutType == "list_images") {
									for ( var j=0; j < widget[i].contents.length; j++) {
										var content = widget[i].contents;
										html += '<li>';
										html += '<p><a href="' + content[j].link + '" target="_blank"><img src="' + content[j].image + '" alt="' + content[j].alt + '" title="' + content[j].alt + '" /></a></p>';
										html += '</li>';
									}
								}
								
								if (layoutType == "list_with_thumbnail") {
									for ( var j=0; j < widget[i].contents.length; j++) {
										var content = widget[i].contents;
										html += '<li class="clearfix">';
										if ( content[j].image == '' ) {
											content[j].image = '/images/24.png';
										}
										html += '<a href="' + content[j].link + '"target="_blank"><img src="' + content[j].image +  '" alt="' + content[j].alt + '" title="' + content[j].alt + '" width="60 height="40" /></a><p><a href="' + content[j].link + '"target="_blank">' + content[j].title + '</a><p>' + content[j].description + '</p>';
										html += '</li>';
									}
								}
								
								html += '</ul>';
							}
						
							html += '</div>';
							html += '<div class="widget-footer clearfix copyright-notice"><p>' + widgets_labels.copyright + '</p>';
                                                        if(widgets_abuse_reporting) {
                                                            html += '<span class="widget-report">' + widgets_labels.report + '</span>';
                                                        }
							if (phwidgets.helper.checkUser() && widget[i].shareable == "1") {
								html += '<span class="widget-share">' + widgets_labels.share + '</span>';
							}
							html += '</div>';
							html += '</li>';
							var position = '#' + data.id + ' .' + 'column-' + widget[i].column;
							$(html).appendTo(position);
							$("li#"+widget[i].id).data("permission",widget[i].own);
						}
					}
				}
				var s = phwidgets.selectors();
				var o = phwidgets.options;
				
				$(document).ready(function(){
					s.tabWrapperLoading.hide();
					phwidgets.ui.tab.init(s.tabWrapper);
					phwidgets.ui.widget(s.widgetWrappers);
					phwidgets.ui.droppable(s.tabNav.children('li').not('li.add-tab, li.add-widget'));
					s.tabWrapper.show('slow');       
				});
			}
		},
		
		helper : {
		
			serialize : function (tabId) {
				var str = '';
				for (i = 1; i <=3;i++) {
					var values = $(tabId + ' .column-' + i + ' li.widget').map(function() { 
						return this.id;
						}).get().join(',');
					str += 'col' + i + '=' + values + ';';
				}
			return(str);
			},
			
			serializeEmailsToJson : function (widgetId, emails, splitter) {
				var jsonBuilder = new StringBuilder();
				jsonBuilder.append("{");
				jsonBuilder.appendFormat(" \"widgetid\" : \"{0}\", ",widgetId);
				jsonBuilder.appendFormat(" \"emails\" : [ ");
				
				var newArray = emails.split(splitter);
				for (i = 0; i < newArray.length; i++) {
					if (newArray[i] != '') {
						jsonBuilder.append("{");
						jsonBuilder.appendFormat(" \"email\" : \"{0}\"  ", newArray[i]);
						jsonBuilder.append("}");
						jsonBuilder.appendFormat(",");
					}
					else {
						var temp = jsonBuilder.toString();
						jsonBuilder.clear();
						temp = temp.substring(0,temp.length-1);
						jsonBuilder.append(temp);
					}
				}
				
				jsonBuilder.append("]}"); // end array and json tag
				
			return jsonBuilder.toString();
			},
			
			toJson : function (tabId, widgetData) {
				var data = widgetData.substring(0,widgetData.length-1);
				var newArray = data.split(';');
				var sb = new StringBuilder();
				
				sb.append("{");

				var j = 1;
				
				for(i = 0; i <newArray.length; i++) {
					var columnId  = 'col' + j + '=';
					var temp = newArray[i].toString();
					var widgetIdString  = temp.replace(columnId, "");
					sb.appendFormat("\"{0}\":[",columnId);
					var ss = widgetIdString.split(',');
					for(z = 0;z < ss.length; z++) {                              
						sb.appendFormat("{\"id\":\"{0}\"}",ss[z]);
						if (z < ss.length-1) {
							sb.appendFormat(",");
						}
					}
					sb.appendFormat("]");
					if (i < newArray.length-1) {
						sb.appendFormat(",");
					}
					j++;                        
				}
				sb.append("}");
				return (sb.toString());
			},
			
			checkUser : function (message) {
				if (globalUsername == '0' || globalUsername == '') {
					if (message !== null) {
						//console.log(message);
						return false;
					}
					else
					return false;
				}
				return true;
			},
			
			parseISO8601 : function (str) {
				// we assume 10 character date in format, YYYY-MM-DD
				
				var dateParts = str.split('-');
				date = new Date();
				
				date.setUTCFullYear(Number(dateParts[0]));
				date.setUTCMonth(Number(dateParts[1])-1);
				date.setUTCDate(Number(dateParts[2]));
				
				return date;
			},
			
			getDateString : function(dateToParse) {
				var date = new Date(dateToParse);
				if (isNaN(date)) {
					date = phwidgets.helper.parseISO8601(dateToParse);
				}
				return date.toDateString();
			}
			
		},
		
		page : {
		
			init : function() {
				this.homepage.init();
				this.widgetLibrary.init();
				this.widgetLibrary.widgetCreate.init();
				this.widgetLibrary.widgetEdit.init();
			},
			
			widgetLibrary : {

				init : function () {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.widgetLibrary) || s.body.is(p.widgetCategory) || s.body.is(p.myWidgets) || s.body.is(p.widgetSearchTitle) || s.body.is(p.widgetSearchContent)){
						this.prepare();
						this.widgetSearch(); 
						this.widgetDelete();
					}
				},
				
				widgetSearch : function () {
					
					var select = $("#search-option");
					select.bind("change", function () {
						var selected = $(this).val();
						var input = $("#search-for-widget form input:hidden");
						input.val(selected);
					});

				},
				
				prepare : function () {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages,
						widget = $("li.widget");

					widget.each(function () {
						$(this).append("<div class='status'></div>");
					});
					
					$('#list-widget button.widget-action-preview').each(function() {
						var wp = this,
						wpId = $(this).closest("li").attr("id"),
						data = 'data={"widgetid":"' + wpId + '"}',
						loadingImage = '<img src="' + o.protocol + o.serverPath + '/images/custom/spinner.gif">';
						
						$(wp).qtip({
							content: loadingImage, 
							style: {
								name: 'dark',
								'max-height' : 400,
								width : 300,
								padding: 0,
								tip: true // Give it a speech bubble tip with automatic corner detection
							},
							position: {
								corner: {
									tooltip: 'bottomMiddle', // Use the corner...
									target: 'topMiddle' // ...and opposite corner
								},
							adjust: { resize: false, screen: false }
								//target: 'mouse',
							},
							show: { 
								delay: 0,
								solo: true,
								when : {
									event: 'click'
								}
							},
							hide: { 
								fixed: true,
								when : {
									event: 'unfocus'
								}                            
							},
							api: {
								beforeShow: function() {
									if ($(wp).qtip('api').options.content.text == loadingImage) {
										phwidgets.getData.preview(data, $(wp));
									}
								}
							}
						});
					});
				
					$("button.widget-action-add").bind("click", function() {
						var wp = this;
						var wpId = $(this).closest("li").attr("id");
						var params =   'widgetId=' +  wpId + '&targetTabId=tab-1';
						phwidgets.saveData('updatetabwidgetplacement', params, phwidgets.page.widgetLibrary.addWidgetCallback, wp);
						return false;
					});
					
					var widgetComments = $("button.widget-action-comments");
					
					widgetComments.each(function(){
						var button = $(this),
							list = button.closest("li"),
							comments = list.find("div.widget-comments");
							
						comments.dialog({
							autoOpen : false,
							draggable : false,
							resizable : false,
							modal : true,
							hide : 'puff'
						});
						
						button.bind("click", function(e){
							e.preventDefault();
							comments.dialog("open");
							return false;
						});
					});
					
				},
				
				addWidgetCallback : function (params, status, error){
					var widget = params;
					//var parent = $(widget).closest("div.widget-action");
					var wrapper = $(widget).parent().siblings("div.status");
					
					if (status) {
						wrapper.empty().append("Widget added");
					} else {
						// TODO : Hardcode error since error param is undefined
						wrapper.empty().append(error);
					}
				},
				
				widgetCreate : {
					
					init: function () {
						var s = phwidgets.selectors(),
							o = phwidgets.options,
							p = phwidgets.pages;
						
						if (s.body.is(p.widgetCreate)){
							//this.prepare();
							this.tab();
						}
					},
					
					tab : function() {
						var el = $("#list-widget div.widget-create");
						var tabOpts = {
							spinner: widgets_labels.retrieving,
							cookie: {
								expires: 14
							},
							select: function (event,ui) {
							}
						};
						el.tabs(tabOpts);
					},
					
					prepare : function () {
						var createRss = $("#create-rss-widget");
						var createManual = $("#create-manual-widget");
						
						createRss.find("form").hide();
						createManual.find("form").hide();
						
						createRss.find("h4").bind("click", function() {
							createRss.find("form").toggle();
						});
						
						createManual.find("h4").bind("click", function() {
							createManual.find("form").toggle();
						});
					}
					
				},
				
				widgetEdit : {
				
					init: function () {
						var s = phwidgets.selectors(),
							o = phwidgets.options,
							p = phwidgets.pages;
						
						if (s.body.is(p.widgetEdit)){
							this.addWidgetEntry();
						}
					},
					
					addWidgetEntry : function () {
						var link = $("#add-new-widget-entry"),
							content = $("#add-new-widget-entry-fields");
						
						link.bind("click", function() {
							content.slideToggle(600);
							return false;
						});
					}
					
				},
				
				widgetDelete : function () {
					var link = $("div.widget-actions a.widget-actions-delete"),
						form = $("#delete_widget"),
						input = form.find("input[name='id']");
					input.val("");
					link.bind("click", function() {
						var id = $(this).parents("li").attr("id");
						input.val(id);
						form.submit();
						return false;
					});
				},
				
				widgetShare : {
					
					addressbook : {
						
						process : function (emailAddress, password, widgetId, callback) {
						
							var splitter = emailAddress.split('@');
							var userName = splitter[0];
							var emailProvider = splitter[1];
							emailProvider = emailProvider.split('.')[0];
							
							var s = phwidgets.selectors(),
								o = phwidgets.options,
								p = phwidgets.pages,
								container = $("#share-widget-dialog").find("div.widget-share");
								
							var params = 'email=' + emailAddress + '&pwd=' + password + '&provider=' + emailProvider;
							
							//$("#share-widget-dialog form").remove().empty();
							
							xhrRequest = jQuery.ajax({
								url : o.protocol + o.serverPath + o.openinviterPath + 'action=retrievecontacts',
								type : 'POST',
								data : params,
								beforeSend : function (){
									var loading = '<h4>' + widgets_labels.contacts_loading + '</h4><p>' + widgets_labels.contacts_loading_wait + '</p>';
									container.empty().append(loading);
								},
								error : function(data) {
									//$("#share-widget-dialog p").remove().empty();
									container.empty().append('<div><p>' + data.responseText + '</p></div>');
									xhrStatus = false;
								},
								success : function (data) {
									//$("#share-widget-dialog p").remove().empty();
									//container.empty();
									callback(widgetId, data);
									xhrStatus = false;
								}
							});

						},
						
						output : function (widgetId, data) {
							var s = phwidgets.selectors(),
								o = phwidgets.options,
								p = phwidgets.pages,
								container = $("#share-widget-dialog").find("div.widget-share"),
								permission = $("#"+widgetId).data("permission"),
								widgetShare = phwidgets.page.widgetLibrary.widgetShare;
								
							var contactListForm  = '<div id="widget-share-contactlist"><h4>' + widgets_labels.contact_list + '</h4><div id="widget-share-addressbook"><h5>' + widgets_labels.contacts + '</h5><ul id="email_listing_id" class="contact-lists clearfix" style="height: 200px; overflow: auto;"></ul></div><div id="widget-share-email"><p>' + widgets_labels.contacts_select + '</p><button id="contact_list_emails_submit" class="btn"><span>' + widgets_labels.next + '</span></button></div></div>';
							container.empty().append(contactListForm);
							
							$.each(data, function(email,name) {
									var emailSelector = '<li>' + '<input class="contact-item-checkbox" type="checkbox"  name="address-book-email" value="' + email + '">'  + email + '</li> ';
									$(emailSelector).appendTo("#email_listing_id");
							});
							
							$('#contact_list_emails_submit').bind("click", function (e) {
								e.preventDefault();
								var emails = [],
									emailInput = $("#email_listing_id input"),
									html = '';
								
								emailInput.each(function (index, item) {
									if($(this).is(":checked")){
										selected = $(this).val();
										emails.push(selected);
									}
								});
								
								if (emails.length === 0) {
									alert(widgets_labels.no_emails);
									return;
								} else {
									html += '<h4>' + widgets_labels.share_title + '</h4><div id="widget-share-full"><p>' + widgets_labels.share_select + '</p><ul id="widget-share-email-list">';
									for ( var i = 0; i < emails.length; i++ ) {
										html += '<li class="clearfix"><div class="email-address-share">' + emails[i] + '</div><div class="email-address-rights">';
										html += '<button class="button-rights no buttonset-state-active">' + widgets_labels.view_only + '</button>';
										if (permission == "self") {
											html += '<button class="button-rights yes">' + widgets_labels.view_edit + '</button>';
										}
										html += '</div>';
										html += '<input type="hidden" class="email-address" id="email_address_' + i + '" name="email_address_' + i + '" value="' + emails[i] + '" /><input type="hidden" class="email-rights" id="email_rights_' + i + '" name="email_rights_' + i + '" value="0" /></li>';
									}
									html += '<li class="buttons clearfix"><button class="btn email-share-rights-submit"><span>' + widgets_labels.share + '</span></button></li></ul></div>';
									container.empty().append(html);
								}
								
								widgetShare.buttons.init(widgetId);
								return false;
							});
						}
					
					},
					
					manual : function (widgetId) {
						var s = phwidgets.selectors(),
							o = phwidgets.options,
							p = phwidgets.pages,
							widgetShare = phwidgets.page.widgetLibrary.widgetShare,
							textbox = $("#widget_share_emails_textarea"),
							container = $("#share-widget-dialog").find("div.widget-share"),
							permission = $("#"+widgetId).data("permission"),
							html = '';
							
						var emailInput = textbox.val(),
							emailStr = emailInput.substring(0, emailInput.length),
							emails = emailStr.split("\n");
						
						if (emailStr == '') {
							alert(widgets_labels.no_emails);
							return;
						} else {
							html += '<h4>' + widgets_labels.share_title + '</h4><div id="widget-share-full"><p>' + widgets_labels.share_select +'</p><ul id="widget-share-email-list">';
							for ( var i = 0; i < emails.length; i++ ) {
								html += '<li class="clearfix"><div class="email-address-share">' + emails[i] + '</div><div class="email-address-rights">';
								html += '<button class="button-rights no buttonset-state-active">'+ widgets_labels.view_only +'</button>';
								if (permission == "self") {
									html += '<button class="button-rights yes">' + widgets_labels.view_edit + '</button>';
								}
								html += '</div><input type="hidden" class="email-address" id="email_address_' + i + '" name="email_address_' + i + '" value="' + emails[i] + '" /><input type="hidden" class="email-rights" id="email_rights_' + i + '" name="email_rights_' + i + '" value="0" /></li>';
							}
							html += '<li class="buttons clearfix"><button class="btn email-share-rights-submit"><span>' + widgets_labels.share + '</span></button></li></ul></div>';
							container.empty().append(html);
						}
						
						widgetShare.buttons.init(widgetId);    				
						return false;                    
					},
				
					buttons : {
					
						init : function(widgetId) {
							this.rights();
							this.submit(widgetId);
						},
						
						rights : function() {
							var buttonRights = $("button.button-rights");
							
							buttonRights.live("click", function () {
								var li = $(this).parents("li"),
									inputRights = li.find("input.email-rights");
								if ($(this).is(".yes")) {
									inputRights.val("1");
								} else {
									inputRights.val("0");
								}
								$(this).addClass("buttonset-state-active");
								$(this).siblings().removeClass("buttonset-state-active");
								return false;
							});
							
							buttonRights.hover(
								function () {
									$(this).addClass("buttonset-state-hover");
								},
								function () {
									$(this).removeClass("buttonset-state-hover");
								}
							);
							
						},
						
						submit : function (widgetId) {
							var buttonSubmit = $("button.btn.email-share-rights-submit");
							buttonSubmit.live("click", function(e) {
								var list = $("#widget-share-full").find("li:not(:last)"),
									json = '',
									widgetId = $("#share-widget-dialog").data("widgetId");
								json += '{ "widgetId" : "' + widgetId + '", "Emails" : [';
								list.each(function(index, item) {
									var email = $(this).find("input.email-address").val();
									var rights = $(this).find("input.email-rights").val();
									if ( index != (list.length -1) ) {
										json += '{ "email" : "'+ email +'", "can_edit" : ' + rights+ ' },';
									} else {
										json += '{ "email" : "'+ email +'", "can_edit" : ' + rights+ ' }';
									}
								});
								json += ']}';
								
								phwidgets.saveShareWidgetData(json);
								e.preventDefault();
								return false;;
							});
						}
					
					}
				}
			},
			
			homepage : {
				
				init: function () {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.homepage)){
						this.ie6Message();
						phwidgets.ui.forms.init();
						phwidgets.ux.homepage();
						s.widgets.corner(o.roundedCorners);
						s.widgets.find("a").bind("click", function(){
							window.open(this.href);
							return false;
						});
					}
					
				},
				
				columns : function () {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages,
						brideHeight = 0,
						groomHeight = 0;
					s.widgetWrappers.css("height","auto");
					s.widgetWrappers.each(function(i) {
						var brideHeight = $(this).height();
						if ( brideHeight > groomHeight ) {
							groomHeight = brideHeight;
						}
					});
					$("#container").attr("style","height:"+groomHeight);
				},
				
				tabs : {
					
					init : function () {
						//this.removed();
					},
					
					removed : function (tab) {
						var nav = $("#tab-navigation li."+tab);
						var content = $("#tab-wrapper #"+tab);
						nav.empty().remove();
						content.empty().remove();
						
						var s = phwidgets.selectors(),
							o = phwidgets.options,
							p = phwidgets.pages,
							existingTabs = s.tabNav.find("li").not(".add-tab, .add-widget"),
							existingAnchors = existingTabs.find("a"),
							existingDivs = s.tabWrapper.children("div").not("#add-tab, #add-widget");
						
						for ( var i = 0; i < existingTabs.length; i++ ) {
							var num = i +1;
							var tabId = 'tab-' + num + ' ';
							$(existingTabs[i])[0].className = $(existingTabs[i])[0].className.replace(/\btab-[0-9]\s?\b/g, tabId);	
						}
						for ( var i = 0; i < existingAnchors.length; i++ ) {
							var num = i +1;
							var tabId = '#tab-' + num;
							$(existingAnchors[i]).attr("href",tabId);
						}
						for ( var i = 0; i < existingDivs.length; i++ ) {
							var num = i +1;
							var tabId = 'tab-' + num;
							$(existingDivs[i]).attr("id",tabId);
						}
					}
				} ,
				
				ie6Message : function (){
					var body = $("body"),
						s = phwidgets.selectors(),
							o = phwidgets.options,
							p = phwidgets.pages,
							msg = '<div id="ie-message"><div class="wrapper clearfix">' + widgets_labels.ie6 + '</div></div>';
						
					if(typeof document.body.style.maxHeight === undefined) { // If IE6
						body.prepend(msg);
					}
					
				}
			}
		},
		
		ui : {
		
			tab : {
				
				init : function (el) {
					var s = phwidgets.selectors();
					var o = phwidgets.options;
					var tabOpts = {
						tabTemplate: '<li><a href="#{href}"><span class="btn-edit-tab">#{label}</span></a> <span class="tab-close ui-icon ui-icon-close">' + widgets_labels.tab_remove + '</span></li>',
						spinner: widgets_labels.retrieving,
						cache: false,
						add: function(event, ui) {
							var tab_content =  '<ul id="" class="column-1 widget-wrapper"></ul><ul id="" class="column-2 widget-wrapper"></ul><ul id="" class="column-3 widget-wrapper"></ul>';
							$(ui.panel).append(tab_content);
						},
						select: function (event,ui) {
							var selectedTabId = ui.panel.id;
							var editing = $("a.editing","#tab-navigation").length;
							if (selectedTabId == 'add-tab')
							{
								phwidgets.ui.forms.addTab();
								return false;
							}
							else if (selectedTabId == 'add-widget')
							{
								var link = '/widgets/library.php';
								window.location.href = link;
								return false;
							}
							else if (editing)
							{
								return false;
							}
							else
							{
								var selectedTab = ui.panel.id;
								var selectedTabId =  selectedTab.replace("tab-",'');
								phwidgets.getData.widget('action=get&type=widgets', phwidgets.outputData.widget, selectedTab);
							}
							var focusTab = $('li.tab-' + selectedTabId);
							focusTab.siblings().removeClass("ui-state-hover ui-state-focus");
						},
						show: function (event, ui) {
							var s = phwidgets.selectors(),
								o = phwidgets.options,
								p = phwidgets.pages;
								
							s.tabWrapper.attr("style","height:auto");
						}
						
					};
					
					var $tabs = el.tabs(tabOpts);

				},
				
				rename : {
					
					init : function (obj) {
						if (phwidgets.helper.checkUser(widgets_labels.tab_login) === false) 
							return;
						var original = $(obj).html();
						var input = '<div class="editable" ><form id="rename_tab_form"><input type="text" id="new_tab_name" value="' + original + '" name="new_tab_name" maxlength="20" /><button id="saveRename" class="btn"><span>' + widgets_labels.save + '</span></button><button id="cancelRename" class="btn"><span>' + widgets_labels.cancel + '</span></button></form></div>';
						$(obj).after(input).remove();
						
						$("div.editable", "#tab-navigation").closest("a").addClass("editing");
						
						$("#new_tab_name").bind("focus", function () {
							this.select();
							phwidgets.validation.renameTab.init();
						}).focus();

						$("#saveRename").bind("click", function (e) {
							$("#rename_tab_form").submit();
							return false;
						});
						
						$("#rename_tab_form").bind("submit", function(e) {
							if ($(this).valid()) {
								phwidgets.ui.tab.rename.process(this, true, $("#new_tab_name").val());
							}
							return false;
						});
						
						$("#cancelRename").bind("click", function (e) {
							phwidgets.ui.tab.rename.process(this, false, original);
						});
						
						$("body").bind("click", function (e) {
							if(e.target.id != "cancelRename") {
								phwidgets.ui.tab.rename.process($("#cancelRename"), false, original);
							}
						});
					},
					
					process : function (obj, action, val) {
						var name = '';
						var currenttab = $(obj).parents("a").attr("href").replace("#","");
						if(action) {
							var params =  'tabId='+ currenttab +'&newName=' + val;
							phwidgets.saveData('renameTab', params);
						} else {
							name = val;
						}

						if (val == '') {
							val = 'Untitled';
						}
						
						$(obj).parents("div.editable").after('<span class="btn-edit-tab">'+ val +'</span>').remove();
						$("span.btn-edit-tab","#tab-navigation").closest("a").removeClass("editing");
						$("#saveRename, #cancelRename, body").unbind();
					}
				}
			},
			
			widget : function (el) {
				var s = phwidgets.selectors();
				var sortOpts = {
					connectWith: '.widget-wrapper',
					handle: '.widget-header',
					items: '.widget',
					tolerance: 'pointer',
					forcePlaceholderSize: 'true',
					sort: phwidgets.page.homepage.columns,
					receive: function (event, ui) {
						var currentTab = $("li.ui-tabs-selected",s.tabWrapper).children().attr("href");
						var t = phwidgets.helper.serialize(currentTab);
						var d = phwidgets.helper.toJson(currentTab, t);
						currentTab = currentTab.replace('#',"");                        
						var params =  'tabId='+ currentTab +'&data=' + d;
						phwidgets.saveData('save', params);
					},
					stop: function (event, ui) {
						var currentTab = $("li.ui-tabs-selected",s.tabWrapper).children().attr("href");
						var t = phwidgets.helper.serialize(currentTab);
						var d = phwidgets.helper.toJson(currentTab, t);
						currentTab = currentTab.replace('#',"");                        
						var params =  'tabId='+ currentTab +'&data=' + d;
						phwidgets.saveData('save', params);
					}
				};
				
				var $widgets = el.sortable(sortOpts);
			},
			
			droppable : function (el) {
				if (phwidgets.helper.checkUser() === false) 
				return;
				var s = phwidgets.selectors();
				var dropOpts = {
					accept: '.widget',
					hoverClass: 'ui-state-hover',
					tolerance: 'pointer',
					greedy: 'true',
					drop: function(event, ui) {
						var $item = $(this);
						var $list = $($item.find('a').attr('href')).find('.widget-wrapper:first');
						
						var $container = s.tabWrapper;
						var $alltabs = s.tabWrapper.tabs();
						var $tab_items = $("#tab-navigation li",$alltabs);

						var targetTabId = $item.find('a').attr('href').replace('#','');
						var selectedWidgetId = ui.draggable.attr("id");

						var selectedWidget = ui.draggable;
						currentTabId = selectedWidget.parent().parent().attr("id");

						ui.draggable.fadeOut(500, function(){
							$(this).empty();
						}).remove();
						phwidgets.page.homepage.columns();
						
						var currentTab = $("li.ui-tabs-selected",s.tabWrapper).children().attr("href");
						var t = phwidgets.helper.serialize(currentTab);
						var d = phwidgets.helper.toJson(currentTab, t);
						currentTab = currentTab.replace('#',"");   
						var save =  'tabId='+ currentTab +'&data=' + d;
						phwidgets.saveData('save', save, updateTab);

						function updateTab(){
							var update =   'widgetId=' +  selectedWidgetId + '&targetTabId=' + targetTabId;
							phwidgets.saveData('updatetabwidgetplacement', update);
						}
					}
				};
				
				var $droppable = el.droppable(dropOpts);
			},
			
			dialog : function () {
				var s = phwidgets.selectors(),
					titleField = $("#tab_title"),
					allFields = $([]).add(titleField);
					
				var dialogOpts = {
					autoOpen: false,
					modal: true,
					open: function() {
						//s.addTabForm.find('#save_order').focus();
					},
					close: function() {
						titleField.parents("li").removeClass("error validation");
						titleField.siblings(".error.validation").empty().remove();
						allFields.val('').removeClass('ui-state-error');
						phwidgets.ux.helper.resetForm('#add-tab-form');
					}
				};
				
				function addTab(){
					var s = phwidgets.selectors();
					var o = phwidgets.options;
					
					var totalTab = $("#tab-navigation li").length + 1;
					var tabCounter = totalTab - 2;
					if (tabCounter <= 0) {
						tabCounter = '1';
					}
					var tabTitle = s.addTabDialog.find("input").val() || 'Tab '+tabCounter;
					
					var tabIndex = $("#tab-wrapper").tabs('length') -2;
					
					$("#tab-wrapper").tabs('add', '#tab-' + tabCounter, tabTitle, tabIndex);
					tabCounter++;
					
					
					$.ajax({
						url : o.protocol + o.serverPath + o.servicePath + 'action=addtab', 
						type : 'POST',
						data : 'tabName=' + tabTitle + '&tabIndex=' + tabIndex,
						success : function (event, ui)
						{
							// console.log("Data has been saved");
						}
					});
				}
				
				var $dialog = s.addTabDialog.dialog(dialogOpts);
				
				$("#add-tab-dialog").bind("submit", function(){
					addTab();
					s.addTabDialog.dialog("close");
					return false;
				});
			},
			
			forms : {
			
				init : function () {
					var s = phwidgets.selectors();
					var o = phwidgets.options;
					var abuseContents = '<div id="report-abuse-dialog" title="' + widgets_labels.report_title + '" class="ui-helper-hidden"> <form onSubmit="return false" class="dialog-form"> <fieldset class="ui-helper-reset "> <div id="report-message"> <p>' + widgets_labels.report_welcome + '</p> </div> <ul class="form-inputs"><li class="clearfix"> <label for="report_comments">' + widgets_labels.comments + '</label> <textarea id="report_comments" name="report_comments"></textarea> </li></ul><button id="reportAbuseSubmit" type="submit" style="display:none"></button> </fieldset> </form> </div> ';
					var shareWidget = '<div id="share-widget-dialog" class="ui-helper-hidden" title="' + widgets_labels.share_title + '"> <form id="" class="dialog-form"> <fieldset class="ui-helper-reset"><div class="widget-share background clearfix">';
					if(widgets_openinviter) {
						shareWidget += '<div id="widget-share-addressbook"> <h5>Share with people in your addressbook</h5> <p>Enter your email and password to get your addressbook.</p> <ul class="form-inputs"> <li class="clearfix"> <label for="widget_share_username">Username</label> <input type="text" name="widget_share_username" id="widget_share_username" value="" /> </li> <li class="clearfix"> <label for="widget_share_password">Password</label> <input type="password" name="widget_share_password" id="widget_share_password" value="" /> </li> <li class="buttons"> <button type="submit" class="btn" id="widget_share_addressbook_submit"><span>Get My Contacts</span></button> </li> </ul> </div>';
					}
					shareWidget += '<div id="widget-share-email"> <h5>Enter email addresses</h5> <p>If you know your friend\'s email, enter them below and click the Share button</p> <ul class="form-inputs"> <li class="clearfix"> <textarea id="widget_share_emails_textarea"></textarea> </li> <li class="buttons"> <button id="widget_share_email_submit" class="btn"><span>' + widgets_labels.share + '</span></button> </li> </ul> </div> </div> </fieldset> </form> </div> ';
					var addTabForm = '<form id="add-tab-dialog" title="' + widgets_labels.tab_add + '" class="ui-helper-hidden"> <fieldset class="ui-helper-reset "><ul class="form-inputs"><li class="clearfix"><label for="tab_title">' + widgets_labels.title + '</label> <input type="text" name="tab_title" id="tab_title" value="" /></li><li class="buttons clearfix"><button class="btn" id="add-tab-submit"><span>' + widgets_labels.create + '</span></button></ul></fieldset> </form>';
					var removeWidget = '<div id="remove-widget-confirmation" title="' + widgets_labels.widget_remove + '" class="ui-helper-hidden"> <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' + widgets_labels.widget_remove_question + '</p></div> ';
					var removeTab = '<div id="remove-tab-confirmation" title="' + widgets_labels.tab_remove + '" class="ui-helper-hidden"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' + widgets_labels.tab_remove_question + '</p></div> ';

					$(abuseContents).appendTo(s.content);
					$(shareWidget).appendTo(s.content);
					$(addTabForm).appendTo(s.content);
					$(removeWidget).appendTo(s.content);
					$(removeTab).appendTo(s.content);
				},
				
				abuse : function (id, num) {
					var reportMessage = $("#report-message");
					var idField = $("#report_id");
					var emailField = $("#report_email");
					var commentsField = $("#report_comments");
					var allFields = $([]).add(idField).add(emailField).add(commentsField);
					var originalForm = $("#report-abuse-dialog").html();
					var abuseDlgForm = $('#report-abuse-dialog form') ;
					var abuseButtons = {};
					abuseButtons[widgets_labels.report_submit] = function() {
						abuseDlgForm.trigger('submit');
					}; 
					abuseButtons[widgets_labels.cancel] = function() {
						$(this).dialog('close');
					}; 
					
					$("#report-abuse-dialog").dialog({
						autoOpen: false,
						height: 300,
						width: 380,
						hide : 'puff',
						modal: true,
						draggable : false,
						resizable: false,
						buttons: abuseButtons,
						open : function() {
							idField.val('').val(id);
						},                        
						close : function() {
							allFields.val('').removeClass('ui-state-error');
							$("#report-abuse-dialog form").remove().empty();
							$("#report-abuse-dialog").append(originalForm);
							phwidgets.validation.reportAbuse.init();
						}
					});
					
					$("#report-abuse-dialog").dialog("open");
					$('#report_email').focus();
					
					$("#reportAbuseSubmit").bind("submit", function (){
						var comments = $("#report-abuse-dialog #report_comments").val();
						var params = 'widgetId=' + id + '&report_comments=' + comments;
						phwidgets.saveData('reportabuse', params, phwidgets.ui.forms.abuseCallBack); 
						return false;
					});
				},
				
				abuseCallBack : function (params, status, error){
					var successContents = '<form id="" class="dialog-form"><fieldset class="ui-helper-reset "><p>' + widgets_labels.sent + '</p></fieldset></form>';
					
					if (!status)
						successContents = '<form id="" class="dialog-form"><fieldset class="ui-helper-reset ">' + widgets_labels.report_error + '</fieldset></form>';
					
					$("#report-abuse-dialog form").remove().empty();
					$("#report-abuse-dialog").append(successContents); 
						
					$("#report-abuse-dialog").dialog("option", "buttons", { "Close": function() { $(this).dialog("close"); } } ); 
					$("#report-abuse-dialog").dialog("option", "height", 130 );                    
				},

				share : function (widgetId) {
					var shareWidgetMessage = $("#share-widget-message");
					var originalContent = $("#share-widget-dialog").html();
					var shareDialog = $("#share-widget-dialog").dialog({
						autoOpen: false,
						height: 'auto',
						width: widgets_openinviter? 800 : 430,
						hide : 'puff',
						modal: true,
						draggable: false,
						resizable: false,
						open : function() {
							xhrStatus = false;
						},
						close : function() {
							var contactListForm  = '<form id=""><h4>' + widgets_labels.contact_lists + '</h4><div id="widget-share-addressbook"> <h5>Here are your contact list from you email address book</h5> <ul class="contact-lists clearfix"> </ul> </div> <div id="widget-share-email"> <h5>Your selected email addresses</h5> <p>Now that you are in your contact list, all you need to do is click on the checkbox next to your contact. When you\'re selecting them, your contact email addresses will appear below. Hit the Add button when you are done.</p> <ul class="form-inputs"> <li class="clearfix"> <textarea></textarea> </li> <li class="buttons"> <button id="contact_list_emails_submit" class="btn"><span>' + widgets_labels.add + '</span></button> </li> </ul> </div> </form>';
							$("#share-widget-dialog form").empty().remove();
							$("#share-widget-dialog p").empty().remove();
							$("#share-widget-dialog").empty().append(originalContent);
							
							if ( xhrStatus === true ) {
								shareDialog.trigger("cancelXhr");
							}
						}
					});
					
					$("#widget_share_addressbook_submit").bind("click", function(e){
						var emailAddress = $("#widget_share_username").val();
						var password  = $("#widget_share_password").val();
						var widgetShare = phwidgets.page.widgetLibrary.widgetShare;
						xhrStatus = true;
						widgetShare.addressbook.process(emailAddress, password, widgetId, widgetShare.addressbook.output);
						e.preventDefault();
					});
					
					$("#widget_share_email_submit").bind("click", function(e){
						var widgetShare = phwidgets.page.widgetLibrary.widgetShare;
						widgetShare.manual(widgetId);
						e.preventDefault();
					});

					$("#share-widget-dialog").dialog("open");
					
					$("#share-widget-dialog").bind("cancelXhr", function(){
						if ( xhrRequest !== undefined ) {
							xhrRequest.abort();
							xhrStatus = false;
							xhrRequest = undefined;
						}
						$("#share-widget-dialog").empty().append(originalContent);
					});
				},
				
				addTab : function () {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						titleField = $("#tab_title"),
						addTabDialog = $("#add-tab-dialog"),
						submit = $("#add-tab-submit"),
						allFields = $([]).add(titleField);
					
					addTabDialog.dialog({
						autoOpen: false,
						width: 380,
						hide : 'puff',
						modal: true,
						draggable : false,
						resizable: false,
						open : function() {
							btn = $(this).parent().find(".ui-state-focus");
							btn.removeClass("ui-state-focus");
						},
						close : function() {
							titleField.parents("li").removeClass("error validation");
							titleField.siblings(".error.validation").empty().remove();
							$("#tab-navigation li.add-tab").removeClass("ui-state-focus");
							allFields.val('').removeClass('ui-state-error');
							phwidgets.ux.helper.resetForm('#add-tab-form');
							submit.unbind("click");
						}
					});
					
					addTabDialog.dialog("open");
					titleField.focus();
					submit.bind("click", function (e){
						e.preventDefault();
						e.stopPropagation();
						if (addTabDialog.valid()){
							
							s.addTabDialog.dialog("close");
							process();
							//$("#tab-navigation li.add-tab").removeClass("ui-state-focus");
						}
						return false;
					});
					
					function process(){				
						
						var totalTab = $("#tab-navigation li").length + 1;
						var tabCounter = totalTab - 2;
						if (tabCounter <= 0) {
							tabCounter = '1';
						}
						var tabTitle = s.addTabDialog.find("input").val() || 'Tab '+tabCounter;
						var tabIndex = $("#tab-wrapper").tabs('length') -2;
						
						$.ajax({
							url : o.protocol + o.serverPath + o.servicePath + 'action=addtab', 
							type : 'POST',
							data : 'tabName=' + tabTitle + '&tabIndex=' + tabIndex,
							success : function (event, ui) 
							{
								submit.unbind("click");
								$("#tab-wrapper").tabs('add', '#tab-' + tabCounter, tabTitle, tabIndex);
								$("#tab-navigation li").eq(-3).addClass('tab-' + tabCounter);
								phwidgets.ui.droppable(s.tabNav.children('li').not('li.add-tab, li.add-widget'));
								tabCounter++;
								return false;
							}
						});
						
					}
					
				}
				
			}
			
		},
		
		validation : {
		
			init : function () {
				this.newRssWidget.init();
				this.newManualWidget.init();
				this.newAdvanceManualWidget.init();
				this.editRssWidget.init();
				this.editManualWidget.init();
				this.editAdvanceManualWidget.init();
				this.addTab.init();
				//this.renameTab.init();
				this.reportAbuse.init();
			},
			
			addTab : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if(s.body.is(p.homepage)) {
						this.validate();
					}
				},
				
				validate : function () {
					$("#add-tab-dialog").validate({
						errorClass : "error validation",
						success : "valid",
						rules : {
							tab_title : {
								required : true,
								maxlength : 20
							}
						},
						errorElement : "div",
						errorPlacement : function (error, element) {
							var nextSibling  = element.siblings("div.error");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == '') {
								error.insertAfter(element); 
							}
						},
						submitHandler: function(form) {
							$("#add-tab-submit").click();
							return false;
						} 
					});
				}
			
			},
			
			renameTab : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if(s.body.is(p.homepage)) {
						this.validate();
					}
				},
				
				validate : function () {
					$("#rename_tab_form").validate({
						errorClass : "error validation",
						success : "valid",
						rules : {
							new_tab_name : {
								required : true,
								maxlength : 20
							}
						},
						errorElement : "div",
						errorPlacement : function (error, element) {
							var nextSibling  = element.siblings("div.error"),
								counter = 1;
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == '') {
								error.insertAfter(element).wrap("<div class='error-wrapper'></div>").parent().append("<div class='error-pointer'></div>");
								$("div.error-wrapper").eq(0).siblings("div.error-wrapper").remove();
								//error.insertAfter(element);
								return false;
							}
						},
						unhighlight : function() {
							$("div.error-wrapper").remove();
						},
						submitHandler: function(form) {
							//$("#saveRename").click();
							//form.submit();
							return false;
						} 
					});
				}
			},
			
			newRssWidget : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.widgetCreate)) {
						this.validate();
					}
				},
				
				validate : function() {
					$("#create-rss").validate({
						errorClass : "error validation",
						success : "valid",
						
						rules : {
							'widget_rss_title' : {
									required : true,
									maxlength : 50
							},
							'widget_rss_desc' : {
									maxlength : 100
							}
						},
						submitHandler: function(form) {
							form.submit();
							$(this.submitButton).attr("disabled", true);
						},
						
						errorElement: "div",
						errorPlacement : function(error, element) {
							var nextSibling  = element.next(".validation");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == ''){
								error.insertAfter(element); 
							} else {
								return false;
							}
						}
					});
				}
			
			},
			
			newManualWidget : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.widgetCreate)) {
						this.validate();
					}
				},
				
				validate : function() {
					$("#create-manual").validate({
						errorClass : "error validation",
						success : "valid",
						
						rules : {
							'widget_manual_title' : {
									required : true,
									maxlength : 50
							},
							'widget_manual_desc' : {
									maxlength : 100
							},
							'widget_manual_content' : {
									maxlength : 255
							}
						},
						submitHandler: function(form) {
							form.submit();
							$(this.submitButton).attr("disabled", true);
						},
						
						errorElement: "div",
						errorPlacement : function(error, element) {
							var nextSibling  = element.next(".validation");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == ''){
								error.insertAfter(element); 
							}
						}
					});
				}
				
			},
			
			newAdvanceManualWidget : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.widgetCreate)) {
						this.validate();
					}
				},
				
				validate : function() {
					$("#create-advance-manual").validate({
						errorClass : "error validation",
						success : "valid",
						
						rules : {
							'widget_advance_manual_title' : {
									required : true,
									maxlength : 50
							},
							'widget_advance_manual_desc' : {
									maxlength : 100
							},
							'widget_advance_manual_content' : {
									maxlength : 255
							}
						},
						submitHandler: function(form) {
							form.submit();
							$(this.submitButton).attr("disabled", true);
						},
						
						errorElement: "div",
						errorPlacement : function(error, element) {
							var nextSibling  = element.next(".validation");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == ''){
								error.insertAfter(element); 
							}
						}
					});
				}
				
			},
			
			editRssWidget : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.widgetEdit)) {
						this.validate();
					}
				},
				
				validate : function() {
					$("#edit-rss").validate({
						errorClass : "error validation",
						success : "valid",
						
						rules : {
							'widget_rss_title' : {
									required : true,
									maxlength : 50
							},
							'widget_rss_desc' : {
									maxlength : 100
							}
						},
						submitHandler: function(form) {
							form.submit();
							return false;
						},
						
						errorElement: "div",
						errorPlacement : function(error, element) {
							var nextSibling  = element.next(".validation");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == ''){
								error.insertAfter(element); 
							} else {
								return false;
							}
						}
					});
				}
			
			},
			
			editManualWidget : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.widgetEdit)) {
						this.validate();
					}
				},
				
				validate : function() {
					$("#edit-manual").validate({
						errorClass : "error validation",
						success : "valid",
						
						rules : {
							'widget_manual_title' : {
									required : true,
									maxlength : 50
							},
							'widget_manual_desc' : {
									maxlength : 100
							},
							'widget_manual_content' : {
									maxlength : 255
							}
						},
						submitHandler: function(form) {
							form.submit();
							return false;
						},
						
						errorElement: "div",
						errorPlacement : function(error, element) {
							var nextSibling  = element.next(".validation");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == ''){
								error.insertAfter(element); 
							}
						}
					});
				}
				
			},
			
			editAdvanceManualWidget : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.widgetEdit)) {
						this.validate();
					}
				},
				
				validate : function() {
					$("#edit-advance-manual").validate({
						errorClass : "error validation",
						success : "valid",
						
						rules : {
							'widget_advance_manual_title' : {
									required : true,
									maxlength : 50
							},
							'widget_advance_manual_desc' : {
									maxlength : 100
							},
							'widget_advance_manual_content' : {
									maxlength : 255
							}
						},
						submitHandler: function(form) {
							form.submit();
							return false;
						},
						
						errorElement: "div",
						errorPlacement : function(error, element) {
							var nextSibling  = element.next(".validation");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == ''){
								error.insertAfter(element); 
							}
						}
					});
				}
				
			},
			
			reportAbuse : {
			
				init : function() {
					var s = phwidgets.selectors(),
						o = phwidgets.options,
						p = phwidgets.pages;
					
					if (s.body.is(p.homepage)) {
						this.validate();
					}
				},
				
				validate : function() {
					
					$('#report-abuse-dialog form').validate({
						errorClass : "error validation",
						success : "valid",

						rules : {
							'report_email' : {
									required : true,
									email : true
							},
							'report_comments' : {
									required : true
							}
						},
						submitHandler: function(form) {
							$("#reportAbuseSubmit").trigger("submit");
						},
						
						errorElement: "div",
						errorPlacement : function(error, element) {
							var nextSibling  = element.siblings("div.error");
							nextSibling.remove().empty();
							var hasErrorElement = nextSibling.val();
							if (hasErrorElement === undefined|| hasErrorElement == ''){
								error.insertAfter(element); 
							}
						}

					});
				}
			
			}
		
		},
		
		ux : {
			
			init : function () {
				
			},
			
			homepage : function () {

				var s = phwidgets.selectors();
				function removeWidgetConfirmation (obj) {
					var removeWidgetConfirmation = $("#remove-widget-confirmation").dialog({
						resizable: false,
						draggable: false,
						height:140,
						modal: true,
						autoOpen: false,
						open: function(event, ui) {
							closeBtn = $(obj).siblings("div.ui-dialog-titlebar").find("a.ui-dialog-titlebar-close");
							btn = $(obj).parent().find(".ui-state-focus");
							btn.removeClass("ui-state-focus");
						},
						buttons: {
							'Remove widget': function() {
								var parent = $(obj).parents("li.widget");
								parent.empty().remove();
								phwidgets.page.homepage.columns();
								var currentTab = $("li.ui-tabs-selected",s.tabWrapper).children().attr("href"), t = phwidgets.helper.serialize(currentTab), d = phwidgets.helper.toJson(currentTab, t);
								currentTab = currentTab.replace('#',"");
								var params =  'tabId='+ currentTab +'&data=' + d;
								phwidgets.saveData('save', params);
								$(this).dialog('close');
							},
							Cancel: function() {
								$(this).dialog('close');
							}
						}
					});
					removeWidgetConfirmation.dialog("open");
				}

				
				$("li.ui-tabs-selected span.btn-edit-tab", "#tab-navigation").livequery("click", function() {
					phwidgets.ui.tab.rename.init(this);
				}).live("mouseover", function() {
					$(this).css("background", "#666");
				}).live("mouseout", function() {
					$(this).css("background","");
				});
				
				s.tabClose.live("click", function(e) {
					var parent = $(this).parent();
					var currentTab = $(this).prev().attr("href").replace("#","");
					var removeTabConfirmation = $("#remove-tab-confirmation").dialog({
						resizable: false,
						draggable: false,
						height:140,
						modal: true,
						autoOpen: false,
						open: function(event, ui) {
							closeBtn = $(this).siblings("div.ui-dialog-titlebar").find("a.ui-dialog-titlebar-close");
							btn = $(this).parent().find(".ui-state-focus");
							btn.removeClass("ui-state-focus");
						},
						buttons: {
							'Remove tab': function() {
								if(parent.is(".ui-tabs-selected")) {
									return false;
								} else {
									var params =  'tabId='+ currentTab;
									phwidgets.saveData('closeTab', params, phwidgets.page.homepage.tabs.removed(currentTab));
								}
								
								$(this).dialog('close');
							},
							Cancel: function() {
								$(this).dialog('close');
							}
						}
					});
					removeTabConfirmation.dialog("open");
				});
				
				s.widgetClose.live("click", function(e) {
					if (phwidgets.helper.checkUser() !== false) {
						removeWidgetConfirmation(this);
					} else {
						var parent = $(this).parents("li.widget");
						parent.html("");
						parent.remove();
						phwidgets.page.homepage.columns();
					}
					return false;
				});
				
				s.widgetToggle.live("click", function(e) {
					var content = $(this).parent().next();
					if ($(this).is(".ui-icon-circle-triangle-n")) {
						$(this).removeClass("ui-icon-circle-triangle-n").addClass("ui-icon-circle-triangle-s");
						content.slideUp("slow");
					} else {
						$(this).removeClass("ui-icon-circle-triangle-s").addClass("ui-icon-circle-triangle-n");
						content.slideDown("slow");
					}
				});
				
				s.widgetShare.live("click", function(e) {
					var widgetId = $(this).parents("li.widget").attr("id");
					phwidgets.ui.forms.share(widgetId);
					$("#share-widget-dialog").data("widgetId", widgetId);
					return false;
				});
				
				s.widgetReport.live("click", function(e) {
					var widgetId = $(this).parents("li.widget").attr("id");
					var widgetNum = widgetId.replace("widget-","");
					
					phwidgets.ui.forms.abuse(widgetId, widgetNum);                    
				});
				
			},
			
			helper : {
				
				resetForm : function (obj) {
					var input = $(obj).find("input, textarea");
					input.each(function(){
						this.reset();
					});
				}
			}
		},
		
		init : function () {
			
			$(document).ready(function() {
				var s = phwidgets.selectors(),
					o = phwidgets.options,
					p = phwidgets.pages;

				if ($("body").is(".homepage")){
					phwidgets.getData.init();
				}
			});
			
			$(window).load(function(){
				var s = phwidgets.selectors(),
					o = phwidgets.options,
					p = phwidgets.pages;
					
				phwidgets.page.init();
				phwidgets.validation.init();
			});
		}
		
	};
	
	$.fn.exists = function(){return jQuery(this).length>0;};
	$.fn.serializeObject = function(){var o = {};var a = this.serializeArray();$.each(a, function() {if (o[this.name]) {if (!o[this.name].push) {    o[this.name] = [o[this.name]];}o[this.name].push(this.value || '');} else {o[this.name] = this.value || '';}});return o;};
		
	phwidgets.init();

})(jQuery);
