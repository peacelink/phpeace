<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,0,false);
$get = $fh->HttpGet();
$l = new Layout;

$unescaped_get = $fh->HttpGet(false);

$id_topic = (int)$get['id_topic'];

$params = array();
$params['q'] = $unescaped_get['q'];
$params['k'] = $unescaped_get['k'];
$params['id_group'] = (int)$get['id_group'];
$params['id_template'] = (int)$get['id_template'];
$params['id_subtopic'] = (int)$get['id_subtopic'];
$page = (int)$get['p'];

$q_len = strlen($params['q']);
$k_len = strlen($params['k']);

$min_str_length = $l->conf->Get("min_str_length");

if(($q_len>0 && $q_len<$min_str_length) || ($k_len>0 && $k_len<$min_str_length))
	$fh->va->MessageSet("error","search_short", array($min_str_length));

echo $l->Output("search",0,$id_topic,$page,$params);
?>
