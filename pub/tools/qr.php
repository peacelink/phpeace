<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$fh->CheckClosure();

$get = $fh->HttpGet();

if (isset($get['hash'])) {
   $hash = strtolower($get['hash']);
   include_once(SERVER_ROOT."/../classes/qr.php");
   $qr = new QR();
   $code = $qr->QRCodeGetByHash($hash);
   if(isset($code['id_qrcode'])) {
      $qr->QRCodeIncrease($code['id_qrcode']);
      header("Location: {$code['url']}");
      exit();
   }
}
header('HTTP/1.0 404 Not Found');
exit();
?>
