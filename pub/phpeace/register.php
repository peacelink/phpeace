<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/phpeace.php");
$phpeace = new Phpeace;
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$id_language = (int)$get['id_language'];

include_once(SERVER_ROOT."/../classes/translator.php");
$trm17 = new Translator($id_language,17);

$page_title = $trm17->Translate("registration");

include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper();
header("Content-Type: " . $ah->content_header); 
echo $ah->DocType();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="<?=$ah->content_header;?>" >
<title><?=$page_title;?></title>
<style type="text/css" media="screen">
@import url(/phpeace/install.css);
</style>
</head>
<body>
<img id="logo" src="/logos/phpeace.png" alt="PhPeace Logo">
<h2><?=$page_title;?></h2>
<?php
if ($phpeace->main)
{
	$build = $get['b'];
	$install_key = $get['k'];
	$id_client = $get['id'];
	$admin_web = urldecode($get['admin']);

	if ($phpeace->KeyCheck($install_key))
	{
		$client = $phpeace->ClientCheck($install_key);
		if ( $client['id_client'] > 0 )
		{
			if ( $client['approved']=="0" )
				echo $trm17->Translate("registration_waiting");
			else
			{
				if ($id_client>0)
					echo $trm17->Translate("registration_successful");
				else
					echo $trm17->TranslateParams("registered",array($client['name']));
			}
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/htmlhelper.php");
			$hh = new HtmlHelper();
			$hh->tr = new Translator($id_language,0);
		?>
<script type="text/javascript">
function dosubmit()
{
	f = document.forms['form1'];
	boolOK = true;
	strAlert = "";
	if (f.name.value=="")
	{
		strAlert = strAlert + "<?=$hh->tr->Translate("missing_website");?>\n";
		boolOK = false;
	}
	if (f.email.value=="" || f.email.value.indexOf('@',0)==-1)
	{
		strAlert = strAlert + "<?=$hh->tr->Translate("missing_email");?>\n";
		boolOK = false;
	}
	if (!boolOK)
		alert(strAlert);
	return boolOK;
}
</script>

<p><?=$trm17->Translate("registration_form");?></p>
<form action="actions.php" method="post" name="form1" onsubmit="return dosubmit();">
<input type="hidden" name="from" value="register">
<input type="hidden" name="install_key" value="<?=$install_key;?>">
<input type="hidden" name="id_version" value="<?=$build;?>">
<input type="hidden" name="admin_web" value="<?=$admin_web;?>">
<input type="hidden" name="id_language" value="<?=$id_language;?>">
		<?php
			echo $hh->input_table_open();
			echo $hh->input_text("website_name","name","",20,0,1);
			echo $hh->input_text("contact_main","contact","",40,0,1);
			echo $hh->input_text("email","email","",40,0,1);
			echo $hh->input_submit("submit","",1);
			echo $hh->input_table_close();
			echo "</form>";
		}
	}
}
else
{
	echo $trm17->Translate("registration_refuse");
}

?>
</body>
</html>
