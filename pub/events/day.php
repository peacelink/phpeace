<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();
$l = new Layout;

$date = $get['d'];
$id_topic = (int)$get['id_topic'];

$params = array();
if ((int)$get['id_article']>0)
	$params['id_article'] = (int)$get['id_article'];

if(strlen($date)==8 && is_numeric($date))
{
	$year = (int)substr($date,0,4);
	$month = (int)substr($date,4,2);
	$day = (int)substr($date,6,2);
	if ($year>0 && $month>0 && $day>0 && checkdate($month,$day,$year))
	{
		$params['ts'] = mktime(0,0,0,$month,$day,$year);
		$params['subtype'] = "day";
		echo $l->Output("events",0,$id_topic,1,$params);
	}
}
?>
