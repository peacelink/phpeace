<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/events.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$allowed_formats = array('ical','json','xml');

$format = isset($get['format'])? $get['format'] : 'json';

if(in_array($format, $allowed_formats)) {
	$params = array();
	$params['format'] = $format;
	$allowed_types = array('event','prov','reg','all');
	$type = isset($get['type'])? $get['type'] : 'all';
	if(in_array($type, $allowed_types)) {
		if($type == 'event' && $format == 'ical') {
			$id = (int)$get['id'];
			if($id>0) {
				$ee = new Events();
				echo $ee->ExportIcsEvent($id);
			} else {
				http_response_code(401);
			}
		} else {
			switch($type) {
				case 'event':
					$id = (int)$get['id'];
					if($id>0) {
						$params['id'] = $id;
					} else {
						http_response_code(401);
						exit;
					}
				break;
				case 'prov':
					$id = $get['id'];
					if($id!='') {
						$params['prov'] = $id;
					} else {
						http_response_code(404);
						exit;
					}
				break;
				case 'reg':
					$id = $get['id'];
					if($id!='') {
						$params['reg'] = $id;
					} else {
						http_response_code(404);
					exit;
					}
				break;
				case 'all':
				break;
			}
			$er = new EventsREST();
			$er->arguments = $params;
			echo $er->Next();
		}
	} else {
		http_response_code(401);
	}
} else {
	http_response_code(401);
}
?>
