
# PhPeace

Portal Management System

http://www.phpeace.org

https://gitlab.com/peacelink/phpeace/

## AUTHORS

Francesco Iannuzzelli <francesco@phpeace.org>

## IDEAS & VISION

Alessandro Marescotti
Carlo Gubitosa
Francesco Iannuzzelli

## CREDITS

PeaceLink (http://www.peacelink.it)
Daniele Marescotti
Elisabetta Paje
Gabriele Garbillo
Giacomo Alessandroni
Lorenzo Salvadorini
Marco Trotta
David Corbett
Rizal Almashoor
Yassir Yahya
