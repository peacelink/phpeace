<?php

/**
 * Configuration settings for Ecommerce module
 * 
 * Append these settings in custom/config/php
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class ConfigEcommerce
{
	/**
	 * Index of images array for product thumb width
	 *
	 * @var integer
	 */
	public $product_thumb = 0;

	/**
	 * Index of images array for product image width
	 *
	 * @var integer
	 */
	public $product_image = 2;
	
	/**
	 * Enforce a specific amount of products per each order
	 * Set to 0 to leave it free
	 *
	 * @var integer
	 */
	public $products_per_order = 0;
	
	/**
	 * Email address to send notifications on each product purchase
	 *
	 * @var string
	 */
	public $notify = "";
	
	/**
	 * Currency code to use
	 *
	 * @var string
	 */
	public $currency = "GBP";
	
	/**
	 * Send confirmation email to buyer once order has been processed
	 *
	 * @var boolean
	 */
	public $send_order_confirm_mail = false;
	
	/**
	 * Store payments in the payments module
	 *
	 * @var boolean
	 */
	public $store_payments = true;
	
	/**
	 * Which payment gateway to use
	 * Possible values: ics_migs, migs_au
	 *
	 * @var string
	 */
	public $pgw;
	
}
?>
