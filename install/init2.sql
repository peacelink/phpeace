
-- Initial values: english
-- Put only language specific values in this script
-- Generic values should go in init.sql

UPDATE global SET map_path='map', gallery_path='galleries', events_path='events', search_path='search', lists_path='lists', books_path='books', campaign_path='campaign', poll_path='poll', forum_path='forum', org_path='orgs', quotes_path='quotes', users_path='users', media_path='media' WHERE id_install=1;

INSERT INTO event_types (id_event_type,type) VALUES (1,'Conference');
INSERT INTO event_types (id_event_type,type) VALUES (2,'Press conference');
INSERT INTO event_types (id_event_type,type) VALUES (3,'Course');
INSERT INTO event_types (id_event_type,type) VALUES (4,'Party');
INSERT INTO event_types (id_event_type,type) VALUES (5,'Concert');
INSERT INTO event_types (id_event_type,type) VALUES (6,'Exhibition');
INSERT INTO event_types (id_event_type,type) VALUES (7,'Meeting');
INSERT INTO event_types (id_event_type,type) VALUES (8,'Rally');

INSERT INTO admin_text (id_text,title,description,active) VALUES (1,'Warning','',0) 

