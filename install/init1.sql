
-- Initial values: italian
-- Put only language specific values in this script
-- Generic values should go in init.sql

UPDATE global SET map_path='mappa', gallery_path='gallerie', events_path='calendario', search_path='cerca', lists_path='liste', books_path='libri', campaign_path='campagne', poll_path='sondaggi', forum_path='forum', org_path='associaz', quotes_path='aforismi', users_path='utenti', media_path='media' WHERE id_install=1;

INSERT INTO event_types (id_event_type,type) VALUES (1,'Conferenza');
INSERT INTO event_types (id_event_type,type) VALUES (2,'Conferenza stampa');
INSERT INTO event_types (id_event_type,type) VALUES (3,'Convegno');
INSERT INTO event_types (id_event_type,type) VALUES (4,'Corso');
INSERT INTO event_types (id_event_type,type) VALUES (5,'Festa');
INSERT INTO event_types (id_event_type,type) VALUES (6,'Fiera');
INSERT INTO event_types (id_event_type,type) VALUES (7,'Incontro');
INSERT INTO event_types (id_event_type,type) VALUES (8,'Manifestazione');
INSERT INTO event_types (id_event_type,type) VALUES (9,'Mostra fotografica');
INSERT INTO event_types (id_event_type,type) VALUES (10,'Presentazione');
INSERT INTO event_types (id_event_type,type) VALUES (11,'Seminario');
INSERT INTO event_types (id_event_type,type) VALUES (12,'Tavola rotonda');

INSERT INTO admin_text (id_text,title,description,active) VALUES (1,'Avviso','',0) 