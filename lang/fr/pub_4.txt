already_signed = Vous avez déjà signé
already_signed_email = Vous avez déjà signé avec adresse %s
already_voted = Vous avez déjà voté pour ce sondage
error_captcha = captcha Blancs
error_email = Email adresse %s n'est pas valide
error_privacy = Sans votre consentement à un traitement de confidentialité, nous ne pouvons pas accepter votre demande
error_var_empty =  %s disparus
link = Lien
must_authenticate = S'il vous plaît authentification préalable
must_register = S'il vous plaît regsiter premier
name = Nom
no_question_selected = S'il vous plaît choisir une option moins
notification_footer = Ce message est automatiquement envoyé pour chaque signature de la " %s" campagne en ligne\n %s\n\nPour plus d'informations, s'il vous plaît contacter %s\n
notify_notification = Une copie de la signature sera envoyé à %s
notification_sent = Une copie de la signature a été envoyé à %s
poll_closed = Ce sondage est maintenant terminé
reject_upload = Le format de fichier <b>(%s)</b> que vous venez de charger n'est pas autorisé.
send_friend_ko = Courrier électronique n'a pas été envoyé
send_friend_ok = Email a été envoyé à %s
thanks_article = Merci! Votre demande a été reçue et sera visible en ligne après un contrôle
thanks_campaign = Merci! Votre proposition sera visible en ligne après un contrôle
thanks_contact = Merci! Vous recevrez une réponse dès que possible
thanks_link = Merci! Votre demande a été reçue et sera visible en ligne après un contrôle
title = Titre
vips = Ont déjà signé:
