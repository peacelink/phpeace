# BOOKS ENGLISH
associated_article = Associated article
book = Book
book_choose = <p>Choose the book to add</p>
books_home_types = { All publishers | All categories of a chosen publisher | List of chosen books  }
books_home_publishers = <p>A list of all publishers is shown in the homepage</p>
books_without_categories = %s books without category
catalog_number = Catalog #
category = Category
categories = Categories
cover = Cover
cover_add = Add cover
cover_warning = It's possible to upload a cover only after submitting book's data
ebook_types = { ___ | File | Subtopic | Article }
field_add = Add field
important = Important
missing_review = Missing review
publisher = Publisher
publishers = Publishers
publisher_choose = <p>Choose the publisher to show</p>
review = Review
review_add = Add new review
reviews = Reviews
reviews_approved = Approved reviews
reviews_to_approve = Reviews to approve
reviews_configuration = Reviews configuration
rights = Rights
subtitle = Subtitle
vote = Vote
