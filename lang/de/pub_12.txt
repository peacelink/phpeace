# EVENTS
# ITALIAN
error_captcha = Captcha non valido
error_date_invalid = Data non valida
error_date_past = La data non può essere nel passato
error_email = Indirizzo email <em>%s</em> non valido
error_var_empty = %s mancante
event_type = Tipo di evento
id_event_type = Tipo di evento
name = Nome
place = Posto
title = Titolo

