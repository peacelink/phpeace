# TOPICS
# ITALIAN
already_signed = Hai già aderito
already_signed_email = Hai già aderito a questa campagna con l'email %s
already_voted = Hai già votato in questo sondaggio
error_captcha = Captcha non valido
error_email = Indirizzo email %s non valido
error_privacy = Senza il consenso al trattamento dei dati non è possibile accettare il tuo invio
error_var_empty = %s mancante
link = Link
must_authenticate = È prima necessario autenticarsi
must_register = È prima necessario registrarsi
name = Nome
no_question_selected = Selezionare almeno un'opzione
notification_footer = Questo messaggio viene automaticamente inviato per ogni adesione alla campagna "%s"\n%s\n\nPer maggiori informazioni, contattare %s\n
notify_notification = Una copia di questa adesione verrà inviata a %s
notification_sent = Una copia dell'adesione è stata inviata a %s
poll_closed = Sondaggio non attivo
reject_upload = Il formato del file (<b>%s</b>) non è consentito.
send_friend_ko = La segnalazione non è stata inviata
send_friend_ok = Segnalazione inviata a %s
thanks_article = Grazie! Il tuo articolo è stato ricevuto e sarà pubblicato online dopo una verifica
thanks_campaign = Grazie! La tua proposta sarà visibile online dopo una verifica 
thanks_contact = Grazie! Riceverai una risposta prima possibile
thanks_link = Grazie! La segnalazione del link è stata ricevuta e sarà disponibile online dopo una verifica
title = Titolo
vips = Hanno già aderito:

