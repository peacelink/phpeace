# PEOPLE ITALIAN
admin_notes = Note amministrative
connections = Connessioni: %s
domain_change = Cambio di dominio
email_check_subj = Email di verifica
email_check_body = Cara/o %1$s !\n\nRicevi questo messaggio per verificare l'indirizzo di posta elettronica %2$s associato al tuo account press il sito di gestione di "%3$s" (%5$s).\n\nClicca sul link sottostante:\n\n%6$s\n\nQuesto link, che ci permetterà di verificare il funzionamento del tuo indirizzo email e confermare la tua iscrizione, sarà valido per cinque giorni a partire dall'emissione di questa email.\n\nPer qualsiasi chiarimento scrivi a %4$s\n\nGrazie e cordiali saluti\n\nLo staff di %3$s\n\n
email_valid = Email valida
error_check2 = Indirizzo email non valido
error_check3 = Messaggio di verifica già inviato
last_conn = Ultima connessione: %s
person_deleted = %s è stato cancellato
registration_check_failed1 = Verifica dell'indirizzo di posta elettronica scaduta, un nuova messaggio è stato appena inviato
registration_check_failed2 = Verifica dell'indirizzo di posta elettronica non riuscita
registration_check_sent = Messaggio di verifica inviato a %s
registration_date = Data di registrazione: %s
registration_ok = Indirizzo email verificato
send_email_verify = Invia messaggio per verifica email
start_date = Data di inserimento: %s
user_exists = ATTENZIONE: Esiste già un utente con email <strong>%s</strong>
verified = Verificata

