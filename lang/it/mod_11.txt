# LIST
# ITALIAN
smartlist_subscribe = È stata inviata una richiesta via email per aggiungere l'indirizzo <strong>%s</strong> alla mailing list <strong>%s</strong>
smartlist_unsubscribe = È stata inviata una richiesta via email per rimuovere l'indirizzo <strong>%s</strong> dalla mailing list <strong>%s</strong>
smartlist_checkdist = È stata richiesta la verifica dell'iscrizione dell'indirizzo <strong>%s</strong> alla lista <strong>%s</strong></p>; il relativo multigram verrà inviato all'owner %s
smartlist_showdist = È stata inviata una richiesta via email per inviare l'elenco degli iscritti alla lista <strong>%s</strong> all'owner <strong>%s</strong>
smartlist_showlog = È stata inviata una richiesta via email per inviare il log della lista <strong>%s</strong> all'indirizzo email <strong>%s</strong>
smartlist_wipelog = È stata inviata una richiesta via email per cancellare il log della lista <strong>%s</strong>
mailman_subscribe = È stata inviata una richiesta via email per aggiungere l'indirizzo <strong>%s</strong> alla mailing list <strong>%s</strong>
mailman_unsubscribe = È stata inviata una richiesta via email per rimuovere l'indirizzo <strong>%s</strong> dalla mailing list <strong>%s</strong>
mailman_showdist = È stata inviata una richiesta via email per inviare l'elenco degli iscritti alla lista <strong>%s</strong> all'owner <strong>%s</strong>
