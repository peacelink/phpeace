# HOMEPAGE / WIDGETS
widget_share_subject = Invitation to share a widget at "%s"
widget_share_message = Dear Sir/Madam,\n\n%1$s would like to share with you a widget at "%2$s".\n\nYou may accept the invitation by registering or logging-in using the link below:\n%3$s\n\nYours sincerely\n%2$s\n\nPlease note: this is an auto generated e-mail that cannot receive replies
widget_share_and_edit_message = Dear Sir/Madam,\n\n%1$s would like to share with you a widget at "%2$s".\n\nYou have also been asked to be the co-admin to maintain the content in this widget.\n\nYou may accept the invitation by registering or logging-in using the link below:\n%3$s\n\nYours sincerely\n%2$s\n\nPlease note: this is an auto generated e-mail that cannot receive replies
