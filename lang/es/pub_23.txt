# MEETINGS
comments = Private comments
meeting = Meeting
meeting_over = Meeting over
meetings = Meetings
not_authorized = You're not authorized to see this meeting
participants = Participants
participation = Your participation
participation_status = { ___ | ___ | No  | Maybe | Yes }
pending_approval = Waiting for approval
slot = Slot
subscribe = Subscribe to this meeting
submit = Submit
thank_you = Thank you
