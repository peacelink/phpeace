<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

$_SERVER['DOCUMENT_ROOT'] = realpath("../");
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/people.php");
include_once(SERVER_ROOT."/../classes/db.php");

$options = getopt("g:d:");

if($options['g']!="")
{
	$id_pt_group = (int)$options['g'];
	$debug = isset($options['d']) && $options['d']!='';

	$db =& Db::globaldb();
	$payers = array();
	$sqlstr = "SELECT py.id_payer,py.id_p
		FROM payers py 
		INNER JOIN payments pa ON py.id_payer=pa.id_payer AND pa.is_in='1'
		WHERE pa.is_transfer=0
		GROUP BY py.id_p";
	$num_payers = $db->QueryExe($payers, $sqlstr);

	if ($debug) {
		print("Identified $num_payers payers to be inserted in group $id_pt_group\n");
	} else {
		$pe = new People();
		foreach($payers as $payer) {
			$pe->TopicGroupAssociate($payer['id_p'], $id_pt_group);
		}
	}
}
?>
