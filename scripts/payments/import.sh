#! /bin/sh

CURRENTDIR=`dirname $0`
cd $CURRENTDIR

# Input parameters
# filename (in import directory under installation path)
# bank (paypal / etica / poste)
# id_account
# type
# donors topic group
# dryrun

php $CURRENTDIR/import.php -f $1 -b $2 -c $3 -t $4 -g $5 -d $6
