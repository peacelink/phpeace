#!/bin/sh
# This script grabs a webpage for archival purposes, both in HTML and PNG format.
# To save the HTML copy with all the necessary stylesheets and images, 
# you need to have wget installed in the path
# To render the page and save it as PNG, you need a vncserver properly configured
# and the KDE application khtml2png2 that will render the page using libkhtml

### CONFIGURATION ###

GRABDIR="/home/grab"
SITE="http://www.myphpeace.org/index.html"
HOSTDISPLAY="myhost:1"

#####################

YEAR=`/bin/date +%Y`
MONTH=`/bin/date +%m`
DAY=`/bin/date +%d`

TEMPHOME="$GRABDIR/$YEAR-$MONTH-$DAY"
mkdir $TEMPHOME
cd $TEMPHOME
wget -p --convert-links -P "home" -k -nH -H -e robots=off $SITE

vncserver -geometry 1024x768
khtml2png2 --display $HOSTDISPLAY --width 1024 --height 3000 $SITE home.png
vncserver -kill $HOSTDISPLAY
