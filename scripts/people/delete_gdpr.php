<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

$_SERVER['DOCUMENT_ROOT'] = realpath("../");
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");

$options = getopt("d:");

include_once(SERVER_ROOT."/../classes/people.php");
$pe = new People();
$rows = $pe->SearchContact(0);
if(count($rows)>0) {
	echo count($rows) . " users found\n";
	foreach($rows as $row) {
		echo "Deleting {$row['email']} - {$row['name1']} {$row['name2']}\n";
		if(!isset($options['d'])) {
			$pe->Delete($row['id_p'],false);
		}
	}
}
?>
