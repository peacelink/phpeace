#!/bin/bash

CURRENTDIR=`dirname $0`
cd $CURRENTDIR

# Input parameters
# pattern (search email)
# bounces (minimum number of bounces)
# dryrun

php $CURRENTDIR/delete.php -e $1 -b $2 -d $3
