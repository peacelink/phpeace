<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

$_SERVER['DOCUMENT_ROOT'] = realpath("../");
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

$options = getopt("k:u:");

if(is_array($options) && isset($options['k']) && isset($options['u']) && $options['k']!="" && $options['u']>0)
{
	$id_user = (int)$options['u'];
	$decoded_keyword = strtolower(urldecode($options['k']));
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	$id_keyword = $k->KeywordGetId($decoded_keyword);
	if($id_user>0 && $id_keyword>0) {
		// find all articles associated to this keyword
		$articles = $k->UseArticlesByUser($id_keyword,$id_user);
		if(count($articles)>0) {
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			foreach($articles as $article) {
				$o->KeywordRemove($article['id_article'], $o->types['article'], $id_keyword);
				echo "Keyword \"$decoded_keyword\" removed from article {$article['id_article']} \"{$article['headline']}\"\n";
			}
		} else {
			echo "No articles with keyword \"$decoded_keyword\" by user $id_user\n";
		}
	} else {
		echo "Keyword \"$decoded_keyword\" not found\n";
	}
}

?>
