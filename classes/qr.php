<?php
use chillerlan\QRCode\{QRCode, QROptions};
use chillerlan\QRCode\Data\QRMatrix;
use chillerlan\QRCode\Output\QRGdImagePNG;

/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/log.php");

define('QR_HASH_LENGTH', 6);

/**
 * Internal Resource Locator
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
*/
class QR
{
	/**
	 * QR domain mask
	 * @var string
	 */
	public $shorturl_mask;

	/**
	 * Initialize local variables 
	 */
	function __construct()
	{
		$conf = new Configuration();
		$mask = $conf->Get("shorturl_mask");
		if ($mask!="")
			$this->shorturl_mask = $mask;
		else {
			include_once(SERVER_ROOT."/../classes/ini.php");
			$ini = new Ini();
			$this->shorturl_mask = $ini->Get("pub_web") . "/qr/";
		}
	}
	/**
	 * Generate a random hash
	 * @return string
	 */
	private function QRHash()
	{
		$hash = substr(md5(uniqid(rand(), true)), 0, QR_HASH_LENGTH);
		if($this->QRHashCheck($hash)) {
			$log = new Log();
			$log->Write("warning","Short URL hash collision");
			$hash = $this->QRHash();
		}
		return $hash;
	}

	/**
	 * Get all QR codes
	 *
	 * @param array $rows		QR codes
	 * @param boolean $paged	Whether results are paginaged or not
	 * @return integer			Number of QR codes
	 */	
	public function QRCodes( &$rows, $paged=true )
	{
		$sqlstr = "SELECT id_qrcode,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,hash,url,description,counter FROM qrcodes ORDER BY insert_date DESC";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	/**
	 * Delete a specific QR code
	 *
	 * @param integer $id_qrcode
	 */
	public function QRCodeDelete($id_qrcode)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "qrcodes" );
		$res[] = $db->query( "DELETE FROM qrcodes WHERE id_qrcode=$id_qrcode" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$fm->Delete("uploads/qrcodes/{$id_qrcode}.png");
	}

	/**
	 * Generate a QR code
	 *
	 * @param integer $hash	URL hash
	 */
	public function QRCodeGenerate($hash)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		include_once(SERVER_ROOT."/../others/vendor/autoload.php");
		$options = new QROptions;
		// $options->version             = 2;
		$options->outputInterface     = QRGdImagePNG::class;
		$options->scale               = 20;
		$options->outputType		  = 'png';
		$options->outputBase64        = false;
		$url = "{$this->shorturl_mask}$hash";
		$out1 = (new QRCode($options))->render($url);
		$fm->WritePage("uploads/qrcodes/{$hash}.png", $out1);
		$options = new QROptions;
		$options->outputBase64        = false;
		$out2 = (new QRCode($options))->render($url);
		$fm->WritePage("uploads/qrcodes/{$hash}.svg", $out2);
	}

	/**
	 * Get a specific QR code
	 *
	 * @param integer $id_qrcode	Redirect ID
	 * @return array
	 */
	public function QRCodeGet($id_qrcode)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_qrcode,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,hash,url,description,counter FROM qrcodes WHERE id_qrcode=$id_qrcode");
		return $row;
	}

	/**
	 * Get a specific QR code based on its URL hash
	 *
	 * @param string $hash
	 * @return array
	 */
	public function QRCodeGetByHash($hash)
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_qrcode,url FROM qrcodes WHERE BINARY hash='$hash'");
		return $row;
	}

	public function QRCodeIncrease($id_qrcode)
	{
		$db =& Db::globaldb();
		$sqlstr = "UPDATE LOW_PRIORITY qrcodes SET counter=counter+1 WHERE id_qrcode='$id_qrcode' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	private function QRHashCheck($hash)
	{
		$qr = $this->QRCodeGetByHash($hash);
		return isset($qr['id_qrcode']);
	}

	/**
	 * Store a QR code definition
	 *
	 * @param integer 	$id_qrcode	QR code ID
	 * @param string	$url			URL to redirect to
	 */
	public function QRCodeStore( $id_qrcode, $url, $description )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "qrcodes" );
		if ($id_qrcode>0) {
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "qrcodes" );
			$sqlstr = "UPDATE qrcodes SET url='$url',description='$description'
			WHERE id_qrcode='$id_qrcode' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			// $qrcode = $this->QRCodeGet($id_qrcode);
			// $this->QRCodeGenerate($qrcode['hash']);
		} else {
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "qrcodes" );
			$qr_hash = $this->QRHash();
			$id_qrcode = $db->nextId( "qrcodes", "id_qrcode" );
			$today = $db->GetTodayDate();
			$sqlstr = "INSERT INTO qrcodes (id_qrcode,insert_date,hash,url,description)
			VALUES ($id_qrcode,'$today','$qr_hash','$url','$description')";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			$this->QRCodeGenerate($qr_hash);
		}
		return $id_qrcode;
	}
}