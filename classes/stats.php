<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Stats
{
	private $stats = array();

	function __construct()
	{
		$this->stats[1] = array('colonna1'=>'utente','colonna2'=>'connessioni',
				'sqlstr'=>'SELECT users.name AS name, (COUNT(users_log.id_user) + connections) AS counter
					FROM users
					LEFT JOIN users_log ON users.id_user=users_log.id_user
					GROUP BY users.name
					ORDER BY counter DESC');

		$this->stats[2] = array('colonna1'=>'utente','colonna2'=>'articoli inseriti',
				'sqlstr'=>'SELECT users.name, COUNT(articles.id_user) AS counter
					FROM users
					LEFT JOIN articles ON users.id_user=articles.id_user
					GROUP BY users.name HAVING counter>0
					ORDER BY counter DESC');

		$this->stats[3] = array('colonna1'=>'utente','colonna2'=>'immagini inserite',
				'sqlstr'=>'SELECT u.name, COUNT(h.id_user) AS counter
					 FROM users u 
					 INNER JOIN history h ON u.id_user=h.id_user AND h.action=0 AND h.id_type=7 
					 GROUP BY u.name HAVING counter>0 
					 ORDER BY counter DESC');

		$this->stats[4] = array('colonna1'=>'utente','colonna2'=>'documenti inseriti',
				'sqlstr'=>'SELECT u.name AS name, COUNT(h.id_user) AS counter
					 FROM users u 
					 LEFT JOIN history h ON u.id_user=h.id_user AND h.action=0 AND h.id_type=6 
					 GROUP BY u.name HAVING counter>0 
					 ORDER BY counter DESC');

		$this->stats[5] = array('colonna1'=>'tematica','colonna2'=>'articoli',
				'sqlstr'=>'SELECT name AS name, COUNT(articles.id_article) AS counter
					FROM topics
					LEFT JOIN articles ON topics.id_topic=articles.id_topic
					WHERE topics.visible=1
					GROUP BY topics.name
					ORDER BY counter DESC');

		$this->stats[6] = array('colonna1'=>'gruppo','colonna2'=>'tematiche',
				'sqlstr'=>'SELECT tg.name, COUNT(t.id_topic) AS counter
					FROM topics_groups tg
					LEFT JOIN topics t USING(id_group)
					WHERE t.visible=1
					GROUP BY tg.name
					ORDER BY counter DESC');

		$this->stats[7] = array('colonna1'=>'galleria','colonna2'=>'immagini',
				'sqlstr'=>'SELECT title AS name, COUNT(images_galleries.id_image) AS counter
					FROM galleries
					LEFT JOIN images_galleries ON galleries.id_gallery=images_galleries.id_gallery
					WHERE visible=1
					GROUP BY galleries.title
					ORDER BY counter DESC');

		$this->stats[8] = array('colonna1'=>'sezione volontari','colonna2'=>'iscritti',
				'sqlstr'=>'SELECT vol_section, COUNT(vol.id_vol_section) AS counter
				FROM vol_sections
				LEFT JOIN vol ON vol_sections.id_vol_section=vol.id_vol_section
				GROUP BY vol_section
				ORDER BY counter DESC');

		$this->stats[9] = array('colonna1'=>'province','colonna2'=>'iscritti',
				'sqlstr'=>'SELECT prov.prov, COUNT(vol.id_vol) AS counter
				FROM prov
				LEFT JOIN vol ON prov.id_prov=vol.id_province
				GROUP BY prov.prov
				ORDER BY counter DESC');

		$this->stats[10] = array('colonna1'=>'regioni','colonna2'=>'iscritti',
				'sqlstr'=>'SELECT reg.reg, COUNT(vol.id_vol) AS counter
				FROM reg
				LEFT JOIN prov ON reg.id_reg=prov.id_reg
				LEFT JOIN vol ON prov.id_prov=vol.id_province
				GROUP BY reg.reg
				ORDER BY counter DESC');

		$this->stats[11] = array('colonna1'=>'tipologia','colonna2'=>'associazioni',
				'sqlstr'=>'SELECT ass_tipo.ass_tipo, COUNT(ass.id_ass) AS counter
				FROM ass_tipo
				LEFT JOIN ass ON ass_tipo.id_ass_tipo=ass.id_tipo
				GROUP BY ass_tipo.ass_tipo
				ORDER BY counter DESC');

		$this->stats[12] = array('colonna1'=>'province','colonna2'=>'associazioni',
				'sqlstr'=>'SELECT prov.prov, COUNT(ass.id_ass) AS counter
				FROM prov
				LEFT JOIN ass ON prov.id_prov=ass.id_prov
				GROUP BY prov.prov
				ORDER BY counter DESC');

		$this->stats[13] = array('colonna1'=>'regioni','colonna2'=>'associazioni',
				'sqlstr'=>'SELECT reg.reg, COUNT(ass.id_ass) AS counter
				FROM reg
				LEFT JOIN prov ON reg.id_reg=prov.id_reg
				LEFT JOIN ass ON prov.id_prov=ass.id_prov
				GROUP BY reg.reg
				ORDER BY counter DESC');
/* TODO
		$this->stats[14] = array('colonna1'=>'lingua','colonna2'=>'articoli',
				'sqlstr'=>'SELECT languages.language, COUNT(articles.id_language) AS counter
				FROM languages
				LEFT JOIN articles ON languages.id_language=articles.id_language
				WHERE articles.approved=1
				GROUP BY languages.language
				ORDER BY counter DESC');
*/
		$this->stats[15] = array('colonna1'=>'banner','colonna2'=>'clicks',
				'sqlstr'=>'SELECT banners.alt_text, COUNT(clicks.id_banner) AS counter
				FROM clicks
				LEFT JOIN banners ON clicks.id_banner=banners.id_banner
				GROUP BY banners.alt_text
				ORDER BY counter DESC');

		$this->stats[16] = array('colonna1'=>'tematica','colonna2'=>'links',
				'sqlstr'=>'SELECT name, COUNT(links_subtopics.id_link) AS counter
					FROM topics
					LEFT JOIN links_subtopics USING(id_topic)
					LEFT JOIN links USING(id_link)
					WHERE approved=1
					GROUP BY topics.name
					ORDER BY counter DESC');
/* TODO
		$this->stats[17] = array('colonna1'=>'lingua','colonna2'=>'links',
				'sqlstr'=>'SELECT languages.language, COUNT(links.id_language) AS counter
				FROM languages
				LEFT JOIN links USING(id_language)
				WHERE links.approved=1
				GROUP BY languages.language
				ORDER BY counter DESC');
*/
		$this->stats[18] = array('colonna1'=>'autore','colonna2'=>'articoli scritti',
				'sqlstr'=>'SELECT users.name,COUNT(id_article) AS counter
				FROM users LEFT JOIN articles a USING(id_user)
				WHERE a.show_author=1 AND a.author="" GROUP BY users.name ORDER BY counter DESC');
	
		$this->stats[19] = array('colonna1'=>'articolo','colonna2'=>'segnalazioni',
				'sqlstr'=>'SELECT CONCAT(topics.name,": ",articles.headline) AS name,COUNT(friends.id_article) as counter
				FROM friends INNER JOIN articles using(id_article) INNER JOIN topics USING(id_topic)
				GROUP BY friends.id_article ORDER BY counter desc');

		$this->stats[20] = array('colonna1'=>'mese','colonna2'=>'articoli',
				'sqlstr'=>'SELECT DATE_FORMAT(published,"%M %Y") AS name,COUNT(id_article) AS counter,DATE_FORMAT(published,"%Y%m") AS sort
				FROM articles WHERE published>0 GROUP BY name ORDER BY sort DESC');

		$this->stats[21] = array('colonna1'=>'mese','colonna2'=>'collegamenti',
				'sqlstr'=>'SELECT DATE_FORMAT(login,"%M %Y") AS name,COUNT(id_user_log) AS counter,
				DATE_FORMAT(login,"%Y%m") AS sort FROM users_log GROUP BY name ORDER BY sort DESC');

		$this->stats[22] = array('colonna1'=>'mese','colonna2'=>'volontari',
				'sqlstr'=>'SELECT DATE_FORMAT(insert_date,"%M %Y") AS name,COUNT(id_vol) AS counter,
				DATE_FORMAT(insert_date,"%Y%m") AS sort FROM vol GROUP BY name ORDER BY sort DESC');
	
		$this->stats[23] = array('colonna1'=>'name','colonna2'=>'connections',
				'sqlstr'=>'SELECT users.id_user AS id_user, users.name, (COUNT(users_log.id_user) + connections) AS counter
					FROM users
					LEFT JOIN users_log ON users.id_user=users_log.id_user
					GROUP BY users.id_user HAVING counter < 5
					ORDER BY counter ASC');
				
		$this->stats[24] = array('colonna1'=>'name','colonna2'=>'connections',
				'sqlstr'=>'SELECT u.id_user,u.name,COUNT(ul.id_user) AS counter
					FROM users u 
					LEFT JOIN users_log ul ON u.id_user=ul.id_user 
					GROUP BY u.id_user HAVING counter=0  AND u.id_user>0 ORDER BY name ASC');
					

	}

	public function Stat( &$rows, $id )
	{
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $this->stats[$id]['sqlstr'], true);
	}

	public function Headers($id)
	{
		$headers = array($this->stats[$id]['colonna1'],$this->stats[$id]['colonna2']);
		return $headers;
	}
}
?>
