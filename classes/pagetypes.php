<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/features.php");

class PageTypes
{
	/** 
	 * @var Features */
	public $ft;
	
	public $gtypes = array(
			'homepage'	=> 0,
			'rss'		=> 1,
			'map'		=> 2,
			'feeds'		=> 3,
			'gallery_group'	=> 4,
			'user'		=> 5,
			'gnewsletter'	=> 6,
			'common_global'	=> 7,
			'widgets'	=> 8,
			'keyword'	=> 9
			);
	public $types = array('common'	=> 0,
			'root'		=> 20,
			'tools'		=> 21,
			'topic_home'	=> 1,
			'subtopic'	=> 2,
			'article'		=> 3,
			'article_box'	=> 4,
			'events'	=> 5,
			'quotes'	=> 6,
			'campaign'	=> 7,
			'forum'		=> 8,
			'poll'		=> 9,
			'gallery_item'	=> 10,
			'people'	=> 11,
			'newsletter'	=> 12,
			'geosearch'	=> 13,
			'print'		=> 14,
			'send_friend'	=> 15,
			'image_zoom'	=> 16,
			'error404'	=> 17,
			'search'	=> 18,
			'random_item'	=> 19,
			'comments'	=> 22
			);
	public $subtypes = array(	'5'	=>	array('next','day','event','insert','search')	);

	private $propagate = true;

	function __construct()
	{
		$this->ft = new Features;
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$this->propagate = $ini->GetModule("layout","feature_propagate",1);
	}

	public function FeaturesByFunction($id_function) {
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT f.condition_id,pf.id_type 
			FROM features f 
			INNER JOIN page_features pf ON f.id_feature=pf.id_feature 
			WHERE $id_function";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function FeatureDelete($id_feature,$id_style,$id_type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "page_features" );
		$res[] = $db->query( "DELETE FROM page_features WHERE id_feature=$id_feature AND id_style=$id_style AND id_type=$id_type" );
		Db::finish( $res, $db);
		if($this->propagate)
			$this->UpdatePropagate($id_feature);
	}

	public function FeatureStore( $id_feature,$name,$description,$id_style,$id_type,$id_module,$id_function,$active,$public,$condition_var,$condition_type,$condition_value,$condition_id,$id_user,$changed=false )
	{
		$id_feature_new = $this->ft->FeatureStore( $id_feature,$name,$description,$id_function,$active,$public,$condition_var,$condition_type,$condition_value,$condition_id,$id_user );
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "page_features" );
		if ($id_feature==0)
		{
			$sqlstr = "INSERT INTO page_features (id_feature,id_style,id_type,id_module) 
				VALUES ($id_feature_new,$id_style,$id_type,$id_module)";
		}
		else
		{
			$sqlstr = "UPDATE page_features SET id_type=$id_type,id_module=$id_module
			WHERE id_feature=$id_feature";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($this->propagate && ($id_feature==0 || $changed))
			$this->UpdatePropagate($id_feature);
		return $id_feature_new;
	}

	public function GlobalFeatureDelete($id_feature,$id_type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "global_features" );
		$res[] = $db->query( "DELETE FROM global_features WHERE id_feature=$id_feature AND id_type=$id_type" );
		Db::finish( $res, $db);
		if($this->propagate)
			$this->UpdatePropagate($id_feature);
	}

	public function GlobalFeatureStore( $id_feature,$name,$description,$id_type,$id_function,$old_id_type,$old_id_function,$active,$public,$id_user )
	{
		$id_feature_new = $this->ft->FeatureStore( $id_feature,$name,$description,$id_function,$active,$public,"",0,"",0,$id_user);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "global_features" );
		if ($id_feature==0)
		{
			$sqlstr = "INSERT INTO global_features (id_feature,id_type) 
				VALUES ($id_feature_new,$id_type)";
		}
		else
		{
			$sqlstr = "UPDATE global_features SET id_type=$id_type
			WHERE id_feature=$id_feature";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($this->propagate && ($id_feature==0 || $id_type!=$old_id_type || $id_function!=$old_id_function))
			$this->UpdatePropagate($id_feature);
		return $id_feature_new;
	}

	public function GlobalFeatureUpdate($id_feature,$id_type)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "global_features" );
		$res[] = $db->query( "UPDATE global_features SET id_type=$id_type WHERE id_feature=$id_feature" );
		Db::finish( $res, $db);
		if($this->propagate)
			$this->UpdatePropagate($id_feature);
	}

	public function PageFunctionsParamsStore($id_feature,$params)
	{
		$feature = $this->ft->FeatureGet($id_feature);
		$this->ft->PageFunctionsParamsStore($id_feature,$params);
		if ($this->propagate && $feature['params'] != $params)
			$this->UpdatePropagateWithParams($id_feature,$params);
	}
	
	private function UpdatePropagate($id_feature)
	{
		$feature = $this->ft->FeatureGet($id_feature);
		$params = $this->ft->ParamsDeserialize($feature['params']);
		$this->UpdatePropagateWithParams($id_feature,$params);
	}
	
	public function UpdatePropagateWithParams($id_feature,$params)
	{
		$this->UpdatePropagateGlobal($id_feature);
		$this->UpdatePropagateLocal($id_feature,$params);
	}
	
	private function UpdatePropagateGlobal($id_feature)
	{
		$pagetype = $this->ft->GlobalFeatureGetType($id_feature);
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic(0);
		switch($pagetype['id_type'])
		{
			case $this->gtypes['homepage']:
				$t->queue->JobInsert($t->queue->types['homepage'],$t->id,"");
			break;
			case $this->gtypes['rss']:
				$t->queue->JobInsert($t->queue->types['homepage'],$t->id,"");
			break;
			case $this->gtypes['map']:
				$t->queue->JobInsert($t->queue->types['map'],$t->id,"");
			break;
			case $this->gtypes['feeds']:
				$t->queue->JobInsert($t->queue->types['homepage'],$t->id,"");
			break;
			case $this->gtypes['gallery_group']:
			break;
			case $this->gtypes['user']:
			break;
		}
	}
	
	private function UpdatePropagateLocal($id_feature,$params)
	{
		$styles = $this->ft->FeatureGetStyles($id_feature);
		if (count($styles)>0)
		{
			include_once(SERVER_ROOT."/../classes/styles.php");
			include_once(SERVER_ROOT."/../classes/topic.php");
			$s = new Styles();
			foreach($styles as $style)
			{
				$topics = $s->Topics($style['id_style']);
				foreach($topics as $topic)
				{
					$t = new Topic($topic['id_topic']);
					if($t->visible)
					{
						switch($style['id_type'])
						{
							case $this->types['common']:
								$t->queue->JobInsert($t->queue->types['all'],$t->id,"");						
							break;
							case $this->types['topic_home']:
								$t->queue->JobInsert($t->queue->types['topic_home'],$t->id,"");
							break;
							case $this->types['subtopic']:
								if ($params['id_subtopic']>0)
									$t->queue->JobInsert($t->queue->types['subtopic'],$params['id_subtopic'],"update");
								else
								{
									$subtopics = $t->Subtopics();
									foreach($subtopics as $subtopic)
										$t->queue->JobInsert($t->queue->types['subtopic'],$subtopic['id_subtopic'],"update");
								}
							break;
							case $this->types['article']:
								if ($params['id_article']>0)
									$t->queue->JobInsert($t->queue->types['article'],$params['id_article'],"update");
								else
								{
									foreach($t->Articles() as $article)
										$t->queue->JobInsert($t->queue->types['article'],$article['id_article'],"update");
								}
							break;
							// TODO Complete pagetype propagation of feature changes
							case $this->types['article_box']:
							break;
							case $this->types['links']:
							break;
							case $this->types['gallery']:
							break;
							case $this->types['gallery_item']:
							break;
							case $this->types['image_zoom']:
							break;
						}
					}
				}
			}
		}
	}
	
}
?>
