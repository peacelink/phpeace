<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/error.php");

/**
 * How many records to be deleted for each MySQL Cluster operation
 *
*/
define('MYSQL_CLUSTER_DELETE_OPERATION_LIMIT',100000);

/**
 * MySQL methods
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
*/
class DbMysql
{
	/**
	 * Database name
	 *
	 * @var string
	 */
	private $db_name;

	/**
	 * DB connection
	 *
	 * @var mixed	MySQL link identifier on success or FALSE on failure
	 */
	private $database;

	/**
	 * Number of records per page
	 *
	 * @var integer
	 */
	public $records_per_page;

	/**
	 * Number of affected rows in previous MySQL operation
	 *
	 * @var integer
	 */
	public $affected_rows;

	/**
	 * Whether an error occurred
	 *
	 * @var boolean
	 */
	public $error_status;

	/**
	 * Whether the error is fatal
	 *
	 * @var boolean
	 */
	private $critical;

	/**
	 * Storage engine
	 *
	 * @var string
	 */
	private $storage_engine;

	/**
	 * Initialize DB connection and select current database
	 *
	 * @param string $server
	 * @param string $db_name
	 * @param string $user
	 * @param string $password
	 * @param boolean $pconn	Persistent connection
	 */
	function __construct( $server, $db_name, $user, $password,$pconn  )
	{
		$this->error_status = false;
		$this->critical = false;
		$this->db_name = $db_name;
		
		$this->database = mysqli_connect( $server, $user, $password );
			
		mysqli_query($this->database,"SET SESSION sql_mode=''");
		if(!$this->database)
		{
			$this->CriticalSet();
			$this->ErrorReport("Cannot connect to database server $server");
		}

		$this->select_db($this->db_name);
	}

	/**
	 * Execute query
	 *
	 * @param array		$array
	 * @param string	$sql
	 * @param integer	$offset
	 * @param integer	$limit
	 * @param integer	$result_type
	 * @return boolean					Success or failure
	 */
	private function array_query( &$array, $sql, $offset = 0, $limit = -1, $result_type = 3)
	{
		$array = array();
		return $this->array_query_append( $array, $sql, $offset, $limit, $result_type);
	}

	/**
	 * Execute query and append results to array
	 *
	 * @param array		$array
	 * @param string	$sql
	 * @param integer	$offset
	 * @param integer	$limit
	 * @param integer	$result_type
	 * @return boolean					Success or failure
	 */
	public function array_query_append( &$array, $sql, $offset = 0, $limit = -1, $result_type = 3)
	{
		if ( $limit != -1 )
			$sql .= " LIMIT $offset, $limit ";
		$result = $this->query( $sql );

		if ( $result == false )
		{
			$this->ErrorReport($sql);
			return false;
		}

		if ( mysqli_num_rows( $result ) > 0 )
		{
			for($i = 0; $i < mysqli_num_rows($result); $i++)
				$array[$i + $offset] = mysqli_fetch_array($result, $result_type);
		}
		return true;
	}

	/**
	 * Select DB
	 *
	 * @param string $db_to_select	DB name to select
	 * @return boolean				Success or failure
	 */
	public function select_db( $db_to_select )
	{
	    $ret = mysqli_select_db( $this->database, $db_to_select );
		$this->db_name = $db_to_select;
		if (!$ret)
		{
			$this->CriticalSet();
			$this->ErrorReport("Cannot select database $db_to_select");
		}
		return $ret;
	}

	/**
	 * Number of results
	 *
	 * @param 	string 	$sql	SQL statement
	 * @return 	integer			Number of records
	 */
	private function num_records( $sql )
	{
		$result = $this->query( $sql );
		$num = mysqli_num_rows($result);
		mysqli_free_result($result);
		return $num;
	}

	/**
	 * Report a DB error
	 *
	 * @param string $sql	SQL statement that generated error
	 */
	private function ErrorReport($sql="")
	{
		$error = array();
		$error_number = 0;
		if($sql!="")
			$error['sql'] = htmlspecialchars($sql);
		if($this->database)
		{
			$error_number = (int)mysqli_errno( $this->database );
			$error['error_number'] = mysqli_errno( $this->database );
			$error['error'] = mysqli_error( $this->database );
		}
		$this->error_status = true;
		$critical = $this->critical || ($error_number==1100);
		UserError("MySQL Error",$error,256,$critical);
	}

	/**
	 * Generic query
	 *
	 * @param string	$sql	SQL statement
	 * @param boolean	$count	Count affected rows and set the local property
	 * @return boolean			Success or failure
	 */
	public function query( $sql, $count=false )
	{
		if($count)
		{
			$this->affected_rows = 0;
		}
		$result = mysqli_query( $this->database, $sql );
		if($count)
		{
			$this->affected_rows = mysqli_affected_rows($this->database);
		}

		if ( $result )
		{
			return $result;
		}
		else
		{
			$this->ErrorReport($sql);
			return false;
		}
	}

	/**
	 * Execute a query
	 *
	 * @param array 	$rows				Results
	 * @param string	$sql				SQL statement
	 * @param boolean	$paged				Paginate results
	 * @param integer	$result_type		Type of array to be fetched (MYSQL_BOTH = 3, MYSQL_ASSOC = 1, MYSQL_NUM = 2)
	 * @param integer	$records_per_page	Custom records per page
	 * @return integer						Number of records
	 */
	public function QueryExe( &$rows, $sql, $paged = false, $result_type = 1, $custom_records_per_page = 0 )
	{
		if($paged)
		{
			if($custom_records_per_page>0)
			{
				$this->records_per_page = $custom_records_per_page;
			}
			elseif ($GLOBALS['records_per_page'] > 0)
			{
				$this->records_per_page = $GLOBALS['records_per_page'];
			}
			$limit = $this->records_per_page;
			$current_page = ($GLOBALS['current_page']>0)? $GLOBALS['current_page'] : 1;
			$offset = ($current_page-1)*$limit;
			$rows = array();
			$this->array_query( $rows, $sql, $offset, $limit, $result_type);
			$num = $this->num_records( $sql );
			$tot_pages = floor(($num-1)/$limit)+1;
			if($num>0 && $current_page>1 && $tot_pages>0 && $current_page>$tot_pages)
			{
				$GLOBALS['current_page'] = $tot_pages;
				$offset = ($tot_pages-1)*$limit;
				$rows = array();
				$this->array_query( $rows, $sql, $offset, $limit, $result_type);
				$num = $this->num_records( $sql );
			}
		}
		else
		{
			$this->array_query( $rows, $sql, 0, -1, $result_type );
			$num = count($rows);
		}
		return $num;
	}

	/**
	 * Retrieve next available ID
	 *
	 * @param string	$table	Table name
	 * @param string	$field	ID column name
	 * @param string	$cond	Optional condition
	 * @return integer			Next ID
	 */
	public function nextId( $table, $field, $cond="" )
	{
		$sqlstr = "SELECT $field FROM $table ";
		if($cond!="")
			$sqlstr .= " WHERE $cond ";
		$sqlstr .= " ORDER BY $field DESC LIMIT 1 ";
		$result = mysqli_query( $this->database, $sqlstr );

		$id = 1;
		if ( $result )
		{
			if ( !mysqli_num_rows( $result ) == 0 )
			{
				$array = mysqli_fetch_row( $result );
				$id = $array[0];
				$id++;
			}
			else
				$id = 1;
		}
		return $id;
	}

	/**
	 * Query for a single row
	 *
	 * @param array 	$row			Result
	 * @param string 	$sql			SQL statement
	 * @param integer 	$result_type	Type of array to be fetched (MYSQL_BOTH, MYSQL_ASSOC, MYSQL_NUM)
	 * @return boolean					Success or failure
	 */
	public function query_single( &$row, $sql, $result_type=1)
	{
		$array = array();
		$ret = $this->array_query_append( $array, $sql, 0, -1, $result_type);
		if ( isset( $array[0] ) )
			$row = $array[0];
		else
			$row = array();
		return $ret;
	}

	/**
	 * Begin DB operation
	 *
	 */
	public function begin()
	{
		$this->query( "BEGIN WORK" );
	}

	/**
	 * Lock a table or an array of tables
	 *
	 * @param mixed $table
	 */
	public function lock( $table )
	{
		if(is_array($table))
		{
			$this->LockTables($table);
		}
		else
		{
			$this->query( "LOCK TABLES $table WRITE" );
		}
	}

	/**
	 * Lock tables
	 *
	 * @param array $tables
	 */
	public function LockTables( $tables )
	{
		$this->query( "LOCK TABLES " . implode(" WRITE,",$tables) . " WRITE" );
	}

	/**
	 * Commmit
	 *
	 */
	public function Commit()
	{
		$this->query( "COMMIT" );
	}

	/**
	 * Rollback
	 *
	 */
	public function Rollback()
	{
		$this->query( "ROLLBACK" );
	}

	/**
	 * Unlock all tables
	 *
	 */
	public function Unlock()
	{
		$this->query( "UNLOCK TABLES" );
	}

	/**
	 * Close DB connection
	 *
	 */
	public function Close()
	{
		mysqli_close( $this->database );
	}

	/**
	 * Add a column to a specific table of current DB
	 *
	 * @param string	$table		Table name
	 * @param string	$name		Column name
	 * @param string	$type		Column data type
	 * @param string	$default	Column default value
	 * @param boolean	$can_be_null	Whether the column can be null
	 */
	public function ColumnAdd($table,$name,$type,$default,$can_be_null=false)
	{
		if($this->TableExists($table))
		{
			if(!$this->ColumnExists($table,$name))
			{
				$db =& Db::globaldb();
				$db->begin();
				$db->lock( $table );
				$sqlstr = "ALTER TABLE $table ADD COLUMN `$name` $type DEFAULT $default " . ($can_be_null? "NULL":"NOT NULL");
				$res[] = $db->query($sqlstr);
				Db::finish( $res, $db);
			}
		}
	}

	/**
	 * Delete a column from a specific table of current DB
	 *
	 * @param string $table		Table name
	 * @param string $name		Column name
	 */
	public function ColumnDelete($table,$name)
	{
		if($this->TableExists($table))
		{
			if($this->ColumnExists($table,$name))
			{
				$db =& Db::globaldb();
				$db->begin();
				$db->lock( $table );
				$sqlstr = "ALTER TABLE $table DROP COLUMN $name ";
				$res[] = $db->query($sqlstr);
				Db::finish( $res, $db);
			}
		}
	}

	/**
	 * Where a column exists in a specific table of current DB
	 *
	 * @param string $table		Table name
	 * @param string $column	Column name
	 * @return boolean
	 */
	public function ColumnExists($table,$column)
	{
		$current_db = $this->db_name;
		$db =& Db::globaldb();
		$db->select_db("INFORMATION_SCHEMA");
		$row = array();
		$sqlstr = "SELECT * FROM columns WHERE table_schema='$current_db' AND table_name='$table' AND column_name='$column' ";
		$db->query_single($row, $sqlstr);
		$this->select_db($current_db);
		return count($row)>0;
	}

	/**
	 * Set local variable for critical errors to true
	 *
	 */
	public function CriticalSet()
	{
		$this->critical = true;
	}

	/**
	 * Delete a number of records based on a specific condition
	 * To be used to avoid MaxNoOfConcurrentOperations errors with MySQL Cluster
	 * If you don't have a condition, use TRUNCATE instead
	 *
	 * @param string $table		Table name
	 * @param string $condition	Condition
	 */
	public function DeleteRows($table,$condition)
	{
		if($table!="" && $condition!="")
		{
			if($this->storage_engine=="NDBCLUSTER")
			{
				$this->DeleteRowsRecursion($table, $condition, MYSQL_CLUSTER_DELETE_OPERATION_LIMIT);
			}
			else
			{
				$db =& Db::globaldb();
				$db->begin();
				$db->lock( $table );
				$res[] = $db->query("DELETE FROM $table WHERE $condition");
				Db::finish( $res, $db);
			}
		}
	}

	/**
	 * Recursively delete a limited number of records based on a specific condition
	 * To be used to avoid MaxNoOfConcurrentOperations errors with MySQL Cluster
	 *
	 * @param string	$table		Table name
	 * @param string	$condition	Condition
	 * @param integer	$limit		Records to be deleted per operation
	 */
	private function DeleteRowsRecursion($table,$condition,$limit)
	{
		$db =& Db::globaldb();
		$row = array();
		$db->query_single($row,"SELECT COUNT(*) AS counter FROM $table WHERE $condition");
		$records_left = (int)$row['counter'];

		if($records_left <= 0)
		{
			// quit from recursion
			return true;
		}

		while($records_left > 0)
		{
			// delete as many record as possible per recursion step
			$db->begin();
			$db->lock( $table );
			$res[] = $db->query("DELETE FROM $table WHERE $condition LIMIT $limit");
			Db::finish( $res, $db);
			$records_left = $records_left - $limit;
		}

		// check if the records is really empty now by call the recursion again
		$this->DeleteRowsRecursion($table,$condition,$limit);
	}

	/**
	 * Add an index to a column in a table of current DB
	 *
	 * @param string $table
	 * @param string $column
	 */
	public function IndexAdd($table,$column)
	{
		if($this->ColumnExists($table,$column))
		{
			if(!$this->IndexExists($table,$column))
			{
				$db =& Db::globaldb();
				$db->begin();
				$db->lock( $table );
				$sqlstr = "ALTER TABLE `$table` ADD INDEX (`$column`)";
				$res[] = $db->query($sqlstr);
				Db::finish( $res, $db);
			}
		}
	}

	/**
	 * Whether an index exist in a specific table of current DB
	 *
	 * @param string $table
	 * @param string $index
	 * @return boolean
	 */
	private function IndexExists($table,$index)
	{
		$current_db = $this->db_name;
		$db =& Db::globaldb();
		$db->select_db("INFORMATION_SCHEMA");
		$row = array();
		$sqlstr = "SELECT * FROM statistics WHERE table_schema='$current_db' AND table_name='$table' AND index_name='$index' ";
		$db->query_single($row, $sqlstr);
		$this->select_db($current_db);
		return count($row)>0;
	}

	public function getMilliseconds()
	{
		$m = explode(' ',microtime());
		return (int)round($m[0]*1000,3);
	}

	/**
	 * Date and time in MySQL format
	 *
	 * @param integer $ts		Optional timestamp
	 * @return string
	 */
	public function getTodayTime($ts=0)
	{
		return ($ts>0)? date("Y-m-d H:i:s",$ts) : date("Y-m-d H:i:s");
	}

	/**
	 * Current date in MySQL format
	 *
	 * @param integer $ts		Optional timestamp
	 * @return string
	 */
	public function getTodayDate($ts=0)
	{
		return ($ts>0)? date("Y-m-d",$ts) : date("Y-m-d");
	}

	/**
	 * Dump current DB in an external file
	 *
	 * @param string 	$file		Filename
	 * @param array 	$tables		Tables to dump
	 * @param boolean	$with_table	With table definition
	 * @param boolean	$with_data	With data
	 */
	public function Dump($file,$tables,$with_table,$with_data)
	{
		$this->DumpDb($file,$tables,$with_table,$with_data,$this->db_name,"--opt --compact");
	}

	/**
	 * Dump a DB in an external file
	 *
	 * @param string 	$file		Filename
	 * @param array 	$tables		Tables to dump
	 * @param boolean	$with_table	With table definition
	 * @param boolean	$with_data	With data
	 * @param string	$db_name	DB name
	 * @param string	$options	mysqldump options
	 */
	public function DumpDb($file,$tables,$with_table,$with_data,$db_name,$options="")
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/config.php");
		include_once(SERVER_ROOT."/../classes/varia.php");
		$conf = new Configuration();
		$dbconf = $conf->Get("dbconf");
		$fm = new FileManager;
		$file = $fm->RealPath($file);
		if (count($tables)>0)
		{
			foreach($tables as $table)
			{
				$t .= "$table ";
			}
		}
		if($options=="")
		{
			$options = "--add-drop-table --add-locks --create-options --skip-extended-insert --disable-keys --lock-tables --quick  --set-charset";
		}
		$credentials = $this->DbCredentials();
		$command = "mysqldump --defaults-extra-file=$credentials $options -c --comments=false " . (($with_table)?"":"-t") . " " . (($with_data)?"":"-d") . " {$db_name} $t > $file";
		$v = new Varia();
		$v->Exec($command);
	}

	/**
	 * Export DB schema into file
	 *
	 * @param string $file
	 * @param array $tables_to_exclude	Tables to exclude
	 */
	public function Schema($file,$tables_to_exclude=array())
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/config.php");
		include_once(SERVER_ROOT."/../classes/varia.php");
		$conf = new Configuration();
		$dbconf = $conf->Get("dbconf");
		$fm = new FileManager;
		$file = $fm->RealPath($file);
		$credentials = $this->DbCredentials();
		$command = "mysqldump --defaults-extra-file=$credentials --compact --skip-opt -d ";
		if(count($tables_to_exclude)>0)
		{
			foreach($tables_to_exclude as $table)
			{
				$command .= " --ignore-table={$dbconf['database']}.$table ";
			}
		}
		$command .= " {$dbconf['database']} > $file";
		$v = new Varia();
		$v->Exec($command);
	}

	private function DbCredentials()
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$credentials_file = "custom/db.conf";
		if(!$fm->Exists($credentials_file))
		{
			include_once(SERVER_ROOT."/../classes/config.php");
			$conf = new Configuration();
			$dbconf = $conf->Get("dbconf");
			$credentials = "[client]\nuser = {$dbconf['user']}\npassword = {$dbconf['password']}";
			$fm->WritePage($credentials_file, $credentials);
		}
		return $fm->RealPath($credentials_file);
	}

	/**
	 * Escape a string for DB operations
	 *
	 * @param string $sqlstr
	 * @return string
	 */
	public function SqlQuote($sqlstr)
	{
		if(!is_array($sqlstr))
		{
			$sqlstr = addslashes($sqlstr);
		}
		return $sqlstr;
	}


	/**
	 * Set current storage engine
	 *
	 * @param string $db_impl	DB implementation
	 */
	public function StorageEngineSet($db_impl)
	{
		$this->storage_engine = $db_impl=="mysql-cluster"? "NDBCLUSTER" : "InnoDB";
	}

	/**
	 * Restore a DB dump
	 * (i.e.e execute all SQL statements from a file)
	 *
	 * @param string $filename
	 */
	public function Restore($filename)
	{
		$sqlstr = '';
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$lines = $fm->TextFileReadArray($filename);
		foreach($lines as $line)
		{
			if (substr($line, 0, 2) != '--' && $line != '')
			{
				$sqlstr .= $line;
				if (substr(trim($line), -1, 1) == ';')
				{
					$this->query($sqlstr);
					$sqlstr = '';
				}
			}
		}
		unset($lines);
	}

	/**
	 * Create a new table
	 *
	 * @param string	$name		Table Name
	 * @param string	$definition	Table definition
	 * @param boolean	$drop_first	Drop table before creating it again
	 */
	public function TableCreate($name,$definition,$drop_first=true)
	{
		if($drop_first)
			$this->query("DROP TABLE IF EXISTS `$name`");
		if(!$this->TableExists($name))
			$this->query("CREATE TABLE `$name` ( $definition ) ENGINE={$this->storage_engine} ;");
	}

	/**
	 * Drop all tables
	 *
	 * @param string $db_name
	 */
	public function TablesDrop($db_name)
	{
		$current_db = $this->db_name;
		$db =& Db::globaldb();
		$db->select_db("INFORMATION_SCHEMA");
		$rows = array();
		$sqlstr = "SELECT table_name FROM tables WHERE table_schema='$db_name' ";
		$this->array_query( $rows, $sqlstr, 0, -1, 3);
		$this->select_db($db_name);
		$this->query("SET foreign_key_checks = 0");
		foreach($rows as $table) {
			$this->query("DROP TABLE IF EXISTS {$table['table_name']}");
		}
		$this->query("SET foreign_key_checks = 1");
		$this->select_db($current_db);
	}
	
	/**
	 * Check if a table exist in current database
	 *
	 * @param string $table
	 * @return boolean
	 */
	public function TableExists($table)
	{
		$current_db = $this->db_name;
		$this->select_db("INFORMATION_SCHEMA");
		$row = array();
		$sqlstr = "SELECT * FROM tables WHERE table_schema='$current_db' AND table_name='$table' ";
		$this->query_single($row, $sqlstr);
		$this->select_db($current_db);
		return count($row)>0;
	}

	/**
	 * Rename a table (if it exist)
	 *
	 * @param string $old_name
	 * @param string $new_name
	 * @return boolean			Success or failure
	 */
	public function TableRename($old_name,$new_name)
	{
		if($this->TableExists($old_name))
		{
			return $this->query("RENAME TABLE `$old_name` TO `$new_name` ");
		}
		return false;
	}

	/**
	 * Retrieve current DB info
	 *
	 * @return string
	 */
	function ServerInfo()
	{
	    return mysqli_get_server_info($this->database);
	}
	
	/**
	 * Retrieve current DB schema version
	 *
	 * @return integer
	 */
	function Version()
	{
		$row = array();
		$this->query_single( $row, "SELECT db_version FROM global WHERE id_install=1");
		return $row['db_version'];
	}

	/**
	 * Set DB schema version
	 *
	 * @param integer $id_version
	 */
	function VersionSet($id_version)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "global" );
		$res[] = $db->query("UPDATE global SET db_version='$id_version' ");
		Db::finish( $res, $db);
	}

	/**
	 * Check if the DB link still connected or active
	 *
	 *
	 */
	function Ping()
	{
		if(mysqli_ping($this->database) === FALSE)
		{
			return false;
		}
		return true;
	}
}

?>
