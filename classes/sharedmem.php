<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/session.php");
include_once(SERVER_ROOT."/../classes/config.php");

/**
 * Shared memory space
 * If a shared memory provider is available, it's used
 * Otherwise it falls back to user session
 * 
 * Current support:
 * - APC
 * 
 * Use this memory space for all variable that can be shared across user sessions
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class SharedMem
{
	/**
	 * The shared memory type: either "apc" or "session"
	 *
	 * @var string
	 */
	private $type;
	
	/** 
	 * @var Session */
	private $session;
	
	/**
	 * If run in isolated mode (i.e. variables are different for each shared PhPeace installation on the same web server istance
	 *
	 * @var boolean
	 */
	private $isolated;
	
	/**
	 * Prefix to identify this PhPeace instance
	 * (usually the absolute path to the local installation)
	 *
	 * @var string
	 */
	private $host_prefix;
	
	/**
	 * The Time To Loose of memory variables (in seconds)
	 * If set to 0, they will live forever until programmatically unset
	 *
	 * @var integer
	 */
	private $ttl;
		
	/**
	 * By default, the constructor sets isolated mode, i.e. variables are prefixed with the realpath,
	 * in order to avoid collisions between shared virtual hosts
	 *
	 * However you may want to share variables across various PhPeace installations
	 * (for example, localization files) in which case you instantiate this class with $isolated = false
	 *
	 * @param boolean $isolated
	 */
	function __construct($isolated=true)
	{
		$this->isolated = $isolated;
		$conf = new Configuration();
		$caching = $conf->Get("caching");
		$this->ttl = $conf->Get("apc_cache_ttl");
		$this->type = ($caching=="apc" && function_exists("apc_fetch"))? "apc" : "session";
		if($this->type=="session")
		{
			$this->session = new Session();
		}
		else 
		{
			$this->host_prefix = "phpeace" . str_replace("/","|",realpath("../../"));
		}
	}
	
	/**
	 * If shared memory is running in isolate mode, 
	 * this method adds a prefix to the variable's name 
	 * with the identifier for this virtual host, 
	 * in order to avoid collisions with other virtual hosts
	 * 
	 * @param string $name
	 * @return string
	 */
	private function UniqueHostVar($name)
	{
		return $this->isolated? "{$this->host_prefix}|{$name}" : $name;
	}
	
	/**
	 * Removed a variable from memory
	 *
	 * @param string $name
	 */
	public function Delete($name)
	{
		switch($this->type)
		{
			case "session":
				$this->session->Delete($name);
			break;
			case "apc";
				apc_delete($this->UniqueHostVar($name));				
			break;
		}
	}
	
	/**
	 * Retrieves a variable from memory
	 *
	 * @param string $name
	 *   Variable's name
	 * @return object
	 *   Variable's value (can be integer, string, boolean, array)
	 */
	public function Get($name)
	{
		switch($this->type)
		{
			case "session":
				$value = $this->session->Get($name);
			break;
			case "apc";
				$value = apc_fetch($this->UniqueHostVar($name));
			break;
		}
		return $value;
	}
	
	/**
	 * Is APC used for shared memory
	 *
	 * @return boolean
	 */
	public function IsAPC()
	{
		return $this->type=="apc";
	}
	
	/**
	 * Checks whether a variable is set
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function IsVarSet($name)
	{
		switch($this->type)
		{
			case "session":
				$is_set = $this->session->IsVarSet($name);
			break;
			case "apc";
				$is_set = (apc_fetch($this->UniqueHostVar($name)) !== false);
			break;
		}
		return $is_set;
	}
	
	/**
	 * Set a variable in memory
	 *
	 * @param string $name
	 *   Variable's name
	 * @param object $value
	 *   Variable's value (can be integer, string, boolean, array)
	 * @param integer $ttl
	 *   The expiration time (in seconds) for the variable; if set to 0, it lasts until manually unset
	 */
	public function Set($name,$value,$ttl=0)
	{
		switch($this->type)
		{
			case "session":
				$this->session->Set($name,$value);
			break;
			case "apc";
				$apc_ttl = ($ttl>0)? $ttl : $this->ttl;
				apc_store($this->UniqueHostVar($name),$value,$apc_ttl);
			break;
		}
	}
	
	/**
	 * Deletes all variables in shared memory
	 *
	 */
	public function ResetAll()
	{
		switch($this->type)
		{
			case "session":
				$this->session->DeleteAll();
			break;
			case "apc";
				if($this->isolated)
				{
					$prefix = $this->UniqueHostVar("");
					$this->ResetAllPrefix($prefix);					
				}
				else 
				{
					apc_clear_cache("user");
				}
			break;
		}
	}

	/**
	 * Resets all variables in shared memory
	 * whose name starts with $var_prefix
	 *
	 * @param string $var_prefix
	 */
	public function ResetAllPrefix($var_prefix)
	{
		switch($this->type)
		{
			case "session":
				$all_vars = $this->session->All();
				foreach($all_vars as $var)
				{
					if(strpos($var,$var_prefix)!==false)
					{
						$this->session->Delete($var);
					}
				}
			break;
			case "apc";
				$all_vars = apc_cache_info("user");
				$var_prefix = $this->UniqueHostVar($var_prefix);
				$var_list = $all_vars['cache_list'];
				if(is_array($var_list))
				{
					foreach($var_list as $var)
					{
						$varname = $var['info'];
						if(strpos($varname,$var_prefix)!==false)
						{
							apc_delete($varname);
						}
					}					
				}
			break;
		}
	}
	
}
?>
