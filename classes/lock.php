<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/file.php");

class Lock
{
	/**
	 * @var FileManager */
	private $fm;
	
	private $filename;
	
	function __construct($type)
	{
		$this->fm = new FileManager();
		$this->filename = "$type.lock";
	}

	public function LockCheck()
	{
		return $this->fm->Exists($this->filename);
	}
	
	public function LockLastTime()
	{
		return $this->fm->FileMTime($this->filename);
	}
	
	public function LockRemove()
	{
		if ($this->LockCheck($this->filename))
			$this->fm->Delete($this->filename);
	}
	
	public function LockSet()
	{
		$this->fm->WritePage($this->filename,"");
	}

}
?>
