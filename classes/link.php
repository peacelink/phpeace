<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Link
{
	public $id;
	public $url;
	public $error;

	/** 
	 * @var History */
	private $h;

	function __construct( $id_link )
	{
		$this->id = $id_link;
		include_once(SERVER_ROOT."/../classes/history.php");
		$this->h = new History();
	}

	public function Check()
	{
		$url = $this->url;
		$this->error = 0;
		$error_desc = "OK";
		if (!(preg_match('/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?/i', $url)))
		{
			$this->error = 1;
			$error_desc = "Invalid url";
		}
		else
		{
			$url_array = parse_url($url);
			$host = $url_array['host'];
			$errno = "";
			$errstr = "";
			set_error_handler("ErrorTrash");
			$fp = @fsockopen ($host, 80, $errno, $errstr, 5);
			if (!$fp)
			{
				$this->error = 1;
				$error_desc = "Host not responding - $errstr ($errno)";
			}
			else
			{
				fputs($fp, "GET $url HTTP/1.0\r\n\r\n\r\n");
				$got = fgets($fp, 256);
				$parts = array();
				if (preg_match("@HTTP/1.(.) ([0-9]*) (.*)@i", $got, $parts) > 0)
				{
					$error_num = $parts[2];
					if ($error_num>400)
					{
						$this->error = 1;
						$error_desc = $got;
					}
				}
				else
				{
					$this->error = 1;
					$error_desc = "Bad Comm";
				}
				fclose ($fp);
			}
			set_error_handler("ErrorHandler");
		}
		return $error_desc;
	}

	public function LinkDelete()
	{
		$row = $this->LinkGet();
		$this->QueueAdd($row[id_topic],$row[id_subtopic]);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "links" );
		$res[] = $db->query( "DELETE FROM links WHERE id_link=$this->id" );
		$db->lock( "links_subtopics" );
		$res[] = $db->query( "DELETE FROM links_subtopics WHERE id_link=$this->id" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($this->id,$o->types['link']);
		$this->h->HistoryAdd($this->h->types['link'],$this->id,$this->h->actions['delete']);
	}

	public function LinkGet()
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,"SELECT links.id_link,url,title,links.description,vote,approved,error404,id_language,
		links_subtopics.id_topic,links_subtopics.id_subtopic,subtopics.name,UNIX_TIMESTAMP(insert_date) AS insert_date
			FROM links
			LEFT JOIN links_subtopics USING(id_link)
			LEFT JOIN subtopics USING(id_subtopic)
			WHERE links.id_link=$this->id");
		return $row;
	}

	public function LinkInsert($insert_date,$url,$title,$description,$keywords,$vote,$id_language,$id_topic,$id_subtopic,$approved,$keywords_internal)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$ip = $v->IP();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "links" );
		$id_link = $db->nextId( "links", "id_link" );
		$sqlstr = "INSERT INTO links (id_link,url,title,description,vote,id_language,approved,insert_date,ip)
				VALUES ($id_link,'$url','$title','$description',$vote,$id_language,$approved,'$insert_date','$ip')";
		$res[] = $db->query( $sqlstr );
		$db->lock( "links_subtopics" );
		$res[] = $db->query("INSERT INTO links_subtopics (id_link,id_subtopic,id_topic) VALUES ($id_link,$id_subtopic,$id_topic)");
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_link, $o->types['link']);
		$o->InsertKeywordsArray($keywords_internal,$id_link,$o->types['link']);
		$this->h->HistoryAdd($this->h->types['link'],$id_link,$this->h->actions['create']);
		if ($approved)
		{
			$this->h->HistoryAdd($this->h->types['link'],$id_link,$this->h->actions['approve']);
			include_once(SERVER_ROOT."/../classes/topic.php");
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$t = new Topic($id_topic);
			$subtopic= $t->SubtopicGet($id_subtopic);
			if($t->visible && $subtopic['visible']<3)
				$s->IndexQueueAdd($o->types['link'],$id_link,$id_topic,$t->id_group,1);
		}
		$this->QueueAdd($id_topic,$id_subtopic);
	}

	public function LinkUpdate($error404,$insert_date,$url,$title,$description,$keywords,$vote,$id_language,$id_topic,$id_subtopic,$approved,$approved_old,$keywords_internal)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "links" );
		$sqlstr = "UPDATE links
			SET url='$url',title='$title',description='$description',vote=$vote,id_language=$id_language,
			approved=$approved,error404=$error404,insert_date='$insert_date'
			WHERE id_link=$this->id";
		$res[] = $db->query( $sqlstr );
		$db->lock( "links_subtopics" );
		$res[] = $db->query("UPDATE links_subtopics SET id_subtopic=$id_subtopic,id_topic=$id_topic WHERE id_link=$this->id");
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $this->id, $o->types['link']);
		$o->InsertKeywordsArray($keywords_internal,$this->id,$o->types['link']);
		$this->QueueAdd($id_topic,$id_subtopic);
		if ($approved!=$approved_old)
		{
			if ($approved)
				$this->h->HistoryAdd($this->h->types['link'],$this->id,$this->h->actions['approve']);
			else
				$this->h->HistoryAdd($this->h->types['link'],$this->id,$this->h->actions['reject']);
		}
		else
		{
			$this->h->HistoryAdd($this->h->types['link'],$this->id,$this->h->actions['update']);
		}
		include_once(SERVER_ROOT."/../classes/search.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$s = new Search();
		if($approved)
		{
			$t = new Topic($id_topic);
			$subtopic= $t->SubtopicGet($id_subtopic);
			if($t->visible && $subtopic['visible']<3)
				$s->IndexQueueAdd($o->types['link'],$this->id,$id_topic,$t->id_group,1);
		}
		elseif($approved_old)
			$s->ResourceRemove($o->types['link'],$this->id);

	}

	private function QueueAdd($id_topic,$id_subtopic)
	{
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($id_topic);
		$t->queue->JobInsert($t->queue->types['subtopic'],$id_subtopic,"update");
	}

	public function Set404( $value=1 )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "links" );
		$res[] = $db->query( "UPDATE links set error404=$value WHERE id_link=$this->id" );
		Db::finish( $res, $db);
	}

}
?>
