<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

abstract class Modules
{
	public static function Activate($id_module,$active=1)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "modules" );
		$res[] = $db->query( "UPDATE modules SET active='$active' WHERE id_module='$id_module' " );
		Db::finish( $res, $db);
		$mod = self::ModuleGet($id_module);
		include_once(SERVER_ROOT."/../classes/log.php");
		$log = new Log();
		$log->Write("info","Module {$mod['path']} has been " . ($active?"activated":"deactivated"));
		if ($mod['layout'] && !$mod['internal'])
		{
			include_once(SERVER_ROOT."/../classes/xsl.php");
			$xslm = new XslManager("module");
			$xslm->id_pagetype = $id_module;
			include_once(SERVER_ROOT."/../classes/css.php");
			$csm = new CssManager("module");
			$csm->id_pagetype = $id_module;
			if($active==1)
			{
				$id_xsl = $xslm->XslCreate("","",$mod['global']=="1");
				$xslm->XslDownload($id_xsl);
				$csm->CssCreate("","",$mod['global']=="1");
			}
			else 
			{
				// Let's not do this for now
				// $xslm->XslDeleteAll();
				// $csm->CssDeleteAll();
			}
		}
	}
	
	public static function AmIAdmin($id_module)
	{
		$admin = array();
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$module_admin = FALSE;
		$db =& Db::globaldb();
		$db->query_single( $admin, "SELECT id_user FROM module_users WHERE id_user='" . $session->Get('current_user_id') . "' AND is_admin=1 AND id_module=$id_module");
		if ($admin['id_user']>0)
			$module_admin = TRUE;
		return $module_admin;
	}
	
	public static function AmIUser($id_module)
	{
		$row = array();
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$module_user = FALSE;
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT id_user FROM module_users WHERE id_user='" . $session->Get('current_user_id') . "' AND id_module=$id_module");
		if ($row['id_user']>0)
			$module_user = TRUE;
		return $module_user;
	}
	
	public static function AvailableModules($active_only=true)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_module,layout,internal,path,sort,restricted,admin,global FROM modules";
		if($active_only)
			$sqlstr .= " WHERE active=1 ";
		$sqlstr .= " ORDER BY id_module";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public static function IsActive($id_module)
	{
		$row = self::ModuleGet($id_module);
		return ($row['active']=="1");
	}
	
	public static function IsActiveByPath($module_path)
	{
		$row = self::ModuleGetId($module_path);
		return ($row['active']=="1");
	}
	
	public static function ModuleDeleteUsers($id_module, $exclude_admins=FALSE)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "module_users" );
		$sqlstr = "DELETE FROM module_users WHERE id_module=$id_module";
		if ($exclude_admins)
			$sqlstr .= " AND is_admin=0";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public static function ModuleInsertUser( $id_user, $is_admin, $id_module )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "module_users" );
		$res[] = $db->query( "INSERT INTO module_users (id_user,is_admin,id_module) VALUES ($id_user,$is_admin,$id_module)" );
		Db::finish( $res, $db);
	}

	public static function ModuleGet( $id_module )
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT restricted,active,path,layout,internal,admin,global FROM modules WHERE id_module=$id_module");
		return $row;
	}

	public static function ModuleGetId( $path )
	{
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row,"SELECT id_module,restricted,active FROM modules WHERE path='$path'");
		return $row;
	}

	public static function ModuleInsert($id_module,$path,$sort,$is_restricted,$has_layout,$is_internal,$is_active,$has_admin)
	{
		$row= array();
		$db =& Db::globaldb();
		$db->query_single( $row, "SELECT id_module FROM modules WHERE path='$path' ");
		if(!$row['id_module']>0)
		{
			$db->begin();
			$db->lock( "modules" );
			$res[] = $sqlstr = "INSERT INTO modules (id_module,path,sort,restricted,layout,internal,active,admin) 
				VALUES ($id_module,'$path',$sort,$is_restricted,$has_layout,$is_internal,$is_active,$has_admin)";
			$db->query($sqlstr);
			Db::finish( $res, $db);
		}
		if($has_layout)
		{
			self::ModuleLayout($path,$id_module);
		}
	}
	
	public static function ModuleLayout($path,$id_module)
	{
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles;
		$styles = $s->StylesAll();
		
		include_once(SERVER_ROOT."/../classes/xsl.php");
		$xslm = new XslManager("module");
		$xsl_empty = $xslm->TypeInit($path);
		$xslm->id_pagetype = $id_module;
		foreach($styles as $style)
		{
			$xslm->XslInsert(0,$style['id_style'],"",$xsl_empty);
		}
		
		include_once(SERVER_ROOT."/../classes/css.php");
		$csm = new CssManager("module");
		$csm->id_pagetype = $id_module;
		foreach($styles as $style)
		{
			$csm->CssInsert(0,$style['id_style'],"",CSS_EMPTY);
		}		
	}
	
	public static function ModuleUpdate( $id_module, $restricted )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "modules" );
		$res[] = $db->query( "UPDATE modules SET restricted=$restricted WHERE id_module='$id_module' " );
		Db::finish( $res, $db);
		if ($restricted=="0")
			self::ModuleDeleteUsers($id_module,true);
	}

	public static function ModuleUpdateUser( $id_user, $is_admin, $id_module )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "module_users" );
		$res[] = $db->query( "UPDATE module_users SET is_admin=$is_admin WHERE id_user='$id_user' AND id_module='$id_module' " );
		Db::finish( $res, $db);
	}

	public static function ModuleUsers( &$rows, $id_module )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT users.id_user,users.name,module_users.is_admin FROM users
		INNER JOIN module_users ON users.id_user=module_users.id_user 
		WHERE module_users.id_module=$id_module AND active=1 ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public static function ModulesInfo()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_module,layout,internal,active FROM modules";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public static function ModulesUsers( &$rows )
	{
		$db =& Db::globaldb();
		$rows2 = array();
		$sqlstr = "SELECT m.id_module,if(restricted=1,count(mu1.id_user),'-') AS users,count(mu2.is_admin) AS admins
			FROM modules m
			LEFT JOIN module_users mu1 ON m.id_module=mu1.id_module
			LEFT JOIN module_users mu2 ON mu1.id_module=mu2.id_module AND mu1.id_user=mu2.id_user AND mu2.is_admin=1 
			WHERE m.admin=1 AND m.internal=1 OR (m.internal=0 AND m.active=1) ";
		$sqlstr .= " GROUP BY m.id_module ORDER BY m.sort";
		$num = $db->QueryExe($rows2, $sqlstr, true);
		include_once(SERVER_ROOT."/../classes/translator.php");
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$tr = new Translator($session->Get("id_language"),0);
		$t_modules = $tr->Translate("modules_names");
		foreach($rows2 as $key => $val)
		{
			$val['name'] = $t_modules[$val['id_module']];
			$rows[$key] = $val;
		}		
		return $num;
	}

}
?>
