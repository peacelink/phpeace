<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/xmlhelper.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class XslManager
{
	/**
	 * @var XmlHelper */
	public $xh;
		
	public $type; // specific,global,ext,custom,module
	public $id_pagetype;
	public $output_doctype;
	
	public $output_indent;
	private $table;
	private $versioning;

	function __construct($type="specific")
	{
		$this->xh = new XmlHelper();
		$this->table = ($type=="specific")? "xsl" : "xsl_" . $type;
		$this->type = $type;
		$ini = new Ini;
		$this->versioning = $ini->GetModule("layout","gra_versioning",1);
		$this->output_doctype = $ini->GetModule("layout","xsl_output_doctype",XSL_DEFAULT_OUTPUT_DOCTYPE);
		$this->output_indent = $ini->GetModule("layout","xsl_output_indent",1);
	}

	public function ConfigurationUpdate($xsl_output_indent,$xsl_output_doctype)
	{
		$changed = false;
		if($this->output_indent!=$xsl_output_indent || $this->output_doctype!=$xsl_output_doctype)
		{
			$ini = new Ini;
			$ini->SetModule("layout","xsl_output_indent",$xsl_output_indent);
			$ini->SetModule("layout","xsl_output_doctype",$xsl_output_doctype);
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles();
			$s->XslUpdateAll();
			$changed = true;
		}
		return $changed;
	}
	
	public static function DocTypes()
	{
		$doctypes = array();
		$doctypes['html4_strict'] = array('public'=>"-//W3C//DTD HTML 4.01//EN",'system'=>"http://www.w3.org/TR/html4/strict.dtd");
		$doctypes['html4_transitional'] = array('public'=>"-//W3C//DTD HTML 4.01 Transitional//EN",'system'=>"http://www.w3.org/TR/html4/loose.dtd");
		$doctypes['html4_frameset'] = array('public'=>"-//W3C//DTD HTML 4.01 Frameset//EN",'system'=>"http://www.w3.org/TR/html4/frameset.dtd");
		$doctypes['xhtml10_strict'] = array('public'=>"-//W3C//DTD XHTML 1.0 Strict//EN",'system'=>"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd");
		$doctypes['xhtml10_transitional'] = array('public'=>"-//W3C//DTD XHTML 1.0 Transitional//EN",'system'=>"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd");
		$doctypes['xhtml10_frameset'] = array('public'=>"-//W3C//DTD XHTML 1.0 Frameset//EN",'system'=>"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd");
		$doctypes['xhtml11_strict'] = array('public'=>"-//W3C//DTD XHTML 1.1//EN",'system'=>"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd");
		$doctypes['html5'] = array('public'=>"",'system'=>"about:legacy-compat");
		return $doctypes;
	}
	
	private function Output($type,$fix)
	{
		$doctypes = $this->DocTypes();
		$doctype = (array_key_exists($fix['doctype_id'],$doctypes))? $doctypes[$fix['doctype_id']] : $doctypes['html4_transitional'];
		$output = "<xsl:output method=\"{$fix['method']}\" ";
		$output .= "encoding=\"". $this->xh->encoding .  "\" ";
		if($fix['method']!="text")
			$output .= "indent=\"{$fix['indent']}\" ";
		if($fix['method']=="xml")
			$output .= "omit-xml-declaration=\"" . (($type=="rss")?"no":"yes") . "\" ";
		if($fix['show_doctype'])
		{
			if($fix['doctype_id']=="html5")
				$output .= "doctype-system=\"{$doctype['system']}\" media-type=\"text/html\" ";
			else
				$output .= "doctype-system=\"{$doctype['system']}\" doctype-public=\"{$doctype['public']}\" ";
		}
		$output .= "/>";
		return $output;
	}
	
	private function OutputFix($type)
	{
		$indent = ($this->output_indent)? "yes" : "no";
		$doctype_id = $this->output_doctype;
		$show_doctype = true;
		$method = ($doctype_id=="html4_strict" || $doctype_id=="html4_transitional" || $doctype_id=="html4_frameset" || $doctype_id=="html5")? "html" : "xml";
		if(($this->type=="global" && $type=="gnewsletter") || ($this->type=="specific" && $type=="newsletter"))
		{
			$method = "text";
			$indent = "no";
			$show_doctype = false;
		}
		if($this->type=="specific" && $type=="random_item")
		{
			$method = "xml";
			$indent = "no";
			$show_doctype = false;
		}
		if($this->type=="specific" && $type=="ebook")
		{
			$method = "xml";
			$indent = "yes";
			$show_doctype = true;
			$doctype_id = "xhtml11_strict";
		}
		if(($this->type=="global" && $type=="rss"))
		{
			$method = "xml";
			$indent = "no";
			$show_doctype = false;
		}
		return array('method'=>$method,'indent'=>$indent,'show_doctype'=>$show_doctype,'doctype_id'=>$doctype_id);
	}
	
	public function OutputUpdate($xsl,$type)
	{
		$fix = $this->OutputFix($type);
		$xs = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"";
		if($fix['method']=="xml" && $type!="random_item")
		{
			if($type=="rss")
				$xs .= " xmlns:content=\"http://purl.org/rss/1.0/modules/content/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" ";
			else 
				$xs .= " xmlns=\"http://www.w3.org/1999/xhtml\"";
		}
		if($type=="tools")
			$xs .= " xmlns:g=\"http://base.google.com/ns/1.0\"";
		$xs .= ">";
		$xsl = preg_replace("'<xsl:stylesheet[^>]*?>'si",$xs,$xsl);
		$output = $this->Output($type,$fix);
		$xsl = preg_replace("'<xsl:output[^>]*?>'si",$output,$xsl);
		return $xsl;
	}
	
	public function RevisionGet($revision)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT UNIX_TIMESTAMP(change_time) AS change_time_ts,xh.*
		FROM xsl_history xh 
		WHERE xh.revision='$revision' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function Revisions(&$rows,$id_xsl,$id_style)
	{
		$id_type = (int)$this->id_pagetype;
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = " SELECT UNIX_TIMESTAMP(change_time) AS change_time_ts,xh.*
		FROM xsl_history xh
		WHERE xh.type='$this->type'  AND xh.id_style='$id_style' ";
		if($this->type=="custom" || $this->type=="ext")
			$sqlstr .= " AND xh.id_xsl='$id_xsl' ";
		else
			$sqlstr .= " AND xh.id_pagetype='$id_type' ";
		$sqlstr .= " ORDER BY xh.revision DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function RevisionsDelete()
	{
		$db =& Db::globaldb();
		$db->query( "TRUNCATE xsl_history" );
	}

	public function StyleAdd($id_style,$id_parent=0) // used only by ext and module
	{
		$xsls = $this->XslAll();
		$xsl_empty = $this->TypeInit("common");
		foreach($xsls as $xsl)
		{
			$insert = true;
			if ($this->type=="module")
			{
				$this->id_pagetype = $xsl['id_module'];
				include_once(SERVER_ROOT."/../classes/modules.php");
				$mod = Modules::ModuleGet($this->id_pagetype);
				$insert = $mod['global']!="1";
				$xsl_empty = $this->TypeInit($mod['path'],$id_parent);
			}
			$id_xsl = ($this->type=="module")? 0 : $xsl['id_xsl'];
			if($insert)
				$this->XslInsert($id_xsl,$id_style,$xsl['name'],$xsl_empty);
		}
	}

	public function TypeAdd($type)
	{
		include_once(SERVER_ROOT."/../classes/pagetypes.php");
		$pt = new PageTypes();
		$id_type = $pt->types[$type];
		$this->id_pagetype = $id_type;
		$xsl0 = $this->TypeInit($type,-1);
		$this->XslInsert(0,0,"",$xsl0,1);
		foreach($this->XslStyles() as $style)
		{
			$xsl = $this->TypeInit($type,$style['id_parent']);
			$this->XslInsert(0,$style['id_style'],"",$xsl);
		}
	}
	
	public function TypeInit($type,$id_parent_style=0)
	{
		$fix = $this->OutputFix($type);
		$page = "<?xml version=\"1.0\"?>\n";
		$page .= "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"";
		if($fix['method']=="xml")
			$page .= " xmlns=\"http://www.w3.org/1999/xhtml\"";
		$page .= ">\n\n";
		if ($type!="common")
		{
			if($type!="root" && $type!="tools")
			{
				if ($id_parent_style>=0)
				{
					$page .= "<xsl:import href=\"../{$id_parent_style}/{$type}.xsl\" />\n\n";
				}
				$page .= $this->Output($type,$fix) . "\n\n";
				$page .= "<xsl:include href=\"common.xsl\" />\n\n";
			}
		}
		else
		{
			$page .= "<xsl:include href=\"root.xsl\" />\n";
			$page .= "<xsl:include href=\"tools.xsl\" />\n\n";
		}
		$page .= "</xsl:stylesheet>\n";
		$page = $this->OutputUpdate($page,$type);
		return $page;
	}
	
	public function XslAll($id_style=0) // used only by ext, module, custom
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_xsl" . (($this->type!="module")? ",name":",id_module") . " FROM $this->table WHERE id_style=$id_style";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function XslAllP( &$rows ) // used only by ext, custom, module
	{
		$db =& Db::globaldb();
		$rows = array();
		if ($this->type=="ext")
			$sqlstr = "SELECT id_xsl,name FROM $this->table WHERE id_style=0 ORDER BY name";
		if ($this->type=="custom")
			$sqlstr = "SELECT id_xsl,x.name,x.id_style,s.name AS style FROM $this->table x
			 LEFT JOIN styles s USING (id_style)  ORDER BY x.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function XslAllXsl($id_style)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_xsl,xsl";
		if ($this->type=="specific")
			$sqlstr .= ",id_type";
		if ($this->type=="module")
			$sqlstr .= ",id_module AS id_type";
		if ($this->type=="global")
			$sqlstr .= ",id_xsl AS id_type";
		$sqlstr .= " FROM $this->table";
		if ($this->type!="global")
			$sqlstr .= " WHERE id_style=$id_style";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function XslCreate($name,$xsl,$global=false) // used only by ext, module
	{
		$styles = $this->XslStyles();
		switch($this->type)
		{
			case "module":
				include_once(SERVER_ROOT."/../classes/modules.php");
				$mod = Modules::ModuleGet($this->id_pagetype);
				$filename = $this->XslFilename(0,0);
				include_once(SERVER_ROOT."/../classes/file.php");
				$fm = new FileManager;
				if($fm->Exists($filename))
					$xsl = $fm->TextFileRead($filename);
				else
					$xsl = $this->TypeInit($mod['path'],-1);
				$this->XslInsert(0,0,"",$xsl,1);
				$id_xsl = 0;
				if(!$global)
				{
					foreach($styles as $style)
					{
						$xsl_empty = $this->TypeInit($mod['path'],$style['id_parent']);
						$this->XslInsert($id_xsl,$style['id_style'],$name,$xsl_empty);
					}
				}
			break;
			case "ext":
				$xsl_empty = $this->TypeInit("root");
				$id_xsl = $this->XslInsert(0,0,$name,$xsl);
				foreach($styles as $style)
				{
					$this->XslInsert($id_xsl,$style['id_style'],$name,$xsl_empty);
				}
			break;
		}
		return $id_xsl;
	}
	
	public function XslDelete($id_xsl,$id_style=0) // used by ext and custom only
	{
		$this->XslRemove($id_xsl,$id_style);
		if ($this->type=="ext")
		{
			$styles = $this->XslStyles();
			foreach($styles as $style)
			{
				$this->XslRemove($id_xsl,$style['id_style']);
			}
		}
	}
	
	/**
	 * Delete all XSL of current module
	 * Used by module only
	 *
	 */
	public function XslDeleteAll()
	{
		$xsl = $this->XslGetByType(0);
		$this->XslRemove($xsl['id_xsl'],0);
		foreach($this->XslStyles() as $style)
		{
			$xsl = $this->XslGetByType($style['id_style']);
			$this->XslRemove($xsl['id_xsl'],$style['id_style']);
		}
	}
	
	public function XslDeleteStyle($id_style)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$res[] = $db->query( "DELETE FROM $this->table WHERE id_style=$id_style" );
		Db::finish( $res, $db);
	}
	
	public function XslDownload($id_xsl)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		$phpeace = new PhPeace();
		if(!$phpeace->main && $phpeace->phpeace_server!="")
		{
			include_once(SERVER_ROOT."/../classes/db.php");
			$db =& Db::globaldb();
			$filename =  $this->XslFile($id_xsl);
			$fm->DirAction("xsl0","check");
			$xsl = $fm->DownLoad($phpeace->phpeace_server . "/xsl0/$filename" ,"xsl0/$filename",true);
			if($fm->Exists("xsl0/$filename"))
			{
				$xsl = $fm->TextFileRead("xsl0/$filename");
				if ($this->xh->Check($xsl))
				{
					$this->XslStore($id_xsl,0,"",$db->SqlQuote($xsl));
					$this->XslWrite(0,$id_xsl,$xsl);
				}
				$fm->Delete("xsl0/$filename");
			}
			$fm->DirDelete("xsl0");
			$fm->PostUpdate();
		}
	}
	
	public function XslFile($id_xsl)
	{
		$filename = $this->type . "_" . $id_xsl;  // custom
		if ($this->type=="specific" || $this->type=="global")
		{
			include_once(SERVER_ROOT."/../classes/pagetypes.php");
			$pt = new PageTypes();
			$filename = array_search($this->id_pagetype,($this->type=="specific")? $pt->types: $pt->gtypes);
		}
		if ($this->type=="module")
		{
			include_once(SERVER_ROOT."/../classes/modules.php");
			$mod = Modules::ModuleGet($this->id_pagetype);
			$filename = $mod['path'];
		}
		return $filename . ".xsl";
	}
	
	private function XslFilename($id_style,$id_xsl)
	{
		return XSL_PATH . "/$id_style/" . $this->XslFile($id_xsl);
	}
	
	public function XslForkSwap($id_xsl,$outsourced)
	{
		$new_value = ($outsourced=="1")? "0" : "1";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "UPDATE $this->table SET outsourced=$new_value WHERE id_xsl=$id_xsl";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($new_value=="1")
		{
			$this->XslDownload($id_xsl);
		}
	}
	
	public function XslGet($id_xsl, $id_style)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_xsl,xsl" . (($this->type!="global")? ",id_style":"") .
		(($this->type!="specific" && $this->type!="global")? ",name":",outsourced") .
		" FROM $this->table WHERE id_xsl=$id_xsl" . 
		(($this->type!="global" && $this->type!="custom")? " AND id_style=$id_style":"") ;
		$db->query_single($row, $sqlstr);
	return $row;
	}

	public function XslGetByType($id_style) // used only by specific and module
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT x.id_xsl,x.xsl,s.name,outsourced FROM $this->table x LEFT JOIN styles s ON x.id_style=s.id_style
			WHERE " .  (($this->type=="specific")? "x.id_type":"x.id_module" )  . "=$this->id_pagetype AND x.id_style=$id_style ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function XslGetLocal($id_xsl,$id_style)
	{
		$filename = $this->XslFilename($id_style,$id_xsl);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$xsl = $fm->TextFileRead($filename);
		return $xsl;
	}

	public function XslInsert($id_xsl,$id_style,$name,$unescaped_xsl,$outsourced=0)
	{
		$db =& Db::globaldb();
		$xsl = $db->SqlQuote($unescaped_xsl);
		$db->begin();
		$db->lock( $this->table );
		$id_xsl_new = ($id_xsl>0  || $this->type=="global")? $id_xsl : $db->nextId( $this->table, "id_xsl" );
		$sqlstr = "INSERT INTO $this->table ";
		if ($this->type=="specific")
			$sqlstr .= "(id_xsl,id_type,id_style,xsl,outsourced) VALUES ($id_xsl_new,$this->id_pagetype,$id_style,'$xsl',$outsourced)";
		elseif($this->type=="global")
			$sqlstr .= "(id_xsl,xsl,outsourced) VALUES ($id_xsl_new,'$xsl',$outsourced)";
		elseif($this->type=="module")
			$sqlstr .= "(id_xsl,id_module,id_style,xsl,outsourced) VALUES ($id_xsl_new,$this->id_pagetype,$id_style,'$xsl',$outsourced)";
		else
			$sqlstr .= "(id_xsl,id_style,name,xsl) VALUES ($id_xsl_new,$id_style,'$name','$xsl')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($this->xh->Check($unescaped_xsl,FALSE))
			$this->XslWrite($id_style,$id_xsl_new,$unescaped_xsl);
		return $id_xsl_new;
	}
	
	public function XslIsOutsourced($id_type)
	{
		$row = array();
		$outsourced = false;
		$db =& Db::globaldb();
		if ($this->type=="global")
			$sqlstr = "SELECT outsourced FROM $this->table WHERE id_xsl=$id_type";
		elseif ($this->type=="module")
			$sqlstr = "SELECT outsourced FROM $this->table WHERE id_style=0 AND id_module=$id_type";
		else
			$sqlstr = "SELECT outsourced FROM $this->table WHERE id_style=0 AND id_type=$id_type";
		$db->query_single($row, $sqlstr);
		if ($row['outsourced'])
			$outsourced = true;
		return $outsourced;
	}

	public function XslOutsourced($outsourced=1)
	{
		$db =& Db::globaldb();
		$rows = array();
		if ($this->type=="global")
			$sqlstr = "SELECT id_xsl FROM $this->table WHERE outsourced=$outsourced";
		elseif ($this->type=="module")
			$sqlstr = "SELECT id_xsl,id_module FROM $this->table WHERE id_style=0 AND outsourced=$outsourced";
		else
			$sqlstr = "SELECT id_xsl,id_type FROM $this->table WHERE id_style=0 AND outsourced=$outsourced";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	private function XslRemove($id_xsl,$id_style)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "DELETE FROM $this->table WHERE id_xsl=$id_xsl AND id_style=$id_style";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->Delete($this->XslFilename($id_style,$id_xsl));
		$fm->PostUpdate();
	}
	
	public function XslStore($id_xsl,$id_style,$name,$xsl)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "UPDATE $this->table SET xsl='$xsl' ";
		if ($this->type=="custom")
			$sqlstr .= ",id_style=$id_style";
		$sqlstr .= " WHERE id_xsl=$id_xsl";
		if ($this->type=="ext")
			$sqlstr .= " AND id_style=$id_style";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		if ($this->type=="ext" || $this->type=="custom")
			$this->XslStoreName($id_xsl,$name);
		return $id_xsl;
	}
	
	private function XslStoreName($id_xsl,$name)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( $this->table );
		$sqlstr = "UPDATE $this->table SET name='$name' WHERE id_xsl=$id_xsl";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function XslStoreWrite($id_xsl,$id_style,$name,$unescaped_xsl)
	{
		if($this->versioning)
		{
			$old_xsl = $this->XslGetLocal($id_xsl,$id_style);
			$rows = array();
			$revisions = $this->Revisions($rows,$id_xsl,$id_style);
			if(!$revisions>0)
				$this->XslHistoryAdd($id_xsl,$id_style,$old_xsl,"OLD",1,true);
		}
		$db =& Db::globaldb();
		$xsl = $db->SqlQuote($unescaped_xsl);
		$id_xsl = $this->XslStore($id_xsl,$id_style,$name,$xsl);
		$valid = $this->xh->Check($unescaped_xsl,FALSE);
		if ($valid)
			$this->XslWrite($id_style,$id_xsl,$unescaped_xsl);
		if($this->versioning && trim($old_xsl) != trim($unescaped_xsl))
			$this->XslHistoryAdd($id_xsl,$id_style,$unescaped_xsl,"",$valid);
		return $id_xsl;
	}
	
	private function XslHistoryAdd($id_xsl,$id_style,$new_xsl,$tag,$valid,$system=false)
	{
		if($system)
			$id_user = 0;
		else 
		{
			include_once(SERVER_ROOT."/../classes/session.php");
			$session = new Session();
			$id_user = (int)$session->Get("current_user_id");
		}
		$db =& Db::globaldb();
		$today_time = $db->getTodayTime();
		$content = $db->SqlQuote($new_xsl);
		$db->begin();
		$db->lock( "xsl_history" );
		$revision = $db->nextId( "xsl_history", "revision" );
		$sqlstr = "INSERT INTO xsl_history (type,id_xsl,id_pagetype,id_style,revision,change_time,id_user,content,tag,valid) 
			VALUES ('$this->type','$id_xsl','$this->id_pagetype','$id_style','$revision','$today_time','$id_user','$content','$tag','$valid')  ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	private function XslStyles()
	{
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles;
		return $s->StylesAll();
	}
	
	public function XslUpdate($id_xsl,$id_style,$unescaped_xsl)
	{
		if ($this->xh->Check($unescaped_xsl,FALSE))
		{
			$db =& Db::globaldb();
			$xsl = $db->SqlQuote($unescaped_xsl);
			$db->begin();
			$db->lock( $this->table );
			$sqlstr = "UPDATE $this->table SET xsl='$xsl'  WHERE id_xsl='$id_xsl' ";
			if ($this->type=="ext")
				$sqlstr .= " AND id_style='$id_style' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			$this->XslWrite($id_style,$id_xsl,$unescaped_xsl);
		}
	}
	
	public function XslWrite($id_style,$id_xsl,$xsl)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->WritePage($this->XslFilename($id_style,$id_xsl),$xsl );
		$fm->PostUpdate();
	}

}
?>
