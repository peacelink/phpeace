<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/modules.php");
include_once(SERVER_ROOT."/../classes/config.php");

class RestService
{
	private $module;
	private $operation;
	private $parts;
	
	function __construct()
	{
		$location = $this->GetUrlLocation();
		$this->parts = explode("/",$location);
		if($this->parts[1]=="rest")
		{
			$module = $this->parts[2];
			include_once(SERVER_ROOT."/../classes/modules.php");
			if(Modules::IsActiveByPath($module))
			{
				$operation = $this->parts[3];
				$this->module = $module;
				$this->operation = $operation;
			}
		}
	}

	private function HandleRequest()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		switch ($method)
		{
			case 'GET':
				$arguments = $_GET;
				array_walk($arguments,array($this,'UrlDecodeLocal'));
			break;
			case 'HEAD':
				$arguments = $_GET;
			break;
			case 'POST':
				$arguments = $_POST;
			break;
			case 'PUT':
			case 'DELETE':
				parse_str(file_get_contents('php://input'), $arguments);
			break;
		}
		return $arguments;
	}
	
	public function Process()
	{
		$return = null;
		switch($this->module)
		{
			case "admin":
				include_once(SERVER_ROOT."/../classes/adminhelper.php");
				$class = new AdminREST();
				if(method_exists($class, $this->operation))
				{
					$arguments = $this->HandleRequest();
					$return = call_user_func_array(array($class, $this->operation), $arguments);
				}
				if(is_array($return))
				{
					$this->JsonEncode($return);
				}
				break;
			case "events":
				include_once(SERVER_ROOT."/../classes/events.php");
				$class = new EventsREST();
				if(method_exists($class, $this->operation))
				{
					$arguments = $this->HandleRequest();
					$class->arguments = $arguments;
					$return = call_user_func_array(array($class, $this->operation),array());
				}
			break;
		}
	}
	
	private function GetUrlLocation()
	{
		$location = $_SERVER['REQUEST_URI'];
		if ($_SERVER['QUERY_STRING'])
		{
			$location = substr($location, 0, strrpos($location, $_SERVER['QUERY_STRING']) - 1);
		}
		return $location;
	}
	
	private function JsonEncode($array)
	{
		header('Content-type: application/json');
		echo json_encode($array);
	}

	private function UrlDecodeLocal(&$val)
	{
		if (is_array($val))
			array_walk($val,array($this,'UrlDecodeLocal'));
   		else
			$val = urldecode($val);
	}
	
}

?>
