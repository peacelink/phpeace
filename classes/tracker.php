<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/varia.php");
define('VISITS_DIFF',15);

class Tracker
{
	/** 
	 * @var Varia */
	private $v;
	private $tracking = false;
	private $id_p = 0;
	private $track_exclude;
	private $track_exclude_ua;
	private $track_all;
	private $visitors_life;
	
	function __construct()
	{
		$conf = new Configuration();
		$this->track_exclude = $conf->Get("track_exclude");
		$this->track_exclude_ua = $conf->Get("track_exclude_ua");
		$this->track_all = $conf->Get("track_all");
		$this->visitors_life = $conf->Get("visitors_life");
		$this->v = new Varia();
	}
	
	private function IsExcluded()
	{
		$ip = $this->v->IP();
		$ua = $this->v->UserAgent();
		$excluded = false;
		foreach($this->track_exclude as $network)
		{
			if(strpos($ip,$network)!==false)
				$excluded = true;
		}
		foreach($this->track_exclude_ua as $bot)
		{
			if(strpos(strtolower($ua),strtolower($bot))!==false)
				$excluded = true;
		}
		return $excluded;
	}
	
	private function VisitorSet()
	{
		if(!$this->IsExcluded())
		{
			if($this->track_all)
			{
				$this->tracking = true;
				$identifier = $_COOKIE['VID'];
				if(ctype_alnum($identifier))
				{
					$row = array();
					$db =& Db::globaldb();
					$sqlstr = "SELECT id_p2 FROM visits_others WHERE identifier='$identifier' ";
					$db->query_single( $row, $sqlstr);
					$this->id_p = $row['id_p2'];
				}
				else
				{
					$identifier = $this->v->Uid();
					$db =& Db::globaldb();
					$db->begin();
					$db->lock( "visits_others" );
					$this->id_p = $db->nextId( "visits_others", "id_p2" );
					$sqlstr =  "INSERT INTO visits_others (id_p2,identifier) VALUES ( '$this->id_p','$identifier') ";
					$res[] = $db->query( $sqlstr );
					Db::finish( $res, $db);
				}
				$timeout = time() + 3600 * 24 * $this->visitors_life;
				setcookie("VID","$identifier",$timeout,'/');
			}
			else
			{
				include_once(SERVER_ROOT."/../classes/people.php");
				$pe = new People();
				$user = $pe->UserInfo(false);
				$this->id_p = (int)$user['id'];
				if($this->id_p >0)
					$this->tracking = true;			
			}
		}
	}
	
	public function Track($cookies_disabled,$url,$title,$width,$height,$color,$browser,$version,$os,$referer,$id_topic,$id_topic_group)
	{
		$this->VisitorSet();
		if($this->tracking)
		{
			include_once(SERVER_ROOT."/../classes/session.php");
			$session = new Session();
			$id_visit = (int)$session->Get("id_visit");
			if(!($id_visit>0))
			{
				if($cookies_disabled=="1")
				{
					$ip = $this->v->IP();
					$db =& Db::globaldb();
					$user_agent = $db->SqlQuote($this->v->UserAgent());
					$hash = $this->v->Hash("$ip|$width|$height|$color|$user_agent",false);
					$visits = array();
					$sqlstr = "SELECT id_visit,ip,hash,UNIX_TIMESTAMP(last_time) AS last_time_ts
						FROM visits_ip 
						WHERE hash='$hash' 
						ORDER BY last_time DESC LIMIT 1 ";
					$db->QueryExe($visits, $sqlstr);
					$visit = $visits[0];
					if($visit['id_visit']>0)
					{
						if(time() > ( (int)$visit['last_time_ts'] + (VISITS_DIFF)*60 ) )
						{
							$id_visit = $this->VisitInit($width,$height,$color,$browser,$version,$os,$referer,0);
							$this->VisitInsert($id_visit,$ip,$hash);
						}
						else 
						{
							$id_visit = $visit['id_visit'];
							$db =& Db::globaldb();
							$today_time = $db->getTodayTime();
							$db->begin();
							$db->lock( "visits_ip" );
							$res[] = $db->query( "UPDATE visits_ip SET last_time='$today_time' WHERE id_visit='$id_visit' " );
							Db::finish( $res, $db);
						}
					}
					else 
					{
						$id_visit = $this->VisitInit($width,$height,$color,$browser,$version,$os,$referer,0);
						$this->VisitInsert($id_visit,$ip,$hash);
					}
				}
				else
				{
					$id_visit = $this->VisitInit($width,$height,$color,$browser,$version,$os,$referer,1);
				}
				$session->Set("id_visit",$id_visit);
				$hostinfo = parse_url($url);
				$matches = array();
				preg_match('/[^\.\/]+\.[^\.\/]+$/', $hostinfo['host'], $matches);
				$domain = $matches[0];
				setcookie(TRACK,$id_visit,0,'/',$domain);
			}
			elseif($cookies_disabled)
			{
				$db =& Db::globaldb();
				$db->begin();
				$db->lock( "visits" );
				$res[] = $db->query( "UPDATE visits SET cookie='1' WHERE id_visit='$id_visit' " );
				Db::finish( $res, $db);
				include_once(SERVER_ROOT."/../classes/log.php");
				$log = new Log();
				$log->Write("warning","Cookie disabled but session present: visit # $id_visit");
			}
			$this->Page($id_visit,$url,$title,$id_topic,$id_topic_group);
		}
	}

	private function Page($id_visit,$url,$title,$id_topic,$id_group)
	{
		$db =& Db::globaldb();
		$today_time = $db->getTodayTime();
		$db->begin();
		$db->lock( "visits_pages" );
		$sqlstr =  "INSERT INTO visits_pages (id_visit,page_date,url,title,id_topic,id_group) VALUES ( '$id_visit','$today_time','$url','$title','$id_topic','$id_group' )";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function Pages( &$rows, $id_visit )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(page_date) AS page_date_ts,vp.url,vp.title,t.name AS topic_name,tg.name AS group_name
			FROM visits_pages vp 
			LEFT JOIN topics t ON vp.id_topic=t.id_topic 
			LEFT JOIN topics_groups tg ON vp.id_group=tg.id_group 
			WHERE id_visit=$id_visit ORDER BY page_date ASC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function Reset()
	{
		$db =& Db::globaldb();
		$res[] = $db->query( "TRUNCATE visits_pages" );
		$res[] = $db->query( "TRUNCATE visits" );
		$this->ResetIPs();
	}
	
	public function ResetIPs()
	{
		$db =& Db::globaldb();
		$db->query( "TRUNCATE visits_ip" );
	}
	
	public function StatsDays(&$rows,$month, $year)
	{
		$monthStart = date('Y-m-d', mktime(0,0,0,$month,1,$year));
		$monthEnd = date('Y-m-d', mktime(0,0,0,$month+1,1,$year));
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DATE_FORMAT(v.visit_date,\"%d\") AS dd,COUNT(*) AS visits,DATE_FORMAT(v.visit_date,\"%Y%m%d\") AS sort ,
			(SELECT COUNT(vp.page_date) AS pages FROM visits_pages vp WHERE DATE_FORMAT(vp.page_date,'%Y%m%d')=DATE_FORMAT(v.visit_date,'%Y%m%d')) AS pages,
			(SELECT COUNT(DISTINCT v2.id_p) FROM visits v2 WHERE v2.id_p>0  AND DATE_FORMAT(v2.visit_date,\"%Y%m%d\")=sort ) AS uniques,
			(SELECT COUNT(v3.id_p) FROM visits v3 WHERE v3.id_p=0  AND DATE_FORMAT(v3.visit_date,\"%Y%m%d\")=sort ) AS unknowns
			FROM visits v
			 WHERE v.visit_date BETWEEN '$monthStart' AND '$monthEnd'
			GROUP BY sort ORDER BY sort DESC ";
 		return $db->QueryExe($rows, $sqlstr, false);
	}
	
	public function StatsMonth($month,$year)
	{
		$monthStart = date('Y-m-d', mktime(0,0,0,$month,1,$year));
		$monthEnd = date('Y-m-d', mktime(0,0,0,$month+1,1,$year));
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT (SELECT COUNT(id_visit) FROM visits WHERE cookie=1 AND (visit_date BETWEEN '$monthStart'AND '$monthEnd')) AS visits1 ,
			(SELECT COUNT(id_visit) FROM visits WHERE cookie=0  AND (visit_date BETWEEN '$monthStart'AND '$monthEnd')) AS visits2, 
			(SELECT COUNT(vp.page_date) FROM visits_pages vp INNER JOIN visits v ON vp.id_visit=v.id_visit AND v.cookie=1  AND (vp.page_date BETWEEN '$monthStart'AND '$monthEnd')) AS pages1,
			(SELECT COUNT(vp.page_date) FROM visits_pages vp INNER JOIN visits v ON vp.id_visit=v.id_visit AND v.cookie=0  AND (vp.page_date BETWEEN '$monthStart'AND '$monthEnd')) AS pages2,
			(SELECT COUNT(DISTINCT v.id_p) FROM visits v WHERE v.id_p>0  AND (v.visit_date BETWEEN '$monthStart'AND '$monthEnd')) AS uniques ";
		$db->query_single( $row, $sqlstr);
		return $row;
	}
	
	public function StatsMonths(&$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DATE_FORMAT(v.visit_date,\"%M %Y\") AS dd,COUNT(*) AS visits,DATE_FORMAT(v.visit_date,\"%Y%m\") AS sort ,
			(SELECT COUNT(vp.page_date) AS pages FROM visits_pages vp WHERE vp.page_date BETWEEN DATE_FORMAT(v.visit_date,'%Y-%m-01')
			    AND DATE_ADD(LAST_DAY(v.visit_date),INTERVAL 1 DAY)) AS pages,
			    (SELECT COUNT(DISTINCT v2.id_p) FROM visits v2 WHERE v2.id_p>0  AND (v2.visit_date BETWEEN DATE_FORMAT(v.visit_date,'%Y-%m-01')
			    AND DATE_ADD(LAST_DAY(v.visit_date),INTERVAL 1 DAY))) AS uniques,
			DATE_FORMAT(v.visit_date,\"%m\") AS month,DATE_FORMAT(v.visit_date,\"%Y\") AS year
			FROM visits v
			GROUP BY sort ORDER BY sort DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function StatsPages(&$rows,$year,$month,$day=0)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT DISTINCT url,title,COUNT(vp.id_visit) AS pages,t.name AS topic_name,tg.name AS group_name 
			FROM visits_pages vp 
			LEFT JOIN topics t ON vp.id_topic=t.id_topic 
			LEFT JOIN topics_groups tg ON vp.id_group=tg.id_group ";
		if($day>0)
		{
			 $month = sprintf("%02d",$month);
			 $day = sprintf("%02d",$day);
			 $sqlstr .= " WHERE DATE_FORMAT(vp.page_date,'%Y-%m-%d')='$year-$month-$day' ";
		}
		else
		{
			$monthStart = date('Y-m-d', mktime(0,0,0,$month,1,$year));
			$monthEnd = date('Y-m-d', mktime(0,0,0,$month+1,1,$year));
			$sqlstr .= " WHERE vp.page_date BETWEEN '$monthStart'AND '$monthEnd' ";
		}
		$sqlstr .= " GROUP BY url ORDER BY pages DESC  ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function StatsPagesTopic(&$rows,$id_topic)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT url,title,COUNT(vp.id_visit) AS pages
			FROM visits_pages vp 
			WHERE vp.id_topic=$id_topic 
			GROUP BY url ORDER BY pages DESC  ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function StatsTopic($id_topic)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT (SELECT COUNT(DISTINCT vp.id_visit) FROM visits_pages vp WHERE vp.id_topic=$id_topic) AS visits,
			(SELECT COUNT(vp.page_date) FROM visits_pages vp WHERE vp.id_topic=$id_topic) AS pages ";
		$db->query_single( $row, $sqlstr);
		return $row;
	}
	
	public function StatsTopics(&$rows,$id_group)
	{
		$db =& Db::globaldb();
		$rows = array();
		if($id_group>0)
		{
			$sqlstr = "SELECT t.id_topic AS id,t.name AS name,COUNT(vp.id_topic) AS pages,COUNT(DISTINCT vp.id_visit) AS visits
			FROM visits_pages vp 
			INNER JOIN topics t ON vp.id_topic=t.id_topic 
			WHERE vp.id_group=$id_group
			GROUP BY t.id_topic ORDER BY t.seq ";
		}
		else 
		{
			$sqlstr = "SELECT tg.id_group AS id,tg.name AS name,COUNT(vp.id_topic) AS pages,COUNT(DISTINCT vp.id_visit) AS visits
			FROM visits_pages vp
			INNER JOIN visits v ON vp.id_visit=v.id_visit 
			INNER JOIN topics_groups tg ON vp.id_group=tg.id_group 
			GROUP BY tg.id_group ORDER BY tg.seq ";
		}
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function VisitDelete($id_visit)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "visits_pages" );
		$res[] = $db->query( "DELETE FROM visits_pages WHERE id_visit='$id_visit' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "visits" );
		$res[] = $db->query( "DELETE FROM visits WHERE id_visit='$id_visit' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "visits_ip" );
		$res[] = $db->query( "DELETE FROM visits_ip WHERE id_visit='$id_visit' " );
		Db::finish( $res, $db);
	}
	
	public function Visit($id_visit)
	{
		$row = array();
		$db =& Db::globaldb();
		if($this->track_all)
		{
			$sqlstr = "SELECT v.*,UNIX_TIMESTAMP(visit_date) AS visit_date_ts,CONCAT('#',v.id_p) AS name,
				 UNIX_TIMESTAMP(MAX(vp.page_date)) - UNIX_TIMESTAMP(MIN(vp.page_date)) AS length
				 FROM visits v 
				 INNER JOIN visits_pages vp ON v.id_visit=vp.id_visit 
				 WHERE v.id_visit='$id_visit' 
				 GROUP BY v.id_visit ";
		}
		else 
		{
			$sqlstr = "SELECT v.*,UNIX_TIMESTAMP(visit_date) AS visit_date_ts,CONCAT(p.name1,' ',p.name2) AS name,
				 UNIX_TIMESTAMP(MAX(vp.page_date)) - UNIX_TIMESTAMP(MIN(vp.page_date)) AS length
				 FROM visits v 
				 LEFT JOIN people p ON v.id_p=p.id_p
				 INNER JOIN visits_pages vp ON v.id_visit=vp.id_visit 
				 WHERE v.id_visit='$id_visit' 
				 GROUP BY v.id_visit ";
		}
		$db->query_single( $row, $sqlstr);
		return $row;
	}

	private function VisitInit($width,$height,$color,$browser,$version,$os,$referer,$cookie)
	{
		$db =& Db::globaldb();
		$ip = $db->SqlQuote($this->v->IP());
		$user_agent = $db->SqlQuote($this->v->UserAgent());
		$today_time = $db->getTodayTime();
		$db->begin();
		$db->lock( "visits" );
		$id_visit = $db->nextId( "visits", "id_visit" );
		$sqlstr =  "INSERT INTO visits (id_visit,id_p,ip,visit_date,user_agent,referer,width,height,color,browser,version,os,cookie)
			 VALUES ( '$id_visit','{$this->id_p}','$ip','$today_time','$user_agent','$referer','$width','$height','$color','$browser','$version','$os','$cookie' )";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_visit;
	}
	
	private function VisitInsert($id_visit,$ip,$hash)
	{
		$db =& Db::globaldb();
		$today_time = $db->getTodayTime();
		$db->begin();
		$db->lock( "visits_ip" );
		$sqlstr =  "INSERT INTO visits_ip (id_visit,ip,last_time,hash)
			 VALUES ( '$id_visit','$ip','$today_time','$hash')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function Visits( &$rows, $id_p, $day=0, $month=0, $year=0 )
	{
		$db =& Db::globaldb();
		$rows = array();
		if($this->track_all)
		{
			$sqlstr = "SELECT UNIX_TIMESTAMP(visit_date) AS visit_date_ts,v.id_visit,v.id_p,CONCAT('#',v.id_p) AS name,v.ip,v.cookie,
			COUNT(vp.title) AS pages,UNIX_TIMESTAMP(MAX(vp.page_date)) - UNIX_TIMESTAMP(MIN(vp.page_date)) AS length
			FROM visits v
			INNER JOIN visits_pages vp ON v.id_visit=vp.id_visit ";
		}
		else 
		{
			$sqlstr = "SELECT UNIX_TIMESTAMP(visit_date) AS visit_date_ts,v.id_visit,v.id_p,CONCAT(p.name1,' ',p.name2) AS name,v.ip,v.cookie,
			COUNT(vp.title) AS pages,UNIX_TIMESTAMP(MAX(vp.page_date)) - UNIX_TIMESTAMP(MIN(vp.page_date)) AS length
			FROM visits v
			LEFT JOIN people p ON v.id_p=p.id_p 
			INNER JOIN visits_pages vp ON v.id_visit=vp.id_visit ";
		}
		if($id_p>0)
			$sqlstr .= " WHERE v.id_p=$id_p ";
		if($day>0)
		{
			 $month = sprintf("%02d",$month);
			 $day = sprintf("%02d",$day);
			 $sqlstr .= " WHERE DATE_FORMAT(v.visit_date,'%Y-%m-%d')='$year-$month-$day' ";
		}
		$sqlstr .= " GROUP BY v.id_visit ORDER BY visit_date DESC ";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}
?>
