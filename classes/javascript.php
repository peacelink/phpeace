<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class JavascriptManager
{
	private $path;

	function __construct()
	{
		$this->path = "pub/js/custom";
	}
	
	function JsAll()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_js,name,id_style FROM javascript ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function JsAllP( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT j.id_js,j.name,j.id_style,s.name AS style FROM javascript j
		 LEFT JOIN styles s USING (id_style)  ORDER BY j.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function JsDelete($id_js,$id_style=0)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "javascript" );
		$sqlstr = "DELETE FROM javascript WHERE id_js='$id_js' AND id_style='$id_style'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->Delete($this->JsFilename($id_style,$id_js));
		$fm->PostUpdate();
	}
	
	public function JsFilename($id_style,$id_js)
	{
		return $this->path . "/s" . $id_style . "_" . $id_js . ".js";
	}
	
	public function JsGet($id_js)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_js,script,id_style,name FROM javascript WHERE id_js='$id_js'";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function JsInsert($id_js,$id_style,$name,$unescaped_script)
	{
		$db =& Db::globaldb();
		$script = $db->SqlQuote($unescaped_script);
		$db->begin();
		$db->lock( "javascript" );
		$id_js_new = ($id_js>0)? $id_js : $db->nextId( "javascript", "id_js" );
		$sqlstr = "INSERT INTO javascript (id_js,id_style,name,script) 
			VALUES ($id_js_new,$id_style,'$name','$script')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->JsWrite($id_style,$id_js_new,$unescaped_script);
	}

	public function JsStore($id_js,$id_style,$name,$unescaped_script)
	{
		$db =& Db::globaldb();
		$script = $db->SqlQuote($unescaped_script);
		$db->begin();
		$db->lock( "javascript" );
		$sqlstr = "UPDATE javascript SET name='$name',script='$script',id_style='$id_style' WHERE id_js='$id_js'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		$this->JsWrite($id_style,$id_js,$unescaped_script);
	}
	
	private function JsWrite($id_style,$id_js,$script)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->DirAction("pub/js/custom","check");
		$fm->WritePage($this->JsFilename($id_style,$id_js),$script );
		$fm->PostUpdate();
	}

}
?>
