<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

/**
 * DB factory
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
abstract class Db
{
	/**
	 * Return DB singleton
	 * Set its properties
	 *
	 * @return DbMysql
	 */
	public static function &globaldb()
	{
		global $db_conn;
		
		// $db_conn is DbMysql
		if($db_conn)
		{
			if(!$db_conn->Ping())
			{
				// DB connection is broken
				// properly close DB Connection
				$db_conn->Close();
			}
			else
			{
				// DB connection is OK
				// return DB Connection
				return $db_conn;
			}
		}
		
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$dbconf = $conf->Get("dbconf");
		$db_impl = $dbconf['impl'];
		$db_pconn = $conf->Get("db_pconn");
		include_once(SERVER_ROOT."/../classes/dbmysql.php" );
		$db_conn = new DbMysql( $dbconf['server'], $dbconf['database'], $dbconf['user'], $dbconf['password'],$db_pconn);
		$db_conn->StorageEngineSet($db_impl);
		$db_conn->records_per_page = $conf->Get("records_per_page");
		return $db_conn;
	}

	/**
	 * Close connection
	 *
	 * @param array $resultArray
	 * @param DbMysql $db
	 */
	public static function finish( &$resultArray, &$db )
	{
		$db->Unlock();
		if ( !is_array( $resultArray ) )
			$resultArray = array( $resultArray );
		if ( in_array( false, $resultArray ) )
			$db->Rollback();
		else
			$db->Commit();
	}
	
	/**
	 * Test DB connection
	 *
	 * @return boolean
	 */
	public static function &test()
	{
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$dbconf = $conf->Get("dbconf");
		$db_impl = $dbconf['impl'];
		$test = true;
		switch($db_impl)
		{
			case "mysql":
			{
				set_error_handler("ErrorTrash");
				$database = mysqli_connect( $dbconf['server'], $dbconf['user'], $dbconf['password'] );
				if (!$database)
					$test = false;
				else
					$test = mysqli_select_db( $database, $dbconf['database']);
				set_error_handler("ErrorHandler");
			}
			break;
		}
		return $test;
	}

}

?>
