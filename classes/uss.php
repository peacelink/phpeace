<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");

/**
 * URL shortening client
 * Connects to a URL shortening service on some PhPeace installations
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class UrlShorteningClient
{
	/**
	 * @var string	USS to connect to
	 */
	private $uss_server;
	
	/**
	 * @var string	Authentication key to be used
	 */
	private $uss_key;
	
	/**
	 * @var string Login for bit.ly
	 */
	private $bitly_login;
	
	/**
	 * @var string API key for bit.ly
	 */
	private $bitly_key;
	
	/**
	 * Initialize local variables 
	 */
	function __construct()
	{
		$conf = new Configuration();
		$this->uss_server = $conf->Get("uss_url");
		$this->uss_key = $conf->Get("uss_key");
		$this->bitly_login = $conf->Get("bitly_login");
		$this->bitly_key = $conf->Get("bitly_key");
	}

	/**
	 * Whether the URL Shortening Service is available
	 * (i.e. a USS server has been configured)
	 * 
	 * @return boolean
	 */
	public function IsShorteningServiceAvailable()
	{
		return $this->uss_server!="";
	}
	
	/**
	 * Shorten a URL connecting to USS service
	 * @param string $url	Original URL
	 * @return string		Short URL
	 */
	public function Shorten($url)
	{
		if($this->uss_server!="")
		{
			switch($this->uss_server)
			{
				case "bit.ly":
					if($this->bitly_key!="" && $this->bitly_login!="")
					{
						$bitly = 'http://api.bit.ly/v3/shorten?longUrl='.urlencode($url).'&login='.($this->bitly_login).'&apiKey='.($this->bitly_key).'&format=json';
						$response = file_get_contents($bitly);
						$json = @json_decode($response,true);
						if(is_array($json) && $json['status_code']==200 && isset($json['data']['url']))
						{
							$url = $json['data']['url'];
						}
					}
				break;
				default:
					$ini = new Ini();
					$params = array();
					$params['url'] = $url;
					$params['key'] = $this->uss_key;
					include_once(SERVER_ROOT."/../classes/webservice.php");
					$ws = new WebService("uss");
					$url = $ws->Call($this->uss_server,"UrlShortening",$params);
			}
		}
		else 
		{
			UserError("URL Shortening Server has not been configured",array());
		}
		return $url;
	}
}

define('URL_KEY_BASE',32);

/**
 * URL Shortening Service (USS)
 * 
 * Based on implementation by Julius Beckmann
 * http://juliusbeckmann.de/blog/create-your-own-url-shortener-for-twitter-with-this-easy-to-use-php-class.html
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class UrlShorteningServer
{
	/**
	 * Authentication Key
	 * @var string
	 */
	private $uss_key;
	
	/**
	 * USS domain mask
	 * @var string
	 */
	private $uss_mask;
	
	/**
	 * Log hits
	 * @var boolean
	 */
	private $log_hits;
	
	/**
	 * Public domain
	 * @var string
	 */
	private $pub_web;
	
	/**
	 * Whether USS service has been enabled
	 * @var boolean
	 */
	public $active;
	
	/**
	 * Initialize local variables 
	 */
	function __construct()
	{
		$conf = new Configuration();
		$this->uss_key = $conf->Get("uss_key");
		$this->log_hits = $conf->Get("uss_log_hits");
		$this->uss_mask = $conf->Get("uss_mask");
		$ini = new Ini();
		$this->pub_web = $ini->Get("pub_web");
		$active = false;
		if($this->uss_key!="")
		{
			include_once(SERVER_ROOT."/../classes/modules.php");
			$active = Modules::IsActiveByPath("uss");
		}
		$this->active = $active;
	}

	/**
	 * Encode a URL ID
	 * 
	 * @param integer $id
	 * @return string
	 */
	private function IdEncode($id)
	{
		return strrev(base_convert($id,10,URL_KEY_BASE));
	}
	
	/**
	 * Decode a URL ID
	 * 
	 * @param string $encoded_id
	 * @return integer
	 */
	private function IdDecode($encoded_id)
	{
		return (int)base_convert(strrev($encoded_id),URL_KEY_BASE,10);		
	}
	
	/**
	 * Whether client can use this service
	 * 
	 * @param string $key	Client Key
	 * @return boolean
	 */
	public function IsKeyValid($key)
	{
		return $key==$this->uss_key;
	}

	/**
	 * Check if a URL is already stored
	 * If so, return its ID
	 * 
	 * @param string $hash	URL md5 hash
	 * @return integer		URL ID 
	 */
	private function UrlGetByHash($hash)
	{
		$row = array();
		$db = Db::globaldb();
		$hash = $db->SqlQuote($hash);
		$db->query_single( $row, "SELECT id_url FROM url_shorts WHERE md5='$hash'");
		return isset($row['id_url'])? (int)$row['id_url'] : 0;
	}
	
	/**
	 * Get a URL from its ID
	 * 
	 * @param integer $encoded_id
	 * @return string
	 */
	public function UrlGet($encoded_id)
	{
		$url = "";
		$id = $this->IdDecode($encoded_id);
		if($id>0)
		{
			$row = array();
			$db = Db::globaldb();
			$db->query_single( $row, "SELECT url FROM url_shorts WHERE id_url='$id'");
			if(isset($row['url']))
			{
				$url = $row['url'];
				if($this->log_hits)
				{
					$this->UrlHit($id);
				}
			}
		}
		return $url;
	}
	
	/**
	 * Update usage statistics for a short URL
	 * 
	 * @param integer $id_url
	 */
	private function UrlHit($id_url)
	{
		$conf = new Configuration();
		$delayed_inserts = $conf->Get("delayed_inserts");
		$db =& Db::globaldb();
		$row = array();
		$db->query_single( $row, "SELECT hits FROM url_shorts_hits WHERE id_url='$id_url'");
		$today = $db->getTodayTime();
		$db->begin();
		if(isset($row['hits']) && count($row)>0)
		{
			if($delayed_inserts)
			{
				$sqlstr = "UPDATE LOW_PRIORITY url_shorts_hits SET hits=hits+1,last_use='$today' WHERE id_url='$id_url' ";
			}
			else
			{
				$db->lock( "url_shorts_hits" );
				$sqlstr = "UPDATE url_shorts_hits SET hits=hits+1,last_use='$today' WHERE id_url='$id_url' ";
			}
		}
		else
		{
			if($delayed_inserts)
			{
				$sqlstr = "INSERT DELAYED INTO url_shorts_hits (id_url,hits,last_use) VALUES ('$id_url','1','$today') ";
			}
			else
			{
				$db->lock( "url_shorts_hits" );
				$sqlstr = "INSERT INTO url_shorts_hits (id_url,hits,last_use) VALUES ('$id_url','1','$today') ";
			}
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Store a URL and its hash
	 * 
	 * @param string $url
	 * @param string $hash
	 * @return integer		New URL ID
	 */
	private function UrlStore($url,$hash)
	{
		$db =& Db::globaldb();
		$today = $db->getTodayTime();
		$url = $db->SqlQuote($url);
		$hash = $db->SqlQuote($hash);
		$db->begin();
		$db->lock( "url_shorts" );
		$id_url = $db->nextId( "url_shorts", "id_url" );
		$sqlstr = "INSERT INTO url_shorts (id_url,url,md5,insert_date) 
			VALUES ('$id_url','$url','$hash','$today') ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_url;
	}
	
	/**
	 * Shorten a URL
	 * 
	 * @param string $url
	 * @return string
	 */
	public function Shorten($url)
	{
		$md5 = md5($url);
		$id_url = $this->UrlGetByHash($md5);
		if($id_url==0)
		{
			$id_url = $this->UrlStore($url,$md5);
		}
		return (($this->uss_mask!="")? $this->uss_mask : $this->pub_web . "/uss?" ) . $this->IdEncode($id_url);
	}
}

/**
 * Web Service for URL Shortening
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class UrlShorteningWS
{
	/** 
	 * @var UrlShorteningServer */
	private $uss;
	
	/**
	 * Initialize local variables 
	 */
	function __construct()
	{
		$this->uss = new UrlShorteningServer();
	}

	/**
	 * Generate a short URL
	 * 
	 * @param string $url	Original URL
	 * @param string $key	Authentication key from client
	 * @return string		Shortened URL
	 */
	public function UrlShortening($url,$key)
	{
		$url_short = $url;
		if($this->uss->active && $this->uss->IsKeyValid($key))
		{
			$url = str_ireplace(array("\n","\r","\t"), '', $url);
			$valid = (bool)preg_match('@http(s?):\/\/@', $url);
			if($valid)
				$url_short = $this->uss->Shorten($url);
		}
		return $url_short;
	}
}

?>
