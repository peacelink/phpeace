<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Resources
{
	public $types;

	function __construct()
	{
		$this->types = array(	'topic'		=> 1,
				'subtopic'	=> 2,
				'gallery'	=> 3,
				'org'		=> 4,
				'article'		=> 5,
				'document'	=> 6,
				'image'		=> 7,
				'event'		=> 8,
				'forum'		=> 9,
				'thread'	=> 10,
				'campaign'	=> 11,
				'quote'		=> 12,
				'book'		=> 13,
				'link'		=> 14,
				'r_event'	=> 15,
				'topics_group'	=> 16,
				'galleries_group'	=> 17,
				'review'	=> 18,
				'poll'		=> 19,
				'question'	=> 20,
				'text'		=> 21,
				'box'		=> 22,
				'video'		=> 23,
				'widget'	=> 24,
				'widget_item'		=> 25,
				'unused' => 26,
				'ebook' => 27,
				'audio'	=> 28);
	}

	public function Param($id_resource_param)
	{
		$db =& Db::globaldb();
		$row= array();
		$db->query_single( $row, "SELECT * from resource_params WHERE id_resource_param=$id_resource_param");
		return $row;
	}
	
	public function ParamDelete($id_param)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "resource_params" );
		$res[] = $db->query( "DELETE FROM resource_params WHERE id_resource_param='$id_param' " );
		Db::finish( $res, $db);
	}
	
	public function ParamStore($resource,$id_param,$id_type,$label,$params,$public)
	{
		$types = $this->ParamsTypes();
		$type = $types[$id_type];
		if($id_param>0)
		{
			$sqlstr = "UPDATE resource_params SET label='$label',type='$type',params='$params',public='$public'
				 WHERE id_resource_param=$id_param";
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "resource_params" );
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
		else
		{
			$id_resource = $this->types[$resource];
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "resource_params" );
			$id_param = $db->nextId( "resource_params", "id_resource_param" );
			$res[] = $db->query( "INSERT INTO resource_params (id_resource_param,id_resource,label,type,params,public)
				VALUES ($id_param,$id_resource,'$label','$type','$params','$public')" );
			Db::finish( $res, $db);
		}
		return $id_param;
	}

	public function ParamsTypes($wysiwyg=false,$upload=false,$subscribe=false)
	{
		$types = array();
		$types[0] = "text";
		$types[1] = "textarea";
		$types[2] = "checkbox";
		$types[3] = "date";
		$types[4] = "geo";
		$types[5] = "dropdown";
		$types[6] = "mchoice";
		if($wysiwyg)
			$types[7] = "html";
		$types[8] = "link";
		if($upload)
			$types[9] = "upload";
		if($subscribe)
			$types[10] = "subscription";
		if($upload)
			$types[11] = "upload_image";
		$types[12] = "dropdown_open";
		$types[13] = "mchoice_open";
		$types[14] = "radio";
		return $types;
	}
	
	public function Params($resource,$only_public=false)
	{
		$id_resource = $this->types[$resource];
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_resource_param,label,params,type,public
		FROM resource_params
		WHERE id_resource='$id_resource' ";
		if($only_public)
			$sqlstr .= " AND public=1 ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function ParamsUpdate($unescaped_params,$previous_params)
	{
		$kparams = array();
		foreach($unescaped_params as $key=>$value)
			$kparams["rp_" . $key] = $value;
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$aparams = $v->Deserialize($previous_params);
		$current_params = (is_array($aparams))? $aparams : array();
		foreach($unescaped_params as $key=>$value)
		{
			$pkey = "rp_" . $key;
			$current_params[$pkey] = $value;
		}
		$current_params2 = array();
		foreach($current_params as $ckey=>$cvalue)
		{
			$current_params2[$ckey] = array_key_exists($ckey,$kparams)? $cvalue : "";
		}
		$ser_array = $v->Serialize($current_params2);
		return $ser_array;
	}

	public function TypeLookup($id_type)
	{
		return array_search($id_type,$this->types);
	}

}
?>
