<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Translations
{
	public $languages = array();

	private $id_module = 21;
	
	function __construct()
	{
		include_once(SERVER_ROOT."/../classes/translator.php");
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$tr = new Translator($session->Get('id_language'),0);
		$this->languages = $tr->Translate("languages");
	}
	
	public function LanguageFrom()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT DISTINCT tl.id_language_from FROM translator_languages tl ORDER BY id_language_from";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function LanguageTo($id_language_from)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT DISTINCT id_language_to 
		FROM translator_languages WHERE id_language_from=$id_language_from ORDER BY id_language_to";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function LanguagesAvailable($id_language_from)
	{
		$lbut = array();
		if ($id_language_from>0)
		{
			$lbut[0] = "--";
			$rows = array();
			$db =& Db::globaldb();
			$sqlstr = "SELECT DISTINCT id_language_to 
			FROM translator_languages WHERE id_language_from=$id_language_from ORDER BY id_language_to";
			$db->QueryExe($rows, $sqlstr);
			foreach($rows as $row)
			{
				$index = $row['id_language_to'];
				$lbut[$index] = $this->languages[$index];
			}
		}
		else
			$lbut = $this->languages;
		return $lbut;
	}
	
	public function TranslationArticle($id_translation,$id_article,$translator,$comments,$completed)
	{
		$db =& Db::globaldb();
		$sqlstr = "UPDATE translations SET translator='$translator',comments='$comments',
			id_article_trad=$id_article,completed='$completed'
			WHERE id_translation=$id_translation";
		$db->begin();
		$db->lock( "translations" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function TranslationArticleAssociate($id_translation,$id_article)
	{
	    $db =& Db::globaldb();
	    $sqlstr = "UPDATE translations SET id_article_trad=$id_article WHERE id_translation=$id_translation";
	    $db->begin();
	    $db->lock( "translations" );
	    $res[] = $db->query( $sqlstr );
	    Db::finish( $res, $db);
	}
	
	public function TranslationDelete($id_translation)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "translations" );
		$res[] = $db->query( "DELETE FROM translations WHERE id_translation='$id_translation' " );
		Db::finish( $res, $db);
	}
	
	public function TranslationDeleteByArticle($id_article)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "translations" );
		$res[] = $db->query( "DELETE FROM translations WHERE id_article_trad='$id_article' " );
		Db::finish( $res, $db);
	}
	
	public function TranslationGet( $id_translation )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT t.id_article,t.id_language,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.notes,t.id_article_trad,
			t.id_language_trad,t.completed,t.comments,t.id_user,t.priority,
			u.name AS user_name,t.translator,t.id_rev,t.id_ad,t.source_url
			FROM translations t
			LEFT JOIN users u ON t.id_user=u.id_user
			WHERE t.id_translation=$id_translation";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function TranslationGetByArticle( $id_article_trad )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT t.id_translation,t.id_language_trad,t.id_article_trad,t.id_user,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,
			u.name AS user_name,t.translator,t.completed,t.source_url
			FROM translations t
			LEFT JOIN users u ON t.id_user=u.id_user
			WHERE t.id_article_trad='$id_article_trad' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function TranslationsGetByArticleOriginal( $id_article, $id_current_translation )
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT t.id_translation,t.id_language_trad,t.id_article_trad,t.id_user,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,
			u.name AS user_name,t.translator,t.completed,t.source_url
			FROM translations t
			LEFT JOIN users u ON t.id_user=u.id_user
			WHERE t.id_article='$id_article' ";
		if($id_current_translation>0)
			$sqlstr .= " AND t.id_translation<>$id_current_translation ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function TranslationInsert($id_article,$id_language,$id_language_trad,$notes,$priority,$source_url)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "translations" );
		$id_translation = $db->nextId( "translations", "id_translation" );
		$sqlstr = "INSERT INTO translations (id_translation,id_article,id_language,id_language_trad,notes,priority,start_date,source_url)
			VALUES ($id_translation,$id_article,$id_language,$id_language_trad,'$notes',$priority,'" . $db->getTodayDate() . "','$source_url' )";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function TranslationTake($id_translation,$id_user)
	{
		$db =& Db::globaldb();
		$db->begin();
		$sqlstr = "UPDATE translations SET id_user=$id_user WHERE id_translation=$id_translation";
		$db->lock( "translations" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function TranslationUpdate($id_translation,$start_date,$id_language,$id_language_trad,$notes,$priority,$id_user,$comments,$translator,$completed,$id_rev,$id_ad,$source_url)
	{
		$db =& Db::globaldb();
		$db->begin();
		$sqlstr = "UPDATE translations SET id_language=$id_language,id_language_trad=$id_language_trad,notes='$notes',
			id_user=$id_user,start_date='$start_date',comments='$comments',priority='$priority',translator='$translator',completed='$completed',
			id_rev=$id_rev,id_ad=$id_ad,source_url='$source_url'
			WHERE id_translation=$id_translation";
		$db->lock( "translations" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
		
	public function TranslationsP( &$rows, $status )
	{
		$db =& Db::globaldb();
		$rows = array();
		switch($status)
		{
			case "pending":
				$sqlstr = "SELECT id_translation,UNIX_TIMESTAMP(start_date) AS start_date_ts,id_language,id_language_trad,
					priority,id_user,id_article,notes,'' AS user_name
					FROM translations WHERE id_user=0 AND id_language_trad>0 AND id_language>0 ORDER BY priority DESC, start_date DESC, id_translation DESC";
				break;
			case "atwork":
				$sqlstr = "SELECT t.id_translation,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.id_language,t.id_language_trad,
					t.priority,t.id_user,t.id_article,t.notes,u.name AS user_name
					FROM translations t
					LEFT JOIN users u ON t.id_user=u.id_user
					WHERE t.id_user>0 AND t.completed=0 ORDER BY t.priority DESC, t.start_date DESC";
				break;
			case "uncomplete":
				$sqlstr = "SELECT t.id_translation,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.id_language,t.id_language_trad,
					t.priority,t.id_user,t.id_article,t.notes,u.name AS user_name
					FROM translations t
					LEFT JOIN users u ON t.id_user=u.id_user
					WHERE t.id_language=0 OR t.id_language_trad=0 ORDER BY t.start_date DESC";
				break;
			default:
				$sqlstr = "SELECT t.id_translation,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.id_language,t.id_language_trad,
					t.priority,t.id_user,t.id_article,t.notes,t.completed,u.name AS user_name
					FROM translations t
					LEFT JOIN users u ON t.id_user=u.id_user
					WHERE t.completed=1 ORDER BY t.id_article_trad DESC";
		}
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function TranslationsUserP( &$rows, $status, $id_user )
	{
		$db =& Db::globaldb();
		$rows = array();
		switch($status)
		{
			case "pending":
				$sqlstr = "SELECT t.id_translation,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.id_language,t.id_language_trad,
					t.priority,t.id_user,t.id_article,t.notes,u.name AS user_name,t.id_article_trad
					FROM translations t 
					LEFT JOIN users u ON t.id_user=u.id_user
					INNER JOIN translator_languages tl ON t.id_language=tl.id_language_from 
					AND t.id_language_trad=tl.id_language_to AND tl.id_user=$id_user 
					WHERE t.id_user=0 AND completed=0 ORDER BY t.priority DESC, t.start_date DESC";
				break;
			case "atwork":
				$sqlstr = "SELECT t.id_translation,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.id_language,t.id_language_trad,
					t.priority,t.id_user,t.id_article,t.notes,u.name AS user_name,t.id_article_trad
					FROM translations t
					LEFT JOIN users u ON t.id_user=u.id_user
					WHERE t.id_user=$id_user AND t.completed=0 ORDER BY t.priority DESC, t.start_date DESC";
				break;
			default:
				$sqlstr = "SELECT t.id_translation,UNIX_TIMESTAMP(t.start_date) AS start_date_ts,t.id_language,t.id_language_trad,
					t.priority,t.id_user,t.id_article,t.notes,t.completed,u.name AS user_name,t.id_article_trad
					FROM translations t
					LEFT JOIN users u ON t.id_user=u.id_user
					WHERE t.id_user=$id_user AND t.completed=1 ORDER BY t.id_article_trad DESC";
		}
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	private function TranslatorLanguage( $id_user,$id_language_from,$id_language_to )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_user FROM translator_languages WHERE id_user=$id_user 
		AND id_language_from=$id_language_from AND id_language_to=$id_language_to";
		$db->query_single($row, $sqlstr);
		return $row['id_user'];
	}
	
	public function TranslatorLanguages( $id_translator )
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_language_from,id_language_to FROM translator_languages WHERE id_user=$id_translator ORDER BY id_language_from,id_language_to";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function TranslatorLanguagesDelete($id_user,$id_language_from,$id_language_to)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "translator_languages" );
		$res[] = $db->query( "DELETE FROM translator_languages WHERE id_user=$id_user 
		AND id_language_from=$id_language_from AND id_language_to=$id_language_to" );
		Db::finish( $res, $db);
	}
	
	public function TranslatorLanguagesUpdate($id_user,$id_language_from,$id_language_to)
	{
		if (!$this->TranslatorLanguage( $id_user,$id_language_from,$id_language_to ) > 0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "translator_languages" );
			$sqlstr = "INSERT INTO translator_languages (id_user,id_language_from,id_language_to )
				VALUES ($id_user,$id_language_from,$id_language_to )";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}
	
	public function Translators()
	{
		include_once(SERVER_ROOT."/../classes/users.php");
		$u = new users;
		return $u->ModuleUsers($this->id_module);
	}

	public function TranslatorsLanguage($id_language_from,$id_language_to)
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT DISTINCT tl.id_user,u.name
		FROM translator_languages tl LEFT JOIN users u USING(id_user) 
		WHERE tl.id_language_from=$id_language_from AND tl.id_language_to=$id_language_to ORDER BY u.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function TranslatorsP( &$rows )
	{
		include_once(SERVER_ROOT."/../classes/modules.php");
		return Modules::ModuleUsers( $rows, $this->id_module );
		// diverso, dovrebbe prendere anche le lingue e i task
	}

	public function Users()
	{
		$db =& Db::globaldb();
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT users.id_user,name FROM users
				INNER JOIN module_users ON module_users.id_user=users.id_user
				WHERE users.active=1 AND users.id_user>0 AND module_users.id_module={$this->id_module}
				GROUP BY users.id_user ORDER BY name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

}
?>
