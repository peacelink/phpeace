# Dev env setup

Get the code from the repository

`mkdir custom`

`cp install/config.php custom/`

Sort out permissions, i.e. put yourself and apache's user (www-data, or apache) in the same group, and then set everything writable by this group.

This needs to be done as user root

`# groupadd phpeacedev`

`# usermod -a -Gphpeacedev yourself`

`# usermod -a -Gphpeacedev www-data`

`# chown -R yourself:phpeacedev phpeace_dev`

`# chmod -R g+w phpeace_dev`

Change custom/config.php setting the following variables

- `public $dev = true;` This lets you see all modules in the Admin menu, regardless their locked state in the modules table

- `public $debug = true;` Outputs all errors

