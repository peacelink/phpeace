#! /bin/bash
#
# Return the build directory

# Load configuration
CURRENTDIR=`dirname $0`
CONFIGFILE="$CURRENTDIR/../../custom/buildconfig.sh"
if [ ! -f "$CONFIGFILE" ]; then exit 1; fi
source $CONFIGFILE

if [ "$1" == "test" ]; then
	DESTDIR="$DESTDIR/testing"
fi

echo $DESTDIR

