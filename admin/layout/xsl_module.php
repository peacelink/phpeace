<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");

$xslm = new XslManager("module");

$id_module = $_GET['id'];
$id_style = $_GET['id_style'];

$title[] = array('Page types',$ui?'page_types.php':'xsls.php');
$t_modules = $hh->tr->Translate("modules_names");
$title[] = array($t_modules[$id_module],'xsl_module_styles.php?id='.$id_module);

if ($id_style>0)
{
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles;
	$style = $s->StyleGet($id_style);
	$title[] = array($style['name'],'');
}
else
	$title[] = array('Generic XSL','');

if(!$ui) {
	$action2 = "update";
	$input_update_right = 0;
	if ($id_style>0)
	{
		$input_right = $s->InputRight($id_style);
	}
	if ($module_admin)
	{
		$input_right = 1;
		$input_update_right = 1;
	}
	
	$xslm->id_pagetype = $id_module;
	$row = $xslm->XslGetByType($id_style);
	$row['xsl'] = $xslm->XslGetLocal(0,$id_style);
	$xslm->xh->Check($row['xsl'],FALSE);
	
	if($row['outsourced'] && $id_style==0)
		$input_right = 0;
}


echo $hh->ShowTitle($title);

echo "<p><a href=\"features.php?id_type=$id_type&id_style=$id_style&id_module=$id_module\">Features</a></p>";

if(!$ui) {
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","xsl_module");
	echo $hh->input_hidden("id_xsl",$row['id_xsl']);
	echo $hh->input_hidden("id_module",$id_module);
	echo $hh->input_hidden("id_style",$id_style);
	echo $hh->input_hidden("outsourced",$row['outsourced']);
	echo $hh->input_table_open();
	
	include_once(SERVER_ROOT."/../classes/topics.php");
	if ($id_style>0)
		echo $hh->input_note("Style: <a href=\"style.php?id=$id_style\">$style[name]</a>");
	else
	{
		if ($row['outsourced'])
			echo $hh->input_note($hh->tr->TranslateParams("update_by_server",array("XSL")));
		else
			echo $hh->input_note($hh->tr->TranslateParams("forked",array("XSL")));
	}
	
	$num = $xslm->Revisions($rows,0,$id_style);
	if ($num>0)
		echo $hh->input_note("Revisions: <a href=\"xsl_revisions.php?id_style=$id_style&type=module&id_pagetype=$id_module\">$num</a>");
	if ($input_right)
		echo $hh->input_note("Warning: &_amp; substitutes &amp;amp; and &lt;_textarea&gt; substitutes  &lt;textarea&gt;; no need to fix them, it's going to be done after form submit");
	echo $hh->input_textarea("xsl","xsl",$row['xsl'],80,30,"",$input_right);
	
	$actions = array();
	$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
	$actions[] = array('action'=>"fork",'label'=>($row['outsourced'])?"fork":"unfork",'right'=>($input_update_right && $id_style==0));
	echo $hh->input_actions($actions,$input_right || $input_update_right);
	
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>

