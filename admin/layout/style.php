<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles;

$id = (int)$get['id'];

$title[] = array('Styles','styles.php');
if ($id>0)
{
	$row = $s->StyleGet($id);
	$action2 = "update";
	$title[] = array($row['name'],'');
}
else
{
	$action2 = "insert";
	$title[] = array('Create new style','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","style");
echo $hh->input_hidden("id_style",$id);
echo $hh->input_table_open();

if ($id>0)
{
	$topics = $s->Topics($id);
	$used = false;
	if (count($topics)>0)
	{
		$used = true;
		include_once(SERVER_ROOT."/../classes/topic.php");
		$desc = "This style is using by the following topics: ";
		$counter = 1;
		foreach($topics as $topic)
		{
			$t = new Topic($topic['id_topic']);
			$desc .= "<a href=\"/topics/topic_gra.php?id=$t->id\">$t->name</a>";
			if ($counter!=count($topics))
				$desc .= ", ";
			if ($t->edit_layout && $t->AmIAdmin())
				$input_right = 1;
			$counter++;
		}
		$used = true;
		echo $hh->input_note($desc);
	}
	if (!$used)
		echo $hh->input_note("This style is currently not used by any topic");
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	$graphics = $s->GraphicsAll($id,true,false);
	$features = array();
	$num_features = $pt->ft->PageFeaturesStyle($features,$id,-1,false);
	$num_boxes = $s->BoxesTypes($boxes, $id );
	if(!$ui) {
	}
	echo $hh->input_note("Associated resources: " . 
	($ui?'':"<a href=\"xsls_style.php?id_style=$id\">XSL</a>, <a href=\"csss_style.php?id_style=$id\">CSS</a>, ") . 
	"<a href=\"graphics.php?id_style=$id\">" . count($graphics) . " graphics</a>, " . 
	"<a href=\"features_style.php?id=$id\">$num_features features</a>, " . 
	"<a href=\"boxes_types.php?id_style=$id\">$num_boxes boxes types</a>");
	echo $hh->input_text("ID","ID",$id,5,0,0); 
}

echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],80,3,"",$input_right);

if(!$ui) {
	echo $hh->input_row("son_of","id_parent",$row['id_parent'],$s->StylesAll(),"none",0,$id==0);
	
	if (!$id>0)
	{
		echo $hh->input_row("copy","id_style_copy",0,$s->StylesAll(),"none",0,$input_right);
	}
}

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id>0 && count($topics)==0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

