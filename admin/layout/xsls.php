<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");

$pt = new PageTypes();

$id_style = $_GET['id_style'];

$title[] = array('Page types','');

echo $hh->ShowTitle($title);

$types = $hh->tr->Translate("page_types");
$types_global = $hh->tr->Translate("page_types_global");

$active_modules = Modules::AvailableModules();
$t_modules = $hh->tr->Translate("modules_names");
echo "<ul>\n<li>Global";
echo "<ul>\n";
foreach($pt->gtypes as $key=>$type)
{
	if($type!=8)
	{
		echo "<li><a href=\"xsl_global.php?id=$type\">" . trim($types_global[$type]) . "</a> - $key.xsl";
		if($type==0 && Modules::IsActiveByPath("widgets"))
		{
			echo "<ul>";
			echo "<li><a href=\"xsl_global.php?id=8\">" . trim($types_global[8]) . "</a> - widgets.xsl</li>\n";
			echo "</ul>";
		}
		echo "</li>\n";
	}
}
echo "</ul></li>\n";
echo "<li>Specific (redefinable by styles)<ul>\n";
$ptypes = $pt->types;
foreach($ptypes as $key=>$type)
{
	if($type!=20 && $type!=21)
	{
		echo "<li><a href=\"xsl_styles.php?id=$type\">" . trim($types[$type]) . "</a> - $key.xsl";
		if($type==0)
		{
			echo "<ul>";
			echo "<li><a href=\"xsl_styles.php?id=20\">" . trim($types[20]) . "</a> - root.xsl</li>\n";
			echo "<li><a href=\"xsl_styles.php?id=21\">" . trim($types[21]) . "</a> - tools.xsl</li>\n";
			echo "</ul>";
		}
		echo "</li>\n";
	}
}
echo "</ul></li>\n";
if(count($active_modules)>0)
{
	echo "<li>Modules (redefinable by styles)<ul>\n";
	foreach($active_modules as $module)
	{
		if($module['layout'] && !$module['internal'] && !$module['global'])
			echo "<li><a href=\"xsl_module_styles.php?id={$module['id_module']}\">" . trim($t_modules[$module['id_module']]) . "</a> - " . $module['path'] . ".xsl</li>\n";
	}
	echo "</ul></li>\n";
}
echo "<li><a href=\"xsl_exts.php\">Extensions</a> (redefinable by styles)</li>\n";
echo "<li><a href=\"xsl_customs.php\">Custom</a></li>\n";
echo "</ul>\n";

echo "<p><a href=\"xsl_forked.php\">Forked</a></p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

