<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
$pt = new PageTypes();

$hhf = new HHFunctions ;

$id_type = ($_GET['id_type']>0)? $_GET['id_type']:0;
$id_gtype = $_GET['id_gtype'];
$id_topic = $_GET['id_topic'];
$id = $_GET['id'];
$id_style = 0;

$title[] = array('XML','');
echo $hh->ShowTitle($title);

$types = $hh->tr->Translate("page_types");
if ($id_topic > 0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$id_style = $t->id_style;
	echo "<p><a href=\"xml_choose.php\">Topic</a>: $t->name</p>\n";
	if ($id_type > 0)
	{
		echo "<p><a href=\"xml_choose.php?id_topic=$id_topic\">Page type</a>: $types[$id_type]</p>\n";
		switch($id_type)
		{
			case "2":
				$hh->topic = $t;
				echo "<div class=\"indent\">" . $hh->ShowSubtopicsTreeForXml(0) . "</div>";
			break;
			case "3":
				$num = $t->ArticlesApproved( $row, 1 );
				$table_headers = array('date','title','in','author');
				$table_content = array('{FormatDate($row[written_ts])}','{LinkTitle("xml.php?id_topic='.$id_topic.'&id_type='.$id_type.'&id=$row[id_article]","$row[headline] [XML])}','{PathToSubtopic('.$id_topic.',$row[id_subtopic])}','$row[author]');
				echo $hh->ShowTable($row, $table_headers, $table_content, $num);
			break;
			case "14":
				$num = $t->ArticlesApproved( $row, 1 );
				$table_headers = array('date','title','in','author');
				$table_content = array('{FormatDate($row[written_ts])}','{LinkTitle("xml.php?id_topic='.$id_topic.'&id_type='.$id_type.'&id=$row[id_article]","$row[headline] [XML])}','{PathToSubtopic('.$id_topic.',$row[id_subtopic])}','$row[author]');
				echo $hh->ShowTable($row, $table_headers, $table_content, $num);
			break;
			case "15":
				$num = $t->ArticlesApproved( $row, 1 );
				$table_headers = array('date','title','in','author');
				$table_content = array('{FormatDate($row[written_ts])}','{LinkTitle("xml.php?id_topic='.$id_topic.'&id_type='.$id_type.'&id=$row[id_article]","$row[headline] [XML]"','{PathToSubtopic('.$id_topic.',$row[id_subtopic])}','$row[author]');
				echo $hh->ShowTable($row, $table_headers, $table_content, $num);
			break;
		}
	}
	else
	{
		echo "<ul>\n<li>Specific<ul>";
		foreach($pt->types as $key=>$type)
		{
			if ($type > 0 && $key!="root" && $key!="tools")
			{
				echo "<li>" . $hhf->PageTypeLink($types[$type],$type,$id_topic) . "</li>\n";
			}
		}
		$active_modules = Modules::AvailableModules();
		echo "</ul></li>\n";
		echo "<li>Modules<ul>\n";
		foreach($active_modules as $module)
		{
			if($module['layout'] && !$module['internal'] && !$module['global'])
				echo "<li>" . $hhf->ModuleLink($module['path'],$module['id_module'],$id_topic) . "</li>\n";
		}
		echo "</ul></li>\n";
		echo "</ul>\n";
	}
}
else
{
	if (!($id_type>0))
	{
		$types_global = $hh->tr->Translate("page_types_global");
		echo "<ul>\n";
		foreach($pt->gtypes as $key=>$type)
			echo "<li><a href=\"xml.php?id_gtype=$type\">" . $types_global[$type] . "</a></li>\n";
		echo "</ul>\n";
		echo "<p>Otherwise choose the topic</p>\n";
	}
	else
		echo "<p><a href=\"xml_choose.php?id_topic=$id_topic\">Page type</a>: $types[$id_type]</p>\n";
	
	include_once(SERVER_ROOT."/../classes/topics.php");
	$tt = new Topics;
	$num = $tt->AllTopicsP( $row );
	$table_headers = array('topic','in','description');
	$table_content = array('{PageTypeLink($row[name],'.$id_type.',$row[id_topic])}','{PathToTopic($row[id_group],$row[id_topic])}','$row[description]');
	echo $hh->showTable($row, $table_headers, $table_content, $num);
}

if ($id_type>0 && $id_style>=0)
{
	$num = $pt->ft->PageFeatures($row,$id_type,$id_style);
	echo "<p><a href=\"features.php?id_type=$id_type&id_style=$id_style\">" . $num . " features</a> - <a href=\"feature.php?id=0&id_type=$id_type&id_style=$id_style\">add</a></p>\n";
}
include_once(SERVER_ROOT."/include/footer.php");


?>

