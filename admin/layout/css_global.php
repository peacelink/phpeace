<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/css.php");

$csm = new CssManager("global");

$id_css = $_GET['id']; 

$csm->id_pagetype = $id_css;
$row = $csm->CssGet($id_css,0);
$row['css'] = $csm->CssGetLocal($id_css,0);

$title[] = array('CSS','csss.php');

$types = $hh->tr->Translate("page_types_global");

$title[] = array($types[$id_css],'');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form("post","actions.php");

echo $hh->input_hidden("from","css_global");
echo $hh->input_hidden("id_css",$id_css);
echo $hh->input_table_open();

$num = $csm->Revisions($rows,$id_css,0);
if ($num>0)
	echo $hh->input_note("Revisions: <a href=\"css_revisions.php?id_style=0&type=global&id_pagetype=$id_css\">$num</a>");
echo $hh->input_textarea("css","css",$row['css'],80,30,"",$input_right,"css");

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"store_close",'label'=>"submit_close",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer_cp.php");
?>

