<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");

$pt = new PageTypes();

$id_feature = $_GET['id'];

$row2 = $pt->ft->GlobalFeatureGetType($id_feature);
$id_type = $row2['id_type'];

$types = $hh->tr->Translate("page_types_global");

if($ui) {
	$title[] = array('Page types','page_types.php');
	$title[] = array($types[$id_type],'page_type_global.php?id=' . $id_type);
} else {
	$title[] = array('Page types','xsls.php');
	$title[] = array($types[$id_type],'xsl_global.php?id=' . $id_type);
}

$title[] = array('Features','features_global.php?id_type='.$id_type);

$row = $pt->ft->FeatureGet($id_feature);
if(!(isset($row['id_feature'])))
	$hh->Stop();

$title[] = array($row['name'],'feature_global.php?id='.$id_feature.'&id_type='.$id_type);

$id_function = $row['id_function'];

$title[] = array('Parameters','');

$action2 = "update";

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
	
$functions = $hh->tr->Translate("page_functions");
echo "<p>Parameters of function <b>{$functions[$id_function]}</b></p>";
?>
<form method="post" action="actions.php" name="form1">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="from" value="feature_global_params">
<input type="hidden" name="id_feature" value="<?=$id_feature;?>">
<input type="hidden" name="id_type" value="<?=$id_type;?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
$params = $pt->ft->params[$id_function];

$params2 = $pt->ft->ParamsDeserialize($row['params']);

// parametri disponibili nel page type
foreach ($params as $param)
{
	echo $hh->input_param($param,$params2[$param],0,$input_right);
}
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

