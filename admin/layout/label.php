<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/labels.php");

$id_style = (int)$_GET['id_style'];
$id_module = (int)$_GET['id_module'];
$id_language = $_GET['id_language'];
$label = $_GET['label'];

$la = new Labels(0,false,$id_style);

$t_modules = $hh->tr->Translate("modules_names");

$title[] = array('labels','label_styles.php');

if ($id_style>0)
{
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles;
	$style = $s->StyleGet($id_style);
	$title[] = array($style['name'],'label_modules.php?id_style=' . $id_style);
}
else
	$title[] = array('Public labels','label_modules.php?id_style=' . $id_style);

$title_module = ($id_module>0)? $t_modules[$id_module] : "Generic";

$title[] = array($title_module,'labels.php?id_style=' . $id_style . '&id_module=' . $id_module);

$title[] = array('Label: ' . $label,'');

echo $hh->ShowTitle($title);

if ($id_style>0)
{
	$input_right = $s->InputRight($id_style);
}
if ($module_admin)
{
	$input_right = 1;
}

$words = $la->CustomCustom($id_language,$id_module);

$tword = $words[$label];

if(!is_array($tword))
{
	$has_changed = (array_key_exists($label,$la->custom_keys));
	$orig_word = ($has_changed)? $la->custom_keys[$label] : $tword;
	$is_not_custom = (strstr($label,"c_")===false);

	echo $hh->input_form_open();
	echo $hh->input_hidden("from","label");
	echo $hh->input_hidden("id_style",$id_style);
	echo $hh->input_hidden("id_module",$id_module);
	echo $hh->input_hidden("id_language",$id_language);
	echo $hh->input_table_open();
	echo $hh->input_text("label","label",$label,70,0,0);
//	if($la->custom_keys[$label] != "" || $is_not_custom)
	if($orig_word!="")
		echo $hh->input_textarea("label_original","label_original",$orig_word,80,3,"",0);
	echo $hh->input_textarea("label_custom","label_custom",$tword,80,3,"",$input_right);
	$actions = array();
	$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $has_changed);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
else
	echo "Array customisation not supported yet";


include_once(SERVER_ROOT."/include/footer.php");
?>
