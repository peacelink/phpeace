<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/css.php");

$id_pagetype = (int)$_GET['id_pagetype'];
$id_style = (int)$_GET['id_style'];
$id_css = (int)$_GET['id_css'];
$type = $_GET['type'];

$csm = new CssManager($type);

$title[] = array('CSS','csss.php');
$csm->id_pagetype = $id_pagetype;

switch($type)
{
	case "specific":
		$types = $hh->tr->Translate("page_types");
		$title[] = array($types[$id_pagetype],'css_styles.php?id=' . $id_pagetype);
		if ($id_style>0)
		{
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles;
			$style = $s->StyleGet($id_style);
			$title[] = array($style['name'],'css.php?id=' . $id_pagetype . '&id_style=' . $id_style);
		}
		else
			$title[] = array('Generic CSS','');
	break;
	case "global":
		$types = $hh->tr->Translate("page_types_global");
		$title[] = array($types[$id_pagetype],'css_global.php?id=' . $id_pagetype);
	break;
	case "module":
		$t_modules = $hh->tr->Translate("modules_names");
		$title[] = array($t_modules[$id_pagetype],'css_module_styles.php?id='.$id_pagetype);
		if ($id_style>0)
		{
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles;
			$style = $s->StyleGet($id_style);
			$title[] = array($style['name'],'css_module?id=' . $id_pagetype . '&id_style=' . $id_style);
		}
		else
			$title[] = array('Generic CSS','');
	break;
	case "custom":
		$title[] = array('Custom CSS','css_customs.php');
		$row2 = $csm->CssGet($id_pagetype,0);
		$title[] = array($row2['name'],'css_custom.php?id=' . $id_pagetype);
		$id_css = $id_pagetype;
	break;
	case "ext":
		$title[] = array('CSS extensions','css_exts.php');
		$row2 = $csm->CssGet($id_pagetype,$id_style);
		$title[] = array($row2['name'],'css_ext_styles.php?id='.$id_pagetype);
		if ($id_style>0)
		{
			include_once(SERVER_ROOT."/../classes/styles.php");
			$s = new Styles;
			$style = $s->StyleGet($id_style);
			$title[] = array($style['name'],'css_ext.php?id=' . $id_pagetype . '&id_style=' . $id_style);
		}
		else
			$title[] = array('Generic CSS','');
		$id_css = $id_pagetype;
	break;
}

$title[] = array('Revisions','');
echo $hh->ShowTitle($title);

$row = array();
$num = $csm->Revisions($row,$id_css,$id_style);

$table_headers = array('revision','date','user','tag','');
$table_content = array('<div class=\"right\">#$row[revision]</div>','{FormatDateTime($row[change_time_ts])}','{UserLookup($row[id_user])}',
'$row[tag]','{LinkTitle("css_revision.php?id=$row[revision]",' . $hh->tr->Translate("details") . ')}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

