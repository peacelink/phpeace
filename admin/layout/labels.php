<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id_style = (int)$_GET['id_style'];
$id_module = (int)$_GET['id_module'];

$id_language = (int)$_GET['id_language'];
if(!$id_language>0)
	$id_language = $ini->Get("id_language");

$t_modules = $hh->tr->Translate("modules_names");

$title[] = array('labels','label_styles.php');

if ($id_style>0)
{
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles;
	$style = $s->StyleGet($id_style);
	$title[] = array($style['name'],'label_modules.php?id_style=' . $id_style);
}
else
	$title[] = array('Public labels','label_modules.php?id_style=' . $id_style);

if ($id_module>0)
{
	$title[] = array($t_modules[$id_module],'');
}
else
	$title[] = array('Common','');

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/labels.php");
$la = new Labels(0,false,$id_style);

$words = $la->CustomCustom($id_language,$id_module);

?>
<script type="text/javascript">
function dosubmit()
{
	document.forms['form1'].submit();
}
</script>
<?php

echo $hh->input_form("get","labels.php");
echo $hh->input_table_open();
echo $hh->input_hidden("id_style",$id_style);
echo $hh->input_hidden("id_module",$id_module);
echo $hh->input_array("language","id_language",$id_language,$phpeace->Languages($hh->tr->id_language),1,"dosubmit()");
echo $hh->input_submit("change","",1);
echo $hh->input_table_close() . $hh->input_form_close();

echo "<p><a href=\"label_custom.php?id_style=$id_style&id_module=$id_module&id_language=$id_language\">Add custom label</a></p>";

echo "<ul class=\"labels\">";
foreach($words as $word=>$tword)
{
	echo "<li><div class=\"label-key\">$word:</div><div class=\"label-value\">";
	echo "<a href=\"label.php?id_style=$id_style&id_module=$id_module&label=" . urlencode($word) . "&id_language=$id_language\">";
	if(array_key_exists($word,$la->custom_keys))
	{
		echo "<em>" . htmlspecialchars($tword) . "</em>";
	}
	else
		echo htmlspecialchars($tword);
	echo  "</a></div><li>";
}
echo "</ul>";

include_once(SERVER_ROOT."/include/footer.php");
?>

