<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");
$xslm = new XslManager();

if ($module_admin)
	$input_right=1;

$title[] = array('XSL configuration','');
echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","config_xsl_update");
echo $hh->input_table_open();
echo $hh->input_note("Changing these values will force an automatic change on all XSLs. Do not proceed if you prefer to have custom xsl:output settings",true);
echo $hh->input_checkbox("output_indent","xsl_output_indent",$xslm->output_indent,0,$input_right);

$cdoctypes = array();
$doctypes = $xslm->DocTypes();
foreach($doctypes as $key=>$value)
	$cdoctypes[$key] = $key;

echo $hh->input_array("output_doctype","xsl_output_doctype",$xslm->output_doctype,$cdoctypes,$input_right);

echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

