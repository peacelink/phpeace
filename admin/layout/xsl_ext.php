<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");

$xslm = new XslManager("ext");

$id = $_GET['id'];
$id_style = $_GET['id_style'];

$title[] = array('Page types','xsls.php');
$title[] = array('XSL extensions','xsl_exts.php');

if ($id>0)
{
	$row = $xslm->XslGet($id,$id_style);
	$title[] = array($row['name'],'xsl_ext_styles.php?id='.$row['id_xsl']);
	$xsl = $row['xsl'];
	$xslm->xh->Check($row['xsl'],FALSE);
}
else
{
	$title[] = array("new",'');
	$xsl = $xslm->TypeInit("root");
}

if ($id_style>0)
{
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles;
	$style = $s->StyleGet($id_style);
	$title[] = array($style['name'],'');
}
else
	$title[] = array('Generic XSL','');


if ($id_style>0)
{
	$input_right = $s->InputRight($id_style);
}
if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","xsl_ext");
echo $hh->input_hidden("id_xsl",$id);
echo $hh->input_hidden("id_style",$id_style);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topics.php");
if ($id_style>0)
	echo $hh->input_note("Style: <a href=\"style.php?id=$id_style\">$style[name]</a>");
$num = $xslm->Revisions($rows,$id,$id_style);
if ($num>0)
	echo $hh->input_note("Revisions: <a href=\"xsl_revisions.php?id_style=$id_style&type=ext&id_pagetype=$id\">$num</a>");
if ($id>0)
{
	echo $hh->input_note("This XSL is never used directly; it can only be included with &lt;xsl:include href=\"../$id_style/ext_$id.xsl\" /&gt; ");
	echo $hh->input_note("Warning: &_amp; substitutes &amp;amp; and &lt;_textarea&gt; substitutes  &lt;textarea&gt;; no need to fix them, it's going to be done after form submit");
}
echo $hh->input_text("name","name",$row['name'],50,0,$input_right && $id_style==0);
echo $hh->input_textarea("xsl","xsl",$xsl,80,30,"",$input_right);

$actions = array();
$actions[] = array('action'=>(($id>0)?"update":"create"),'label'=>"submit",'right'=>$input_right);
if ($id>0 && $id_style==0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
