<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

/*
if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

include_once(SERVER_ROOT."/../classes/xmlhelper.php");
include_once(SERVER_ROOT."/../classes/layout.php");

$xh = new XmlHelper();

$id_xsl = $_GET['id'];

$title[] = array('XSL e tipi di pagine','xsls.php');

$types = $hh->tr->Translate("page_types");

// broken da rifare
$row = $xh->XslGetById($id_xsl);

$title[] = array($types[$row['id_type']],'xsl.php?id='.$row['id_type']);

if ($row['id_style']>0)
	$title[] = array($row['name'],'');
else
	$title[] = array('XSL generale','');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

if ($input_right==1)
{
function startElement($parser, $name, $attrs)
{
	if ($name=="XSL:TEMPLATE" || $name=="XSL:VARIABLE" || $name=="XSL:PARAM")
	{
		echo "<ul>\n";
		echo "<li>$name ";
		foreach($attrs as $key=>$attr)
		{
			if ($key=="NAME")
				echo " - <b>$attr</b>";
			if ($key=="MATCH")
				echo " - <i>match: $attr</i>";
			if ($key=="MODE")
				echo " - <i>mode: $attr</i>";
		}
		echo "</li>\n";
	}
}

function endElement($parser, $name)
{
	if ($name=="XSL:TEMPLATE" || $name=="XSL:VARIABLE" || $name=="XSL:PARAM")
	{
		echo "</ul>\n";
	}
}

	$xp = xml_parser_create();
	xml_set_element_handler($xp, "startElement", "endElement");
	xml_parse($xp, $row['xsl']);
	xml_parser_free($xp);
}
include_once(SERVER_ROOT."/include/footer.php");
*/
?>

