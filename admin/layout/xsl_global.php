<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");

$xslm = new XslManager("global");

$id_xsl = $_GET['id']; 
$xslm->id_pagetype = $id_xsl;
$row = $xslm->XslGet($id_xsl,0);
$row['xsl'] = $xslm->XslGetLocal($id_xsl,0);
$xslm->xh->Check($row['xsl'],FALSE);

$title[] = array('Page types','xsls.php');

$types = $hh->tr->Translate("page_types_global");

$title[] = array($types[$id_xsl],'');

$input_update_right = 0;
if ($module_admin)
{
	$input_right = 1;
	$input_update_right = 1;
}
if($row['outsourced'])
	$input_right = 0;

echo $hh->ShowTitle($title);
echo "<p><a href=\"features_global.php?id_type=$id_xsl\">Features</a></p>";

echo $hh->input_form_open();
echo $hh->input_hidden("from","xsl_global");
echo $hh->input_hidden("id_xsl",$id_xsl);
echo $hh->input_hidden("outsourced",$row['outsourced']);
echo $hh->input_table_open();

if ($row['outsourced'])
	echo $hh->input_note($hh->tr->TranslateParams("update_by_server",array("XSL")));
else 
	echo $hh->input_note($hh->tr->TranslateParams("forked",array("XSL")));
$num = $xslm->Revisions($rows,$id_xsl,0);
if ($num>0)
	echo $hh->input_note("Revisions: <a href=\"xsl_revisions.php?id_style=0&type=global&id_pagetype=$id_xsl\">$num</a>");

echo $hh->input_textarea("xsl","xsl",$row['xsl'],80,30,"",$input_right);
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"fork",'label'=>($row['outsourced'])?"fork":"unfork",'right'=>$input_update_right);
echo $hh->input_actions($actions,$input_right || $input_update_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

