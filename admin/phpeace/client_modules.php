<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id_client = $_GET['id'];

$title[] = array('clients','clients.php');

$phpeace = new Phpeace;
$client = $phpeace->ClientGet($id_client);

$client_modules_ids = $phpeace->ClientModulesIDs($id_client);

$external_modules = array();
$active_modules = array();
$tra_modules = $hh->tr->Translate("modules_names");
$modules = Modules::AvailableModules(false);

foreach($modules as $module)
{
	if($module['internal']=="0")
	{
		$id_module = $module['id_module'];
		$active_modules[$id_module] = $tra_modules[$id_module];
		$external_modules[$id_module] = in_array($id_module,$client_modules_ids)? 1 : 0;
	}
}
$title[] = array($client['name'],'client.php?id='.$id_client);
$title[] = array("modules_on",'');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","client_modules");
echo $hh->input_hidden("id_client",$id_client);
echo $hh->input_table_open();
echo $hh->input_list_array("modules_on","modules_on",$external_modules,$active_modules,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

