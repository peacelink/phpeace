<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/phpeace.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();

if(count($post)==0 && count($_GET)==0)
	header("Location: /gate/index.php");

$phpeace = new PhPeace;

$action2	= $post['action2'];
$from		= $post['from'];

if ($from=="css")
{
	$ucss	= $fh->HttpPostUnescapedVar("css");
	$phpeace->CssStore($ucss);
	header("Location: index.php");
}

if ($from=="js")
{
	$ujs	= $fh->HttpPostUnescapedVar("js");
	$ah->JsStore($ujs);
	header("Location: index.php");
}

if ($from=="logo")
{
	$file	= $fh->UploadedFile("img");
	if ($file['ok'])
		$phpeace->LogoUpdate($file);
	header("Location: index.php");
}

if ($from=="favicon")
{
	$file	= $fh->UploadedFile("img");
	if ($file['ok'])
		$phpeace->FaviconUpdate($file);
	header("Location: index.php");
}

if ($from=="client")
{
	$action = $fh->ActionGet($post);
	$id_client		= $post['id_client'];
	$name			= $post['name'];
	$contact		= $post['contact'];
	$email			= $post['email'];
	$approved	 	= $fh->Checkbox2bool($post['approved']);
	if ($action=="update" && $name!="")
		$phpeace->ClientUpdate( $id_client, $name, $contact, $email, $approved );
	if ($action=="delete")
		$phpeace->ClientDelete($id_client);
	header("Location: clients.php");
}

if($from=="client_modules")
{
	$action = $fh->ActionGet($post);
	$id_client		= $post['id_client'];
	if ($action=="update")
	{
		$client_modules = array();
		$modules = Modules::AvailableModules(false);
		foreach($modules as $module)
		{
			if($module['internal']==0)
			{
				$module['active'] = $fh->CheckBox2Bool($post['modules_on_' . $module['id_module'] ]);
				$client_modules[] = $module;
			}
		}
		$phpeace->ClientModulesUpdate($id_client,$client_modules);
	}
	header("Location: client.php?id=$id_client");
}

if ($from=="config_update")
{
	$install_date 	= $fh->DateMerge("install_date",$post);
	$debug_mail		= $fh->Checkbox2bool($post['debug_mail']);
	$mail_output	= $post['mail_output'];
	$phpeace_server	= $fh->String2Url($post['phpeace_server']);
	$phpeace->ConfigurationUpdate($debug_mail,$mail_output,$phpeace_server,$install_date);
	header("Location: index.php");
}

if ($from=="at_work")
{
	$at_work	= $fh->Checkbox2bool($post['at_work']);
	$phpeace->AtWork($at_work);
	header("Location: index.php");
}

if ($from=="manual_update")
{
	$execute_script	= $fh->Checkbox2bool($post['execute_script']);
	$file		= $fh->UploadedFile("tar");
	if ($phpeace->AmIAdmin() && $file['ok'])
	{
		$phpeace->AtWork(1);
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$fm->DirAction("temp","check");
		$fm->MoveUpload($file['temp'],"/temp/$file[name]");
		$build = $phpeace->FileName2VersionId($file['name']);
		$phpeace->VersionInstall($build,FALSE,$execute_script);
		$phpeace->AtWork(0);
	}
	header("Location: index.php");
}
?>
