<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id_client = $_GET['id'];

$title[] = array('clients','clients.php');

$phpeace = new Phpeace;
$client = $phpeace->ClientGet($id_client);

$title[] = array($client['name'],'');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","client");
echo $hh->input_hidden("id_client",$id_client);
echo $hh->input_table_open();
echo $hh->input_text("name","name",$client['name'],20,0,$input_right);
echo $hh->input_text("admin_web","admin_web",$client['admin_web'],40,0,0);
echo $hh->input_text("contact_main","contact",$client['contact'],20,0,$input_right);
echo $hh->input_text("email","email",$client['email'],20,0,$input_right);
echo $hh->input_text("php","php_version",$client['php_version'],30,0,0);
echo $hh->input_text("install_key","install_key",$client['install_key'],20,0,0);
echo $hh->input_text("build","build",$client['latest_version'],10,0,0);
echo $hh->input_checkbox("approved","approved",$client['approved'],0,$input_right);
$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && $id_client>0));
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

$active = array();
$deactive = array();
$t_modules = $hh->tr->Translate("modules_names");
include_once(SERVER_ROOT."/../classes/varia.php");
$v = new Varia();
$modules = $v->Deserialize($client['modules'],false);
if (is_array($modules))
{
	// TODO should compare internal and layout fields with server ones
	foreach($modules as $module)
	{
		if($module['internal']=="1" && $module['active']=="0")
			$deactive[] = $t_modules[$module['id_module']];
		if($module['internal']=="0" && $module['active']=="1")
			$active[] = $t_modules[$module['id_module']];
	}
}

if (count($deactive)>0)
	echo "<p>" . $hh->tr->Translate("modules_off") . ": " . $hh->Array2String($deactive) . "</p>";

echo "<p><a href=\"client_modules.php?id=$id_client\">" . $hh->tr->Translate("modules_on") . "</a>: " . $hh->Array2String($active) . "</p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

