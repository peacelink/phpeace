<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
include_once(SERVER_ROOT."/../modules/assos.php");

$as = new Assos();
$sqlstr = "SELECT ass.id_ass,ass.nome,ass.nome2,ass_tipo.ass_tipo,ass.indirizzo,ass.citta,ass.cap,prov.prov,ass.tel,ass.fax,ass.email,ass.sito,ass.ccp,ass.note,ass.referente,ass.pubblicaz,ass.fonte,date_format(ass.lastupd,'%d.%m.%Y') from ass,prov,ass_tipo where ass.id_prov=prov.id_prov and ass.id_tipo=ass_tipo.id_ass_tipo";
include_once(SERVER_ROOT."/../classes/xmlhelper.php");
$xh = new XmlHelper();
$xml = $xh->header . $as->DumpXml($sqlstr);

if ($xh->Check($xml))
{
	header('Content-type: text/xml');
	echo $xml;
}


?>

