<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");

$id = $_GET['id'];

$asso = new Asso($id);

if ($module_admin || $module_right)
	$input_right = 1;

$title[] = array($asso->name,'org.php?id='.$id);
$title[] = array("history",'');
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('details','org.php?id='.$id);
$tabs[] = array('categories','keywords.php?id='.$id);
$tabs[] = array('additional_fields','params.php?id='.$id);
$tabs[] = array('image_associated','org_image.php?id='.$id);
$tabs[] = array('docs','org_docs.php?id='.$id);
$tabs[] = array('events','org_events.php?id='.$id);
$tabs[] = array('history','');
echo $hh->Tabs($tabs);

include_once(SERVER_ROOT."/../classes/history.php");
$h = new History;

$row = [];
$id_type = $ah->r->types['org'];
$num = $h->HistoryAll( $row, $id_type, $id );

$table_headers = array('date','ip','user','action');
$table_content = array('{FormatDateTime($row[ts_time])}','$row[ip]','{UserVisitor($row[id_user],$row[name],$row[id_p],$row[name_p])}','{HistoryAction($row[action])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
