<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/assos.php");

$id_org = $_GET['id'];

if($module_admin)
	$input_right = 1;
if ($id_org>0 && $input_right)
{
	$asso = new Asso($id_org);	
	$title[] = array($asso->name,'org.php?id='.$id_org);

	$title[] = array('delete','');
	echo $hh->ShowTitle($title);

	echo "<h3>" . $hh->tr->Translate("delete") . " \"{$asso->name}\"</h3>";	
	
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","org_delete");
	echo $hh->input_hidden("id_org",$id_org);
	echo $hh->input_table_open();
	$actions = array();
	$actions[] = array('action'=>"submit",'label'=>"confirm",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>

