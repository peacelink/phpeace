<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/htmlhelper.php");
include_once(SERVER_ROOT."/../modules/assos.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
$hh = new HtmlHelper();
$fh = new FormHelper();
$get = $fh->HttpGet();
$as = new Assos();

$page_title = $hh->tr->Translate("org_related");
include_once(SERVER_ROOT."/../admin/include/head.php");
?>
<body>
<script type="text/javascript">
function set_org(id_org,org_name)
{
	opener.document.forms['form1'].id_org.value = id_org;
	opener.document.forms['form1'].descriz_id_org.value = org_name;
	self.close();
}
</script>
<?php
$w = $get['w'];

// PAGING GLOBALS
$conf = new Configuration();
$records_per_page = $conf->Get("records_per_page");
$current_page = ($get['p'] > 0)? $get['p'] : 1;
$params = array(	'name' => $get['name'],
					);
$num = $as->Search( $row, $params, 1, 0 );

foreach($rows as $row)
{
	$row['headline_js'] = $hh->th->TextReplaceForJavascript($row['topic_name'].": ".$row['headline']);
	$rows2[] = $row;
}

$table_headers = array('type','name','town');
if ($w=="event") {
   $table_content = array('$row[ass_tipo]',
	'<div><a href=\"#\" onClick=\"set_org(\'$row[id_ass]\',\'$row[nome]\')\">$row[nome]</a></div>',
   '$row[citta] ($row[geo_name]');
}
echo $hh->showTable($row, $table_headers, $table_content, $num);
?>
<form method="get" action="/orgs/popup_search2.php" name="form1">
<table border=0 cellpadding=0 cellspacing=7>
<?php
echo $hh->input_text("name","name",$get['name'],30,0,1);
?>
<tr><td>&nbsp;</td><td><input type="submit" class="input-submit" value="<?=$hh->tr->Translate("search");?>">
&nbsp;&nbsp;<input type="reset" value="<?=$hh->tr->Translate("cancel");?>"></td></tr>
</table>
<input type="hidden" name="id_org" value="<?=$id_org;?>">
<input type="hidden" name="w" value="<?=$w;?>">
</form>

</body>
</html>
