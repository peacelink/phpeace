<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../modules/mastodon.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$m = new Mastodon();

$fh = new FormHelper;
$post = $fh->HttpPost();
$unescaped_post = $fh->HttpPost(false,false,false);
$get = $fh->HttpGet();
$from		= $post['from'];
$from2		= $get['from2'];

if ($from=="config" && $module_admin) {
    $ini = new Ini;
    $last_follower_id	= $fh->Null2Zero($post['last_follower_id']);
    $welcome	= $unescaped_post['welcome'];
    $ini->SetModule("mastodon","last_follower_id",$last_follower_id);
    $ini->SetModule("mastodon","welcome",$welcome);
    header("Location: index.php");
}
?>
