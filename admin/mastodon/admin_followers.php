<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/mastodon.php");

if ($module_admin)
	$input_right=1;

$m = new Mastodon();
$admin = $m->Account($m->mastodon_admin);

$title[] = array("@{$admin['username']} followers",'');
echo $hh->ShowTitle($title);

echo "<p><a href=\"{$m->server}/users/{$admin['username']}/followers\" target=\"_blank\">{$admin['followers_count']} Followers</a></p>";

$nofollowers = $m->FollowAdmin();

echo "<h3>Account che non seguono @{$admin['username']} (".count($nofollowers).")</h3>";
echo "<ul>";
foreach($nofollowers as $nofollower) {
    echo "<li><a href=\"{$m->server}/users/$nofollower\" target=\"_blank\">$nofollower</a></li>\n";
}
echo "</ul>";


include_once(SERVER_ROOT."/include/footer.php");
?>

