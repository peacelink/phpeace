<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$title[] = array('feeds','');
echo $hh->ShowTitle($title);

$o = new Ontology;
$num = $o->KeywordsFeeds( $rows );

$table_headers = array('keyword','description','public');
$table_content = array('{LinkTitle("feed.php?id=$row[id_keyword]",$row[keyword])}','$row[description]','{Bool2YN($row[public])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if($module_admin)
{
	echo "<p><a href=\"feed.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>

