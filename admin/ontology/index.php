<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology();

echo $hh->ShowTitle($title);

$trm13 = new Translator($hh->tr->id_language,13);

echo $hh->input_form("get","find.php");
echo $hh->input_table_open();
echo $hh->input_text("keyword","q","",20,0,1);
$types = $hh->tr->Translate("keyword_types");
$types[0] = $hh->tr->Translate("all_option");
echo $hh->input_array("type","id_type",0,$types,1);
$actions[] = array('action'=>"search",'label'=>"search",'right'=>1);
echo $hh->input_actions($actions,1);
echo $hh->input_table_close() . $hh->input_form_close();

echo "<h3>" . $trm13->Translate("keywords") . "</h3>";
echo "<ul>";
echo "<li><a href=\"keywords.php\">" . $hh->tr->Translate("list") . "</a></li>";
echo "<li><a href=\"keyword.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>";
if($module_admin) 
	echo "<li><a href=\"feeds.php\">Feeds</a></li>";
echo "<li><a href=\"landings.php\">Landing</a></li>";
echo "</ul>";

echo "<h3><a href=\"relations.php\">" . $trm13->Translate("relations") . "</a></h3>";

echo "<h3><a href=\"statements.php\">" . $trm13->Translate("statements") . "</a></h3>";
$relations = $o->RelationsAll();
echo "<ul>";
foreach($relations as $relation)
	echo "<li><a href=\"statements.php?id_relation={$relation['id_relation']}\">{$relation['name']}</a></li>";
echo "</ul>";

$ktypes = $hh->tr->Translate("keyword_types");
echo "<h3>" . $trm13->Translate("tree_types") . "</h3>";
echo "<ul>";
foreach($ktypes as $key=>$ktype)
	if($key>0)
		echo "<li><a href=\"tree.php?id_type={$key}\">{$ktype}</a></li>";
echo "</ul>";

if ($module_admin)
{
	echo "<h3>" . $hh->tr->Translate("mainteinance") . "</h3>";
	echo "<ul>";
	echo "<li><a href=\"notused.php\">" . $trm13->Translate("notused") . "</a>";
	echo "<li><a href=\"orphans.php\">" . $trm13->Translate("orphans") . "</a>";
	echo "<li><a href=\"statements_broken.php\">" . $trm13->Translate("statements_broken") . "</a>";
	echo "<li><a href=\"merge.php\">" . $trm13->Translate("merge") . "</a>";
	echo "</ul>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
