<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");

$id = (int)$get['id'];
$bb = new Books();

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array('list','books.php');
$article_title = "";
$article_topic = 0;

if ($id>0)
{
	$b = new Book($id);
	$title[] = array($b->title,'');
	$row = $b->BookGet();
	$id_language = $row['id_language'];
	$approved = $row['approved'];
	if($row['id_article']>0)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($row['id_article']);
		$a->ArticleLoad();
		$article_title = htmlspecialchars($a->headline);
		$article_topic = $a->id_topic;
	}
}
else
{
	$title[] = array('add_new','');
	$id_language = 1;
	$approved = 0;
}

if ($module_admin || $module_right)
	$input_right = 1;

if ($module_admin)
	$input_super_right = 1;

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("details",'');
$tabs[] = array($trm16->Translate("cover"),'book_cover.php?id='.$id);
if($ah->IsModuleActive(31))
	$tabs[] = array("ebook",'ebook.php?id='.$id);
$tabs[] = array($trm16->Translate("reviews"),'reviews.php?id_book='.$id);
echo $hh->Tabs($tabs);

?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			title: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","book");
echo $hh->input_hidden("id_book",$id);
echo $hh->input_table_open();

if ($id>0)
{
	$reviews = $b->Reviews( $rows,false,false,0,false,0,true );
	
	if ($row['cover_format']!="")
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$filename = "covers/0/$id".".".$i->convert_format;
		echo "<tr><td align=\"right\">" . $trm16->Translate("cover") . "</td><td><a href=\"book_cover.php?id=$id\"><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[0]}\" border=\"0\"></a> <a href=\"book_cover.php?id=$id\">" . $hh->tr->Translate("change") . "</a></td></tr>\n";
	}
}

echo $hh->input_text("author","author",$row['author'],60,0,$input_right);
echo $hh->input_text("title","title",$row['title'],60,0,$input_right);
echo $hh->input_textarea($trm16->Translate("subtitle"),"summary",$row['summary'],70,3,"",$input_right);
echo $hh->input_textarea("description","description",$row['description'],70,5,"",$input_right);

$p = new Publisher;
echo $hh->input_row($trm16->Translate("publisher"),"id_publisher",$row['id_publisher'],$p->AllPublishers(),"none_option",0,$input_right);
if ($row['id_publisher']>0)
	echo $hh->input_row($trm16->Translate("category"),"id_category",$row['id_category'],$p->CategoriesAll($row['id_publisher']),"-",0,$input_right);

echo $hh->input_month("month_item","p_month",$row['p_month'],$input_right,true);
echo $hh->input_text("year","p_year",$row['p_year'],5,0,$input_right);
echo $hh->input_text("ISBN","isbn",$row['isbn'],15,0,$input_right);
echo $hh->input_text($trm16->Translate("catalog_number"),"catalog",$row['catalog'],30,0,$input_right);
echo $hh->input_array("language","id_language",$id_language,$hh->tr->Translate("languages"),$input_right);
echo $hh->input_text("pages","pages",$row['pages'],5,0,$input_right);
echo $hh->input_textarea("notes","notes",$row['notes'],70,4,"",$input_right);
echo $hh->input_article($trm16->Translate("associated_article"),"id_article","'/articles/popup_search.php?w=book&id_article='+document.forms['form1'].id_article.value",$row['id_article'],$article_title,$article_topic,$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id,$o->types['book'],$keywords,$input_right);

echo $hh->input_money("price","price",$row['price'],10,0,$input_right,$ini->Get("default_currency"));

$tikeywords = $bb->KeywordsInternal();
echo $hh->input_internal_keywords($id,$tikeywords,"book",$input_super_right,$input_right);

echo $hh->input_separator("administration");
echo $hh->input_checkbox("approved","approved",$row['approved'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id>0 && !$reviews);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
