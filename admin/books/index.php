<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");
$bb = new Books();

echo $hh->ShowTitle($title);

$url = $ini->Get("pub_web") . "/" . $ini->Get("books_path") . ($bb->id_topic>0? "?id_topic={$bb->id_topic}":"");
echo "<div>" . $hh->tr->Translate("published_info") ." <a href=\"$url\" target=\"_blank\">$url</a>";
echo " - <a href=\"/topics/preview.php?id_type=0&id_topic={$bb->id_topic}&id_module=16&module=books\" target=\"_blank\">" . $hh->tr->Translate("preview") . "</a>";

$trm16 = new Translator($hh->tr->id_language,16);

echo "<h3>" . $hh->tr->Translate("books") . "</h3>";
echo "<ul>";
$num1 = $bb->BooksApproved( $row, array('approved'=>0) );
echo "<li><a href=\"books.php?approved=0\">" . $hh->tr->Translate("to_approve") . "</a> ($num1)</li>";
$num2 = $bb->BooksApproved( $row, array('approved'=>1) );
echo "<li><a href=\"books.php?approved=1\">" . $hh->tr->Translate("approveds") . "</a> ($num2)</li>\n";
echo "<li><a href=\"book.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>\n";

echo "<h3>" . $trm16->Translate("publishers") . "</h3>";
echo "<ul>";
echo "<li><a href=\"publishers.php\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"publisher.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>\n";

echo "<h3>" . $hh->tr->Translate("reviews") . "</h3>";
$num3 = $bb->Reviews( $row, 0 );
$num4 = $bb->Reviews( $row, 1 );
echo "<ul>";
echo "<li><a href=\"reviews.php?approved=0\">" . $trm16->Translate("reviews_to_approve") . "</a> ($num3)</li>";
echo "<li><a href=\"reviews.php?approved=1\">" . $trm16->Translate("reviews_approved") . "</a> ($num4)</li>";
echo "</ul>\n";
	
echo "<h3>" . $hh->tr->Translate("search") . "</h3>";
echo $hh->input_form("get","search2.php");
echo $hh->input_table_open();
echo $hh->input_text("text","q","","30",0,1);
$actions = array();
$actions[] = array('action'=>"submit",'label'=>"search",'right'=>true);
echo $hh->input_actions($actions,1);
echo $hh->input_table_close() . $hh->input_form_close();

if ($module_admin)
{
	echo "<h3>" . $hh->tr->Translate("management") . "</h3>";
	echo "<ul>";
	echo "<li><a href=\"homepage.php\">" . $hh->tr->Translate("homepage") . "</a></li>\n";
	echo "<li><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></li>\n";
	echo "<li><a href=\"reviews_config.php\">" . $trm16->Translate("reviews_configuration") . "</a></li>\n";
	echo "</ul>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
