<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");

$id_publisher = $_GET['id'];

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array($trm16->Translate("publishers"),'publishers.php');
$p = new Publisher;
$row0 = $p->PublisherGet($id_publisher);
$title[] = array($row0['name'],'publisher.php?id='.$id_publisher);
$title[] = array($trm16->Translate("categories"),'');

echo $hh->ShowTitle($title);

$num = $p->Categories( $row, $id_publisher );
$table_headers = array('name','description','books');
$table_content = array('{LinkTitle("category.php?id=$row[id_category]&id_publisher='.$id_publisher.'",$row[name])}','$row[description]',
'{LinkTitle("books.php?id_c=$row[id_category]","$row[counter]")}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"category.php?id_publisher=$id_publisher&id=0\">" . $hh->tr->Translate("add_new") ."</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

