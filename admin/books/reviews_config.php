<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
$hhf = new HHFunctions();

if ($module_admin)
	$input_right=1;

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array($trm16->Translate("reviews_configuration"),'');
echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/resources.php");

$r = new Resources();

$params = $r->Params("review");
if(count($params)>0)
{
	echo "<ul>\n";
	foreach($params as $param)
		echo "<li>" . ($param['public']=="0"? "(inv.) ":"") . $hhf->LinkTitle("review_param.php?id={$param['id_resource_param']}",$param['label']) . " (" . $hh->tr->Translate("paramtype_" . $param['type']) . ")</li>\n";
	echo "</ul>\n";
}
if($input_right)
	echo "<p><a href=\"review_param.php?id=0\">" . $trm16->Translate("field_add") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

