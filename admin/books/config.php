<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");
$bb = new Books();
$row = $bb->TopicConfig(0);

if ($module_admin)
	$input_right=1;

$title[] = array('configuration','');
echo $hh->ShowTitle($title);

$trm16 = new Translator($hh->tr->id_language,16);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			path: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","config_update");
echo $hh->input_table_open();
echo $hh->input_text("path","path",$hh->ini->Get("books_path"),20,0,$input_right);

echo $hh->input_array("homepage_type","books_home_type",$row['books_home_type'],$trm16->Translate("books_home_types"),$input_right);
echo $hh->input_array("allow_reviews","books_reviews",$row['books_reviews'],$hh->tr->Translate("allow_feedback"),$input_right);
$reviews_options = $hh->tr->Translate("reviews_options");
unset($reviews_options[1]);
unset($reviews_options[2]);
unset($reviews_options[3]);
echo $hh->input_array("show_reviews","show_reviews",$row['show_reviews'],$reviews_options,$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$topics = new Topics;
echo $hh->input_topics($bb->id_topic,0,$topics->AllTopics(),"none_option",$input_right,false,"books_id_topic","force_topic");

echo $hh->input_checkbox("search_include","search_books",$hh->ini->GetModule("books","search_books",0),0,$input_right);

$ikeywords = $bb->KeywordsInternal();
echo $hh->input_text("keywords_internal","keywords_internal",$hh->th->ArrayMulti2StringIndex($ikeywords,'keyword'),30,0,0,$hh->Wrap("change","<a href=\"keywords.php\">","</a>",$input_right));

echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

