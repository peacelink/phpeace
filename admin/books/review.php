<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");

$id = $_GET['id'];
$id_book = $_GET['id_book'];

$b = new Book($id_book);

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array('list','books.php');
$title[] = array($b->title,'book.php?id='.$id_book);
$title[] = array($trm16->Translate("reviews"),'reviews.php?id_book='.$id_book);
if ($id>0)
{
	$action2 = "update";
	$review = $b->ReviewGet($id);
	$title[] = array($trm16->Translate("review"),'');
	$approved = $review['approved'];
	$vote = $review['vote'];
}
else
{
	$action2 = "insert";
	$title[] = array($trm16->Translate("review_add"),'');	
	$approved = 1;
	$vote = 3;
}

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}	

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("details",'book.php?id='.$id_book);
$tabs[] = array($trm16->Translate("cover"),'book_cover.php?id='.$id_book);
$tabs[] = array("ebook",'ebook.php?id='.$id_book);
$tabs[] = array($trm16->Translate("reviews"),'reviews.php?id_book='.$id_book);
echo $hh->Tabs($tabs);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			review: "required"
		}
	});
});
</script>
<?php
echo $hh->input_form("post","/books/actions.php");
echo $hh->input_hidden("from","review");
echo $hh->input_hidden("id_book",$id_book);
echo $hh->input_hidden("id_review",$id);
echo $hh->input_table_open();

echo $hh->input_text($trm16->Translate("book"),"book_title",$b->title,50,0,0);
echo $hh->input_date("insert_date","insert_date",$review['insert_date_ts'],$input_right);
echo $hh->input_textarea($trm16->Translate("review"),"review",$review['review'],80,10,"",$input_right);

$combo_values = array('5'=>'*****','4'=>'****','3'=>'***','2'=>'**','1'=>'*');
echo $hh->input_array($trm16->Translate("vote"),"vote",$vote,$combo_values,$input_right);

if($review['id_p']>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$user = $pe->UserGetById($review['id_p']);
	echo $hh->input_text("name","name",$user['name1'],50,0,0);
	echo $hh->input_text("email","email",$user['email'],50,0,0);
}
else
{
	echo $hh->input_text("name","name",$review['name'],50,0,$input_right);
	echo $hh->input_text("email","email",$review['email'],50,0,$input_right);
}
echo $hh->input_array("language","id_language",$review['id_language'],$phpeace->Languages($hh->tr->id_language),$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo $hh->input_topics($review['id_topic'],0,$tt->AllTopics(),"all_option",$input_right);

echo $hh->input_checkbox($trm16->Translate("important"),"important",$review['important'],0,$input_right);
echo $hh->input_checkbox("approved","approved",$approved,0,$input_right);

include_once(SERVER_ROOT."/../classes/varia.php");
$v =new Varia();
$deparams = $v->Deserialize($review['rparams']);

include_once(SERVER_ROOT."/../classes/resources.php");
$r = new Resources();
$rparams = $r->Params("review");
if(count($rparams)>0)
{
	echo $hh->input_separator("additional_fields");
	foreach($rparams as $rparam)
	{
		echo $hh->input_keyword_param($rparam['id_resource_param'],$rparam['label'],$rparam['type'],$deparams['rp_'.$rparam['id_resource_param']],$input_right,$rparam['public'],$rparam['params']);
	}
}

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($id>0 && $input_super_right));
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

