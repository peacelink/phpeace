<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");

$bb = new Books();

$trm16 = new Translator($hh->tr->id_language,16);

$approved = $_GET['approved'];
if (!isset($approved))
	$approved = 1;

$title[] = array('list','');
echo $hh->ShowTitle($title);

$params = array();
if (isset($_GET['approved']))
	$params['approved'] = $_GET['approved'];
if (isset($_GET['id_c']))
	$params['id_c'] = $_GET['id_c'];
if (isset($_GET['id_p']))
	$params['id_p'] = $_GET['id_p'];

$num = $bb->BooksApproved( $row, $params );

$table_headers = array('author','title',$trm16->Translate("publisher"),$trm16->Translate("category"),'published');
$table_content = array('$row[author]','{LinkTitle("book.php?id=$row[id_book]",$row[title])}',
'{LinkTitle("publisher.php?id=$row[id_publisher]",$row[name])}',
'{LinkTitle("category.php?id=$row[id_category]&id_publisher=$row[id_publisher]",$row[category])}','$row[p_month]/$row[p_year]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><hr><a href=\"book.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

