<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");

$id_book = $_GET['id_book'];
$approved = (int)$_GET['approved'];

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array('list','books.php');
if ($id_book>0)
{
	$b = new Book($id_book);
	$title[] = array($b->title,'book.php?id='.$id_book);
	$title[] = array($trm16->Translate("reviews"),'');
	$num = $b->Reviews( $row,false,false,0,false,0,true );
	$table_headers = array('date',$trm16->Translate("review"),'approved','important','topic');
	$table_content = array('{FormatDate($row[insert_date_ts])}','{LinkTitle("review.php?id=$row[id_review]&id_book='.$id_book.'",$row[review],20)}',
	'{Bool2YN($row[approved])}','{Bool2YN($row[important])}','$row[topic_name]');
}
else
{
	$title[] = array($trm16->Translate("reviews"),'');
	$bb = new Books();
	$num = $bb->Reviews( $row, $approved );
	$table_headers = array('date','book','topic',$trm16->Translate("reviews"));
	$table_content = array('{FormatDate($row[insert_date_ts])}','$row[title]','$row[topic_name]','{LinkTitle("review.php?id=$row[id_review]&id_book=$row[id_book]",$row[review],20)}');
}
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("details",'book.php?id='.$id_book);
$tabs[] = array($trm16->Translate("cover"),'book_cover.php?id='.$id_book);
$tabs[] = array("ebook",'ebook.php?id='.$id_book);
$tabs[] = array($trm16->Translate("reviews"),'');
echo $hh->Tabs($tabs);

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($id_book>0)
	echo "<p><a href=\"review.php?id_book=$id_book&id=0\">" . $trm16->Translate("review_add") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

