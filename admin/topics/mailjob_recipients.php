<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/mailjobs.php");

$id_mail = (int)$_GET['id'];
$id_topic = (int)$_GET['id_topic'];

include_once(SERVER_ROOT."/../classes/topic.php");
$t = new Topic($id_topic);
$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
$title[] = array("mailjobs",'/topics/mailjobs.php?id='.$id_topic);

$mj = new Mailjobs();
$row = $mj->MailJobGet($id_mail);
$title[] = array($row['subject'],'');

echo $hh->ShowTitle($title);

if($row['track'])
{
	$statuses = $hh->tr->Translate("mailjob_status");
	echo "<ul>";
	for($i=1;$i<4;$i++)
	{
		$rows = array();
		$num = $mj->RecipientsStatus($rows,$id_mail,$i);
		echo "<li>{$statuses[$i]}: " . $hh->Wrap($num,"<a href=\"mailjob_recipients_status.php?id=$id_mail&id_topic=$id_topic&status=$i\">","</a>",$num>0) . "</li>";
	}
	echo "</ul>";
}

$hhf = new HHFunctions();

echo $hh->input_form_open();
echo $hh->input_hidden("from","mailjob");
echo $hh->input_table_open();
echo $hh->input_date("date","send_date",$row['send_date_ts'],0);
echo $hh->input_text("from","from_name","{$row['from_name']} &lt;{$row['from_email']}&gt;",50,0,0);
echo $hh->input_text("subject","subject",$row['subject'],70,0,1);

if($row['is_html'])
{
	if($row['id_article']>0)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$a = new Article($row['id_article']);
		$a->ArticleLoad();
		$t = new Topic($a->id_topic);
		$hhf = new HHFunctions;
		echo $hh->input_text("article","id_article","<a href=\"/articles/article.php?id={$row['id_article']}&w=topics\"><b>" . $a->headline . "</b></a>
 (" . $t->name . " - " . $hhf->PathToSubtopic($a->id_topic,$a->id_subtopic) . ")",70,0,0);
	}
	echo $hh->input_wysiwyg("text_html","body",$row['body'],1,20,1,true);
	echo $hh->input_textarea("alt_text","alt_text",$row['alt_text'],80,10,"",1);
}
else 
	echo $hh->input_textarea("text","body",$row['body'],80,20,"",1);
echo $hh->input_textarea("footer","footer",$row['footer'],80,7,"",1);
if($row['format']!="")
{
	$filename = "mailjobs/$id_mail.{$row['format']}";
	echo "<tr><td align=\"right\">" . $hh->tr->Translate("attachment") . "</td><td><a href=\"/docs/upload.php?src=$filename\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a>" . $hh->FileSize($filename) . "</td></tr>\n";
	
}

echo $hh->input_checkbox("send_password","send_password",$row['send_password'],0,0);
echo $hh->input_array("mailjob_footer","auto_footer",$row['auto_footer'],$hh->tr->Translate("mailjob_footer_options"),0);
echo $hh->input_checkbox("track","track",$row['track'],0,0);

if($row['track'])
	echo $hh->input_text("track_redirect","track_redirect",$row['track_redirect'],30,0,1);

echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>
