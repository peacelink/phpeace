<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/polls.php");

$id_poll = $_GET['id_poll'];
$id_question = $_GET['id'];
$id_p = $_GET['id_p'];

$pl = new Polls();
$poll = $pl->PollGet($id_poll);
$id_topic = $poll['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);
$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
if($poll['id_type']!="4")
{
	$row = $pl->QuestionGet($id_question);
	$title[] = array($row['question'],'poll_question.php?id='.$id_question.'&id_poll='.$id_poll);
	$title[] = array('votes','poll_question_answers.php?id='.$id_question.'&id_poll='.$id_poll);
}
else 
	$title[] = array('votes','poll_question_answers.php?id_poll='.$id_poll);


$vote = $pl->VotePerson($id_p,$id_question,$poll['id_type']);

$title[] = array($vote['name'],'');
echo $hh->ShowTitle($title);

if($id_poll>0)
{
	$tabs = array();
	$tabs[] = array('poll',"poll.php?id=$id_poll&id_topic=$id_topic");
	if($poll['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions","poll_questions.php?id=$id_poll&approved=-1");
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");

	if($poll['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes',"poll_question_answers.php?id_poll=$id_poll");
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"poll_status.php?id=$id_poll");
	echo $hh->Tabs($tabs);
}

echo "<ul>";
echo "<li>" . $hh->tr->Translate("date") . ": " . $hh->FormatDateTime($vote['insert_date_ts']) . "</li>\n";
echo "<li>IP: {$vote['ip']}</li>\n"; 
echo "<li>" . $hh->tr->Translate("name") . ": ";
if(Modules::AmIAdmin(28))
{
	echo "<a href=\"/people/person.php?id=$id_p\">{$vote['name']}</a>";
}
elseif($t->profiling)
{
	echo "<a href=\"/topics/visitor.php?id=$id_topic&id_p=$id_p\">{$vote['name']}</a>";
}
else 
{
	echo $vote['name'];
}
echo "</li>\n";

if($poll['id_type']!="4")
{
	echo "<li>" . $hh->tr->Translate("poll_question") . ": " . $vote['question'] . "</li>\n";
	echo "<li>" . $hh->tr->Translate("vote") . ": " . $vote['vote'] . "</li>\n";
}
else 
{
	$token = $pl->PersonTokenGetById($id_poll,$id_p);
	$token_url = $ini->Get("pub_web") . "/" . $ini->Get("poll_path") . "/result.php?t={$token['token']}";
	echo "<li>" . $hh->tr->Translate("results") . ": <a href=\"$token_url\" target=\"_blank\">$token_url</a></li>\n";
	echo "<li>" . $hh->tr->Translate("poll_answers") . "<ul>";
	$groups = $pl->PersonResultsByGroup($id_poll,$id_p);
	foreach($groups as $group)
	{
		echo "<li><h3>{$group['name']}</h3>";
		$questions = $group['questions'];
		if(is_array($questions) && count($questions)>0)
		{
			echo "<ul>";
			foreach($questions as $question)
			{
					echo "<li><em>{$question['question']}</em>: {$question['vote']} ({$question['vote_weight']}*{$question['weight']}={$question['total']})</li>";
			}
			echo "<li>". $hh->tr->Translate("total") . ": {$group['total']}/{$group['total_max']} ({$group['total_perc']}%)</li>";
			echo "</ul>";
		}
		echo "</li>";
	}
	echo "</ul></li>";
}
echo "</ul>";

if($id_poll>0 && $id_p>0)
{
	echo $hh->input_form_open();
	echo $hh->input_hidden("id_poll",$id_poll);
	echo $hh->input_hidden("id_p",$id_p);
	if($poll['id_type']!="4")
		echo $hh->input_hidden("id_question",$id_question);
	echo $hh->input_hidden("from","poll_vote");
	echo $hh->input_table_open();
	$actions = array();
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>
