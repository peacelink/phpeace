<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id = $_GET['id'];

$t = new Topic($id);
$topic = $t->row;

$title[] = array($t->name,'ops.php?id='.$id);
$title[] = array('homepage','');

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

echo $hh->ShowTitle($title);

if ($topic['home_type']=="0")
{
	echo "<p><b>" . $hh->tr->Translate("topic_home_article") . "</b></p>\n";
	$num = $t->HomepageArticle( $row );
	$table_headers = array('date','title','in');
	$table_content = array('{FormatDate($row[written_ts])}','{LinkTitle("/articles/article.php?w=topics&id=$row[id_article]",$row[headline])}',
	'{PathToSubtopic('.$id.',$row[id_subtopic])}');
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	echo "<p><a href=\"homepage_add.php?id=$id\">" . $hh->tr->Translate("article_change") . "</a>\n";
	if($row[0]['id_article']>0)
		echo " - <a href=\"actions.php?from2=homepage&action3=add&home_type={$topic['home_type']}&id=0&id_topic={$id}\">" . $hh->tr->Translate("remove") . "</a>";
	echo "</p>";
	
}

if ($topic['home_type']=="1" || $topic['home_type']=="5")
{
	echo "<p><b>" . $hh->tr->Translate("homepage_articles") . "</b> (<a href=\"homepage_add.php?id=$id\">" . $hh->tr->Translate("add_new") . "</a>)</p>\n";
	$num = $t->HomepageArticles( $row,true );
	$table_headers = array('date','title','in','show_up','order','&nbsp;');
	$table_content = array('{FormatDate($row[written_ts])}','{LinkTitle("/articles/article.php?id=$row[id_article]&w=topics",$row[headline])}',
	'{PathToSubtopic('.$id.',$row[id_subtopic])}',
	'{Bool2YN($row[is_main],"actions.php?from2=homepage&action3=swap_main&id=$row[id_article]&current=$row[is_main]&id_topic='.$id.'")}',
	'$row[id_visibility]','{LinkTitle("actions.php?from2=homepage&action3=remove&id=$row[id_article]&id_topic='.$id.'","' . $hh->tr->Translate("remove") . '")}');
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	echo "<p><a href=\"homepage_add.php?id=$id\">" . $hh->tr->Translate("add_new") . "</a>\n";
}

if ($topic['home_type']=="2")
{
	echo "<p>" . $hh->tr->Translate("topic_home_last") . "</p>\n";
	$last = $t->HomepageLastArticle();
	$hhf = new HHFunctions;
	if ($last['id_article']>0)
		echo "<p>" . $hh->FormatDate($last['written_ts']) . "<br><a href=\"/articles/article.php?id={$last['id_article']}&w=topics\"><b>{$last['headline']}</b></a><div>{$last['subhead']}</div><div>(in " . $hhf->PathToSubtopic($id,$last['id_subtopic']) . ")</div></p>\n";
}

if ($topic['home_type']=="3" || $topic['home_type']=="6")
	echo "<p>" . $hh->tr->TranslateParams("topic_home_latest",array($topic['articles_per_page'])) . "</p>\n";

if ($topic['home_type']=="4")
{
	echo "<p>" . $hh->tr->Translate("topic_home_subtopic") . "</p>\n";
	$subtopic = $t->HomepageSubtopic();
	$hhf = new HHFunctions;
	if ($subtopic['id_subtopic']>0)
		echo "<p><b>" . $hh->SubtopicIcon( $subtopic['id_type'], $subtopic['id_item'], false, $subtopic['visible']) . $hhf->PathToSubtopic($id,$subtopic['id_subtopic']) . "</b><div>{$subtopic['description']}</div></p>\n";
	echo "<p><a href=\"homepage_add.php?id=$id\">" . $hh->tr->Translate("subtopic_change") . "</a>\n";
}

echo "<p><a href=\"features.php?id=$id&id_type=1\">" . $hh->tr->Translate("features") ."</a></p>\n";

echo "<p><a href=\"preview.php?id_topic=$id&id_type=1\" target=\"_blank\">" . $hh->tr->Translate("preview") ."</a></p>\n";

echo "<p><a href=\"publish_homepage.php?id=$id\">" . $hh->tr->Translate("publish") ."</a></p>\n";

echo "<p><a href=\"topic_gra.php?id=$id\">" . $hh->tr->Translate("layout_settings") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

