<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
    define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");
include_once(SERVER_ROOT."/../classes/resources.php");
$fo = new Forms();

$id_topic = (int)$get['id_topic'];
$id_form = (int)$get['id_form'];
$id_action = (int)$get['id'];

if ($module_admin)
    $input_right = 1;

$form = $fo->FormGet($id_form);

if ($id_topic>0)
{
    $t = new Topic($id_topic);
    if ($id_topic==$form['id_topic'] && $t->AmIAdmin())
        $input_right = 1;
    $title[] = array($t->name,'ops.php?id='.$id_topic);
    $trs = new Translator($hh->tr->id_language,0,false,$t->id_style);
}
else 
{
	$t = new Topic(0);
    $trs = new Translator($hh->tr->id_language,0,false,0);
}

$title[] = array('forms','forms.php?id='.$id_topic);

$title[] = array($form['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array("actions",'form_actions.php?id='.$id_form.'&id_topic='.$id_topic);

if ($id_action>0)
{
    $row = $fo->ActionGet($id_action);
    $title[] = array("{$row['score_min']} - {$row['score_max']}",'');
}
else
{
    $title[] = array('add_new','');
}

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
if($form['store'])
{
    $posts = array();
    $num_posts = $fo->Posts($posts,$id_form,$id_topic);
    $tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
    $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
}
if($form['weights'])
	$tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
echo $hh->Tabs($tabs);

?>
<script type="text/javascript">
jQuery.validator.addMethod(
		  "greaterThan",
		  function(value, element) {
			  var min_value = $("#score_min-field").val();
		    if (element.value == "none")
		    {
		      return false;
		    }
		    else return value > min_value;
		  },
		  "Max must be greater than Min"
		);

$().ready(function() {
$("#form1").validate({
		rules: {
			score_min: {
				required: true,
				number: true
			},
			score_max: {
				required: true,
				number: true,
				greaterThan: true
			}
		}
	});
});
</script>

<?php

echo $hh->input_form_open();
echo $hh->input_hidden("from","form_action");
echo $hh->input_hidden("id_form",$id_form);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_action",$id_action);
echo $hh->input_table_open();

echo $hh->input_text($hh->tr->Translate("score") . " Min","score_min",$row['score_min'],6,0,$input_right);
echo $hh->input_text($hh->tr->Translate("score") . " Max","score_max",$row['score_max'],6,0,$input_right);
echo $hh->input_text("recipient","recipient",$row['recipient'],60,0,$input_right);
echo $hh->input_text("redirect","redirect",$row['redirect'],60,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_action>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
