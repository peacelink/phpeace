<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id = $_GET['id'];

$t = new Topic($id);

$title[] = array($t->name,'ops.php?id='.$id);
$title[] = array('homepage','homepage.php?id='.$id);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

if ($t->row['home_type']!=4)
{
	$title[] = array('articles','');
	echo $hh->ShowTitle($title);
	echo "<p>" . $hh->tr->Translate("topic_home_article_add") . "</p>";
	$num = $t->HomepageArticlesAvailable( $row );
	$table_headers = array('date','title','in','author');
	$table_content = array('{FormatDate($row[written_ts])}','{LinkTitle("actions.php?from2=homepage&action3=add&home_type='.$t->row['home_type'].'&id=$row[id_article]&id_topic='.$id.'",$row[headline])}','{PathToSubtopic('.$id.',$row[id_subtopic])}','$row[author]');
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}
else
{
	$hh->topic = $t;
	$title[] = array('subtopics','');
	echo $hh->ShowTitle($title);
	echo "<p>" . $hh->tr->Translate("topic_home_subtopic_add") . "</p>";
	echo $hh->ShowSubtopicsTreeForHomepage(0, $t->row['id_article_home']);
}

include_once(SERVER_ROOT."/include/footer.php");
?>

