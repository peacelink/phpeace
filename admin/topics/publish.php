<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];
$step = (int)$_GET['step'];
$type = $_GET['type'];
$ok = $_GET['ok'];

$t = new Topic($id_topic);

if($step==0)
	$step = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('publish','');

echo $hh->ShowTitle($title);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin )
	$input_super_right = 1;
$show_time = false;

if($input_right)
{
	include_once(SERVER_ROOT."/../classes/publishmanager.php");
	$pm = new PublishManager();
	$ah->TimeStart();
	
	$pm->step = $step;
	
	if ($type=="all")
	{
		if($input_super_right)
		{
			$articles = $t->Articles();
			$steps = $pm->StepsCounter(count($articles));
			if($steps>2)
			{
				if($ok=="1")
				{
					$pm->TopicEverything($id_topic);
					echo $pm->MessageSet("execution_time",array($ah->TimeStat()));
					$show_time = true;
				}
				else
				{
					echo "<p>" . $hh->tr->TranslateParams("publish_heavy_warning",array(count($articles))) . "</p>";
					if($t->queue->JobPresentAll())
					{
						$t->queue->Confirm($ah->current_user_id);
						echo "<p>" . $hh->tr->Translate("publish_queued") . "</a></p>\n";
					}
					else
					{
						echo "<p>" . $hh->tr->TranslateParams("publish_heavy",array("publish_schedule.php?id=$id_topic")) . "</p>";
					}
					echo "<p>" . $hh->tr->TranslateParams("publish_force",array("publish.php?id=$id_topic&type=all&ok=1&step=$step",$steps+1)) . "</p>";
				}
			}
			else
			{
				$pm->TopicEverything($id_topic);
				echo $pm->MessageSet("execution_time",array($ah->TimeStat()));
				$show_time = true;
			}
		}
		else 
			$hh->tr->Translate("user_no_auth");
	}
	else
	{
		$articles = $t->Articles();
		$steps = $pm->StepsCounter(count($articles));
		if($t->queue->JobPresentAll() && $steps>2)
		{
			$t->queue->Confirm($ah->current_user_id);
			echo "<p>" . $hh->tr->TranslateParams("publish_heavy_warning",array(count($articles))) . "</p>";
			echo "<p>" . $hh->tr->Translate("publish_queued") . "</a></p>\n";
			if($input_super_right)
				echo "<p>" . $hh->tr->TranslateParams("publish_force",array("publish.php?id=$id_topic&type=all&ok=1&step=$step",$steps)) . "</p>";
		}
		else
		{
			$pm->Topic($id_topic);
			echo $pm->MessageSet("execution_time",array($ah->TimeStat()));
			$show_time = true;
		}
	}
	
	if($show_time)
		echo $pm->MessageGet();
	
	if ($pm->steps > 1)
	{
		if ($step < $pm->steps)
			echo "<p><a href=\"publish.php?id=$id_topic&step=" . ($step+1) . (($type=="all")? "&type=all":"") . "&ok=$ok\">" . $hh->tr->Translate("publish_next_step") . "</a></p>";
		else
			echo "<p>" . $hh->tr->Translate("publish_done") . "</p>";
	}
	
}
else 
	echo $hh->tr->TranslateParams("user_no_auth_topic",array($t->name));

include_once(SERVER_ROOT."/include/footer.php");
?>

