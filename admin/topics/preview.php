<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth();

include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0);

include_once(SERVER_ROOT."/../classes/pagetypes.php");			
$pt = new PageTypes();

include_once(SERVER_ROOT."/../classes/layout.php");
$id_topic = (int)$_GET['id_topic'];
$id_gtype = $_GET['id_gtype'];
$id_type = $_GET['id_type'];
$id = (int)$_GET['id'];
$page = (int)$_GET['p'];
$params = array();
foreach($_GET as $key=>$value)
{
	if ($value!="")
	{
		$params[$key] = $value;
		if ($key=="offset")
			$params['ts'] = time() + ($value*86400);
	}
}

if($params['subtype']=="month" && isset($params['m']))
{
	$params['ts'] = mktime(12,0,0,substr($params['m'],4,2),1,substr($params['m'],0,4));
}
if($params['subtype']=="day" && isset($params['d']))
{
	$params['ts'] = mktime(12,0,0,substr($params['d'],4,2),substr($params['d'],6,2),substr($params['d'],0,4));
}

$unescaped_get = $fh->HttpGet(false);
$params['q'] = $unescaped_get['q'];

if($_GET['id_style']>0)
	$params['id_style'] = $_GET['id_style'];

$l = new Layout(true);

if (!isset($id_gtype) && !isset($id_type))
	$id_gtype = 0;

if (isset($id_gtype))
{
	$xml = $l->PageTypeGlobal($id_gtype,$id,$page,$params);
	$type = array_search($id_gtype,$pt->gtypes);
}
else
{
	$xml = $l->PageType($id_type,$id_topic,$id,$page,$params);
	$type = ($params['module']!="")? $params['module'] : array_search($id_type,$pt->types);
}

if (isset($id_gtype) && $id_gtype==1)
	header('Content-type: text/xml');

$xhtml = $l->xh->Transform($type,$xml,$l->id_style);
$l->DoctypeHTML5Fix($xhtml);
echo $xhtml;
?>

