<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../modules/books.php");

$id_topic = $_GET['id'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$bb = new Books();
$bconfig = $bb->TopicConfig($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('books','');
echo $hh->ShowTitle($title);

$trm16 = new Translator($hh->tr->id_language,16);

echo $hh->input_form_open();
echo $hh->input_hidden("from","books_config");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();

echo $hh->input_array("homepage_type","books_home_type",$bconfig['books_home_type'],$trm16->Translate("books_home_types"),$input_right);
echo $hh->input_array("allow_reviews","books_reviews",$bconfig['books_reviews'],$hh->tr->Translate("allow_feedback"),$input_right);
$reviews_options = $hh->tr->Translate("reviews_options");
echo $hh->input_array("show_reviews","show_reviews",$bconfig['show_reviews'],$reviews_options,$input_right);

echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

echo "<div class=\"box2\">";
echo "<h3>" . $hh->tr->Translate("homepage_content") . "</h3>";

$row = array();
switch($bconfig['books_home_type'])
{
	case "0":
		echo $trm16->Translate("books_home_publishers");
	break;
	case "1":
		$num = $bb->HomepageContent( $row, $id_topic, $bconfig['books_home_type'] );
		$table_headers = array($trm16->Translate("publisher"),'books','');
		$table_content = array('$row[name]','<div class=\"right\">$row[counter]</div>','{LinkTitle("actions.php?from2=books_homepage&id_topic='.$id_topic.'&action3=delete&id=$row[id_item]","'.$hh->tr->Translate("remove").'")}');
		echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	break;
	case "2":
		$num = $bb->HomepageContent( $row, $id_topic, $bconfig['books_home_type'] );
		$table_headers = array('author','title',$trm16->Translate("publisher"),'');
		$table_content = array('$row[author]','{LinkTitle("/books/book.php?id=$row[id_item]",$row[title])}','$row[name]',
		'{LinkTitle("actions.php?from2=books_homepage&id_topic='.$id_topic.'&action3=delete&id=$row[id_item]","'.$hh->tr->Translate("remove").'")}');
		echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	break;
}

if ($input_right && $bconfig['books_home_type']>0)
	echo "<p><a href=\"books_homepage_add.php?id=$id_topic\">" . $hh->tr->Translate("change_add") . "</a></p>\n";

echo "</div>";

include_once(SERVER_ROOT."/include/footer.php");
?>
