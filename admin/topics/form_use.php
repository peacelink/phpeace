<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");
$fo = new Forms();

$id_topic = (int)$_GET['id_topic'];
$id_form = $_GET['id'];

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}

$is_system = false;

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	$users = $t->Users();
}
else 
{
	include_once(SERVER_ROOT."/../classes/users.php");
	$uu = new Users();
	$users = $uu->Active();
	$t = new Topic(0);
}
$users[] = array('0'=>0,'id_user'=>0,'1'=>"--" . $hh->tr->Translate("contact_main") . "--");

$title[] = array('forms','forms.php?id='.$id_topic);

$row = $fo->FormGet($id_form);
$title[] = array($row['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('usage','');

echo $hh->ShowTitle($title);

if($id_form>0)
{
	$tabs = array();
	$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
	$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
	if($row['store'])
	{
		$posts = array();
		$num_posts = $fo->Posts($posts,$id_form,$id_topic);
		$tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
        $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
	}
	if($row['weights'])
		$tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
	$tabs[] = array("usage","");
	echo $hh->Tabs($tabs);
	
	
	$subtopics = $fo->Subtopics($id_form);
	if(count($subtopics)>0)
	{
		echo "<h3>" . $hh->tr->Translate("used_in") . ":</h3>";
		echo "<ul>";
		foreach($subtopics as $subtopic)
		{
			echo "<li>{$subtopic['topic_name']}: <a href=\"subtopic.php?id={$subtopic['id_subtopic']}&id_topic={$subtopic['id_topic']}\">{$subtopic['name']}</a></li>";
		}
		echo "</ul>";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>

