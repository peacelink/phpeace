<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];

$t = new Topic($id_topic);

$p = new Payment();

$trm15 = new Translator($hh->tr->id_language,15);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array($trm15->Translate("payment_types"),'');
echo $hh->ShowTitle($title);

$num = $p->Types( $row, $id_topic  );

$table_headers = array($trm15->Translate("payment_type"));
$table_content = array('{LinkTitle("payment_type.php?id=$row[id_payment_type]&id_topic=$row[id_topic]",$row[payment_type])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_admin || $t->AmIAdmin())
	echo "<p><a href=\"payment_type.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

