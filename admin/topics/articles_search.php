<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id_topic'];

$t = new Topic($id_topic);
$row = $t->row;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('search','');
echo $hh->ShowTitle($title);
?>

<form method="get" action="articles_search2.php" name="form1">
<input type="hidden" name="id_topic" value="<?= $id_topic;?>">
<table border=0 cellpadding=0 cellspacing=7>
<?php
$topic_templates = $t->Templates(5);
if(count($topic_templates)>0)
{
	$templates = array();
	$templates[] = array(0=>-1,1=>$hh->tr->Translate("all_option")); 
	$templates[] = array(0=>0,1=>$hh->tr->Translate("articles"));
	foreach($topic_templates as $template)
		$templates[] = array(0=>$template['id_template'],1=>$template['name']);
	echo $hh->input_row("template","id_template",-1,$templates,"",0,1);
} 

echo $hh->input_text("title","title","",30,0,1);
echo $hh->input_text("author","author","",30,0,1);
echo $hh->input_text("text","content","",30,0,1);

include_once(SERVER_ROOT."/../classes/articles.php");
$period = Articles::Period();
$min = $period['min_art_ts']<0? 1 : $period['min_art_ts'];
echo $hh->input_date("from","written1",$min,1);
echo $hh->input_date("to","written2",$period['max_art_ts'],1);

echo $hh->input_link("subtopic","id_subtopic","'../articles/article_subtopic.php?id_topic='+document.forms['form1'].id_topic.value+'&id_subtopic='+document.forms['form1'].id_subtopic.value",0,"",1);
echo $hh->input_array("sort_articles_by","sort_by",1,$hh->tr->Translate("sort_by_options"),1);

?>
<tr><td>&nbsp;</td><td><input type="submit" class="input-submit" value="<?=$hh->tr->Translate("search");?>">
&nbsp;&nbsp;<input type="reset" value="<?=$hh->tr->Translate("cancel");?>"></td></tr>
</table>
</form>

<?php
echo  $hh->tr->Translate("search_notes");
include_once(SERVER_ROOT."/include/footer.php");
?>

