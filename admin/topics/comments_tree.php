<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_item = $_GET['id_item'];
$type = $_GET['type'];

switch($type)
{
	case "article":
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_item);
		$article = $a->ArticleGet();
		$id_topic = $article['id_topic'];
		$t = new Topic($id_topic);
		$title[] = array($t->name,'ops.php?id='.$id_topic);
		$title[] = array('articles_list','/topics/articles.php?id=' . $id_topic);
		$title[] = array($article['headline'],'/articles/article.php?w=topics&id='.$id_item);
	break;
	case "question":
		include_once(SERVER_ROOT."/../classes/polls.php");
		$pl = new Polls();
		$question = $pl->QuestionGet($id_item);
		$id_poll = $question['id_poll'];
		$poll = $pl->PollGet($id_poll);
		$id_topic = $poll['id_topic'];
		$t = new Topic($id_topic);
		$title[] = array($t->name,'ops.php?id='.$id_topic);
		$title[] = array('polls','polls.php?id='.$id_topic);
		$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
		$row = $pl->QuestionGet($id_item);
		$title[] = array($row['question'],'poll_question.php?id='.$id_item.'&id_poll='.$id_poll);
	break;
	case "thread":
		include_once(SERVER_ROOT."/../classes/forum.php");
		$f = new Forum(0);
		$thread = $f->ThreadGet($id_item);
		$f->id = $thread['id_topic_forum'];
		$id_forum = $f->id;
		$forum = $f->ForumGet();
		$id_topic = $forum['id_topic'];
		$t = new Topic($id_topic);
		$title[] = array($t->name,'ops.php?id='.$id_topic);
		$title[] = array('forum','forums.php?id='.$id_topic);
		$title[] = array($forum['name'],'forum.php?id='.$id_forum.'&id_topic='.$id_topic);
		$title[] = array('threads','forum_threads.php?id='.$id_forum.'&id_topic='.$id_topic.'&p='.$current_page);
		$title[] = array($thread['title'],'forum_thread.php?id='.$id_item.'&id_forum='.$id_forum.'&id_topic='.$id_topic);
	break;
}


if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array("comments",'');

echo $hh->ShowTitle($title);

$co = new Comments($type,$id_item);
$num = $co->LoadTree($co,0);
if($num>0)
	echo "<div class=\"indent\">" . $hh->ShowCommentsTree($co,0,"comment.php?type=$type&id_item=$id_item") . "</div>";
else 
	echo "<p>" . $hh->tr->Translate("comments_none") . "</p>\n";

echo "<p><a href=\"comment.php?type=$type&id_item=$id_item&id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

