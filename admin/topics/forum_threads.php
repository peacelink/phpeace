<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forum.php");

$id_topic = $_GET['id_topic'];
$id_forum = $_GET['id'];
$approved = (int)$_GET['approved'];

$f = new Forum($id_forum);
$forum = $f->ForumGet();
$id_topic = $forum['id_topic'];
$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('forum','forums.php?id='.$id_topic);
$title[] = array($forum['name'],'forum.php?id='.$id_forum.'&id_topic='.$id_topic);
$title[] = array($approved?"threads_approved":"threads_to_approve",'');
echo $hh->ShowTitle($title);

$row = array();
$num = $f->ThreadsApprove( $row, $approved );

$table_headers = array('date','title','description','comments');
$table_content = array('{FormatDate($row[insert_date_ts])',
'{LinkTitle("forum_thread.php?id_forum='.$id_forum.'&id_topic='.$id_topic.'&id=$row[id_thread]&approved='.$approved.'&p='.$current_page.'",$row[title])}','$row[description]','<div class=\"right\">$row[comments]</div>');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if($input_right)
	echo "<p><a href=\"forum_thread.php?id_forum=$id_forum&id_topic=$id_topic&id=0\">".$hh->tr->Translate("add_new")."</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
