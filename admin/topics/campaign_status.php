<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/campaign.php");
include_once(SERVER_ROOT."/../classes/topic.php");
$post = $fh->HttpPost();

$id_campaign = $get['id'];

$c = new Campaign($id_campaign);
$row = $c->CampaignGet();
$id_topic = $row['id_topic'];

if($id_campaign>0 && $id_topic>0)
{
	$t = new Topic($id_topic);
	
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	$title[] = array('campaigns','campaigns.php?id='.$id_topic);
	$title[] = array($row['name'],'campaign.php?id='.$id_campaign.'&id_topic='.$id_topic);
	$title[] = array('status_changes','');

	if ($module_admin || $t->AmIAdmin())
	{
		echo $hh->ShowTitle($title);
		
		include_once(SERVER_ROOT."/../classes/scheduler.php");
		$sc = new Scheduler();
		$changes = $sc->StatusChanges(11,$id_campaign);
		if(count($changes)>0)
		{
			$status_options = $hh->tr->Translate("status_options");
			echo "<ul>";
			foreach($changes as $change)
			{
				echo "<li><a href=\"status_change.php?id_res=11&id=$id_campaign&id_topic=$id_topic&date=" . urlencode($change['change_date']) . "\">";
				echo $hh->FormatDate($change['change_date_ts']) . "</a>: {$status_options[$change['status']]}</li>";
			}
			echo "</ul>";
		}
		echo "<p><a href=\"status_change.php?id_res=11&id=$id_campaign&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a></p>\n";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
