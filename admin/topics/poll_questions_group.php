<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/polls.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_poll = $_GET['id_poll'];
$id_questions_group = $_GET['id'];

$pl = new Polls();
$poll = $pl->PollGet($id_poll);
$id_topic = $poll['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);
$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
$title[] = array('groups',"poll_questions_groups.php?id=$id_poll");

if ($id_questions_group>0)
{
	$row = $pl->QuestionsGroupGet($id_questions_group);
	$title[] = array($row['name'],'');
}
else
{
	$title[] = array("add_new",'');
}
echo $hh->ShowTitle($title);

if($id_poll>0)
{
	$tabs = array();
	$tabs[] = array('poll',"poll.php?id=$id_poll&id_topic=$id_topic");
	if($poll['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions","poll_questions.php?id=$id_poll&approved=-1");
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");

	if($poll['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes',"poll_question_answers.php?id_poll=$id_poll");
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"poll_status.php?id=$id_poll");
	echo $hh->Tabs($tabs);
}

echo $hh->input_form_open();
echo $hh->input_hidden("id_questions_group",$id_questions_group);
echo $hh->input_hidden("id_poll",$id_poll);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","poll_questions_group");
echo $hh->input_table_open();

echo $hh->input_text("name","name",$row['name'],50,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
//$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_questions_group>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

if($id_questions_group>0 && $poll['id_type']=="4")
{
	$row = array();
	$num = $pl->Questions( $row, $id_poll, $poll['sort_questions_by'], -1, $id_questions_group, true );

	$table_headers = array('seq','move','question','approved','weight');
	$table_content = array('$row[pos]','{PollQuestionMove($row[pos],'.$num.',$row[id_question],$row[id_questions_group])}',
	'{LinkTitle("poll_question.php?id=$row[id_question]&id_poll='.$id_poll.'",$row[question])}',
	'{Bool2YN($row[approved])}','<div class=\"right\">$row[weight]</div>');

	echo $hh->ShowTable($row, $table_headers, $table_content, $num);

	echo "<p><a href=\"poll_question.php?id=0&id_poll=$id_poll&id_questions_group=$id_questions_group\">" . $hh->tr->Translate("add_new") . "</a></p>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
