<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];
$appr = $_GET['appr'];
$id_template = (int)$_GET['id_template'];

$t = new Topic($id_topic);

$approved = 1;
$title1 = "articles_list_approved";

if (($appr=="0") && ($module_admin || $t->AmIUser()))
{
	$approved = 0;
	$title1 = "articles_list_to_approve";
}

if (($appr=="-1") && ($module_admin || $t->AmIUser()))
{
	$approved = -1;
	$title1 = "articles_list_at_work";
}

if (!($module_admin || $t->AmIUser()))
	$t->only_visible = 1;

if($id_template>0)
{
	include_once(SERVER_ROOT."/../classes/template.php");
	$te = new Template();
	$template = $te->TemplateGet($id_template);
	$current_template = $template['name'];
	$te->ResourceSet($template['id_res']);	
}

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array($title1,'');

echo $hh->ShowTitle($title);

if($id_template>0)
	echo "<p>" . $hh->tr->Translate("template") . ": <strong>$current_template</strong></p>";

$num = $t->ArticlesApproved( $row, $approved, $id_template );

if($id_template=="-1")
{
	$table_headers = array('image','date','title','in','author','template');
	$table_content = array('{ThumbImage($row[id_image],"images",$row[format],$row[id_image])}','{FormatDate($row[written_ts])}','{LinkTitle("/articles/article.php?w=topics&id=$row[id_article]&p='.$current_page.'",$row[headline])}',
'{PathToSubtopic('.$id_topic.',$row[id_subtopic])}','$row[author]','{TemplateLookup($row[id_template])}');

}
else 
{
	$table_headers = array('image','date','title','in','author');
	$table_content = array('{ThumbImage($row[id_image],"images",$row[format],$row[id_image])}','{FormatDate($row[written_ts])}','{LinkTitle("/articles/article.php?w=topics&id=$row[id_article]&p='.$current_page.'",$row[headline])}',
	'{PathToSubtopic('.$id_topic.',$row[id_subtopic])}','$row[author]');
}

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"/articles/article.php?w=topics&id=0&id_topic=$id_topic";
if($id_template>0)
	echo "&id_template=$id_template&id_subtopic={$template['default_subtopic']}";
echo "\">" . $hh->tr->Translate("article_add") . "</a></p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

