<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/images.php");


$id_subtopic = $_GET['id'];
$id_topic = $_GET['id_topic'];

$t = new Topic($id_topic);
$row = $t->SubtopicGet($id_subtopic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('subtopics_tree','subtopics.php?id='.$id_topic);
$title[] = array($row['name'],'subtopic.php?id='.$id_subtopic."&id_topic=".$id_topic);
$title[] = array('image_associated','');
echo $hh->ShowTitle($title);

$i = new Images();
if($row['id_type']==($t->subtopic_types['gallery']))
	$num = $i->Gallery( $row, $row['id_item'],0,0,true );
else
	$num = $i->Topic( $row, $id_topic,true );

$table_headers = array('image','caption');
$table_content = array('{LinkImage("actions.php?from2=subtopic&id='.$id_subtopic.'&action3=image&id_image=$row[id_image]&id_topic='.$id_topic.'","$row[id_image]",0,"$row[format]")}','$row[caption]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"actions.php?from2=subtopic&id=$id_subtopic&action3=image&id_image=0&id_topic=$id_topic\">" . $hh->tr->Translate("none") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

