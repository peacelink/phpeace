<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/campaign.php");

$id_campaign = $_GET['id_c'];
$id_topic = $_GET['id_topic'];
$name = $_GET['name'];
$from = $_GET['from'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('campaigns','campaigns.php?id='.$id_topic);

$c = new Campaign($id_campaign);
$row2 = $c->CampaignGet();
$title[] = array($row2['name'],'campaign.php?id='.$id_campaign.'&id_topic='.$id_topic);

echo $hh->ShowTitle($title);

echo "<p>Cerca i dati di un'associazione da far aderire alla campagna \"{$row2['name']}\"</p>";

if($from=="org_search") {
   // include_once(SERVER_ROOT."/../modules/assos.php");
   // $as = new Assos();   
   // $params = array('name' => $name);
   // $num = $as->Search( $row, $params, TRUE, FALSE );
   // $table_headers = array('type','name','town','approved','expired','lastupdate','adesione');
   // $table_content = array('$row[ass_tipo]','{LinkTitle("org.php?id=$row[id_ass]",$row[nome])}','$row[citta] ($row[geo_name])','{Bool2YN($row[approved])}','{Bool2YN($row[expired])}','{FormatDate($row[lastupd_ts])}',
	// '{LinkTitle("actions.php?from2=campaign_org_sign&id_org=$row[id_ass]&id_campaign='.$id_campaign.'&id_topic='.$id_topic.'","Aderisci")}');
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();	
	$params = array( 'id_p' => $get['id_p'], 'name' => $get['name'], 'email' => $get['email']);
	$num = $pe->Search($row, $params);
	$table_headers = array('id','email','name','verified','contact_email',$hh->geo_label($hh->ini->Get("geo_location")),'insert_date','adesione');
	$table_content = array('{LinkTitle("/people/person.php?id=$row[id_p]",$row[id_p])}','$row[real_email]','{List2String(" ",$row[name1],$row[name2],$row[name3])}','{Bool2YN($row[verified])}',
	'{Bool2YN($row[contact])}','{List2String(" ",$row[town],$row[geo_name])}','{FormatDate($row[start_date_ts])}',
	'{LinkTitle("actions.php?from2=campaign_org_sign&id_p=$row[id_p]&id_campaign='.$id_campaign.'&id_topic='.$id_topic.'","Aderisci")}');
   
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}

echo "<hr>";
echo $hh->input_form("get","campaign_org_search.php");
echo $hh->input_hidden("from","org_search");
echo $hh->input_hidden("id_c",$id_campaign);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
echo $hh->input_text("name","name",$name,50,0,$input_right);
$actions = array();
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
