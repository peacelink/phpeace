<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/users.php");
include_once(SERVER_ROOT."/../classes/user.php");


$u = new User;

$id_topic = $_GET['id'];
$g = $_GET['g'];

$t = new Topic($id_topic);
if ($module_admin || $t->AmIAdmin() || $u->ModuleAdmin(1))
	$input_right = 1;

$uu = new Users();
$users = $uu->TopicUsersAdmins($id_topic);
switch($g)
{
	case "user":
		$desc = $hh->tr->Translate("collaborators");
	break;
	case "admin":
		function only_users($var)
		{
			return ($var['id_topic']>0);
		}
		$desc = $hh->tr->Translate("administrators");
		$users = array_filter($users,"only_users");
	break;
	case "contact":
		function only_admins($var)
		{
			return ($var['is_admin']==1);
		}
		$desc = $hh->tr->Translate("contact_main");
		$users = array_filter($users,"only_admins");
	break;
}
$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array($desc,'');

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
function validate()
{
	f = document.forms['form1'];
	boolOK = true;
	strAlert = "";
	counter = 0;
	for (i=0;i<f.length;i++)
	{
		if(f.elements[i].checked)
			counter = counter + 1;
	}
	if (f.action2.value=="add_contact" && !(counter==1))
	{
		strAlert += "<?=$hh->tr->Translate("contact_one");?>";
		boolOK = false;
	}
	if (boolOK==true)
		f.submit();
	else
		alert(strAlert);
}
</script>

<form name="form1" action="actions.php" method="post">
<input type="hidden" name="from" value="users">
<input type="hidden" name="action2" value="add_<?=$g;?>">
<input type="hidden" name="id_topic" value="<?=$id_topic;?>">
<table cellpadding="4" border="0" cellspacing="10">
<?php
$counter = 0;
$tot_users = ceil(count($users)/2);
echo "<tr><td valign=top>\n";
foreach($users as $user)
{
	if ($input_right==1)
	{
		echo "<input type=\"checkbox\" class=\"input-checkbox\" name=\"{$user['id_user']}\"";
		if (($user['id_topic']>0 && $g=="user") || ($user['is_admin']>0 && $g=="admin")  || ($user['is_contact']>0 && $g=="contact"))
			echo " checked ";
		echo ">";
	}
	echo "$user[name]<br>\n";
	$counter ++;
	if ($counter==$tot_users)
		echo "</td><td valign=\"top\">\n";

}
echo "</td></tr>\n";
echo "<tr><td colspan=2 align=center>";
if ($input_right==1 && count($users)>0)
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") . "\" onClick=\"validate()\">";
echo "</td></tr>\n";

?>
</table>
</form>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>


