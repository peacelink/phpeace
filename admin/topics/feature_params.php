<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");

$pt = new PageTypes();

$id_topic = (int)$_GET['id_topic'];
$id_feature = $_GET['id'];

if ($module_admin)
	$input_right = 1;

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($t->AmIAdmin())
	$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
}

$title[] = array('features','features.php?id='.$id_topic);

$row = $pt->ft->FeatureGet($id_feature);
if(!(isset($row['id_feature'])))
	$hh->Stop();

$title[] = array($row['name'],'feature.php?id='.$id_feature.'&id_topic='.$id_topic);

$id_function = $row['id_function'];
$ptype = $pt->ft->FeatureGetTypeAndModule($id_feature);
$id_type = $ptype['id_type'];
$id_module = $ptype['id_module'];

$title[] = array('parameters','');

echo $hh->ShowTitle($title);

$functions = $hh->tr->Translate("page_functions");
echo "<p>" . $hh->tr->TranslateParams("function_params",array($functions[$id_function])) . "</p>";
?>
<form method="post" action="actions.php" name="form1">
<input type="hidden" name="from" value="feature_params">
<input type="hidden" name="id_feature" value="<?=$id_feature;?>">
<input type="hidden" name="id_topic" value="<?=$id_topic;?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
$params = $pt->ft->params[$id_function];

$params2 = $pt->ft->ParamsDeserialize($row['params']);

// parametri disponibili nel page type
foreach ($params as $param)
{
	echo $hh->input_param($param,$params2[$param],$id_topic,$input_right);
}
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

