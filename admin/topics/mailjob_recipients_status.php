<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/mailjobs.php");

$id_mail = (int)$_GET['id'];
$id_topic = (int)$_GET['id_topic'];
$status = (int)$_GET['status'];

include_once(SERVER_ROOT."/../classes/topic.php");
$t = new Topic($id_topic);
$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
$title[] = array("mailjobs",'/topics/mailjobs.php?id='.$id_topic);

$mj = new Mailjobs();
$job = $mj->MailJobGet($id_mail);
$title[] = array($job['subject'],'/topics/mailjob_recipients.php?id='.$id_mail.'&id_topic='.$id_topic);

$statuses = $hh->tr->Translate("mailjob_status");
$title[] = array($statuses[$status],'');
echo $hh->ShowTitle($title);

if($job['track'])
{
	$row = array();
	$num = $mj->RecipientsStatus($row,$id_mail,$status);
	
	$table_headers = array('name','email');
	$table_content = array('{LinkTitle("visitor.php?id_p=$row[id_p]&id='.$id_topic.'",$row[name])}','$row[email]');
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
