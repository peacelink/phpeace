<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('search','');
echo $hh->ShowTitle($title);

echo $hh->input_form("get","search2.php");
echo $hh->input_table_open();

echo $hh->input_text("text","content","","30",0,1);

include_once(SERVER_ROOT."/../classes/events.php");
$ee = new Events();
$period = $ee->Period();
$min = $period['min_ev_ts']<0? 1 : $period['min_ev_ts'];
echo $hh->input_date("from","start_date1",$min,1);
echo $hh->input_date("to","start_date2",$period['max_ev_ts'],1);
echo $hh->input_geo(0,1);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
$topics = $tt->AllTopics();
echo $hh->input_topics(0,0,$topics,"all_option",1);

$actions = array();
$actions[] = array('action'=>"submit",'label'=>"search",'right'=>true);
echo $hh->input_actions($actions,1);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
