<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/events.php");


$approved = $_GET['approved'];
$id_month = $_GET['id_month'];

$title[] = array((($approved==1)? "anniversaries_approved" : "anniversaries_to_approve"),'');
echo $hh->ShowTitle($title);

$ee = new Events();
$num = $ee->Recurring( $row, $approved, $id_month );
$table_headers = array('when','anniversary');
$table_content = array('<div class=\"right\">$row[r_year]-$row[r_month]-$row[r_day]</div>','{LinkTitle("recurring_event.php?id=$row[id_event]",$row[description])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_right || $module_admin)
	echo "<p><a href=\"recurring_event.php?id=0&id_month=$id_month\">" . $hh->tr->Translate("anniversary_add") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
