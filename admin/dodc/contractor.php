<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

$id_contractor = $_GET['id'];

if($id_contractor>0)
{
	$dodc = new DodContractors();
	$hhf = new HHFunctions();
	$trm25 = new Translator($hh->tr->id_language,25);
	$row = $dodc->ContractorGet($id_contractor);
	if($row['id_contractor']>0)
	{
		$ctotal = $dodc->ContractorTotalGet($id_contractor);
		$title[] = array("Contractors",'contractors.php?country='.$row['country_code']);
		$title[] = array($row['name'],'');
		echo $hh->ShowTitle($title);
		
		echo "<h1>{$row['name']}</h1>";
		if($row['id_parent']>0)
		{
			$parent = $dodc->ContractorTotalGet($row['id_parent']);
			echo "<p>" . $trm25->Translate("parent") . ": <a href=\"contractor.php?id={$row['id_parent']}\">{$parent['name']}</a> (" . $hhf->FormatMoney($parent['total'],4,false) . ")";
		}
		echo "<p>DUNS: <a href=\"duns.php?id=$id_contractor\">{$row['duns']}</a></p>";
		echo "<p>" . $hh->tr->Translate("address") . ": {$row['address']}";
		if($row['postcode']!="")
		{
			if($row['address']!="")
			echo " - ";
			echo $row['postcode'];
		}
		if($row['city']!="")
			echo ", {$row['city']}";
		echo "</p>\n";
		
		$subcontractors = array();
		$num_subcontractors = $dodc->SubContractors($subcontractors,$id_contractor);
		if($num_subcontractors>0)
		{
			echo "<h3>" . $trm25->Translate("subcontractors") . "</h3>";
			echo "<ul>";
			$total = $ctotal['total'];
			foreach($subcontractors as $subcontractor)
			{
				echo "<li><a href=\"contractor.php?id={$subcontractor['id_contractor']}\">{$subcontractor['name']}</a> (" . $hhf->FormatMoney($subcontractor['total'],4,false) . ")</li>";
				$total += $subcontractor['total'];
			}
			echo "</ul>";
			
		}

		echo "<p>" . $trm25->Translate("contracts_own") . " (" . $hhf->FormatMoney($ctotal['total'],4,false) . ")</p>";
		if($num_subcontractors>0)
			echo "<p>" . $trm25->Translate("subtotal") . ": ". $hhf->FormatMoney($total,4,false) . "</p>";

		echo "<h3>" . $trm25->Translate("contracts") . "</h3>";
		echo "<ul>";
		$parent = $num_subcontractors>0? 1 : 0;
		$num = $dodc->Contracts( $rows, $id_contractor, $parent );
		echo "<li><a href=\"contracts.php?id=$id_contractor&parent=$parent\">" . $trm25->Translate("contracts_time") . "</a> ($num)</li>";
		echo "<li><a href=\"contracts_period.php?id=$id_contractor&parent=$parent\">" . $trm25->Translate("contracts_period") . "</a></li>";
		echo "<li><a href=\"contracts_category.php?id=$id_contractor&parent=$parent\">" . $trm25->Translate("contracts_category") . "</a></li>";
		echo "<li><a href=\"contracts_office.php?id=$id_contractor&parent=$parent\">" . $trm25->Translate("contracts_office") . "</a></li>";
		echo "</ul>";

	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
