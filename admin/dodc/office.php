<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

$id_office = $_GET['id'];

$trm25 = new Translator($hh->tr->id_language,25);

if($id_office>0)
{
	$dodc = new DodContractors();
	$row = $dodc->OfficeGet($id_office);
	if($row['id_office']>0)
	{
		$title[] = array($row['name'],'');
		echo $hh->ShowTitle($title);
		
		echo "<p>{$row['description']}</p>";
		echo "<h1>{$row['name']}</h1>";

		echo "<ul>";
		$num = $dodc->OfficeContractors( $row, $id_office );
		echo "<li><a href=\"contractors.php?id_office=$id_office\">" . $trm25->Translate("contractors") . "</a> ($num)</li>";
		echo "</ul>";
		
		// fornitori
		// contratti in ordine cronologico
		// contratti per periodo
		// contratti per servizio
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
