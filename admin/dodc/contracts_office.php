<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

$id_contractor = $_GET['id'];
$parent = $_GET['parent'];

if($id_contractor>0)
{
	$trm25 = new Translator($hh->tr->id_language,25);
	$dodc = new DodContractors();
	$row = $dodc->ContractorGet($id_contractor);
	if($row['id_contractor']>0)
	{
		$title[] = array($trm25->Translate("contractors"),'contractors.php?country='.$row['country_code']);
		$title[] = array($row['name'],'contractor.php?id='.$id_contractor);
		
		$id_office = (int)$_GET['id_office'];
		if($id_office>0)
		{
			$office = $dodc->OfficeGet($id_office);
			$title[] = array($trm25->Translate("contracts_office"),'contracts_office.php?id='.$id_contractor.'&parent='.$parent);
			$title[] = array($office['name'],'');
			echo $hh->ShowTitle($title);

			echo "<p>" . $trm25->Translate("office") . ": <a href=\"office.php?id=$id_office\">{$office['name']}</a></p>";

			$num = $dodc->ContractsOffice( $row, $id_contractor, $id_office, $parent);
			$table_headers = array($trm25->Translate("period"),'name','description','amount');
			$table_content = array('{FormatMonth($row[start_date_ts])}','$row[contractor_name]','{LinkTitle("contract.php?id=$row[id_contract]",$row[description])}','{FormatMoney($row[amount],4)}');
		}
		else 
		{
			$title[] = array($trm25->Translate("contracts_office"),'');
			echo $hh->ShowTitle($title);

			$num = $dodc->ContractsOffices( $row, $id_contractor,$parent );
			$table_headers = array($trm25->Translate("office"),$trm25->Translate("contracts"),'amount');
			$table_content = array('{LinkTitle("contracts_office.php?id_office=$row[id_office]&id='.$id_contractor.'&parent='.$parent.'","$row[name]")}','<div class=\"right\">$row[contracts]</div>','{FormatMoney($row[amount],4)}');		
		}
		
		echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
