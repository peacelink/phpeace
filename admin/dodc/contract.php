<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");
$hhf = new HHFunctions();

$id_contract = $_GET['id'];

function DodContractField($label,$value)
{
	return "<p>$label: <i>$value</i></p>\n";
}

if($id_contract>0)
{
	$dodc = new DodContractors();
	$trm25 = new Translator($hh->tr->id_language,25);
	$row = $dodc->ContractGet($id_contract);
	if($row['id_contract']>0)
	{
		$contractor = $dodc->ContractorGet($row['id_contractor']);
		$title[] = array("Contractors",'contractors.php?country='.$contractor['country_code']);
		$title[] = array($contractor['name'],'contractor.php?id='.$row['id_contractor']);
		$title[] = array("Contracts",'contracts.php?id='.$row['id_contractor']);
		$title[] = array($row['number'],'');
		echo $hh->ShowTitle($title);
		
		$period = $hh->FormatDate($row['start_date_ts']);
		if($row['end_date_ts']>0 && $row['end_date_ts']!=$row['start_date_ts'])
			$period .= " - " . $hh->FormatDate($row['end_date_ts']);
		
		echo DodContractField($hh->tr->Translate("date"),$period);
		echo DodContractField($hh->tr->Translate("amount"),$hhf->FormatMoney($row['amount'],4,false));
		echo DodContractField($hh->tr->Translate("amount") . " (infl.)",$hhf->FormatMoney($row['iamount'],4,false));
		echo DodContractField($hh->tr->Translate("description"),$row['category_description']);
		echo DodContractField($hh->tr->Translate("details"),$row['product_description']);
		if($row['notes']!="")
			echo DodContractField($hh->tr->Translate("notes"),$row['notes']);
		
		$office = $dodc->OfficeGet($row['id_office']);
		
		echo DodContractField($trm25->Translate("office"),"<a href=\"office.php?id={$office['id_office']}\">{$office['name']}");
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
