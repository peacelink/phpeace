<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

echo $hh->ShowTitle($title);

$dodc = new DodContractors();

$trm25 = new Translator($hh->tr->id_language,25);

$url = $ini->Get("pub_web") . "/dodc";
// echo "<p>" . $hh->tr->Translate("website") . ": <a href=\"$url\" target=\"blank\">$url</a></p>\n";
?>
<h3>Contractors</h3>
<ul>
<li><a href="contractors.php">Tutti</a></li>
<li><a href="contractors.php?country=IT">Italiani</a></li>
<!-- <li><a href="contractor_search.php">Ricerca</a></li> -->
</ul>

<?php
echo "<h3><a href=\"offices.php\">" . $trm25->Translate("offices") . "</a></h3>";
echo "<h3><a href=\"categories.php\">" . $trm25->Translate("categories") . "</a></h3>";

if ($module_admin)
{
	echo "<h3>" . $hh->tr->Translate("configuration") . "</h3>\n";
	echo "<ul>";
	echo "<li><a href=\"database.php\">Database</a></li>\n";
	echo "<li><a href=\"tools.php\">Tools</a></li>\n";
	echo "</ul>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
