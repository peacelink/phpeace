<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/list.php");
include_once(SERVER_ROOT."/../classes/users.php");

$uu = new Users;

$id = $_GET['id'];

$ml = new MailingList($id);
$list_params = $ml->UserAdmin();

$input_super_right = 0;
if ($ah->current_user_id==$list_params['id_user'] || $module_admin)
	$input_right = 1;

if ($module_admin)
	$input_super_right = 1;

if ($id>0)
{
	$list = $ml->GetList($id);
	$id_user = $list['id_user'];
	$title[] = array($list['email'],'ops.php?id='.$id);
	$title[] = array('Modifica','');
	$action2 = "update";
}
else
{
	$id_user = $ah->current_user_id;
	$action2 = "insert";
	$title[] = array('Inserisci nuova lista','');
}

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","list");
echo $hh->input_hidden("id_list",$id);
echo $hh->input_table_open();
echo $hh->input_text("name","name",$list['name'],30,0,$input_right);
echo $hh->input_text("email","email",$list['email'],30,0,$input_super_right);
echo $hh->input_textarea("description","description",$list['description'],50,5,"",$input_right);
echo $hh->input_text("archive","archive",$list['archive'],50,0,$input_right);
echo $hh->input_text("feed","feed",$list['feed'],50,0,$input_right);
$users = $uu->ModuleUsers($ah->session->Get("module_id"));


echo $hh->input_separator("administration");
include_once(SERVER_ROOT."/../classes/topics.php");
$t = new Topics;
echo $hh->input_topics($list['id_topic'],0,$t->AllTopics(),"--",$input_super_right);
echo $hh->input_row("administrator","id_user",$id_user,$users,"choose_option",0,$input_super_right);
echo $hh->input_text("owner_email","owner_email",$list['owner_email'],30,0,$input_super_right);
include_once(SERVER_ROOT."/../modules/lists.php");
$li = new Lists();
echo $hh->input_row("group","id_group",$list['id_group'],$li->GroupsAll(),"--",0,$input_super_right);
echo $hh->input_checkbox("public","public",$list['public'],6,$input_super_right);
echo $hh->input_array("software","impl",$list['impl'],$hh->tr->Translate("list_impl"),$input_super_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

if ($input_right==1 && $id>0)
{
	echo "<p><a href=\"list_password.php?id=$id\">Cambia password</a>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>


