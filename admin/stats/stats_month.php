<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/tracker.php");

$month = (int)$_GET['m'];
$year = (int)$_GET['y'];

$months = $hh->tr->Translate("month");

$title[] = array("visitors",'stats.php');
$title[] = array($months[$month-1] . " " . $year,'');
	
echo $hh->ShowTitle($title);

if ($module_right)
{
	$tk = new Tracker();
	
	$smonth = $tk->StatsMonth($month,$year);
	$tot_visits = $smonth['visits1'] + $smonth['visits2'];
	$tot_pages = $smonth['pages1'] + $smonth['pages2'];
	echo "<ul>";
	echo "<li>" . $hh->tr->Translate("pages") . ": <b>" . $tot_pages . "</b></li>";
	echo "<li>" . $hh->tr->Translate("visits") . ": <b>" . $tot_visits . "</b></li>";
	echo "<li>" . $hh->tr->Translate("pages") . "/" . $hh->tr->Translate("visit") . ": <b>" . number_format($tot_pages/$tot_visits,2) . "</b></li>";
	echo "<li>" . $hh->tr->Translate("unique") . ": <b>" . $smonth['uniques'] . "</b></li>";
	echo "</ul>";
	
	$num = $tk->StatsDays( $row, $month, $year );
	
	$table_headers = array('day','unique','visits','pages','','');
	$table_content = array('<div class=\"right\">$row[dd]</div>',
	'<div class=\"right\">$row[uniques]</div>',
	'<div class=\"right\">$row[visits]</div>',
	'<div class=\"right\">$row[pages]</div>',
	'{LinkTitle("visits.php?d=$row[dd]&m=' . $month . '&y=' . $year .'",' . $hh->tr->Translate("visits"). ')}',
	'{LinkTitle("pages.php?d=$row[dd]&m=' . $month . '&y=' . $year .'",' . $hh->tr->Translate("pages"). ')}'
	);
	
	$hh->records_per_page = 31;
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);

	$row = array();
	$num2 = $tk->StatsPages( $row, $year, $month );
	
	$table_headers = array('group','topic','page','hits');
	$table_content = array('$row[group_name]','$row[topic_name]','{LinkTitle($row[url],$row[title])}','<div class=\"right\">$row[pages]</div>');
	
	$hh->records_per_page = $conf->Get("records_per_page");
	
	echo "<br><h3>" . $hh->tr->Translate("pages") . "</h3>";
	echo $hh->ShowTable($row, $table_headers, $table_content, $num2);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
