<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/users.php");

$title[] = array('users_list','');

$name = $get['u'];

echo $hh->ShowTitle($title);

$users = new Users();
$num = $users->Search( $row, $name );

$table_headers = array('user','msg','email','phone','mobile','active');
$table_content = array('{LinkTitle("user.php?id=$row[id_user]&p='.$current_page.'",$row[name])}','{SendMessageToUser($row[id_user],$row[name])}',
'$row[email]','$row[phone]','$row[mobile]','{Bool2YN($row[active])}');
?>
<form action="find.php" method="get">
<table border=0 cellpadding=1 cellspacing=3>
<?php
echo $hh->input_text("<input type=\"submit\" class=\"input-submit\" value=\"" . $hh->tr->Translate("search") . "\">","u",$unescaped_get['u'],20,0,1);
?>
</table>
</form>
<?php
echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
