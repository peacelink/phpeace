<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/users.php");

$uu = new Users;

$id = $_GET['id'];
$g = $_GET['g'];
$w = $_GET['w'];

$mod = Modules::ModuleGet($id);
$modules_names = $hh->tr->Translate("modules_names");

$desc = ($g=="user")? $hh->tr->Translate("users") : $hh->tr->Translate("administrators");
$title[] = array('modules_mng','modules.php');
$title[] = array($hh->tr->TranslateParams("module_name",array($modules_names[$id])),'module.php?id='.$id);
$title[] = array($desc,'');

echo $hh->ShowTitle($title);

if ($module_admin || Modules::AmIAdmin($id))
	$input_right = 1;

echo "<p>$desc " . $hh->tr->TranslateParams("module_users",array($modules_names[$id])) . "</p>";

if (!$mod['active'] || $mod['admin']!="1")
{
	echo "<p>" . $hh->tr->Translate("module_locked") . "</p>\n";
	$input_right = 0;
}
if ($g=="admin" && ($id=="1" || $id=="17"))
{
?>
<p>(<?=$hh->tr->Translate("missing_admin");?>)</p>
<script type="text/javascript">
function validate()
{
	f = document.forms['form1'];
	boolOK = true;
	strAlert = "";
	counter = 0;
	for (i=0;i<f.length;i++)
	{
		if(f.elements[i].checked)
			counter = counter + 1;
	}
	if (f.action2.value=="add_admin" && (counter<1))
	{
		strAlert += "<?=$hh->tr->Translate("missing_admin");?>\n";
		boolOK = false;
	}
	if (boolOK==true)
		f.submit();
	else
		alert(strAlert);
}
</script>

<?php
} 

if ($mod['restricted']==0 && $g=="user")
	echo "<p>" . $hh->tr->Translate("module_generic_warn") . "</p>\n";
?>

<form name="form1" action="actions.php" method="post">
<input type="hidden" name="from" value="module">
<input type="hidden" name="action2" value="add_<?=$g;?>">
<input type="hidden" name="w" value="<?=$w;?>">
<input type="hidden" name="id_module" value="<?=$id;?>">
<?php
$counter = 0;
$users = $uu->ModuleUsersAdmins($id, $g);

$items = array();
foreach($users as $user)
{
	$item = "";
	if ($input_right)
	{
		$item .= "<input type=\"checkbox\"  class=\"input-checkbox\" name=\"$user[id_user]\"";
		if (($user['id_module']>0 && $g=="user") || ($user['is_admin']>0 && $g=="admin"))
			$item .= " checked ";
		$item .= ">";
	}
	$item .= $user['name'];
	$items[] = $item;
}
echo $hh->ColumnsMaker($items,3);
if ($input_right=="1")
{
	if ($g=="admin" && ($id=="1" || $id=="17"))
		echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") . "\" onClick=\"validate()\">\n";
	else
		echo "<input type=\"submit\" class=\"input-submit\" value=\"" . $hh->tr->Translate("submit") . "\">\n";
}
?>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>


