<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/services.php");

$se = new Services();

$title[] = array('services','');

echo $hh->ShowTitle($title);

$services = $se->ServicesAvailable();

if(count($services)>0)
{
	$hhf = new HHFunctions();
	echo "<ul>";
	foreach($services as $service)
	{
		echo "<li>{$service['name']}";
		$users = $se->ServiceUsers($service['name']);
		if(count($users)>0)
		{
			echo "<ul>";
			foreach($users as $user)
			{
				echo "<li><a href=\"user_service.php?id={$user['id_user']}&id_user_service={$user['id_user_service']}\">{$user['name']}</a></li>";
			}
			echo "</ul>";
		}
		echo "</li>";
	}
	echo "</ul>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>


