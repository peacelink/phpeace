
/* SUBTOPIC TYPES */
function subSubtopic(subtopic_type_enc,input_right)
{
	var subtopic_type = parseInt(subtopic_type_enc.substring(2));
	var is_module = subtopic_type_enc.substring(0,1)=="m";
	var subitems = is_module? subtopic_modules[subtopic_type] : subtopic_subtypes[subtopic_type];
	subtopicOptions(subitems,input_right);
}

function subtopicOptions(subitems,input_right)
{
	var div_item = document.getElementById("type_item");
	var item_html = "";
	if(subitems.length>0)
	{
		for(i=0;i<subitems.length;i++)
		{
			item_html += '<div class="subitem"><label for="' + subitems[i].id + '">' + subitems[i].label + '</label>';
			if(subitems[i].type == 'list')
			{
				if(input_right==1)
				{
					item_html += '<select name="' + subitems[i].id + '">';
					for(var j in subitems[i].options)
					{
						item_html += '<option value="' + j + '"';
						if(j==subitems[i].value)
							item_html += " selected";
						item_html += '>' + subitems[i].options[j] + '</option>';
						
					}
					item_html += '</select>';
				}
				else
				{
					for(var j in subitems[i].options)
					{
						if(j==subitems[i].value)
							item_html += subitems[i].options[j];
					}
				}
				
			}
			if(subitems[i].type == 'text')
			{
				if(input_right==1)
				{
					item_html += '<input type="text" size="' + subitems[i].size + '" name="' + subitems[i].id + '" value="' + subitems[i].value + '" >';
				}
				else
				{
					item_html += subitems[i].value;
				}
				
			}
			item_html += '</div>';
		}
	}
	else
	{
		item_html += ' - ';
	}
	div_item.innerHTML = item_html;
}

/* CHECKBOXES ALERTS */
function alertCheckboxClick(cbox,warning)
{
	if(cbox.checked)
		alert(warning);
}

var lastActivity = 0;

$(function() {
  // Keep session alive
  var keepAliveTimeout = 290000;  // < 5 min

  // when logged in
  if( $('#user-info a').length) {
    var checkInterval = setInterval( function() {
      // check if there has been any activity
      if(new Date().getTime() - lastActivity > keepAliveTimeout) {
        // stop running
        clearInterval(checkInterval);
      } else {
        // refresh session cookie
        $.get('/gate/keepalive.php');
      }
    }, keepAliveTimeout);
    $(document).keydown( function() {	
      // refresh activity timestamp
      lastActivity = new Date().getTime();
    });
  }
})