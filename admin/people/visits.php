<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/tracker.php");

$id_p = (int)$_GET['id_p'];

$day = (int)$_GET['d'];

if($id_p>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$row = $pe->UserGetDetailsById($id_p);
	$title[] = array($row['name1'] . " " . $row['name2'],'');
	$qs = "&id_p=$id_p";
}
elseif($day>0) 
{
	$month = (int)$_GET['m'];
	$year = (int)$_GET['y'];
	$months = $hh->tr->Translate("month");
	
	$title[] = array("stats",'stats.php');
	$title[] = array($months[$month-1] . " " . $year,'stats_month.php?m=' . $month . '&y=' . $year);
	$title[] = array($day,'');
	$qs = "&d=$day&m=$month&y=$year";
}
else
{
	$title[] = array("visits",'');
	$qs = "";
}
	
echo $hh->ShowTitle($title);

if($id_p>0)
{
	$tabs = array();
	$tabs[] = array('user_data','/people/person.php?id='.$id_p);
	if($ah->IsModuleActive(29))
		$tabs[] = array('Associations',$id_p>0? '/people/person_association.php?id='.$id_p:'');
	$tabs[] = array('user_admin','/people/person_admin.php?id='.$id_p);
	$tabs[] = array('groups','/people/person_groups.php?id='.$id_p);
	$tabs[] = array('history','/people/person_history.php?id='.$id_p);
	$tabs[] = array('topics','/people/person_topics.php?id='.$id_p);
	$tabs[] = array('user_stats','/people/person_stats.php?id='.$id_p);
	$tabs[] = array('visits','');
	echo $hh->Tabs($tabs);
}

$tk = new Tracker();

$num = $tk->Visits( $row, $id_p, $day, $month, $year );
$table_headers = array('date','ip','name','pages','length','cookie');
$table_content = array('{FormatDateTime($row[visit_date_ts])}','$row[ip]',
'{LinkTitle("visit.php?id=$row[id_visit]'.$qs.'",$row[name])}','<div class=\"right\">$row[pages]</div>','<div class=\"right\">$row[length]</div>','{Bool2YN($row[cookie])}');
echo "<h3>" . $hh->tr->Translate("visits") . "</h3>";
echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
