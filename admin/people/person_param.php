<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
    define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");

$pe = new People();

if($module_admin)
	$input_right = 1;

$id_p = (int)$_GET['id_p'];

if($id_p==0)
    exit();

$person = $pe->UserGetDetailsById($id_p);
$title[] = array($person['name1'] . " " . $person['name2'],'/people/person.php?id='.$id_p);
$title[] = array("add_new");

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/people/person.php?id='.$id_p);
$tabs[] = array('user_admin','/people/person_admin.php?id='.$id_p);
$tabs[] = array('groups','/people/person_groups.php?id='.$id_p);
$tabs[] = array('history','/people/person_history.php?id='.$id_p);
$tabs[] = array('topics','/people/person_topics.php?id='.$id_p);
$tabs[] = array('user_stats','/people/person_stats.php?id='.$id_p);
if($conf->Get("track") || $conf->Get("track_all"))
	$tabs[] = array('visits','/people/visits.php?id_p='.$id_p);
echo $hh->Tabs($tabs);

echo $hh->input_form_open();
echo $hh->input_hidden("from","person_param");
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_table_open();

echo $hh->input_text("name","name",$param_name,20,0,$input_right);
echo $hh->input_text("value","value",$param_value,20,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
