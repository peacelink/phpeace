<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/tracker.php");


$day = (int)$_GET['d'];

if($day>0) 
{
	$month = (int)$_GET['m'];
	$year = (int)$_GET['y'];
	$months = $hh->tr->Translate("month");
	
	$title[] = array("stats",'stats.php');
	$title[] = array($months[$month-1] . " " . $year,'stats_month.php?m=' . $month . '&y=' . $year);
	$title[] = array($day,'');
	$qs = "&d=$day&m=$month&y=$year";
}
else
{
	$title[] = array("pages",'');
	$qs = "";
}
	
echo $hh->ShowTitle($title);

$tk = new Tracker();

$row = array();
$num = $tk->StatsPages( $row, $year, $month, $day );
$table_headers = array('group','topic','page','hits');
$table_content = array('$row[group_name]','$row[topic_name]','{LinkTitle($row[url],$row[title])}','<div class=\"right\">$row[pages]</div>');

echo "<h3>" . $hh->tr->Translate("pages") . "</h3>";
echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
