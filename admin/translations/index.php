<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/translations.php");

$tra = new Translations();
echo $hh->ShowTitle($title);

if ($module_admin)
	$input_right = 1;
$id_user = $ah->current_user_id;
?>
<h2>Le tue traduzioni</h2>
<ul>
<li><a href="translations.php?id_user=<?=$id_user;?>&s=atwork">In corso</a> (<?=$tra->TranslationsUserP( $row, "atwork", $id_user );?>)</li>
<li><a href="translations.php?id_user=<?=$id_user;?>&s=pending">Che potresti fare</a> (<?=$tra->TranslationsUserP( $row, "pending", $id_user );?>)</li>
<li><a href="translations.php?id_user=<?=$id_user;?>">Completate</a> (<?=$tra->TranslationsUserP( $row, "", $id_user );?>)</li>
</ul>

<h2>Articoli da tradurre</h2>
<?php
if ($input_right!="1")
	echo "<i>(vengono mostrati solo quelli nelle <a href=\"translator_languages.php?id={$ah->current_user_id}\">lingue</a> per le quali ti sei reso disponibile)</i>";
?>
<ul>
<li><a href="translations.php?s=pending">Ancora da assegnare</a> (<?=$tra->TranslationsP( $row, "pending" );?>)</li>
<li><a href="translations.php?s=atwork">Gia' assegnati ma non ancora completati</a> (<?=$tra->TranslationsP( $row, "atwork" );?>)</li>
<li><a href="translations.php">Completati</a> (<?=$tra->TranslationsP( $row, "" );?>)</li>
</ul>

<h2>Richiedi traduzione</h2>
<ul>
<li><a href="/articles/article.php?id=0&w=tra">Inserendo l'articolo originale</a></li>
<li><a href="article_search.php">Di un articolo gia' presente in archivio</a></li>
<li><a href="translation.php?id=0">Segnalando il link a un articolo su internet</a></li>
</ul>

<h2>Inserisci traduzione:</h2>
<ul>
<li><a href="translations.php?id_user=<?=$id_user;?>&s=atwork&insert=1">Inserimento traduzione</a>
<div class="notes">Prima di inserire una traduzione, deve essere presente la relativa richiesta</div></li>
</ul>

<h2>Elenco traduttori</h2>
<ul>
<li><a href="translators.php">Per nome</a></li>
<li><a href="languages.php">Per lingua</a></li>
</ul>

<?php
if ($input_right=="1")
{
?>
<h2>Amministrazione</h2>
<ul>
<li><a href="/users/module_add.php?id=21&g=user&w=tra">Abilita un utente al modulo traduzioni</a></li>
<li><a href="translations.php?s=uncomplete">Traduzioni con dati mancanti</a> (<?=$tra->TranslationsP( $row, "uncomplete" );?>)</li>
</ul>
<?php
}
include_once(SERVER_ROOT."/include/footer.php");
?>
