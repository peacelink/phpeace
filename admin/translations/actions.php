<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/translations.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action2	= $post['action2'];
$from		= $post['from'];
$action3	= $_GET['action3'];
$from2	= $_GET['from2'];

$tra = new Translations();

if ($from=="translator_language" || $from2=="translator_language")
{
	if ($action2=="update")
	{
		$id_user	= $post['id_user'];
		$id_language_from	= $post['id_language_from'];
		$id_language_to	= $post['id_language_to'];
		if ($id_language_from>0 && $id_language_to>0 && $id_language_from!=$id_language_to)
			$tra->TranslatorLanguagesUpdate($id_user,$id_language_from,$id_language_to);
	}
	if ($action3=="remove")
	{
		$id_user	= $_GET['id'];
		$id_language_from	= $_GET['id_f'];
		$id_language_to	= $_GET['id_t'];
		$tra->TranslatorLanguagesDelete($id_user,$id_language_from,$id_language_to);
	}
	header("Location: translator.php?id=$id_user");
}

if ($from=="translation")
{
	$id_translation	= $post['id_translation'];
	$id_language	= $post['id_language'];
	$id_language_trad	= $post['id_language_trad'];
	$id_language_trad2	= $post['id_language_trad2'];
	$notes		= $post['notes'];
	$source_url		= $fh->String2Url($post['source_url']);
	$priority	= $post['priority'];
	$id_user	= $fh->String2Number($post['id_user']);
	$id_rev		= $fh->String2Number($post['id_rev']);
	$id_ad		= $fh->String2Number($post['id_ad']);
	$id_article	= $fh->String2Number($post['id_article']);
	$start_date	= $fh->DateMerge("start_date",$post);
	$comments	= $post['comments'];
	$translator	= $post['translator'];
	$completed		= $fh->Checkbox2bool($post['completed']);
	if ($id_language_trad>0)
	{
		if ($action2=="update")
		    $tra->TranslationUpdate($id_translation,$start_date,$id_language,$id_language_trad,$notes,$priority,$id_user,$comments,$translator,$completed,$id_rev,$id_ad,$source_url);
		if ($action2=="insert")
		    $tra->TranslationInsert($id_article,$id_language,$id_language_trad,$notes,$priority,$source_url);
		if ($action2=="take")
			$tra->TranslationTake($id_translation,$ah->current_user_id);
		if ($action2=="delete")
			$tra->TranslationDelete($id_translation);
		if ($action2=="translate")
			header("Location: /articles/article.php?id=0&id_translation=$id_translation&w=tra");
		else if ($action2=="associate")
		    header("Location: translation_associate.php?id=$id_translation");
		else
			header("Location: index.php");
	}
	else
	{
		$ah->MessageSet("translation_lang_choose");
		header("Location: translation.php?id=$id_translation&id_a=$id_article");
	}
}

if ($from=="translation_associate") {
    $id_translation	= $post['id_translation'];
    $id_article	= $fh->String2Number($post['id_article']);
    if ($id_translation>0 && $id_article>0) {
        $tra->TranslationArticleAssociate($id_translation, $id_article);
        header("Location: translation.php?id=$id_translation");
    } else {
        $ah->MessageSetTranslated("ID articolo mancante");
        header("Location: translation_associate.php?id=$id_translation");
    }
}
?>
