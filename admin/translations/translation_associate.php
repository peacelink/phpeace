<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/translations.php");
include_once(SERVER_ROOT."/../classes/article.php");

$tra = new Translations();

$id_translation = $_GET['id'];

$row = $tra->TranslationGet( $id_translation );

$title[] = array('Traduzione',"translation.php?id=$id_translation");
$title[] = array('Associa articolo esistente','');

if ($module_right || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_note("<p>ID traduzione richiesta: <a href=\"/translations/translation.php?id=$id_translation\">{$id_translation}</a></p>");

echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("from","translation_associate");
echo $hh->input_hidden("id_translation",$id_translation);
echo $hh->input_table_open();

echo $hh->input_text("ID articolo esistente","id_article",$row['id_article_trad'],10,0,$input_right);

$actions = array();
$actions[] = array('action'=>("associate"),'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
    
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
