<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

include_once(SERVER_ROOT."/../classes/translations.php");

$tra = new Translations();

$status = $_GET['s'];
$insert = (int)$_GET['insert'];
$id_user = $_GET['id_user'];

switch($status)
{
	case "pending":
		$title2 = "Traduzione da assegnare";
		break;
	case "atwork":
		$title2 = "Traduzioni in corso";
		break;
	case "uncomplete":
		$title2 = "Traduzione con dati mancanti";
		break;
	default:
		$title2 = "Traduzioni completate";
}

$title[] = array($title2,'');
echo $hh->ShowTitle($title);

if ($id_user>0)
{
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User;
	$user = $u->UserGet();
	echo "<p>$title2 ({$user['name']})" . ($insert==1?" - Clicca sulla traduzione di cui vuoi inserire il testo":"") . "</p>";
}

if ($id_user>0)
	$num = $tra->TranslationsUserP( $row, $status, $id_user );
else
	$num = $tra->TranslationsP( $row, $status );
	
$table_headers = array('date','priority','da lingua','a lingua','info','traduttore');
$table_content = array('{FormatDate($row[start_date_ts])}','{PriorityLookup($row[priority])}',
'{LanguageLookup($row[id_language])}','{LanguageLookup($row[id_language_trad])}',
'{TranslationInfo($row[id_translation],$row[id_article],$row[notes],'.$insert.')}','$row[user_name]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

