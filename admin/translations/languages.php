<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/translations.php");



$title[] = array('Elenco traduttori per lingua','');

echo $hh->ShowTitle($title);

$tra = new Translations();

$languages_from = $tra->LanguageFrom();
if (count($languages_from)>0)
{
	echo "<ul>\n";
	foreach($languages_from as $language_from)
	{
		echo "<li>" . $tra->languages[$language_from['id_language_from']];
		$languages_to = $tra->LanguageTo($language_from['id_language_from']);
		if (count($languages_to)>0)
		{
			echo "<ul>\n";
			foreach($languages_to as $language_to)
			{
				echo "<li>" . $tra->languages[$language_to['id_language_to']];
				$translators = $tra->TranslatorsLanguage($language_from['id_language_from'],$language_to['id_language_to']);
				echo "<ul>\n";
				foreach($translators as $translator)
				{
					echo "<li><a href=\"translator.php?id=$translator[id_user]\">$translator[name]</a></li>\n";
				}
				echo "</ul>\n";
				echo "</li>\n";
			}
				
			echo "</ul>\n";
		}
		
		echo "</li>\n";
	}
		
	echo "</ul>\n";
}


include_once(SERVER_ROOT."/include/footer.php");
?>
