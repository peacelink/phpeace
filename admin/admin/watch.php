<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/scheduler.php");

$sc = new Scheduler();

$id = $_GET['id'];

$title[] = array('Site watches','watches.php');

if ($module_admin)
	$input_right = 1;

if ($id>0)
{
	$row = $sc->WatchGet($id);
	$title[] = array($row['url'],'');
}
else
{
	$title[] = array('add_new','');
}

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","watch");
echo $hh->input_hidden("id_watch",$id);
echo $hh->input_table_open();

echo $hh->input_text("url","url",$row['url'],70,0,$input_right);
echo $hh->input_text("keyword","keyword",$row['keyword'],70,0,$input_right);
echo $hh->input_text("email","notify",$row['notify'],70,0,$input_right);
if($id>0) {
	echo $hh->input_text("counter","counter",$row['counter'],70,0,0);
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && $id>0));
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
