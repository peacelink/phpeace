<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

 ********************************************************************/

if (! defined ( 'SERVER_ROOT' ))
	define ( 'SERVER_ROOT', $_SERVER ['DOCUMENT_ROOT'] );
include_once (SERVER_ROOT . "/include/header.php");
include_once (SERVER_ROOT . "/../classes/qr.php");
include_once (SERVER_ROOT . "/../classes/file.php");

$qr = new QR();
$fm = new FileManager();

$id = $_GET ['id'];

$title [] = array('QR codes', 'qrcodes.php' );
if ($id > 0) {
	$row = $qr->QRCodeGet( $id );
	$title [] = array ('change', '' );
} else {
	$row = array();
	$title [] = array ('add_new', '' );
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle ( $title );

echo $hh->input_form_open ();
echo $hh->input_hidden ( "from", "qrcode" );
echo $hh->input_hidden ( "id_qrcode", $id );
echo $hh->input_table_open ();
if ($id>0) {
	$i = new Images();
	$filename_png = "qrcodes/{$row['hash']}.png";
	$filename_svg = "qrcodes/{$row['hash']}.svg";
	if($fm->Exists("uploads/$filename_png"))
	{
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$url_png = $irl->PublicUrlGlobal("qrcode",array('format'=>"png",'hash'=>$row['hash']));
		echo "<tr><td align=\"right\"></td><td><img src=\"/images/upload.php?src=$filename_png\" width=\"{$i->img_sizes[2]}\"></td></tr>";
		$url_svg = $irl->PublicUrlGlobal("qrcode",array('format'=>"svg",'hash'=>$row['hash']));
    echo $hh->input_info("Link PNG","<a href=\"$url_png\" target=\"blank\">$url_png</a> - <a href=\"/images/upload.php?src=$filename_png\" download>download</a>");
    echo $hh->input_info("Link SVG","<a href=\"$url_svg\" target=\"blank\">$url_svg</a> - <a href=\"/images/upload.php?src=$filename_svg\" download>download</a>");
  }
  echo $hh->input_text ( "QR link", "hash", $qr->shorturl_mask . $row['hash'], 80, 0, 0 );
  echo $hh->input_text ( "count", "count", $row['counter'], 5, 0, 0 );
}
echo $hh->input_text ( "URL", "url", $row['url'], 70, 0, $input_right );
echo $hh->input_text("description","description",$row['description'],70,0,$input_right);
$actions = array ();
$actions [] = array ('action' => "store", 'label' => "submit", 'right' => $input_right );
if ($id > 0)
	$actions [] = array ('action' => "delete", 'label' => "delete", 'right' => $input_right );
echo $hh->input_actions ( $actions, $input_right );
echo $hh->input_table_close () . $hh->input_form_close ();

include_once (SERVER_ROOT . "/include/footer.php");
?>

