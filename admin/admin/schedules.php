<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/scheduler.php");
include_once(SERVER_ROOT."/../classes/cron.php");

$cr = new Cron();
$sc = new Scheduler();
$actions = $sc->SchedulableActions();
$hhf = new HHFunctions();

$trm14 = new Translator($hh->tr->id_language,14);
$title[] = array($trm14->Translate("schedules"),'');

echo $hh->ShowTitle($title);

$slots = $sc->SchedulerSlots();
if($cr->lock->LockCheck())
{
	$filemtime = $cr->lock->LockLastTime();
	$minutes = floor((time() - $filemtime)/60);
	if($dev_right || $minutes > cron::MinimumTimeAdminWarning)
	{
		echo "<ul class=\"msg\"><li>" . $hh->tr->TranslateParams("cron_locked",array($minutes,$hh->FormatDateTime($filemtime),"cron_lock.php")) . "</li></ul>\n";	
	}
}

if(count($slots)>0)
{
	echo "<ul class=\"slots\">\n";
	foreach($slots as $slot)
	{
		echo "<li><span class=\"time\">{$slot['time']}" . "</span>";
		echo " <span class=\"notes\"> - <a href=\"scheduler.php?id={$slot['id_scheduler']}\">" . $hh->tr->Translate("change") . "</a> - ";
		echo "<a href=\"scheduler_execute.php?id={$slot['id_scheduler']}\">" . $hh->tr->Translate("execute") . "</a></span>\n";
		$schedules = $sc->SchedulerActions($slot['id_scheduler'],false);
		if(count($schedules)>0)
		{
			echo "<ul>\n";
			foreach($schedules as $schedule)
			{
				echo "<li><a href=\"schedule.php?id={$schedule['id_schedule']}\">" . $actions[$schedule['id_action']] .  "</a>";
				echo "<div class=\"notes\">" . $hh->tr->Translate("user") . ": " . $hhf->UserLookup($schedule['id_user']);
				echo " - " . $hh->tr->Translate("active") . ": " . $hh->tr->Translate(($schedule['active'])? "yes":"no") . "</li>\n";
			}
			echo "</ul>\n";
		}
		echo "</li>\n";
	}
	echo "</ul>\n";
}

?>
<p><a href="schedule.php?id=0"><?=$hh->tr->Translate("schedule_add");?></a>
 - <a href="scheduler.php?id=-1"><?=$hh->tr->Translate("scheduler_add");?></a></p>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

