<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/publishmanager.php");
$pm = new PublishManager();

$title[] = array('robots.txt','');

if ($module_admin)
	$input_right = 1;

	
$fm = new FileManager;
// $txt = $fm->TextFileRead("pub/robots.txt");
$contents = $fm->TextFileRead("pub/robots.txt");
$lines = explode("\n",$contents);
for($i = 0; $i < count($lines); $i++) {
   if(strpos($lines[$i],$pm->robots_txt_separator)!==false)
      array_splice($lines, 0, $i+1);
}
$txt = implode("\n", $lines);

$robots = $pm->RobotsTxt();
echo $hh->ShowTitle($title);

echo "Questo form e' solo oper eventuali aggiunte al robots.txt, che include gia' alcune impostazioni predefinite e gli articoli marcati per essere esclusi dai motori di ricerca, come mostrato qui di seguito";

echo $hh->input_form_open();
echo $hh->input_hidden("from","robots");
echo $hh->input_table_open();
echo $hh->input_textarea("predefiniti","robots_predefined",$robots,60,20,"",0);
echo $hh->input_textarea("robots.txt","txt",$txt,60,20,"",$input_right);
echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
