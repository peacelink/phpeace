<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/search.php");

$id_stat = $_GET['id'];

$trm14 = new Translator($hh->tr->id_language,14);

$title[] = array($trm14->Translate("search_stats"),'');

echo $hh->ShowTitle($title);

$s = new Search();

if($id_stat>0)
{
	if($_GET['id_res']>0)
		$params['id_res'] = $_GET['id_res'];
	if($_GET['id_item']>0)
		$params['id_item'] = $_GET['id_item'];
	if($_GET['q']!="")
		$params['q'] = $_GET['q'];
	if($_GET['id_word']>0)
		$params['id_word'] = $_GET['id_word'];
	$row = array();
	$stat = $s->Stats($row,$id_stat,$params);
	if($params['id_res']>0)
	{
		$resources = $hh->tr->Translate("resources");
		unset($resources[6]);
		unset($resources[22]);
		echo "<p>" . $hh->tr->Translate("type") . ": " . $resources[$params['id_res']] . "</p>\n";
		if($params['id_item']>0)
			echo $hh->ResourceLink($params['id_res'],$params['id_item']);
	}
	echo $hh->ShowTable($row, $stat['headers'], $stat['content'], $stat['num']);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
