<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/scheduler.php");
include_once(SERVER_ROOT."/include/header.php");

$sc = new Scheduler();

$id_scheduler = $_GET['id'];

if ($module_admin)
	$input_right = 1;
	
$row = $sc->SchedulerSlotGet($id_scheduler);

$trm14 = new Translator($hh->tr->id_language,14);
$title[] = array($trm14->Translate("schedules"),'schedules.php');

if($id_scheduler>=0)
	$title[] = array($row['hour'] . ":" . $row['minute'],'');
else
	$title[] = array("scheduler_add",'');

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","scheduler");
echo $hh->input_hidden("id_scheduler",$id_scheduler);
echo $hh->input_table_open();

$time = ($id_scheduler>=0)? mktime($row['hour'],$row['minute']) : time();

echo $hh->input_time("hour","time",$time,$input_right);
$actions = array();
$actions[] = array('action'=>"time",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
