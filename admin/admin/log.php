<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/log.php");
$log = new Log;

$line = (int)$get['line'];


if ($module_admin)
{
	$errors = $log->Items();
	if($line>0)
	{
		$title[] = array('Log','log.php');
		$error = $errors[$line]['error'];
		if(is_array($error))
		{
			$title[] = array($errors[$line]['error']['msg'],'');
			echo $hh->ShowTitle($title);
			$lines = $errors['stats']['num'];
			echo "<p>" . $hh->Wrap($hh->tr->Translate("previous"),"<a href=\"log.php?line=" . ($line-1) . "\">","</a>",$line>1) . " / " . $hh->Wrap($hh->tr->Translate("next"),"<a href=\"log.php?line=" . ($line+1) . "\">","</a>",$line<$lines) . "</p>";
			echo ErrorStyle();
			echo ErrorHTML($errors[$line]['error']);
		}
	}
	else 
	{
		$title[] = array('Log','');
		echo $hh->ShowTitle($title);
		echo "<ul>";
		echo "<li>Errors: " . ($errors['stats']['levels']['error'] + $errors['stats']['levels']['user error']) . "</li>";
		echo "<li>Warnings: " . ($errors['stats']['levels']['warning'] + $errors['stats']['levels']['user warning']) . "</li>";
		echo "<li>Scheduler: {$errors['stats']['levels']['scheduler']}</li>";
		echo "<li>Info: {$errors['stats']['levels']['info']}</li>";
		echo "<li>Total: {$errors['stats']['num']}</li>";
		echo "</ul>";
		if($errors['stats']['num']>0)
		{
			echo "<h3>" . $hh->tr->Translate("list") . "</h3>";
			echo "<ul class=\"errors\">";
			foreach($errors as $line => $error)
			{
				if(isset($error['ts']) && $error['ts']>0)
				{
					echo "<li>";
					echo "<div class=\"notes\">" . $hh->FormatDateTimeSeconds($error['ts']) . "</div>";
					echo "<span class=\"error-type\">[" . $error['error']['type'] . "]</span> {$error['error']['msg']} (<a href=\"log.php?line=$line\">" .  $hh->tr->Translate("details") . "</a>)";
					echo "</li>";			
				}
			}
			echo "</ul>";
		
			echo $hh->input_form_open();
			echo $hh->input_hidden("from","log");
			echo $hh->input_table_open();
			$actions = array();
			$actions[] = array('action'=>"reset",'label'=>"reset",'right'=>$module_admin);
			$actions[] = array('action'=>"send",'label'=>"send",'right'=>$module_admin);
			echo $hh->input_actions($actions,$module_admin);
			echo $hh->input_table_close() . $hh->input_form_close();		
		}
	}
}
include_once(SERVER_ROOT."/include/footer.php");
?>
