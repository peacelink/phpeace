<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/quotes.php");

$qu = new Quotes;
echo $hh->ShowTitle($title);

echo "<h2>" . $hh->tr->Translate("quotes") . "</h2><ul>";
$num1 = $qu->QuotesApproved( $row, 0, 0 );
echo "<li><a href=\"quotes.php?approved=0\">" . $hh->tr->Translate("quotes_to_approve") . "</a> ($num1)</li>\n";
$num2 = $qu->QuotesApproved( $row, 0, 1 );
echo "<li><a href=\"quotes.php?approved=1\">" . $hh->tr->Translate("quotes_approved") . "</a> ($num2)</li>\n";
echo "<li><a href=\"quote.php?id=0\">" . $hh->tr->Translate("quotes_new") . "</a></li>\n";
echo "<li><a href=\"search.php\">" . $hh->tr->Translate("search") . "</a></li>\n";
if ($module_admin)
	echo "<li><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></li>\n";
echo "</ul>\n";

echo $hh->input_code("quote_html","quote_html","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/quote.php\"></script>",60,2);

include_once(SERVER_ROOT."/include/footer.php");
?>
