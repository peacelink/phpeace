<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/video.php");
include_once(SERVER_ROOT."/../classes/audio.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper(true);
$trm9 = new Translator($fh->va->tr->id_language, 9);
$post = $fh->HttpPost();

$action 	= $fh->ActionGet($post);
$from		= $post['from'];

$vi = new Video();
$au = new Audio();

if ($from=="video_insert")
{
	$insert_date	= $fh->DateMerge("insert_date",$post);
	$title			= $post['title'];
	$description	= $post['description'];
	$author			= $post['author'];
	$source			= $post['source'];
	$link			= $post['link'];
	$id_licence		= $post['id_licence'];
	$id_language		= $post['id_language'];
	$keywords		= $post['keywords'];
	$auto_start		= $fh->Checkbox2bool($post['auto_start']);
	$approved		= $fh->Checkbox2bool($post['approved']);
	$encode			= $fh->Checkbox2bool($post['encode']);
	$file			= $fh->UploadedFile("vid",true,$vi->video_max_size);
	if($file['ok'])
		$id_video = $vi->VideoInsert($insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$approved,$file,$auto_start,$encode);
	$url = $id_video>0? "video_details.php?id=$id_video" : "video_insert.php";
	if($id_video>0 && $encode)
		$ah->MessageSetTranslated($trm9->Translate("media_received"));
	header("Location: $url");
}

if ($from=="audio_insert")
{
	$insert_date	= $fh->DateMerge("insert_date",$post);
	$title			= $post['title'];
	$description	= $post['description'];
	$author			= $post['author'];
	$source			= $post['source'];
	$link			= $post['link'];
	$id_licence		= $post['id_licence'];
	$id_language		= $post['id_language'];
	$keywords		= $post['keywords'];
	$auto_start		= $fh->Checkbox2bool($post['auto_start']);
	$download		= $fh->Checkbox2bool($post['download']);
	$approved		= $fh->Checkbox2bool($post['approved']);
	$encode			= $fh->Checkbox2bool($post['encode']);
	$file			= $fh->UploadedFile("aud",true,$au->audio_max_size);
	if($file['ok'])
		$id_audio = $au->AudioInsert($insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$approved,$file,$auto_start,$encode,$download);
	$url = $id_audio>0? "audio_details.php?id=$id_audio" : "audio_insert.php";
	if($id_audio>0 && $encode)
		$ah->MessageSetTranslated($trm9->Translate("media_received"));
	header("Location: $url");
}

if ($from=="video")
{
	$id_video		= (int)$post['id_video'];
	$insert_date	= $fh->DateMerge("insert_date",$post);
	$title			= $post['title'];
	$description	= $post['description'];
	$author			= $post['author'];
	$source			= $post['source'];
	$link			= $fh->String2Url($post['link']);
	$id_licence		= $post['id_licence'];
	$id_language		= $post['id_language'];
	$keywords		= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	$auto_start		= $fh->Checkbox2bool($post['auto_start']);
	$approved		= $fh->Checkbox2bool($post['approved']);
	$url = "video.php?id=$id_video";
	if($action=="store")
	{
		if ($vi->AdminRight($ah->current_user_id,$id_video) || $ah->CheckModule())
			$vi->VideoUpdate($id_video,$insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$ikeywords,$approved,$auto_start);
		else 
			$ah->MessageSet("user_no_auth");
	}
	if ($action=="delete")
	{
		$vi->VideoDelete($id_video);
		$url = "videos.php";
	}
	if ($action=="queue")
	{
		$me = new Media("video");
		$me->EncodeQueueAdd($id_video);
	}
	header("Location: $url");
}

if ($from=="audio")
{
	$id_audio		= (int)$post['id_audio'];
	$insert_date	= $fh->DateMerge("insert_date",$post);
	$title			= $post['title'];
	$description	= $post['description'];
	$author			= $post['author'];
	$source			= $post['source'];
	$link			= $fh->String2Url($post['link']);
	$id_licence		= $post['id_licence'];
	$id_language		= $post['id_language'];
	$keywords		= $post['keywords'];
	$ikeywords = array();
	if(is_array($post['ikeywords']))
	{
		foreach($post['ikeywords'] as $ikeyword)
			$ikeywords[] = array('id_keyword'=>$ikeyword);
	}
	$auto_start		= $fh->Checkbox2bool($post['auto_start']);
	$download		= $fh->Checkbox2bool($post['download']);
	$approved		= $fh->Checkbox2bool($post['approved']);
	$url = "audio.php?id=$id_audio";
	if($action=="store")
	{
		if ($au->AdminRight($ah->current_user_id,$id_audio) || $ah->CheckModule())
			$au->AudioUpdate($id_audio,$insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$ikeywords,$approved,$auto_start,$download);
		else 
			$ah->MessageSet("user_no_auth");
	}
	if ($action=="delete")
	{
		$au->AudioDelete($id_audio);
		$url = "audios.php";
	}
	if ($action=="queue")
	{
		$me = new Media("audio");
		$me->EncodeQueueAdd($id_audio);
	}
	header("Location: $url");
}
if ($from=="video_orig")
{
	$id_video		= (int)$post['id_video'];
	$file			= $fh->UploadedFile("vid",true,$vi->video_max_size);
	$encode		= $fh->Checkbox2bool($post['encode']);
	$url = "video.php?id=$id_video";
	if($action=="store" && $file['ok'])
	{
		if ($vi->AdminRight($ah->current_user_id,$id_video))
			$vi->VideoUpdateOrig($id_video,$file,$encode);
		else 
			$ah->MessageSet("user_no_auth");
	}
	if ($action=="delete")
	{
		$vi->VideoDelete($id_video);
		$url = "videos.php";
	}
	if ($action=="queue")
	{
		$me = new Media("video");
		$me->EncodeQueueAdd($id_video);
	}
	if ($action=="scan")
	{
		$vi->Scan($id_video);
	}
	header("Location: $url");
}

if ($from=="audio_orig")
{
	$id_audio	= (int)$post['id_audio'];
	$file		= $fh->UploadedFile("aud",true,$au->audio_max_size);
	$encode		= $fh->Checkbox2bool($post['encode']);
	$url = "audio.php?id=$id_audio";
	if($action=="store" && $file['ok'])
	{
		if ($au->AdminRight($ah->current_user_id,$id_audio))
			$au->AudioUpdateOrig($id_audio,$file,$encode);
		else 
			$ah->MessageSet("user_no_auth");
	}
	if ($action=="delete")
	{
		$au->AudioDelete($id_audio);
		$url = "audios.php";
	}
	if ($action=="queue")
	{
		$me = new Media("audio");
		$me->EncodeQueueAdd($id_audio);
	}
	if ($action=="scan")
	{
		$me = new Media("audio");
		$me->Scan($id_audio);
	}
	header("Location: $url");
}

if ($from=="video_thumbs")
{
	$action 	= $fh->ActionGet($post);
	$id_video	= (int)$post['id_video'];
	$id_image	= $post['associate'];
	$file		= $fh->UploadedFile("img",true);
	if($action=="store")
		$vi->ImageAssociate($id_image,$id_video,$file);
	header("Location: video.php?id=$id_video");
}

if ($from=="watermark")
{
	$file	= $fh->UploadedFile("img");
	if ($file['ok'])
		$vi->Watermark($file);
	header("Location: watermark.php");
}

if ($from=="video_keyword")
{
	$action 		= $fh->ActionGet($post);
	$id_keyword		= $post['id_keyword'];
	$role 			= $fh->String2Number($post['role']);
	$module_admin = $ah->CheckModule();
	if($module_admin && $id_keyword>0)
	{
		if ($action=="delete")
			$vi->KeywordDelete($id_keyword);
		if ($action=="insert")
			$vi->KeywordAdd($id_keyword,$role);
		if ($action=="update")
			$vi->KeywordUpdate($id_keyword,$role);
	}
	header("Location: keywords.php");
}

if ($from=="audio_keyword")
{
	$action 		= $fh->ActionGet($post);
	$id_keyword		= $post['id_keyword'];
	$role 			= $fh->String2Number($post['role']);
	$module_admin = $ah->CheckModule();
	if($module_admin && $id_keyword>0)
	{
		if ($action=="delete")
			$au->KeywordDelete($id_keyword);
		if ($action=="insert")
			$au->KeywordAdd($id_keyword,$role);
		if ($action=="update")
			$au->KeywordUpdate($id_keyword,$role);
	}
	header("Location: keywords.php");
}

if ($from=="config_update")
{
	$me = new Media();
	$module_admin = $ah->CheckModule();
	if($module_admin)
	{
		$media_path		= $post['media_path'];
		$me->ConfigurationUpdate($media_path);
	}
	header("Location: index.php");
}

?>
