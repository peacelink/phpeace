<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$year = (int)$_GET['year'];
$id_balance = isset($_GET['id_balance'])? (int)$_GET['id_balance'] : 1;

$p = new Payment();
$balance = $p->BalanceGet($id_balance);

$title[] = array($trm15->Translate("balance_year"),'balance_years.php');
$title[] = array("{$balance['balance']} $year",'');
echo $hh->ShowTitle($title);

if($id_balance==1) {
   echo "<h3>Bilancio conti $year</h3>";
   $payments = $p->TotalsAccountsYearSummary($year);
   $table_headers = array('account',"Saldo iniziale","Saldo finale ",'Differenza');
   $table_content = array('$row[name]','{Money($row[prev],2)}','{Money($row[current],2)}','{Money($row[year],2)}');
   echo $hh->ShowTableNoPagination($payments, $table_headers, $table_content, ['','prev','current','year']);   

   $out = $p->TotalsAccountsYearTransfers($year);
   if(count($out)>0) {
      echo "<h3>" . $trm15->Translate("transfers") . " $year</h3>";
      $table_headers = array('from','to',"total");
      $table_content = array('$row[account_from]','$row[account_to]','{Money($row[total],2)}');
      echo $hh->ShowTableNoPagination($out, $table_headers, $table_content);    
   }
}

echo "<h3>Uscite {$balance['balance']} $year</h3>";
$out = $p->TotalsAccountsYearSummaryInOut($year,0,$id_balance);
if(count($out)>0) {
   $table_headers = array('account',$trm15->Translate("payment_type"),$trm15->Translate("transactions"),"total");
   $table_content = array('$row[account_name]','{LinkTitle("search2.php?id_balance='.$id_balance.'&is_in=0&id_payment_type=$row[id_payment_type]&id_account=$row[id_account]&year='.$year.'",$row[payment_type])}','{FormatNumber($row[count],0)}','{Money($row[total],2)}');
   echo $hh->ShowTableNoPagination($out, $table_headers, $table_content, ['','','','total']);
} else {echo "0"; }

echo "<h3>Entrate {$balance['balance']} $year</h3>";
$in = $p->TotalsAccountsYearSummaryInOut($year,1,$id_balance);
if(count($in)>0) {
   $table_headers = array('account',$trm15->Translate("payment_type"),$trm15->Translate("transactions"),"total");
   $table_content = array('$row[account_name]','{LinkTitle("search2.php?id_balance='.$id_balance.'&is_in=1&id_payment_type=$row[id_payment_type]&id_account=$row[id_account]&year='.$year.'",$row[payment_type])}','{FormatNumber($row[count],0)}','{Money($row[total],2)}');
   echo $hh->ShowTableNoPagination($in, $table_headers, $table_content, ['','','','total']);
} else {echo "0"; }

$saldo = array_sum(array_column($in, 'total')) - array_sum(array_column($out, 'total'));
echo "<p>" . $trm15->Translate("total") . " {$balance['balance']} $year: <strong>" . number_format($saldo,2) . "</strong></p>";

echo "<p><a href=\"balance_year_xlsx.php?id_balance=$id_balance&year=$year\">Esportazione Excel</a></p>";

echo "<p><a href=\"balance_year_csv.php?id_balance=$id_balance&year=$year\">Esportazione CSV</a></p>";

$balances = $p->BalancesAll();
echo "<h3>Altri bilanci</h3>";
echo "<ul>";
foreach($balances as $bal) {
   if($bal['id_balance']!= $id_balance) {
      $out = $p->TotalsAccountsYearSummaryInOut($year,0,$bal['id_balance']);
      $in = $p->TotalsAccountsYearSummaryInOut($year,1,$bal['id_balance']);
      $saldo = array_sum(array_column($in, 'total')) - array_sum(array_column($out, 'total'));
      echo "<li><a href=\"balance_year.php?year=$year&id_balance={$bal['id_balance']}\">{$bal['balance']}</a> (<strong>" . number_format($saldo,2) . "</strong>)</li>";
   }
}
echo "</ul>";

include_once(SERVER_ROOT."/include/footer.php");
?>

