<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$id_account = (int)$_GET['id'];

$p = new Payment();

if ($module_admin)
	$input_right = 1;

$title[] = array('accounts','accounts.php');

if ($id_account>0)
{
	$row = $p->AccountGet($id_account);
	$title[] = array($row['name'],'');
	$active = $row['active'];
}
else
{
	$title[] = array('add_new','');
	$active = 1;
}

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","account");
echo $hh->input_hidden("id_account",$id_account);
echo $hh->input_table_open();

echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_array("type","id_type",$row['id_type'],$hh->tr->Translate("account_types"),$input_right);
echo $hh->input_checkbox("active","active",$active,0,$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($row['id_topic'],0,$tt->AllTopics(),"all_option",$input_right);
echo $hh->input_checkbox("shared","shared",$row['shared'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
 $actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_account>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


if($id_account>0)
{
	echo "<h3>" . $hh->tr->Translate("parameters") . "</h3>";
	$aparams = $p->AccountParams($row['id_type']);
	$params = $p->AccountParamsGet($id_account);
	if(count($aparams)>0)
	{
		echo "<ul>\n";
		$counter = 1;
		foreach($aparams as $key=>$value)
		{
			echo "<li><a href=\"account_param.php?id=$key&id_account=$id_account\">" . 
			$hh->tr->TranslateTry($value) . "</a>: " . $params[$value] .  "</li>\n";
			$counter ++;
		}
		echo "</ul>\n";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
