<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$p = new Payment();
$title[] = array('search','psearch.php');
$title[] = array('results','');

echo $hh->ShowTitle($title);

$params = array(	'name' => $get['name'],
			'email' => $get['email'],
			'contact' => $get['contact'],
			'id_geo' => $get['id_geo'] );

$num = $p->PayerSearch( $row, $params);

$table_headers = array('name','email','contact_email','active',$hh->geo_label($hh->ini->Get("geo_location")),$trm15->Translate("balance"));
$table_content = array('{LinkTitle("payer.php?id=$row[id_p]","$row[name2]&nbsp;$row[name1]")}','$row[email]',
'{Bool2YN($row[contact])}','{Bool2YN($row[active])}','$row[geo_name]','{MoneyCurrency($row[balance],$row[id_currency])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

