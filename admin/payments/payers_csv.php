<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
include_once(SERVER_ROOT."/../classes/payment.php");
include_once(SERVER_ROOT."/../classes/geo.php");
$ini = new Ini();
$geo = new Geo();

$p = new Payment();

$is_in = $_GET['is_in'];

$rows = array();
$num = $p->PayersAmounts($rows,$is_in,false);

$headers = array("-","NOME","NOME AGGIUNTIVO","INDIRIZZO","INDIRIZZO2","CAP","CITTA","PROVINCIA","PROV","TEL","EMAIL","TOT","ADMIN_LINK");

$data = array();
$link = $ini->get("admin_web") . "/people/person.php?id=";
foreach($rows as $row)
{
	$data_row = array();
	$data_row[] = $row['salutation'];
	$data_row[] = $row['name'];
	$data_row[] = $row['name3'];
	$data_row[] = $row['address'];
	$data_row[] = $row['address_notes'];
	$data_row[] = $row['postcode'];
	$data_row[] = $row['town'];
	$data_row[] = $row['prov'];
	$data_row[] = $row['pr'];
	$data_row[] = $row['phone'];
	$data_row[] = $row['email'];
	$data_row[] = $row['balance'];
	$data_row[] = $link . $row['id_p'];
	$data[] = $data_row;
}
include_once(SERVER_ROOT."/../classes/texthelper.php");
$th = new TextHelper();
echo $th->CSVDump("payers$is_in",$headers,$data);
?>

