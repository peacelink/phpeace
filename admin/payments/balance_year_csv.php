<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
   
$year = (int)$_GET['year'];
$id_balance = isset($_GET['id_balance'])? (int)$_GET['id_balance'] : 1;

header('Content-Type: text/plain'); 
header('Content-Type: application/force-download');
header("Content-Disposition: attachment; filename=\"payments_{$year}.csv\"" );

$sqlstr = "SELECT id_payment,date_format(pay_date,'%d/%m/%Y') AS payment_date,IF(pa.is_in=1,amount,0-amount) AS amount,
b.balance,a.name AS account,pt.payment_type,CONCAT(p.name1,' ',p.name2) AS payer
FROM payments pa 
LEFT JOIN payers py ON pa.id_payer=py.id_payer 
LEFT JOIN people p ON py.id_p=p.id_p 
LEFT JOIN accounts a ON pa.id_account=a.id_account 
LEFT JOIN payment_types pt ON pa.id_payment_type=pt.id_payment_type 
INNER JOIN balances b ON pt.id_balance=b.id_balance  
WHERE pa.verified='1' AND pt.id_balance=$id_balance AND YEAR(pay_date)=$year AND pa.is_transfer=0
ORDER BY pay_date DESC,id_payment DESC ";

$separator = "|";

function NewlineReplace( &$mytext, $replace_quotes=false )
{
   global $separator;
   $mytext = str_replace($separator,"", $mytext);
   $mytext = preg_replace('/\n/',"", $mytext);
   $mytext = preg_replace('/\r/',"", $mytext);
   if($replace_quotes)
      $mytext = str_replace("\"", "\"\"",$mytext);
   return $mytext;
}

$fields_titles = array('id','date','amount','balance','account_from','account_to','payment_type','payer');

$dump_txt = implode($separator, $fields_titles) . "\n";
$rows = array();
$db =& Db::globaldb();
$db->QueryExe($rows, $sqlstr);

foreach($rows as $payment)
{
   $ass = "";
   $ass .= NewlineReplace($payment['id_payment'],true) . $separator;
   $ass .= NewlineReplace($payment['payment_date'],true) . $separator;
   $ass .= NewlineReplace($payment['amount'],true) . $separator;
   $ass .= NewlineReplace($payment['balance'],true) . $separator;
   $ass .= NewlineReplace($payment['account'],true) . $separator;
   $ass .= NewlineReplace($payment['payment_type'],true) . $separator;
   $ass .= NewlineReplace($payment['payer'],true);
   $ass .= "\n";
   $dump_txt .= $ass;
   $counter ++;
}
echo mb_convert_encoding($dump_txt, 'UTF-16LE', 'UTF-8');
?>
