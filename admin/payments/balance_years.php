<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

$trm15 = new Translator($hh->tr->id_language,15);

$title[] = array($trm15->Translate("balance_year"),'');
echo $hh->ShowTitle($title);

$p = new Payment();
$row = $p->BalanceYears();

$balances = $p->BalancesAll();
$table_headers = array('year');
$table_content = array('{LinkTitle("balance_year.php?year=$row[year]",$row[year])}');
foreach($balances as $balance) {
   $table_headers[] = $balance['balance'];
	$table_content[] = '{Money($row[b_' . $balance['id_balance'] . '])}';
}
$table_headers[] = 'total';
$table_headers[] = $trm15->Translate("total");
$table_content[] = '{Money($row[total])}';
$table_content[] = '{Money($row[bigtotal])}';

echo $hh->ShowTableNoPagination($row, $table_headers, $table_content);

include_once(SERVER_ROOT."/include/footer.php");
?>

