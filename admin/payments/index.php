<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/payment.php");

echo $hh->ShowTitle($title);

$trm15 = new Translator($hh->tr->id_language,15);
$p = new Payment();
?>
<div class="box2c">
<h2><?=$trm15->Translate("payments");?></h2>
<h3><?=$trm15->Translate("incoming");?></h3>
<ul>
<?php 
echo "<li><a href=\"payments.php?is_in=1&verified=1\">" . $hh->tr->Translate("list") . "</a></li>\n";
$rows = array();
$num_unverified = $p->Payments( $rows, 1, 0, 1 );
echo "<li><a href=\"payments.php?is_in=1&verified=0\">" . $trm15->Translate("unverified") . " ({$num_unverified})</a></li>\n";
echo "<li><a href=\"payment.php?id=0&is_in=1\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "<li><a href=\"search.php?is_in=1\">" . $hh->tr->Translate("search") . "</a></li>\n";
?>
</ul>

<h3><?=$trm15->Translate("outgoing");?></h3>
<ul>
<?php 
$rows = array();
$num_unverified = $p->Payments( $rows, 0, 0, 1 );
echo "<li><a href=\"payments.php?is_in=0&verified=1\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"payments.php?is_in=0&verified=0\">" . $trm15->Translate("unverified") . " ({$num_unverified})</a></li>\n";
echo "<li><a href=\"payment.php?id=0&is_in=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "<li><a href=\"search.php?is_in=0\">" . $hh->tr->Translate("search") . "</a></li>\n";
?>
</ul>

<h3><?=$trm15->Translate("contacts");?></h3>
<ul>
<?php 
echo "<li><a href=\"payers.php?is_in=0\">" . $trm15->Translate("payees") . "</a> (<a href=\"payers_csv.php?is_in=0\">CSV</a>)</li>\n";
echo "<li><a href=\"payers.php?is_in=1\">" . $trm15->Translate("payers") . "</a> (<a href=\"payers_csv.php?is_in=1\">CSV</a>)</li>\n";
echo "<li><a href=\"payer.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "<li><a href=\"psearch.php\">" . $hh->tr->Translate("search") . "</a></li>\n";
?>
</ul>
<h3><a href="accounts.php"><?=$hh->tr->Translate("accounts");?></a></h3>
<ul>
<?php 
echo "<li><a href=\"payment.php?is_in=1&is_transfer=1\">" . $trm15->Translate("transfer") . "</a></li>\n";
?>
</ul>
<?php
if ($module_admin)
{
	echo "<h3><a href=\"payment_types.php\">" . $trm15->Translate("payment_types") . "</a></h3>\n";
	echo "<ul><li><a href=\"payment_type_merge.php\">" . $trm15->Translate("merge") . "</a></li></ul>\n";
   ?>
   </div>
   <div class="box2c">
   <h2><?=$trm15->Translate("balances");?></h2>
   <?php
	echo "<h3><a href=\"balance_years.php\">" . $trm15->Translate("balance_year") . "</a></h3>\n";
	$totals1 = $p->TotalsAccounts();
	$totals2 = $p->TotalsBalances();
	echo "<p>" . $hh->tr->Translate('total') . ": <strong>" . number_format(array_sum(array_column($totals2, 'total')),2) . "</strong></p>";
	$table_headers1 = array('account','total');
	$table_content1 = array('$row[name]','{FormatNumber($row[total],2)}');
	echo $hh->ShowTableNoPagination($totals1, $table_headers1, $table_content1);
	$table_headers2 = array($trm15->Translate("balance"),'total');
	$table_content2 = array('$row[balance]','{FormatNumber($row[total],2)}');
	echo $hh->ShowTableNoPagination($totals2, $table_headers2, $table_content2);
   echo "<h3><a href=\"balances.php\">" . $hh->tr->Translate("configuration") . ' ' . $trm15->Translate("balances") . "</a></h3>\n";
}
?>
</div>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
