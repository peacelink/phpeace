<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../others/vendor/autoload.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
use Shuchkin\SimpleXLSXGen;

$ah = new AdminHelper;
$ah->CheckAuth(false);
   
include_once(SERVER_ROOT."/../classes/payment.php");
include_once(SERVER_ROOT."/../classes/htmlhelper.php");
$hh = new HtmlHelper();
$hhf = new HHFunctions();

$trm15 = new Translator($hh->tr->id_language,15);
$year = (int)$_GET['year'];
$id_balance = isset($_GET['id_balance'])? (int)$_GET['id_balance'] : 1;

$p = new Payment();
$balance = $p->BalanceGet($id_balance);

function table_title($array) {
   return array_map(function($e){return "<style font-size=\"14\">$e</style>";},$array);
}
function table_header($array) {
   return array_map(function($e){return "<style bgcolor=\"#666666\" color=\"#FFFFFF\">$e</style>";},$array);
}
function table_summary($array) {
   return array_map(function($e){return strlen($e)>0? "<style bgcolor=\"#DDDDDD\"><b>$e</b></style>" : $e;},$array);
}

$content = array();
if($id_balance==1) {
   $content[] = table_title(["Bilancio Conti $year"]);
   $content[] = table_header(['Conto',"Saldo iniziale","Saldo finale ",'Differenza']);
   $payments = $p->TotalsAccountsYearSummary($year);
   foreach($payments as $row) {
      $content[] = array($row['name'],$hhf->Money($row['prev'],2),$hhf->Money($row['current'],2),$hhf->Money($row['year'],2));
   }

   $out = $p->TotalsAccountsYearTransfers($year);
   if(count($out)>0) {
      $content[] = array();
      $content[] = table_title([$trm15->Translate("transfers")]);
      $content[] = table_header(['Da','A',"Totale"]);
      foreach($out as $row) {
         $content[] = array($row['account_from'],$row['account_to'],$hhf->Money($row['total'],2));
      }
   }
}

$content[] = array();

$out = $p->TotalsAccountsYearSummaryInOut($year,0,$id_balance);
if(count($out)>0) {
   $content[] = table_title(array("Uscite {$balance['balance']} $year"));
   $content[] = table_header(['Conto',$trm15->Translate("payment_type"),$trm15->Translate("transactions"),"Totale"]);
   $total = 0;
   foreach($out as $row) {
      $content[] = array($row['account_name'],$row['payment_type'],$hhf->FormatNumber($row['count'],0),$hhf->Money($row['total'],2));
      $total += $row['total'];
   }
   $content[] = table_summary(['','','Totale: ',$hhf->Money($total,2)]);
}

$content[] = array();

$in = $p->TotalsAccountsYearSummaryInOut($year,1,$id_balance);
if(count($in)>0) {
   $content[] = table_title(array("Entrate {$balance['balance']} $year"));
   $content[] = table_header(['Conto',$trm15->Translate("payment_type"),$trm15->Translate("transactions"),"Totale"]);
   $total = 0;
   foreach($in as $row) {
      $content[] = array($row['account_name'],$row['payment_type'],$hhf->FormatNumber($row['count'],0),$hhf->Money($row['total'],2));
      $total += $row['total'];
   }
   $content[] = table_summary(['','','Totale: ',$hhf->Money($total,2)]);
}

$content[] = array();
$saldo = array_sum(array_column($in, 'total')) - array_sum(array_column($out, 'total'));
$content[] = table_summary([' ',' ','Saldo: ',$hhf->Money($saldo,2)]);

$out = array();
$p->Search(0, $out, array('id_balance'=>$id_balance,'year'=>$year,'sort_by'=>2), false);
$content_out = array();
$content_out[] = table_header(['Data','Conto','Centro di costo','Nome','Descrizione','Cifra']);
foreach($out as $row) {
   $content_out[] = [$hhf->FormatDate($row['pay_date_ts']),$row['account'],$row['payment_type'],$row['name'],$row['description'],$hhf->Money($row['amount'],2)];
}

$in = array();
$p->Search(1, $in, array('id_balance'=>$id_balance,'year'=>$year,'sort_by'=>2), false);
$content_in = array();
$content_in[] = table_header(['Data','Conto','Centro di costo','Nome','Cifra']);
foreach($in as $row) {
   $content_in[] = [$hhf->FormatDate($row['pay_date_ts']),$row['account'],$row['payment_type'],$row['name'],$hhf->Money($row['amount'],2)];
}

$xlsx = new SimpleXLSXGen();
$xlsx->setTitle("Bilancio {$balance['balance']} {$year}");
$xlsx->addSheet($content,"Bilancio $year");
$xlsx->mergeCells('A7:D7');
$xlsx->mergeCells('A12:D12');
$xlsx->addSheet($content_out,"Uscite $year");
$xlsx->addSheet($content_in,"Entrate $year");
$xlsx->downloadAs("Bilancio_{$balance['balance']}_{$year}.xlsx");
?>

