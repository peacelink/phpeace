<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users;

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting = $_GET['id'];

if ($module_admin || ($id_meeting>0 && $me->AmIAdmin($id_meeting,$ah->current_user_id)))
	$input_right = 1;

if($id_meeting>0)
{
	$row = $me->MeetingGet($id_meeting);
	$id_user = $row['id_user'];
	$status = $row['status'];
}
else 
{
	$id_user = $ah->current_user_id;
	$status = 1;
}

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($id_meeting>0?$row['title']:"add_new",'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'');
$tabs[] = array($trm23->Translate("slots"),$id_meeting>0?'meeting_slots.php?id='.$id_meeting:'');
$tabs[] = array($trm23->Translate("participants"),$id_meeting>0?'meeting_participants.php?id='.$id_meeting:'');
echo $hh->Tabs($tabs);

if($id_meeting>0)
{
	$url = $ini->Get("pub_web") . "/meet/meeting.php?id=$id_meeting&id_topic={$row['id_topic']}";
	echo "<p>" . $hh->tr->Translate("link") .": <a href=\"$url\" target=\"_blank\">$url</a></p>";
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","meeting");
echo $hh->input_hidden("id_meeting",$id_meeting);
echo $hh->input_table_open();

echo $hh->input_text("title","title",$row['title'],70,0,$input_right);
echo $hh->input_wysiwyg("description","description",$row['description'],1,20,$input_right,true);
echo $hh->input_array("status","status",$status,$hh->tr->Translate("status_options"),$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($row['id_topic'],0,$tt->User($ah->current_user_id),"none_option",$input_right);

echo $hh->input_checkbox("private","is_private",$row['is_private'],0,$input_right);
$users = $uu->ModuleUsers($ah->session->Get("module_id"));
echo $hh->input_user("administrator",$users,$id_user,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_meeting>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
