<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting = $get['id_meeting'];

$row = $me->MeetingGet($id_meeting);

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($row['title'],'meeting.php?id='.$id_meeting);
$title[] = array('search','');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'meeting.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("slots"),'meeting_slots.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("participants"),'meeting_participants.php?id='.$id_meeting);
echo $hh->Tabs($tabs);

$me = new Meetings();
$params = array(	'name' => $get['name'],
		'email' => $get['email'],
		'status' => (int)$get['status']
		);

$row = array();
$num = $me->MeetingParticipantsSearch( $row, $id_meeting, $params);

$table_headers = array('date','name','comments');
$table_content = array('{FormatDate($row[join_date_ts])}',
'{LinkTitle("meeting_participant.php?id_p=$row[id_p]&id_meeting='.$id_meeting.'",$row[name])}','<em>$row[comments]<em>');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

