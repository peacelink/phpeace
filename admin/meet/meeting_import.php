<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users;

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting = $_GET['id'];

$row = $me->MeetingGet($id_meeting);
$id_user = $row['id_user'];
$status = $row['status'];

if ($module_admin || $me->AmIAdmin($id_meeting,$ah->current_user_id))
	$input_right = 1;

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($row['title'],'meeting.php?id='.$id_meeting);

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'meeting.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("slots"),'meeting_slots.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("participants"),'meeting_participants.php?id='.$id_meeting);
echo $hh->Tabs($tabs);

echo $hh->input_form_open();
echo $hh->input_hidden("from","meeting_import");
echo $hh->input_hidden("id_meeting",$id_meeting);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
$groups = array();
$tt->PeopleGroups( $groups, false );
echo $hh->input_row("group","id_pt_group",0,$groups,"none_option",0,$input_right);

echo $hh->input_textarea("data<br>(name|surname|email)","people","",60,20,"",$input_right);

$actions = array();
$actions[] = array('action'=>"import",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
