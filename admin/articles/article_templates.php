<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id_topic = $_GET['id_topic'];
$w = $_GET['w'];

if($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$templates = $t->Templates(5);
}
else
{
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User();
	$templates = $u->Templates(5);
}

if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id=' . $id_topic);
	$title[] = array('articles_list','/topics/articles.php?id=' . $id_topic);
}
else
	$title[] = array('list','articles.php');

$title[] = array('templates','');

echo $hh->ShowTitle($title);

echo "<h3>" . $hh->tr->Translate("templates_available") . "</h3>\n";
echo "<ul>";
echo "<li><a href=\"/articles/article.php?w=$w&id=0&id_topic=$id_topic&id_template=0\">" . ucwords($hh->tr->Translate("article")) . "</a></li>\n";
foreach($templates as $template)
{
		echo "<li><a href=\"/articles/article.php?w=$w&id=0&id_topic=$id_topic&id_template={$template['id_template']}\">{$template['name']}</a></li>\n";
}
echo "</ul>";

include_once(SERVER_ROOT."/include/footer.php");
?>
