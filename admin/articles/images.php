<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/images.php");

$id_article = $_GET['id_article'];
$id_topic = $_GET['id_topic'];
$w = $_GET['w'];
if (!isset($w))
	$w = "topics";



$a = new Article($id_article);
$a->ArticleLoad();
if (!isset($id_topic))
	$id_topic = $a->id_topic;

$t = new Topic($id_topic);
if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
}
else
	$title[] = array('list','articles.php');
$title[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
$title[] = array('image_choose','');

echo $hh->ShowTitle($title);

$id_gallery = (int)$get['id_gallery'];
$id_topic2 = (int)$get['id_topic2'];
$text = $get['text'];
$author = $get['author'];

echo "<p>". $hh->tr->TranslateParams("choose_image",array($a->headline)) . "</p>";

echo $hh->input_form("get","images.php");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_article",$id_article);
echo $hh->input_hidden("w",$w);
echo $hh->input_table_open();
echo $hh->input_text("text","text",$unescaped_get['text'],"30",0,1);
echo $hh->input_text("author","author",$unescaped_get['author'],"30",0,1);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
$topics = $tt->AllTopics();
echo $hh->input_topics($id_topic2,0,$topics,"all_option",1,false,"id_topic2");

include_once(SERVER_ROOT."/../classes/galleries.php");
$gg = new Galleries();
echo $hh->input_galleries($id_gallery,$gg->AllGalleries(false,true),"all_option",1);

echo $hh->input_submit("search","",1);
echo $hh->input_table_close() . $hh->input_form_close();

$i = new Images();
$num = $i->Search( $row, $id_topic, $id_topic2, $id_gallery, $text, $author );


$table_headers = array('image','caption','author','');
$table_content = array('{Graphic("/images/upload.php?src=images/0/$row[id_image].'.$i->convert_format.'&format=$row[format]",'.$i->img_sizes[0].',floor('.$i->img_sizes[0].'*$row[ratio]),false,$row[id_image])}',
'$row[caption]','$row[author]','{LinkTitle("/articles/image.php?id=$row[id_image]&id_article='.$id_article.'&w='.$w.'","'.$hh->tr->Translate("choose").'")}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

