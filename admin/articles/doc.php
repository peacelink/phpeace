<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$fm = new FileManager;
$o = new Ontology;

$id = (int)$_GET['id'];
$id_article = (int)$_GET['id_article'];
$id_topic = (int)$_GET['id_topic'];
$w = isset($_GET['w'])? $_GET['w'] : "topics";

if ($id_article>0)
{
	$a = new Article($id_article);
	$article = $a->ArticleLoad();
	$title1[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
	$id_topic = $a->id_topic;
	if ($a->id_user==$ah->current_user_id)
		$input_right = 1;
	$tot_docs = count($a->DocGetAll());
}
else
	$title1[] = array('docs_archive','/topics/docs.php?id='.$id_topic);


if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	if ($t->AmIAdmin() || $ah->ModuleAdmin(4))
	{
		$input_right = 1;
		$topic_right = 1;
		$input_super_right = 1;
	}
	else 
		$topic_right = 0;
	if ($w=="topics")
	{
		$ah->ModuleForce(4);
		$module_admin = $ri->ModuleAdmin();
		$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
		if($id_article>0)
			$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
	}
	else
		$title[] = array('list','articles.php');
	$title = array_merge($title,$title1);
	$subtopic_forms = $t->SubtopicsByType($t->subtopic_types['contact']);
}
else
{
	$ah->ModuleForce(14);
	$module_admin = $ri->ModuleAdmin();
	$title[] = array('doc_orphans','/admin/doc_orphans.php');
}

if ($module_admin)
{
	$input_right = 1;
	$input_super_right = 1;
}

$maxfilesize = $fm->MaxFileSize();

$keywords = array();
if ($id>0)
{
	$action2 = "update";
	include_once(SERVER_ROOT."/../classes/doc.php");
	$d = new Doc($id);
	$doc = $d->DocGet();
	$id_licence = $doc['id_licence'];
	$id_language = $doc['id_language'];
	$filename = "docs/{$id}.{$doc['format']}";
	if ($ah->current_user_id==($d->CreatorId()))
		$input_right = 1;
	if($d->AdminRight())
		$input_super_right = 1;
	if ($id_article>0)
	{
		$doc_article = $a->DocInfo($id);
		if (!$a->HasDoc($id))
			$action2 = "add";
	}
	$title[] = array($doc['title'],'');
}
else
{
	$action2 = "insert";
	$id_licence = $hh->ini->Get("id_licence");	
	$title[] = array('document','');
	if ($id_article>0)
	{
		$id_language = $article['id_language'];
	}
	elseif($id_topic>0)
	{
		$id_language = $t->id_language;
	}
}

echo $hh->ShowTitle($title);

?>

<script type="text/javascript">
$(document).ready(function() {
$("#form1").validate({
		rules: {
			title: "required"
		}
	});
});
</script>

<?php
$languages = $hh->tr->Translate("languages");
asort($languages);

echo $hh->input_form("post","actions.php",true);

echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_article",$id_article);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","doc");
echo $hh->input_hidden("w",$w);
echo $hh->input_hidden("action2",$action2);
echo $hh->input_hidden("id_doc",$id);
echo $hh->input_table_open();

if ($action2=="insert")
{
	echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
	echo $hh->input_upload("choose_file","doc",50,$input_right);
	echo $hh->input_text("title","title",$doc['title'],"50",0,$input_right);
	echo $hh->input_textarea("description","description",$doc['description'],70,5,"",$input_right);
	echo $hh->input_text("author","author",$doc['author'],80,0,$input_right);
	echo $hh->input_textarea("source","source",$doc['source'],70,3,"",$input_right);
	echo $hh->input_array("language","id_language",$id_language,$languages,$input_right);
	echo $hh->input_keywords($id,$o->types['document'],$keywords,$input_right);
	if ($hh->ini->Get("licences"))
		echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_right);
	if($id_topic>0)
	{
		if(count($subtopic_forms)>0)
		{
			echo $hh->input_row("form_download","id_subtopic_form",0,$subtopic_forms,"-",0,$input_right);
		}
		$tikeywords = $t->KeywordsInternal($o->types['document']);
		echo $hh->input_internal_keywords($id,$tikeywords,"document",$topic_right,$input_right);
	}
}
else
{
	if($id_topic>0)
	{
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		echo $hh->input_note("Link: " . $irl->PublicUrlTopic("article_doc",array('id'=>$id,'format'=>$doc['format']),$t));
	}
	$tokens = array();
	$num_tokens = $d->Tokens($tokens);
	echo $hh->input_note("Tokens: " . $hh->Wrap($num_tokens,"<a href=\"doc_tokens.php?id=$id&id_article=$id_article&id_topic=$id_topic&w=$w\">","</a>",$num_tokens>0) . " - " . $hh->Wrap($hh->tr->Translate("add_new"),"<a href=\"doc_token.php?id=$id&id_article=$id_article&id_topic=$id_topic&w=$w\">","</a>"));

	$docs_covers_size = $conf->Get("docs_covers_size");
	if($docs_covers_size > -1)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$width = $i->img_sizes[$docs_covers_size];
		$height = 0;
		echo "<tr><td>&nbsp;</td><td class=\"doc_cover\">";
		$cover_filename = "docs/covers/{$id}.{$i->convert_format}";
		echo $hh->Graphic("/images/upload.php?src=$cover_filename",$width,$height,false);
		echo "</td></tr>\n";
	}
	
	echo "<tr><td align=\"right\">&nbsp;</td><td><a href=\"/docs/upload.php?src=$filename\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a>" . $hh->FileSize($filename) . "</td></tr>\n";
	echo $hh->input_upload("substitute_with","doc",50,$input_super_right);
	if ($id>0)
	{
		echo "<tr><td>&nbsp;</td><td>";
		$articles = $d->Articles();
		if (count($articles) > 0)
		{
			echo "<ul>" . $hh->tr->Translate("doc_used");
			foreach($articles as $art)
				echo "<li>{$art['topic_name']}: <a href=\"doc.php?id={$id}&id_article={$art['id_article']}&w=$w\">{$art['headline']}</a></li>\n";
			echo  "</ul>\n";
		}
		else
			echo "<p>" . $hh->tr->Translate("doc_used_not") . "</p>\n";
		echo "</td></tr>\n";
	}
	
	if($input_right)
	{
		echo $hh->input_text("title","title",$doc['title'],"50",0,$input_super_right);
		echo $hh->input_textarea("description","description",$doc['description'],70,5,"",$input_super_right);
		echo $hh->input_text("author","author",$doc['author'],80,0,$input_super_right);
		echo $hh->input_textarea("source","source",$doc['source'],70,3,"",$input_super_right);
		echo $hh->input_array("language","id_language",$id_language,$languages,$input_super_right);
		echo $hh->input_keywords($id,$o->types['document'],$keywords,$input_super_right);
		if ($hh->ini->Get("licences"))
			echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_super_right);		
		if($id_topic>0)
		{
			if(count($subtopic_forms)>0 && $id_article>0)
			{
				echo $hh->input_row("form_download","id_subtopic_form",$doc_article['id_subtopic_form'],$subtopic_forms,"none_option",0,$input_right);
			}
			$tikeywords = $t->KeywordsInternal($o->types['document']);
			echo $hh->input_internal_keywords($id,$tikeywords,"document",$topic_right,$input_super_right);
		}
	}
}

if($id_article>0 && $tot_docs>0)
{
	$combo_values = array();
	for($i=1;$i <= ($tot_docs+1);$i++)
		$combo_values["".$i.""] = $i;
	$seq = ($tot_docs+1);
	if($id>0)
	{
		if($doc_article['seq']>0)
			$seq = $doc_article ['seq'];
	}
	echo $hh->input_array("position","seq",$seq,$combo_values,$input_right);
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if ($id>0)
{
	$actions[] = array('action'=>"delete",'label'=>"doc_delete",'right'=>$input_right && $action2 != "add" && $id_article>0);
	$actions[] = array('action'=>"remove",'label'=>"doc_remove",'right'=>$input_right && count($articles)<1 && $input_super_right);
}
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

if ($id==0)
	echo $hh->tr->Translate("doc_instr");

include_once(SERVER_ROOT."/include/footer.php");
?>
