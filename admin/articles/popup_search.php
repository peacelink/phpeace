<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/htmlhelper.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
$hh = new HtmlHelper();

$page_title = $hh->tr->Translate("article_related");
include_once(SERVER_ROOT."/../admin/include/head.php");
?>
<body>
<script type="text/javascript">
function set_article(id_article,headline)
{
	opener.document.forms['form1'].id_article.value = id_article;
	opener.document.forms['form1'].descriz_id_article.value = headline;
	self.close();
}
</script>
<?php
$w = $_GET['w'];
$id_article = $_GET['id_article'];
$id_topic = $_GET['id_topic'];

if ($id_article>0)
{
	include_once(SERVER_ROOT."/../classes/article.php");
	$a = new Article($id_article);
	$row = $a->ArticleGet();
	echo $hh->tr->TranslateParams("article_link_current",array($row['headline']));
}
?>
<form method="get" action="/articles/popup_search2.php" name="form1">
<table border=0 cellpadding=0 cellspacing=7>
<?php

echo $hh->input_text("title","title","",30,0,1);
echo $hh->input_text("author","author","",30,0,1);
echo $hh->input_text("text","content","",30,0,1);

include_once(SERVER_ROOT."/../classes/articles.php");
$period = Articles::Period();
$min = $period['min_art_ts']<0? 1 : $period['min_art_ts'];
echo $hh->input_date("from","written1",$min,1);
echo $hh->input_date("to","written2",$period['max_art_ts'],1);

include_once(SERVER_ROOT."/../classes/topics.php");
$t = new Topics;
$topics = $t->AllTopics(true);
echo $hh->input_topics($id_topic,0,$topics,"all_option",1);
?>
<tr><td>&nbsp;</td><td><input type="submit" class="input-submit" value="<?=$hh->tr->Translate("search");?>">
&nbsp;&nbsp;<input type="reset" value="<?=$hh->tr->Translate("cancel");?>"></td></tr>
</table>
<input type="hidden" name="id_article" value="<?=$id_article;?>">
<input type="hidden" name="w" value="<?=$w;?>">
</form>
<?=$hh->tr->Translate("search_notes");?>
</body>
</html>

