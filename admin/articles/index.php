<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);
?>

<p><a href="articles.php"><?=$hh->tr->Translate("articles_yours");?></p>

<p><a href="article.php?from=articles&id=0"><?=$hh->tr->Translate("article_add");?></p>

<?php
if ($module_admin)
	echo "<p><a href=\"articles.php?u=all\">" . $hh->tr->Translate("articles_list_all") . "</p>\n";

?>
<p><a href="article_search.php"><?=$hh->tr->Translate("search");?></a></p>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
