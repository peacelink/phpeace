<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");

$trm27 = new Translator($hh->tr->id_language,27);
$ec = new eCommerce();

$id_p = (int)$_GET['id_p'];
$id_order = (int)$_GET['id_order'];

$title[] = array($trm27->Translate("orders"),'orders.php');
$order = $ec->OrderGet($id_order);
$title[] = array($trm27->Translate("order") . ' #' . $id_order,'order.php?id='.$id_order);

$title[] = array("pub_user",'');
echo $hh->ShowTitle($title);

$input_right = $module_admin;

if($id_p>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$row = $pe->UserGetDetailsById($id_p);
	echo $hh->input_form_open();
	echo $hh->input_table_open();
	echo $hh->input_text("name","name1",$row['name1'],30,0,0);
	echo $hh->input_text("name2","name2",$row['name2'],30,0,0);
	echo $hh->input_text("email","email","<a href=\"mailto:{$row['email']}\">{$row['email']}</a>",50,0,0);
    echo $hh->input_text("phone","phone",$row['phone'],20,0,0);
	echo $hh->input_textarea("address","address",$row['address'],80,4,"",0);
	echo $hh->input_text("postcode","postcode",$row['postcode'],15,0,0);
	echo $hh->input_text("town","town",$row['town'],50,0,0);
	echo $hh->input_geo($row['id_geo'],0);
    echo $hh->input_textarea("address_notes","address_notes",$row['address_notes'],80,4,"",0);
	echo $hh->input_table_close() . $hh->input_form_close();
	if ($ah->ModuleAdmin(28))
		echo "<a href=\"/people/person.php?id=$id_p\">" . $hh->tr->Translate("management") . "</a>";
}
elseif(isset($_GET['action_search']))
{
	$params = array(	'name' => $get['name'], 'email' => $get['email'] );
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$num = $pe->Search( $row, $params);
	$table_headers = array('name','email','verified','contact_email',$hh->geo_label($hh->ini->Get("geo_location")),'insert_date');
	$table_content = array('{LinkTitle("actions.php?from2=order_user&id_order='.$id_order.'&id_p=$row[id_p]","$row[name1] $row[name2]")}','$row[email]','{Bool2YN($row[verified])}',
	'{Bool2YN($row[contact])}','$row[town] ($row[geo_name])','{FormatDate($row[start_date_ts])}');
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}
else 
{
	echo $hh->input_form("get","order_user.php","");
	echo $hh->input_hidden("id_order",$id_order);
	echo $hh->input_table_open();
	echo $hh->input_note("pub_user_search");
	echo $hh->input_text("name","name","","30",0,$input_right);
	echo $hh->input_text("email","email","","30",0,$input_right);
	$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>


