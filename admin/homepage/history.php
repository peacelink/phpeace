<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_type = $_GET['id_type'];
$id_widget = (int)$get['id'];

$row = $wi->WidgetGet($id_widget);
$widget_title = $row['title'];

$title[] = array("widgets",'widgets.php');
if($id_widget>0)
	$title[] = array($row['title'],'widget.php?id='.$id_widget);
$title[] = array('history','');

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/history.php");
$h = new History;

if($id_widget>0)
{
	$tabs = array();
	$tabs[] = array("widget",'widget.php?id='.$id_widget);
	$tabs[] = array("widget content",$id_widget>0?'widget_content.php?id='.$id_widget:'');  
    $tabs[] = array("widget category",$id_widget>0?'widget_category.php?id='.$id_widget:'');
	$tabs[] = array('history','');
	echo $hh->Tabs($tabs);
	
	$num = $h->HistoryAll( $row, $id_type, $id_widget );
    foreach($row as &$widget)
    {
        $widget['title'] = $widget_title;
    }
}
else 
{
	$num = $h->HistoryAllByResourceType($row,$id_type);
	foreach($row as &$widget)
	{
		$wid = $wi->WidgetGet($widget['id']);
		$widget['title'] = $wid['title'];
	}
}

$table_headers = array('date','ip','widget','user','action');
$table_content = array('{FormatDateTime($row[ts_time])}','$row[ip]','{LinkTitle("widget.php?id=$row[id]",$row[title])}','$row[name]','{HistoryAction($row[action])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
