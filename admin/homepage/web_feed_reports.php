<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");

$wf = new WebFeeds();

$id_report = (int)$get['id_report'];
$id_day = (int)$get['id_day'];
$num_items = (int) $get['num_items'];

if ($module_right)
	$input_right = 1;

switch ($id_report)
{
    case 1:
        $report_title = 'Distribution of feed items Report';
        break;
    case 2:
        $report_title = 'Average Items Per Feed Report';
        break;
    case 3:
        $report_title = 'Feed Performance below threshold';
        break;    
}    

$title[] = array("Web feeds reporting",'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($report_title);
echo $hh->Tabs($tabs);


$days = array(1=>"Today", 2=>"Yesterday", 3=>"7 days ago", 4=>"14 days ago", 5=>"30 days ago");

echo $hh->input_form_open();
echo $hh->input_hidden("from","web_feed_reports");
echo $hh->input_hidden("id_report",$id_report);
echo $hh->input_table_open();


echo $hh->input_array("day","id_day",$id_day,$days,$input_right);    
if ($id_report == 3)
    echo $hh->input_text("number new items (threshold)","num_items",$num_items,20,0,$input_right);


$actions = array();
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if ($id_day > 0)
{
    $tomorrow = mktime(0,0,0,date("m"),date("d")+1,date("Y"));

    switch ($id_day)
    {
        // today
        case 1:
            $from_date = date('Y-m-d');
    		$to_date = date('Y-m-d', $tomorrow);
            break;
        
        // yesterday
        case 2:
            $yesterday = mktime(0,0,0,date("m"),date("d")-1,date("Y"));
            $from_date = date('Y-m-d', $yesterday);
    		$to_date = date('Y-m-d');
            break;
        
        // last 7 days
        case 3:
            $seven_days_ago = mktime(0,0,0,date("m"),date("d")-6,date("Y"));
            $from_date = date('Y-m-d', $seven_days_ago);
    		$to_date = date('Y-m-d', $tomorrow);
            break;
        
        // last 14 days
        case 4:
            $forteen_days_ago = mktime(0,0,0,date("m"),date("d")-13,date("Y"));
            $from_date = date('Y-m-d', $forteen_days_ago);
    		$to_date = date('Y-m-d', $tomorrow);
            break;
        
        // last 30 days
        case 5:
            $thirty_days_ago = mktime(0,0,0,date("m"),date("d")-29,date("Y"));
            $from_date = date('Y-m-d', $thirty_days_ago);
    		$to_date = date('Y-m-d', $tomorrow);
            break; 
    }
    
    $rows = array();
    switch ($id_report)
    {
        // distribution of feed items 
        case 1:
            $num = $wf->DistributionOfFeedItemsGet($rows, $from_date, $to_date);
            $table_headers = array('web feed','new items','updated items', 'date');
            $table_content = array('$row[title]', '$row[inserted]', '$row[updated]', '$row[pull_date]');

            echo $hh->ShowTable($rows, $table_headers, $table_content, $num);
            break;
        
        // average feed items
        case 2:
            $num = $wf->AverageItemsPerFeedGet($rows, $from_date, $to_date);
            $table_headers = array('web feed','average new items','average updated items', 'date');
            $table_content = array('$row[title]', '$row[average_inserted]', '$row[average_updated]', '$row[pull_date]');

            echo $hh->ShowTable($rows, $table_headers, $table_content, $num);
            break;
        
        // feed performance below threshold
        case 3:
            $num = $wf->WebFeedPerformanceBelowThresholdGet($rows, $from_date, $to_date, $num_items);
            $table_headers = array('web feed','new items', 'date', 'status');
            $table_content = array('$row[title]', '$row[inserted]', '$row[pull_date]', '{WebFeedLogStatusLookup($row[status])}');

            echo $hh->ShowTable($rows, $table_headers, $table_content, $num);
            break;    
    }    
}    

include_once(SERVER_ROOT."/include/footer.php");
?>
