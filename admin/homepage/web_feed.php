<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");
include_once(SERVER_ROOT."/../classes/widgets.php");       
$wf = new WebFeeds();

$id_web_feed = (int)$get['id'];
$failed = (int)$get['failed'];
$verify = (int)$get['verify'];
$error = $get['error'];

if ($module_admin)
	$input_right = 1;

if($id_web_feed>0)
{
	$row = $wf->WebFeedGet($id_web_feed);
}
else 
{
	$id_user = $ah->current_user_id;
	$status = 1;
}

$title[] = array("web feeds",'web_feeds.php');
$title[] = array($id_web_feed>0?$row['title']:"add_new",'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("web_feed");
$tabs[] = array("web feed items",$id_web_feed>0?'web_feed_items.php?id='.$id_web_feed:'');  
echo $hh->Tabs($tabs);


echo $hh->input_form_open();
echo $hh->input_hidden("from","web_feeds");
echo $hh->input_hidden("id_web_feed",$id_web_feed);
echo $hh->input_table_open();

switch ($failed)
{
    case 1:
        echo $hh->input_note("<p>Feed url is invalid</p>", TRUE);
        break;
    case 2:
        echo $hh->input_note("<p>Web feed cannot be deleted because it is used by widget </p>", TRUE);  
        break;
}

switch ($verify)
{
    case 1:
        echo $hh->input_note("<p>Feed url is valid</p>");
        break;
    case 2:
        echo $hh->input_note("<p>Feed url is invalid</p>", TRUE);
        break;
}

$title = "";
$feed_url = "";
$interval = $wf->minimum_feed_update_interval;

if ($id_web_feed > 0)
{
    $feed_url = $row['feed_url'];
    $title = $row['title'];
    $interval = $row['update_interval'];
}
  
$postback = $ah->session->Get('postvars');
if (is_array($postback))
{
    $feed_url = $postback['feed_url'] ? $postback['feed_url'] : $feed_url;
    $title = $postback['title'] ? stripslashes($postback['title']) : $title;  
    $interval = $postback['update_interval'] ? $postback['update_interval'] : $interval;  
    $ah->session->Delete('postvars');
}
if (!isset($feed_url))
	$feed_url = "http://"; 
	
echo $hh->input_text("Title","title",$title,50,0,$input_right);
echo $hh->input_text("Feed url","feed_url",$feed_url,50,0,$input_right);
echo $hh->input_text("Update interval (minutes)","update_interval",$interval,10,0,$input_right,"(no less than " . $conf->Get("minimum_feed_update_interval") . ")");

if ($id_web_feed > 0)
{
	echo $hh->input_date("Last updated","last_updated_ts",$row['last_updated_ts'],0);
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_web_feed>0);
$actions[] = array('action'=>"verifyfeedurl",'label'=>"Verify feed url",'right'=>$input_right);
$actions[] = array('action'=>"import",'label'=>"Import feed items",'right'=>$input_right && $id_web_feed>0);

echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

// get the list of widgets using this web feed
if ($id_web_feed > 0)
{
    $wi = new Widgets();   
    $count = $wi->WidgetsByWebFeed($id_web_feed, $rows,false);

    echo $hh->input_note("<br/>");
    echo $hh->input_note("Widgets list use this web feed");

    $table_headers = array('widget id', 'title');
    $table_content = array('$row[id_widget]','{LinkTitle("widget.php?id=$row[id_widget]",$row[title])}');

    echo $hh->ShowTable($rows, $table_headers, $table_content, $count);
}
$back_btn = "<input type='button' value='Back' onclick='javascript:history.go(-1);'";
echo $back_btn;
include_once(SERVER_ROOT."/include/footer.php");
?>
