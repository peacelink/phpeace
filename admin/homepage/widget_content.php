<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_widget = $_GET['id'];
$id_parent = $_GET['id_parent'];
$failed = (int)$get['failed']; 

$widget = $wi->WidgetGet($id_widget);    
$widget_type = $widget['widget_type'];
$widget_params = $wi->WidgetParamGet($id_widget);
$current_params = $widget_params['params'];
$status = $widget['status'];
// RSS
if ($widget['widget_type'] == 1)
    $id_web_feed = $widget['id_web_feed'];
else
    $id_web_feed = 0;

if($id_widget>0)
{
    include_once(SERVER_ROOT."/../classes/ontology.php");
    $o = new Ontology;
    include_once(SERVER_ROOT."/../classes/history.php");
    $h = new History;
    $id_user = $h->CreatorId($o->types['widget'],$id_widget);
    $id_user = $id_user['id_user'];
}
else 
{
    $id_user = $ah->current_user_id;
}

if (($module_admin || $id_user == $ah->current_user_id) && $status <> 3)
	$input_right = 1;
    
$title[] = array("widgets",'widgets.php');
$title[] = array($widget['title'],'widget.php?id='.$id_widget);

echo $hh->ShowTitle($title);

$tabs = array();
if (isset($id_parent))
{
    $tabs[] = array("widget",'widget.php?id='.$id_parent);
    $tabs[] = array("widget content",'');
    $tabs[] = array("widget category",'widget_category.php?id='.$id_parent);  
    $tabs[] = array('history','history.php?id_type=24&id='.$id_parent);
}
else
{
    $tabs[] = array("widget",'widget.php?id='.$id_widget);
    $tabs[] = array("widget content",'');
    $tabs[] = array("widget category",'widget_category.php?id='.$id_widget);  
    $tabs[] = array('history','history.php?id_type=24&id='.$id_widget);
}
echo $hh->Tabs($tabs);

if($widget_type==0)
{
	include_once(SERVER_ROOT."/../classes/images.php");
	include_once(SERVER_ROOT."/../classes/file.php");
	$fm = new FileManager;
	$maxfilesize = $fm->MaxFileSize();
	echo $hh->input_form("post","actions.php",true);
	echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
}
else 
{
	echo $hh->input_form_open();
}
echo $hh->input_hidden("from","widget_content");
echo $hh->input_hidden("id_widget",$id_widget);
echo $hh->input_hidden("id_parent",$id_parent);
echo $hh->input_hidden("widget_type",$widget_type);
echo $hh->input_hidden("id_web_feed", $id_web_feed);
echo $hh->input_table_open();
if ($failed == 1)
    echo $hh->input_note("<p>Invalid url link</p>", TRUE);

$content = '';
$layout_type = '';
$rss_url = '';

$params = array();
if ($current_params <> '')
{
	include_once(SERVER_ROOT."/../classes/varia.php"); 
	$v = new Varia; 
	$params = $v->Deserialize($current_params);
}
$enter = "
";

switch ($widget_type)
{
    case 0:
		$link_url = $params['link_url'];
		$content = stripcslashes(str_replace('\r\n', $enter, $params['content']));
	break;
    case 1:
		$layout_type = (int)$params['layout_type'];
		$items_display = (int) $params['items_display'];
	break;
    case 2:
		$content = stripcslashes(str_replace('\r\n', $enter, $params['content']));
		$is_html = isset($params['is_html'])? $params['is_html'] : 1;
	break;    
    case 3:
		$layout_type = (int)$params['layout_type'];
	break;
    case 4:
		$id_feature = (int)$params['id_feature'];
		$update_interval = isset($params['update_interval'])? (int)$params['update_interval'] : 10;
	break;
}

if (!isset($id_parent))
    echo $hh->input_text("widget type","widget_type_desc",$wi->types[$widget_type],50,0,0);

if ($widget_type == 0)
{
    echo $hh->input_text("url link","link_url",$link_url,50,0,$input_right);      
    echo $hh->input_textarea("content","content",$content,50,5,"",$input_right);
	$i = new Images();
	$filename = "widgets/0/$id_widget".".".$i->convert_format;
	echo $hh->input_note("Please upload images in JPG format only");
	if($fm->Exists("uploads/$filename"))
	{
		$width = $i->img_sizes[WIDGET_IMAGE_SIZE];
		echo "<tr><td align=\"right\">" . $hh->tr->Translate("image") . "</td><td><img src=\"/images/upload.php?src=$filename\" width=\"{$width}\"></td></tr>\n";
		echo $hh->input_upload("substitute_with","img",50,$input_right);
	}
	else
		echo $hh->input_upload("image","img",50,$input_right);
}
else if ($widget_type == 1)
{
    // rss widget
    include_once(SERVER_ROOT."/../classes/web_feeds.php"); 
    $wf = new WebFeeds();
    $wf->WebFeedsAll($rows,false);
    $web_feeds = array();
    foreach ($rows as $row)
    {
        $web_feeds[$row['id_web_feed']] = $row['title'] . ' (' . $row['feed_url'] . ')';    
    }

    echo $hh->input_array("layout type","layout_type",$layout_type,array("list_with_image", "list_item", "list_images", "list_with_thumbnail"),$input_right);    
    echo $hh->input_array("feed","id_web_feed",$id_web_feed,$web_feeds,$input_right, "", 10);    
    echo $hh->input_text("items displayed","items_display",$items_display>0? $items_display:15,20,0,$input_right);
    

}
else if ($widget_type == 2)
{
    // custom widget    
    echo $hh->input_textarea("content","content",$content,80,20,"",$input_right,"",true,false);
    echo $hh->input_checkbox("no_html","is_html",$is_html,0,$input_right);
}
else if ($widget_type == 3)
{
    // advance manual widget
    echo $hh->input_array("layout type","layout_type",$layout_type,array("list_with_image", "list_item", "list_images", "list_with_thumbnail"),$input_right);    
    $rows = $wi->PublicWidgetChildGet($id_widget);    
    $table_headers = array('url');
    $table_content = array('{LinkTitle("widget_content.php?id=$row[id_widget]&id_parent='.$id_widget.'",$row[link_url])}');
    echo $hh->ShowTable($rows, $table_headers, $table_content, count($rows));
    echo "<p><a href=\"widget_content.php?id=0&id_parent=$id_widget\">" . $hh->tr->Translate("add_new") . "</a></p>\n";
}
else if ($widget_type == 4)
{
    // feature
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	$rows = array();
	$pt->ft->GlobalPageFeaturesAll($rows,false);
	echo $hh->input_row("feature","id_feature",$id_feature,$rows,"choose_option",0,$input_right);
	echo $hh->input_text("Update interval (minutes)","update_interval",$update_interval,10,0,$input_right);	
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if (isset($id_parent) && $id_parent > 0)
    $actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_widget>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>
