<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");
$wi = new Widgets();

$title[] = array('Widgets Categories','widgets_categories.php');

$id_category = $_GET['id'];

$row = $wi->CategoryGet($id_category);
$title[] = array($row['name'],'widgets_category.php?id='.$id_category);
$title[] = array('Select widget','');

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","category_widgets_selection");
echo $hh->input_hidden("id_category",$id_category);

$category_widgets = $wi->CategoryWidgetsSelection($id_category);

$id_widgets = array();
foreach($category_widgets as $category_widget)
{
	$id_widgets[] = $category_widget['id_widget'];
}

$widgets = array();
$wi->CategoryWidgets($widgets,$id_category,false,true);

echo "<p><em>Only approved widgets are shown for selection</em></p>";
echo "<ul id=\"categories\">";
foreach($widgets as $widget)
{
	echo "<li>";
	echo "<input type=\"checkbox\" name=\"widget[]\" value=\"{$widget['id_widget']}\"";
	if(in_array($widget['id_widget'],$id_widgets))
		echo " checked=\"checked\" ";
	echo ">{$widget['title']}";
	echo "</li>";
}
echo "</ul>";

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

