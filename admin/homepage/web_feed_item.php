<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");
include_once(SERVER_ROOT."/../classes/texthelper.php"); 

$wf = new WebFeeds();
$texthelper = new TextHelper();

$id_web_feed = (int)$get['id_web_feed'];
$id_web_feed_item = (int)$get['id'];
$filter = (int)$get['filter'];

$web_feed = $wf->WebFeedGet($id_web_feed); 

if ($module_admin)
	$input_right = 1;

if($id_web_feed_item>0)
{
	$row = $wf->WebFeedItemGet($id_web_feed, $id_web_feed_item);
}

if ($filter == 0)
{
    $title[] = array("web feeds",'web_feeds.php');
    $title[] = array($web_feed['title'],'web_feed_items.php?id='.$id_web_feed);
    $title[] = array($id_web_feed_item>0?$row['title']:'');
}
else
{
    $title[] = array("filtered feeds items",'filtered_web_feed_items.php');
    $title[] = array($id_web_feed_item>0?$row['title']:'');
}

echo $hh->ShowTitle($title);

if ($filter == 0)
{
    $tabs = array();
    $tabs[] = array("web feed", 'web_feed.php?id=' . $id_web_feed);
    $tabs[] = array("web feed items",'web_feed_items.php?id=' . $id_web_feed);
    $tabs[] = array("history",'history_items.php?id=' . $id_web_feed_item);
    echo $hh->Tabs($tabs);
}

$approve_list = array("Filtered","Approved");

echo $hh->input_form_open();
echo $hh->input_hidden("from","web_feed_items");
echo $hh->input_hidden("id_web_feed_item",$id_web_feed_item);
echo $hh->input_hidden("id_web_feed",$id_web_feed);
echo $hh->input_hidden("filter",$filter);
echo $hh->input_table_open();

echo $hh->input_text("Title","title",$row['title'],150,0,0);
echo $hh->input_text("Url","url",$row['link_url'],150,0,0,$row['link_url']!=""? " [<a href=\"{$row['link_url']}\" target=\"_blank\">open</a>]":"");
echo $hh->input_textarea("Description","description",$row['description'],50,10,"",0);
echo $hh->input_textarea("Content","content",$row['content'],50,10,"",0);
// implements bad word retrieval
$content = trim($row['title']) . ' ' . trim($row['description']) . ' ' . trim($row['content']);
$bad_words = $texthelper->BlockedWords($content);
$filtered_content = '';
foreach ($bad_words as $bad_word)
{
    if ($filtered_content == '')
        $filtered_content = $bad_word;
    else
        $filtered_content .= "\n" . $bad_word;
}
if ($filtered_content <> '')
    echo $hh->input_text("Bad words","bad_words",nl2br($filtered_content),150,0,0);
    
echo $hh->input_text("Published date","published_date",$row['published_date'],150,0,0);
    
echo $hh->input_array("", "is_approved", $row['is_approved'], $approve_list, $input_right);
echo $hh->input_checkbox("Active","status",$row['status'],0,$input_right);
echo $hh->input_text("Last updated","last_updated",$row['last_updated'],150,0,0);


$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_web_feed_item>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

// get the list of widgets using this web feed
if ($id_web_feed > 0)
{
    $wi = new Widgets();   
    $count = $wi->WidgetsByWebFeed($id_web_feed, $rows,false);

    echo $hh->input_note("<br/>");
    echo $hh->input_note("Widgets list use this web feed");

    $table_headers = array('widget id', 'title');
    $table_content = array('$row[id_widget]','{LinkTitle("widget.php?id=$row[id_widget]",$row[title])}');

    echo $hh->ShowTable($rows, $table_headers, $table_content, $count);
}

include_once(SERVER_ROOT."/include/footer.php");
?>
