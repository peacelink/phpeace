<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");

$wf = new WebFeeds();

$id_type = 25;
$id_feed_item = (int)$get['id'];

$title[] = array("Web feeds",'web_feeds.php');
if($id_feed_item>0)
{
	$row = $wf->WebFeedItemGetById($id_feed_item);
	$feed = $wf->WebFeedGet($row['id_web_feed']);
	$title[] = array($feed['title'],"web_feed_items.php?id={$row['id_web_feed']}");
	$title[] = array($row['title'],"web_feed_item.php?id=$id_feed_item&id_web_feed={$row['id_web_feed']}");
}
$title[] = array('history','');

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/history.php");
$h = new History;

if($id_feed_item>0)
{
	$tabs = array();
    $tabs[] = array("web feed", 'web_feed.php?id=' . $row['id_web_feed']);
    $tabs[] = array("web feed items",'web_feed_items.php?id=' . $row['id_web_feed']);
    $tabs[] = array("history",'');
	echo $hh->Tabs($tabs);
	
	$num = $h->HistoryAll( $row, $id_type, $id_feed_item );
	$table_headers = array('date','ip','user','action');
	$table_content = array('{FormatDateTime($row[ts_time])}','$row[ip]','$row[name]','{HistoryAction($row[action])}');
}
else 
{
	$num = $h->HistoryAllByResourceType($row,$id_type);
	foreach($row as &$feed_item)
	{
		$wfi = $wf->WebFeedItemGetById($feed_item['id']);
		$feed_item['title'] = $wfi['title'];
		$feed_item['id_web_feed'] = $wfi['id_web_feed'];
	}
	$table_headers = array('date','ip','feed item','user','action');
	$table_content = array('{FormatDateTime($row[ts_time])}','$row[ip]','{LinkTitle("web_feed_item.php?id=$row[id]&id_web_feed=$row[id_web_feed]",$row[title])}','$row[name]','{HistoryAction($row[action])}');
}

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
