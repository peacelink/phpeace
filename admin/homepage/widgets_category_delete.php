<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");
$wi = new Widgets();

$id_category = $_GET['id'];

$title[] = array('Widgets Categories','widgets_categories.php');
$row = $wi->CategoryGet($id_category);
$title[] = array($row['name'],'widgets_category.php?id='.$id_category);
$title[] = array("delete",'');

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo "<h3>Do you really want to delete category <em>\"{$row['name']}\"</e></h3>";

echo $hh->input_form_open();
echo $hh->input_hidden("from","category_delete");
echo $hh->input_hidden("id_category",$id_category);
echo $hh->input_table_open();
$actions = array();
$num_widgets = $wi->CategoryWidgets($widgets,$id_category,true);
$actions[] = array('action'=>"submit",'label'=>"confirm",'right'=>$input_right && $num_widgets==0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>

