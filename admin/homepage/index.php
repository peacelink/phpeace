<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

echo "<div class=\"box\">\n";

echo "<h2>Homepage</h2>\n";

$pub_web = $ini->Get("pub_web");
echo "[<a href=\"$pub_web\" target=\"_blank\">$pub_web</a>]\n";

echo "<p><a href=\"publish.php\">" . $hh->tr->Translate("publish") . "</a></p>\n";

echo "<h3>" . $hh->tr->Translate("preview") . "</h3>\n";
echo "<ul>";
echo "<li><a href=\"../topics/preview.php?id_gtype=0\" target=\"_blank\">Homepage</a></li>\n";
$landings = $conf->Get("landing");
if(count($landings)>0) {
	echo "<li><h4>Landing</h4>";
	echo "<ul>";
	foreach($landings as $landing) {
		echo "<li><a href=\"../topics/preview.php?id_gtype=0&subtype=landing&landing=$landing\" target=\"_blank\">$landing</a></li>\n";
	}
	echo "</ul>";
	echo "<li>";
}
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology();
$klandings = array();
$o->KeywordsLanding($klandings,false);
if(count($klandings)>0) {
	echo "<li><h4>Keywords</h4>";
	echo "<ul>";
	foreach($klandings as $klanding) {
		echo "<li><a href=\"../topics/preview.php?id_gtype=0&subtype=keyword&id_keyword={$klanding['id_keyword']}\" target=\"_blank\">{$klanding['keyword']}</a></li>\n";
	}
	echo "</ul>";
	echo "<li>";
	
}
echo "</ul>";

echo "<p><a href=\"/topics/features_global.php?id_gtype=0\">" . $hh->tr->Translate("features") . "</a></p>\n";

if ($ah->ModuleAdmin(10))
	echo "<p><a href=\"../layout/xml.php?id_gtype=0\" target=\"_xml\">XML</a></p>\n";

if($module_admin)
{
	echo "<p><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></p>";
	$hometype = $hh->ini->GetModule("homepage","hometype",0);
	if($hometype==2)
	{
		$trm3 = new Translator($hh->tr->id_language,3);
		$hometypes = $trm3->Translate("hometype");
		echo "<h3>{$hometypes[2]}</a></h3>\n";
		echo "<ul>\n";
		echo "<li><a href=\"geo_topic.php?default\">Default</a></li>\n";
		echo "<li><a href=\"geo_countries.php\">Rules</a></li>\n";
		echo "</ul>\n";
	}
}

echo "</div>\n";

if ($ah->IsModuleActive(32))
{
    include_once(SERVER_ROOT."/../classes/widgets.php");
    $wi = new Widgets();
    
	echo "<div class=\"box\">\n";
	echo "<h2>Widgets</h2>\n";
	echo "<h3>" . "Widgets" . "</h3>";
	echo "<ul>";
	echo "<li><a href=\"widget.php?id=0\">" . "Add new widget" . "</a></li>";
	echo "<li><a href=\"widgets.php\">" . "List widgets" . "</a></li>";
	echo "<li><a href=\"widgets_categories.php\">Categories</a></li>";
	echo "<li><a href=\"widgets_search.php\">" . $hh->tr->Translate("search") . "</a></li>";
	echo "</ul>";
	
	echo "<h3>Default widgets</h3>";
	echo "<ul>";
	if ($module_admin)
	{
	    echo "<li><a href=\"widgets_tab.php?id=0\">" . "Add new tab" . "</a></li>";
	}
	echo "<li><a href=\"widgets_tabs.php\">" . "List tabs" . "</a></li>";
	echo "</ul>";
        if($wi->widgets_abuse_reporting)
        {
            echo "<h3>" . "Abuses" . "</h3>";
            echo "<ul>";
            echo "<li><a href=\"widget_abuses.php\">" . "List reported widget abuses" . "</a></li>";
            echo "</ul>";
        }
	echo "<h3>" . "Reporting" . "</h3>";
	echo "<ul>";
	echo "<li><a href=\"widgets_usage.php\">" . "Widgets usage" . "</a></li>";
	echo "<li><a href=\"history.php?id_type=24\">" . $hh->tr->Translate("history") . "</a></li>";
	echo "</ul>";
	if ($module_admin)
	{
	    echo "<h3><a href=\"widgets_config.php\">" . $hh->tr->Translate("configuration") . "</a></h3>";
	}
	echo "</div>\n";
	echo "<div class=\"box\">\n";
	echo "<h2>Web feeds</h2>\n";
	echo "<h3>" . "Web feeds" . "</h3>";
	echo "<ul>";
	if ($module_admin)
	{
	    echo "<li><a href=\"web_feed.php?id=0\">" . "Add new web feed" . "</a></li>";
	}
	echo "<li><a href=\"web_feeds.php\">" . "List web feeds" . "</a></li>";
	echo "<li><a href=\"web_feeds_search.php\">" . $hh->tr->Translate("search") . "</a></li>";
	echo "</ul>";
	echo "<h3>Pprofanity filtered feed items</h3>";
	echo "<ul>";
	echo "<li><a href=\"filtered_web_feed_items.php\">" . "List filtered feed items" . "</a></li>";
	echo "</ul>";
	echo "<h3>" . "Reporting" . "</h3>";
	echo "<ul>";
	echo "<li><a href=\"web_feed_reports.php?id_report=1\">" . "Distribution of feed items" . "</a></li>";
	echo "<li><a href=\"web_feed_reports.php?id_report=2\">" . "Average items per feed" . "</a></li>";
	echo "<li><a href=\"web_feed_reports.php?id_report=3\">" . "Feed performance below threshold" . "</a></li>";
	echo "<li><a href=\"history_items.php\">" . $hh->tr->Translate("history") . "</a></li>";
	echo "</ul>";
	
	echo "</div>\n";
}


   
include_once(SERVER_ROOT."/include/footer.php");
?>
