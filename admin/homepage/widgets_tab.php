<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_tab = (int)$get['id'];
$id_p = 0; // default widget
$fail = $get['fail'];

if ($module_admin)
	$input_right = 1;

if($id_tab>0)
{
	$row = $wi->WidgetsTabGet($id_p, $id_tab);
	$num_widgets = $wi->WidgetsTabCounter($id_p, $id_tab);
}

$title[] = array("list tabs",'widgets_tabs.php');
$title[] = array($id_tab>0?$row['tab_name']:"add new tab",'');

echo $hh->ShowTitle($title);


echo $hh->input_form_open();
echo $hh->input_hidden("from","widgets_tabs");
echo $hh->input_hidden("old_id_tab",$id_tab);
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_table_open();

if ($fail == 'duplicate')
    echo $hh->input_note("<p>Duplicate tab position</p>", TRUE);   
else if ($fail == 'emptytab')
    echo $hh->input_note("<p>Tab name cannot be empty</p>", TRUE);   
	
    
if ($id_tab > 0)
    echo $hh->input_text("Tab pos","id_tab",$row['id_tab'],50,0,0);
else
{
	$rows = array();
	$num = $wi->WidgetsTabsAll($rows, $id_p,false);
    echo $hh->input_text("Tab pos","id_tab",$rows[$num-1]['id_tab']+1,50,0,0);
}

if ($fail == 'emptytab')
	echo $hh->input_text("Tab name","tab_name","",50,0,$input_right);
else
	echo $hh->input_text("Tab name","tab_name",$row['tab_name'],50,0,$input_right);


$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_tab>0 && $num_widgets==0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
