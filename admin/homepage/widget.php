<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_widget = (int)$get['id'];
$failed = (int)$get['failed'];

if($id_widget>0)
{
    include_once(SERVER_ROOT."/../classes/ontology.php");
    $o = new Ontology;
    include_once(SERVER_ROOT."/../classes/history.php");
    $h = new History;
	$row = $wi->WidgetGet($id_widget);
	$id_user = $h->CreatorId($o->types['widget'],$id_widget);
    $id_user = $id_user['id_user'];
	$status = $row['status'];
    $old_widget_type = $row['widget_type'];
}
else 
{
	$id_user = $ah->current_user_id;
	$status = 0;
    $old_widget_type = -1;
}
             
if ($module_admin || $id_user == $ah->current_user_id)
    $input_right = 1;

$title[] = array("widgets",'widgets.php');
$title[] = array($id_widget>0?$row['title']:"add_new",'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("widgets");
$tabs[] = array("widget content",$id_widget>0?'widget_content.php?id='.$id_widget:'');  
$tabs[] = array("widget category",$id_widget>0?'widget_category.php?id='.$id_widget:'');
if($id_widget>0)
	$tabs[] = array('history','history.php?id_type=24&id='.$id_widget);

echo $hh->Tabs($tabs);


echo $hh->input_form_open();
echo $hh->input_hidden("from","widgets");
echo $hh->input_hidden("id_widget",$id_widget);
echo $hh->input_hidden("old_widget_type",$old_widget_type);
echo $hh->input_table_open();
if ($failed == 1)
    echo $hh->input_note("<p>Widget cannot be updated, must be approved and enabled because it is used by default widgets for homepage </p>", TRUE);
else if ($failed == 2)
    echo $hh->input_note("<p>Widget cannot be deleted because it is used by default widgets for homepage </p>", TRUE);

echo $hh->input_text("title","title",$row['title'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],80,5,"",$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id_widget,$o->types['widget'],$keywords,$input_right);

if ($id_widget > 0)
    echo $hh->input_array("Widget type","widget_type",$row['widget_type'],$wi->types,0);
else
    echo $hh->input_array("Widget type","widget_type",$row['widget_type'],$wi->types,$input_right);

echo $hh->input_separator("administration");
$id_web_feed = $row['id_web_feed'];
if($id_web_feed != 0)
{
    echo $hh->input_note('View Feed', false, 'web_feed.php?id='.$id_web_feed);
}
echo $hh->input_array("status","status",$status,$wi->Statuses(true),$module_admin || Modules::AmIAdmin(10)); 
echo $hh->input_textarea("comments","comment",$row['comment'],80,5,"",$module_admin);
echo $hh->input_checkbox("Comments visible to creator","comment_visible",$row['comment_visible'],0,$module_admin);
echo $hh->input_checkbox("Shareable","shareable",$row['shareable'],0,$module_admin);

if ($id_widget > 0 && $row['id_p']>0)
{
	echo $hh->input_separator("submitted_by");
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$pub_user = $pe->UserGetById($row['id_p']);
    echo $hh->input_text("Name","user","{$pub_user['name1']} {$pub_user['name2']}",150,0,0);
	echo $hh->input_date("Insert_date","insert_date",$row['insert_date_ts'],0);
	echo $hh->input_time("Hour","insert_date",$row['insert_date_ts'],0);
	echo $hh->input_text("IP","ip",$row['ip'],5,0,0);
}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$module_admin && $id_widget>0);
if($wi->widgets_hard_delete)
	$actions[] = array('action'=>"remove",'label'=>"remove",'right'=>$module_admin && $id_widget>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();
?>
<script type="text/javascript">
$(function() {
  $("form").submit(function(){
    $("input.input-submit", this).click(function(e) {
      $(this).attr("disabled", true);
      return false;
    });
  });
});
</script>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
