<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$id_widget = $get['id_widget'];
$status = $get['status'];
$widget_type = $get['widget_type'];
$owner = $get['owner'];

$wi = new Widgets();

$statuses = $wi->Statuses(true);

if ($id_widget == '')
    $id_widget = 0;
if ($status == '')
    $status = count($statuses);
if ($widget_type == '')
    $widget_type = count($wi->types);
if ($owner == '')
    $owner = 2;
    
$status_list = array_merge($statuses,array("All"));
$widget_type_list = array_merge($wi->types,array("All"));
$owner_list = array("Admin", "Public", "All");

$title[] = array("widgets");

echo $hh->ShowTitle($title);

$num = $wi->WidgetsBySearchCriteria($id_widget, $widget_type, $owner, $status, $rows);

$table_headers = array('date','id', 'title','widget type','status', 'owner');
$table_content = array('{FormatDate($row[insert_date_ts])}','$row[id_widget]', '{LinkTitle("widget.php?id=$row[id_widget]",$row[title])}', '{WidgetsTypeLookup($row[widget_type])}', '{WidgetsStatusLookup($row[status])}', '{WidgetOwnerLookup($row[id_p])}');

$input_right = 1;
echo $hh->input_form_open();
echo $hh->input_hidden("from","widgets");
echo $hh->input_text("Widget id","id_widget",$id_widget,3,0,$input_right);
echo $hh->input_array("Widget type","widget type",$widget_type,$widget_type_list,$input_right);
echo $hh->input_array("Status","status",$status, $status_list,$input_right);
echo $hh->input_array("Owner","owner",$owner, $owner_list,$input_right);
$actions = array();
$actions[] = array('action'=>"list",'label'=>"list",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
$hh->input_form_close(); 

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if ($module_right)
	echo "<p><a href=\"widget.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

