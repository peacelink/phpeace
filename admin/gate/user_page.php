<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$u = new User;
$id = $u->id;
$row = $u->UserGet();

$active = $row['active'];
if ($active!=1)
	$ah->MessageSet("user_no_active_warn");
$title[] = array($row['name'],'user.php');
$title[] = array('user_link','');

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
elseif($hh->ini->Get("user_show"))
{
	$tabs = array();
	$tabs[] = array('user_data','/gate/user.php');
	$tabs[] = array('user_link','');
	$tabs[] = array('image_associated','/gate/user_image.php');
	$tabs[] = array('responsibilities','/gate/user_roles.php');
	$tabs[] = array('services','/gate/user_services.php');
	$tabs[] = array('user_stats','/gate/user_stats.php');
	echo $hh->Tabs($tabs);
	
	$input_right = 1;

	echo $hh->input_form_open();
	echo $hh->input_hidden("from","user_page");
	echo $hh->input_table_open();
	$url = $hh->ini->Get('pub_web') . "/tools/author.php?u=$id";
	echo $hh->input_text("link","user_public_link",$row['user_show']? "<a href=\"$url\" target=\"_blank\">$url</a>" : $url,20,0,0);
	echo $hh->input_checkbox("active","user_show",$row['user_show'],0,$input_right && $hh->ini->Get("user_show"));
	echo $hh->input_textarea("biography","notes",$row['notes'],60,20,"",$input_right);
	echo $hh->input_submit("submit","",$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
include_once(SERVER_ROOT."/include/footer.php");
?>


