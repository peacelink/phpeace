<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$u = new User;
$id = $u->id;
$row = $u->UserGet();

$id_p = $_GET['id'];

$title[] = array($row['name'],'user.php');
$title[] = array('pub_user','');
echo $hh->ShowTitle($title);

if ($conf->Get("user_auth")=="internal" && $ah->current_user_id==$id)
	$input_right = 1;
	
if($id_p>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$pub_user = $pe->UserGetById($id_p);
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","user_pub");
	echo $hh->input_hidden("id_p",$id_p);
	echo $hh->input_table_open();
	echo $hh->input_note($hh->tr->TranslateParams("pub_user_associate",array("{$pub_user['name1']} {$pub_user['name2']}",$pub_user['email'])));
	echo $hh->input_text("password","password","",20,0,$input_right);
	$actions[] = array('action'=>"submit",'label'=>"submit",'right'=>$input_right);
	$actions[] = array('action'=>"remove",'label'=>"remove",'right'=>$input_right && $row['id_p']>0);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
elseif($get['name']!="" || $get['email']!="")
{
	$params = array(	'name' => $get['name'], 'email' => $get['email'] );
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$num = $pe->Search( $row, $params);
	$table_headers = array('name','email','verified','contact_email',$hh->geo_label($hh->ini->Get("geo_location")),'insert_date');
	$table_content = array('{LinkTitle("user_pub.php?id=$row[id_p]","$row[name1] $row[name2]")}','$row[email]','{Bool2YN($row[verified])}',
	'{Bool2YN($row[contact])}','$row[town] ($row[geo_name])','{FormatDate($row[start_date_ts])}');
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}
else 
{
	echo $hh->input_form("get","user_pub.php");
	echo $hh->input_table_open();
	echo $hh->input_note("pub_user_search");
	echo $hh->input_text("name","name","","30",0,$input_right);
	echo $hh->input_text("email","email","","30",0,$input_right);
	$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>


