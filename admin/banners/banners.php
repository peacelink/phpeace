<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/banners.php");

$id = $_GET['id'];

$b = new Banners;
$group = $b->GroupGet($id);

$title[] = array('banner_groups','banners_groups.php');
$title[] = array($group['name'],'');
echo $hh->ShowTitle($title);

echo "<p>";
if($group['description']!="")
	echo "<div>{$group['description']}</div>";
echo "<div>" . $hh->tr->Translate("size") . ": {$group['width']}x{$group['height']} px</div>";
$actives = $b->GroupActiveBanners($id);
$num = $b->GroupBanners( $row, $id );

echo "<div>" . $hh->tr->Translate("banners_active") . ": " . count($actives) .  " " . $hh->tr->Translate("of") . " $num</div>";


$table_headers = array('banner','link','text','active','');
$table_content = array('{ThumbImage($row[id_banner],"banners",$row[format])}','$row[link]','$row[alt_text]','{Bool2YN($row[active])}',
'{LinkTitle("banner.php?id_group='.$id.'&id=$row[id_banner]","' . $hh->tr->Translate("change") . '")}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_admin || ($group['id_user']==$ah->current_user_id))
	echo "<p><a href=\"banner.php?id=0&id_group=$id\">" . $hh->tr->Translate("banner_add") . "</a></p>\n";

if ($id>0)
{
	echo $hh->input_code("banner_html","banner_html","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/banner.php?id_g=$id\"></script>",60,2);
	echo $hh->input_code("banner_xsl","banner_xsl","<xsl:call-template name=\"bannerGroup\"><xsl:with-param name=\"id\" select=\"'$id'\"/></xsl:call-template>",60,2);
}

include_once(SERVER_ROOT."/include/footer.php");
?>

