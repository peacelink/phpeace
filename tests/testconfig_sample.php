<?php

/**
 * This class containts configuration variables for unit tests.
 * Move this file to your custom directory and name it testconfig.php
 *
 */
class TestConfig
{
	/**
	 * Insert content in current installation
	 *
	 */
	public $content_create = false;

	/**
	 * Number of test installations
	 *
	 */
	public $installations_number = 2;
	
	/**
	 * Local directory for test installations
	 *
	 */
	public $installation_directory = "/var/www/phpeace_installs";
	
	/**
	 * URL of web portal for test installations
	 * (N will be substituted with installation number)
	 *
	 */
	public $installation_pub = "http://www.phpeaceinstallN.dev.local";
		
	/**
	 * URL of admin interface for test installations
	 * (N will be substituted with installation number)
	 *
	 */
	public $installation_admin = "http://admin.phpeaceinstallN.dev.local";
		
	/**
	 * DB name for test installations
	 * (N will be substituted with installation number)
	 *
	 */
	public $installation_dbname = "phpeace_install_N";
		
	/**
	 * Group name to own installation directory
	 * (usually shared by dev and Apache users)
	 *
	 */
	public $installation_group = "phpeacedev";
	
	/**
	 * Username of admin account created on test installations
	 *
	 */
	public $installation_admin_username = "admin";
		
	/**
	 * Password of admin account created on test installations
	 *
	 */
	public $installation_admin_password = "phpeace123";

}
?>
