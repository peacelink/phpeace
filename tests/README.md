
Unit tests must be run as Apache user, i.e.

`$ su www-data -c /var/www/phpeace/dev/scripts/phtest.sh`

For this to happen:

- Apache user must have a shell
  Try 
  `# su - www-data `
  and see if it works. If not, run
  `# chsh -s /bin/sh www-data`

- Apache must get the latest from source control
  Remember to set Apache's user home directory writable to itself.


You may want to run the build and installation unit tests too.

Installation options are configured in the file

`custom/testconfig.php`

A sample copy is available in

`tests/testconfig_sample.php`

If you want to disable build and installer unit tests, just set

`public $installations_number = 0;`

By default, the installer unit tests create 2 installations,
all under the same directory, which you have to create
and set it writable by Apache user

# mkdir /var/www/phpeace_installs
# chown www-data.www-data /var/www/phpeace_installs

Then configure 4 virtual hosts

```
<VirtualHost *:80>
        ServerName www.phpeaceinstall1.dev.local
        DocumentRoot /var/www/phpeace_installs/phpeace1/pub
</VirtualHost>
```

```
<VirtualHost *:80>
        ServerName admin.phpeaceinstall1.dev.local
        DocumentRoot /var/www/phpeace_installs/phpeace1/admin
</VirtualHost>
```

```
<VirtualHost *:80>
        ServerName www.phpeaceinstall2.dev.local
        DocumentRoot /var/www/phpeace_installs/phpeace2/pub
</VirtualHost>
```

```
<VirtualHost *:80>
        ServerName admin.phpeaceinstall2.dev.local
        DocumentRoot /var/www/phpeace_installs/phpeace2/admin
</VirtualHost>
```

Edit your /etc/hosts file so that they point to localhost

`127.0.0.1   [... your currents hostnames] www.phpeaceinstall1.dev.local admin.phpeaceinstall1.dev.local www.phpeaceinstall2.dev.local admin.phpeaceinstall2.dev.local`

Reload Apache configuration


Finally, in MySQL, create 2 databases

`> CREATE DATABASE phpeace_install_1;`

`> CREATE DATABASE phpeace_install_2;`

and grant their permissions to the same user you set for the local DB (i.e. the one you configured in custom/config.php)

`> GRANT ALL PRIVILEGES ON phpeace_install_1.* TO 'your_current_username'@localhost;`

`> GRANT ALL PRIVILEGES ON phpeace_install_2.* TO 'your_current_username'@localhost;`
