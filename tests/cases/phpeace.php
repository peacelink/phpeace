<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/phpeace.php");
include_once(SERVER_ROOT."/../classes/phpeace_updates.php");

/**
 * Unit Tests for PhPeace class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class PhPeaceTest extends PHPUnit\Framework\TestCase 
{
	/** 
	 * @var PhPeace */
	private $phpeace;
	
	/**
	 * Initialise local variables
	 *
	 */
	function setUp(): void
	{
		$this->phpeace = new PhPeace();
		$pu = new PhPeaceUpdates(0);
		$pu->UpdateDBAll();
	}
	
	/**
	 * Test build and DB numbers
	 *
	 */
	function testBuildNumber()
	{
		$this->assertTrue(is_numeric($this->phpeace->build) ,'Invalid build value');
		$this->assertTrue(is_numeric($this->phpeace->db_version) ,'Invalid DB version');
	}

	/**
	 * Test module paths
	 *
	 */
	function testPaths()
	{
		foreach($this->phpeace->PublicPaths() as $path)
		{
			$this->assertFileExists(SERVER_ROOT."/../pub/$path");
		}
	}
}
?>
