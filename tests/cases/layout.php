<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/layout.php");

/**
 * Unit Tests for Layout class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class LayoutTest extends PHPUnit\Framework\TestCase 
{
	/** 
	 * @var Layout */
	private $obj;
	
	/**
	 * Existing topic ID to use for testing
	 *
	 * @var integer
	 */
	private $id_topic;
	
	/**
	 * Whether create content in local installation or use existing
	 *
	 * @var boolean
	 */
	private $content_create;
	
	/**
	 * Initialize local variables
	 *
	 */
	function setUp(): void
	{
		$this->obj = new Layout();
		$this->IdTopic();
		$tconf = new TestConfiguration();
		$this->content_create = $tconf->Get("content_create");
	}
	
	/**
	 * Test that at least one topic exists
	 *
	 */
	function testTopic()
	{
		$this->assertGreaterThan(0,$this->id_topic,"Could not find a topic");
	}
	
	/**
	 * Test XML generation of global pagetypes
	 *
	 */
	function testXMLPagetypeGlobal()
	{
		foreach($this->obj->pt->gtypes as $gtype=>$id_type)
		{
			if($gtype!="common_global" && $gtype!="widgets")
			{
				$params = $this->PagetypeParams($gtype);
				$xml = $this->obj->PageTypeGlobal($id_type,0,1,$params);
				$schema_file = SERVER_ROOT .  "/../tests/schemas/{$gtype}.xsd";
				$this->ValidateXML($xml,$schema_file);
				$this->ContentCleanup($gtype,$params['id']);
			}
		}
	}
	
    /**
     * Test XML generation of specific pagetypes
     * 
     * @depends testTopic
     */	
	function testXMLPagetypeSpecific()
	{
		foreach($this->obj->pt->types as $type=>$id_type)
		{
			//if($type!="common" && $type!="root" && $type!="tools")
			if($type=="topic_home" || $type=="subtopic" || $type=="article")
			{
				$params = $this->PagetypeParams($type);
				$id = (int)$params['id'];
				$xml = $this->obj->PageType($id_type,$this->id_topic,$id,1,$params);
				$schema_file = SERVER_ROOT .  "/../tests/schemas/{$type}.xsd";
				$this->ValidateXML($xml,$schema_file);
				$this->ContentCleanup($type,$params['id']);
			}
		}
	}
	
	/**
	 * Parameters to be used for each page type testing
	 *
	 * @param string $type
	 * @return array
	 */
	private function PagetypeParams($type)
	{
		$params = array();
		switch($type)
		{
			case "user":
				include_once(SERVER_ROOT."/../classes/users.php");
				include_once(SERVER_ROOT."/../classes/user.php");
				$uu = new Users();
				$users = $uu->AllUsers();
				$id_user = $users[0]['id_user'];
				$iuser = new User();
				$iuser->id = $id_user;
				$iuser->UserUpdatePage(1,'User notes');
				$params['u'] = $id_user;
			break;
			case "subtopic":
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($this->id_topic);
				if($this->content_create)
				{
					$params['id'] = $t->SubtopicInsert("Subtopic Test","",0,"","",$t->subtopic_types['folder'],1,0,0,0,"","",array(),3,0);
				}
				else 
				{
					$subtopics = $t->SubtopicsByType($t->subtopic_types['folder']);
					if(count($subtopics)>0)
					{
						$params['id'] = $subtopics[0]['id_subtopic'];
					}
				}
			break;
			case "article":
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($this->id_topic);
				if($this->content_create)
				{
					$id_subtopic = $t->SubtopicInsert("Subtopic Test","",0,"","",$t->subtopic_types['folder'],1,0,0,0,"","",array(),3,0);
					include_once(SERVER_ROOT."/../classes/article.php");
					$a = new Article(0);
					$a->ArticleStore(date("Y-m-d"),1,1,"Author name","Author notes","External source",date("Y-m-d"),1,$this->id_topic,$id_subtopic,"halftitle","Article title","Summary of article",1,"<p>Article content</p>",1,"Article notes",0,1,1,0,5,"keyword1, keyword2",1,1,0,0,0,0,array(),0,0,"",0);
					$params['id'] = $a->id;
				}
				else 
				{
					$articles = $t->Articles();
					if(count($articles)>0)
					{
						$params['id'] = $articles[0]['id_article'];
					}
				}
			break;
		}
		return $params;
	}
	
	/**
	 * Validate XML against existing XSD schema
	 *
	 * @param string $xml		XML content
	 * @param string $schema	XSD file
	 */
	private function ValidateXML($xml,$schema)
	{
		$xmldoc = new DOMDocument('1.0',$this->obj->xh->encoding);
		$xmldoc->loadxml($xml);
		set_error_handler("ErrorTrash");
		libxml_use_internal_errors(true); 
		$validation = $xmldoc->schemaValidate($schema);
		set_error_handler("ErrorHandler");
		$this->assertTrue($validation,"XML validation failed using $schema:\n" . $this->obj->xh->LibXMLDisplayErrors());
	}
	
	/**
	 * Test (X)HTML generation of global pagetypes
	 *
	 */
	function testXhtmlOutput()
	{
		foreach($this->obj->pt->gtypes as $gtype=>$id_type)
		{
			if($gtype!="common_global" && $gtype!="widgets")
			{
				$params = $this->PagetypeParams($gtype);
				$xhtml = $this->obj->Output($gtype,0,1,0,$params,true);
				$strlen = strlen($xhtml);
				$this->assertGreaterThan(0,$strlen,"Empty return from transformation for $gtype");
				if($strlen>0)
				{
					if($gtype=="gnewsletter")
					{
						$this->assertStringContainsString("Newsletter",$xhtml);
					}
					elseif($gtype=="rss")
					{
						$this->assertStringContainsString("</rss>",$xhtml);
					}
					else
					{
						$this->assertStringContainsString("</html>",$xhtml,"Failed transformation for $gtype");
					}				
				}
				$this->ContentCleanup($gtype,$params['id']);
			}
		}
	}
	
    /**
     * Test (X)HTML generation of specific pagetypes
     * 
     * @depends testTopic
     */	
	function testXhtmlOutputSpecific()
	{
		foreach($this->obj->pt->types as $type=>$id_type)
		{
			if($type=="topic_home" || $type=="subtopic")
			{
				$params = $this->PagetypeParams($type);
				$xhtml = $this->obj->Output($type,0,$this->id_topic,0,$params,true);
				$strlen = strlen($xhtml);
				$this->assertGreaterThan(0,$strlen,"Empty return from transformation for $type");
				if($strlen>0)
				{
					$this->assertStringContainsString("</html>",$xhtml,"Failed transformation for $type");
				}
				$this->ContentCleanup($type,$params['id']);
			}
		}
	}
	
	/**
	 * Deletes content created for testing
	 *
	 * @param string	$type	Page type
	 * @param integer	$id		ID
	 */
	private function ContentCleanup($type,$id)
	{
		if($this->content_create && $id>0)
		{
			switch($type)
			{
				case "subtopic":
					include_once(SERVER_ROOT."/../classes/topic.php");
					$t = new Topic($this->id_topic);
					$t->SubtopicDelete($id);
				break;
				case "article":
					include_once(SERVER_ROOT."/../classes/topic.php");
					$t = new Topic($this->id_topic);
					include_once(SERVER_ROOT."/../classes/article.php");
					$a = new Article($id);
					$a->ArticleLoad();
					$id_subtopic = $a->id_subtopic;
					$a->ArticleDelete();
					$t->SubtopicDelete($id_subtopic);
				break;
			}
		}
	}
	
	/**
	 * Set current topic ID
	 *
	 */
	private function IdTopic()
	{
		include_once(SERVER_ROOT."/../classes/topics.php");
		$tt = new Topics();
		$topics = $tt->AllTopics(false,true);
		$this->id_topic = (int)$topics[0]['id_topic'];
	}
}
?>
