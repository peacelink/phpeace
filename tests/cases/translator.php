<?php
/**********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2023 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/translator.php");

/**
 * Unit Tests for Translator class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class TranslatorTest extends PHPUnit\Framework\TestCase 
{
	/** 
	 * @var Translator */
	private $obj;
	
	/**
	 * Initialize local properties
	 *
	 */
	function setUp(): void
	{
		$this->obj = new Translator(0,0);
	}
	
	/**
	 * Compare localization files of 2 languages
	 *
	 * @param integer $id_language1	First Language
	 * @param integer $id_language2	Second Language
	 * @param integer $id_module	Module ID
	 * @param boolean $is_admin		Whether it's for admin interface or portal
	 */
	function CompareLanguages($id_language1,$id_language2,$id_module,$is_admin)
	{
		$tr1 = new Translator($id_language1,$id_module,$is_admin);
		$tr2 = new Translator($id_language2,$id_module,$is_admin);
		$tr1a = $tr1->ParseLangFile();
		$tr2a = $tr2->ParseLangFile();
		$this->assertTrue(count($tr1a)==count($tr2a) ,"Different lines in language files: {$tr1->lang}/{$tr2->lang} (mod. {$tr1->id_module} - " . ($is_admin? "admin":"pub") . ")\n");
		foreach($tr1a as $key=>$value)
		{
			if(is_array($value))
				$this->assertTrue(count($tr1a[$key])==count($tr2a[$key]) ,"Different lines in translation of word \"" . $key . "\" between {$tr1->lang} and {$tr2->lang}.\n");
			else
				$this->assertTrue(array_key_exists($key,$tr2a) ,"Missing key \"" . $key . "\" in " . $tr2->mem_var . ".\n");
		}
		
	}
	
	/**
	 * Test localization files for admin interface
	 *
	 */
	function testAdmin()
	{
		$admin_languages = array(2);
		foreach($admin_languages as $id_language)
		{
			$this->CompareLanguages(1,$id_language,0,true);
			$this->CompareLanguages(1,$id_language,4,true);
			$this->CompareLanguages(1,$id_language,8,true);
			$this->CompareLanguages(1,$id_language,9,true);
			$this->CompareLanguages(1,$id_language,11,true);
			$this->CompareLanguages(1,$id_language,14,true);
			$this->CompareLanguages(1,$id_language,15,true);
			$this->CompareLanguages(1,$id_language,16,true);
			$this->CompareLanguages(1,$id_language,17,true);
			$this->CompareLanguages(1,$id_language,23,true);
			$this->CompareLanguages(1,$id_language,25,true);
			$this->CompareLanguages(1,$id_language,26,true);
			$this->CompareLanguages(1,$id_language,27,true);
			$this->CompareLanguages(1,$id_language,28,true);
		}
	}

	/**
	 * Test localization files for portal
	 *
	 */
	function testPub()
	{
		$pub_languages = array(2,3,4,5);
		foreach($pub_languages as $id_language)
		{
			$this->CompareLanguages(1,$id_language,0,false);
			$this->CompareLanguages(1,$id_language,3,false);
			$this->CompareLanguages(1,$id_language,4,false);
			$this->CompareLanguages(1,$id_language,6,false);
			$this->CompareLanguages(1,$id_language,8,false);
			$this->CompareLanguages(1,$id_language,11,false);
			$this->CompareLanguages(1,$id_language,12,false);
			$this->CompareLanguages(1,$id_language,16,false);
			$this->CompareLanguages(1,$id_language,17,false);
			$this->CompareLanguages(1,$id_language,23,false);
			$this->CompareLanguages(1,$id_language,25,false);
			$this->CompareLanguages(1,$id_language,26,false);
			$this->CompareLanguages(1,$id_language,27,false);
			$this->CompareLanguages(1,$id_language,28,false);
			$this->CompareLanguages(1,$id_language,29,false);
			$this->CompareLanguages(1,$id_language,30,false);
			//$this->CompareLanguages(1,$id_language,32,false);
		}
	}
}
?>
